﻿using Corp.Billing.AsyncShared.AsyncApiClient.AbTesting;
using Corp.Billing.AsyncShared.AsyncApiClient.Account;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.AsyncShared.AsyncApiClient.Catalog;
using Corp.Billing.AsyncShared.AsyncApiClient.Discounting;
using Corp.Billing.AsyncShared.AsyncApiClient.Orders;
using Corp.Billing.AsyncShared.AsyncApiClient.Products;
using Corp.Billing.AsyncShared.AsyncApiClient.Subscription;
using Corp.Billing.Shared.ApiClient;
using System;
using System.Threading.Tasks;

namespace Corp.Billing.Shared.CLI
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Existing setting as of today
            ApiConfiguration.BillingServiceUrl = "http://mabillapi.match.com/";

            // Set to what levels to run the `AsyncApiCommand.DebugCallback()`.
            // Default value is `None`.
            // AsyncApiCommand.DebugLevel = AsyncApiCommand.DebugLevelEnum.SuccessfulResponses
            //     | AsyncApiCommand.DebugLevelEnum.ClientErrors
            //     | AsyncApiCommand.DebugLevelEnum.ServerErrors;

            // The default value of `DebugCallback` just prints to the console. It can be overridden by custom implementation.
            // AsyncApiCommand.DebugCallback = new Func<System.Net.Http.HttpRequestMessage, object, System.Net.Http.HttpResponseMessage, object, ServerErrorResponseDTO, Task> ...

            // By default throws `ApiCommandException` on server or client error codes.
            // AsyncApiCommand.ExceptionLevel = ExceptionLevelEnum.ClientErrors | ExceptionLevelEnum.ServerErrors;
            // To turn off, set the level to `None`:
            // AsyncApiCommand.ExceptionLevel = AsyncApiCommand.ExceptionLevelEnum.None;

            int userId = 332448494;
            int otherUserId = 332450534;

            // await IProductApiAsyncClientCalls.PutMeInUserListAsync(userId, otherUserId);
            // await IProductApiAsyncClientCalls.MatchMe30DayEligibilityCheck(userId, otherUserId);
            // await IProductApiAsyncClientCalls.GetTopSpotStatusAsync(userId);
            // await IProductApiAsyncClientCalls.ProvisionAndActivateTopSpotAsync(userId);
            // await IProductApiAsyncClientCalls.GetTopSpotStatusAsync(userId);
            // await IProductApiAsyncClientCalls.ActivateUnclaimedImpulseFeatureAsync(userId, 26);
            // await IProductApiAsyncClientCalls.GetTopSpotStatusAsync(userId);
            // await IProductApiAsyncClientCalls.GetUnitUserStatusPcsAsync(userId);
            // await IProductApiAsyncClientCalls.GetUnitUserStatusAsync(userId);
            // await IProductApiAsyncClientCalls.GetUnitDisplayStatusAsync(userId);
            // await IProductApiAsyncClientCalls.GetImpulseFeaturesStatusAsync(userId);
            // await IProductApiAsyncClientCalls.ActivateImpulseFeatureAsync(userId, otherUserId);
            // await IProductApiAsyncClientCalls.GetProfileProRedemptionsAsync(userId);
            // await IProductApiAsyncClientCalls.RedeemProfileProAsync(userId);
            // await IProductApiAsyncClientCalls.GetDateCoachStatusAsync(userId);
            // await IProductApiAsyncClientCalls.ActivateDateCoachAutoRenewalAsync(userId);
            // await IProductApiAsyncClientCalls.DeactivateDateCoachAutoRenewalAsync(userId);
            // await IProductApiAsyncClientCalls.ToggleDateCoachAutoRenewalAsync(userId);

            // await ISubscriptionStatusApiAsyncClientCalls.GetDisplayStatusAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetDisplayStatusV2Async(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserStatusPromoInfoAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserSubscritionStatusAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserSubStatusMessageAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserStatusDateAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserStatusDateV1Async(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserFeaturesAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserFeaturesLightAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.CancelUserAsync(); // Good server error example
            // await ISubscriptionStatusApiAsyncClientCalls.GetUserBillingDataAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetCurrentPackageAsync(userId);
            // await ISubscriptionStatusApiAsyncClientCalls.GetSubscriptionSummaryAsync(userId);

            await ISubscriptionBenefitsApiAsyncClientCalls.GetCountOfNewSubscriptionBenefits(userId);
            await ISubscriptionBenefitsApiAsyncClientCalls.GetSubscriptionBenefitsStatusAsync(userId);

            // await IOrderApiAsyncClientCalls.UpdateOfflinePaymentStatusAsync(userId);
            // await IOrderApiAsyncClientCalls.GetOfflineTransferDataAsync(userId);
            // await IOrderApiAsyncClientCalls.BillEventTicketsAsync(userId);
            // await IOrderApiAsyncClientCalls.GetImpulseBuyLastPurchaseAsync(userId);

            // await IRateCardApiAsyncClientCalls.GetExtendedRateCard(userId);
            // await IRateCardApiAsyncClientCalls.GetRateCard(userId);
            // await IRateCardApiAsyncClientCalls.GetLegacyAddOnsRateCard(userId);

            // await IDiscountingApiAsyncClientCalls.GetDiscountInfo(userId);

            // await IAccountApiAsyncClientCalls.GetAccountPaymentMethodV2(userId);

            // await IAbTestingAsyncClientCalls.Evaluate(userId, "Upsell_MemberUpgrade");
            // await IAbTestingAsyncClientCalls.EvaluateTestVariables(userId, "Upsell_MemberUpgrade");

            // Console.WriteLine("Press <Enter> to finish.");
            // Console.ReadLine();
        }

        static class IAbTestingAsyncClientCalls
        {
            private static IAbTestingAsyncClient ApiClient;

            static IAbTestingAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new AbTestingAsyncClient(apiCommand);
            }

            public static async Task Evaluate(int userId, string testName)
            {
                PrintApiFunction(
                    nameof(IAbTestingAsyncClient.Evaluate),
                    nameof(IAbTestingAsyncClient),
                    nameof(Domain.ProductCatalog.RulesEngine.PCRulesRequest),
                    "bool");
                var request = new Domain.ProductCatalog.RulesEngine.PCRulesRequest
                {
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.Evaluate(request, testName);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task EvaluateTestVariables(int userId, string testName)
            {
                PrintApiFunction(
                    nameof(IAbTestingAsyncClient.EvaluateTestVariables),
                    nameof(IAbTestingAsyncClient),
                    nameof(Domain.ProductCatalog.RulesEngine.PCRulesRequest),
                    nameof(Domain.ProductCatalog.RulesEngine.EvaluateTestVariablesResult));
                var request = new Domain.ProductCatalog.RulesEngine.PCRulesRequest
                {
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.EvaluateTestVariables(request, testName);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

        }

        static class IAccountApiAsyncClientCalls
        {
            private static IAccountApiAsyncClient ApiClient;

            static IAccountApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new AccountApiAsyncClient(apiCommand);
            }

            public static async Task GetAccountPaymentMethodV2(int userId)
            {
                PrintApiFunction(
                    nameof(IAccountApiAsyncClient.GetAccountPaymentMethodV2),
                    nameof(IAccountApiAsyncClient),
                    nameof(Domain.Account.GetPaymentMethodRequest),
                    nameof(Domain.Account.GetPaymentMethodResult));
                var request = new Domain.Account.GetPaymentMethodRequest
                {
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.GetAccountPaymentMethodV2(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }
        }

        static class IDiscountingApiAsyncClientCalls
        {
            private static IDiscountingApiAsyncClient ApiClient;

            static IDiscountingApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new DiscountingApiAsyncClient(apiCommand);
            }

            public static async Task GetDiscountInfo(int userId)
            {
                PrintApiFunction(
                    nameof(IDiscountingApiAsyncClient.GetDiscountInfo),
                    nameof(IDiscountingApiAsyncClient),
                    nameof(Domain.ProductCatalog.RulesEngine.PCRulesRequest),
                    nameof(Domain.ProductCatalog.DiscountEngine.DiscountInfoResult));
                var request = new Domain.ProductCatalog.RulesEngine.PCRulesRequest
                {
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.GetDiscountInfo(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }
        }

        static class IRateCardApiAsyncClientCalls
        {
            private static IRateCardApiAsyncClient ApiClient;

            static IRateCardApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new RateCardApiAsyncClient(apiCommand);
            }

            public static async Task GetExtendedRateCard(int userId)
            {
                PrintApiFunction(
                    nameof(IRateCardApiAsyncClient.GetExtendedRateCard),
                    nameof(IRateCardApiAsyncClient),
                    nameof(Domain.RateCardV3.Services.GetRateCardRequest),
                    nameof(Domain.RateCardV3.Services.GetExtendedRateCardResponse));
                var request = new Domain.RateCardV3.Services.GetRateCardRequest
                {
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.GetExtendedRateCard(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetRateCard(int userId)
            {
                PrintApiFunction(
                    nameof(IRateCardApiAsyncClient.GetRateCard),
                    nameof(IRateCardApiAsyncClient),
                    nameof(Domain.RateCardV3.Services.GetRateCardRequest),
                    nameof(Domain.RateCardV3.Services.GetRateCardResponse));
                var request = new Domain.RateCardV3.Services.GetRateCardRequest
                {
                    UserId = userId,
                    CatalogTypeParam = Domain.RateCardV3.Data.CatalogType.InApp,
                    CatalogSourceParam = Domain.RateCardV3.Data.CatalogSource.Apple,
                };
                try
                {
                    var response = await ApiClient.GetRateCard(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetLegacyAddOnsRateCard(int userId)
            {
                PrintApiFunction(
                    nameof(IRateCardApiAsyncClient.GetLegacyAddOnsRateCard),
                    nameof(IRateCardApiAsyncClient),
                    nameof(Domain.RateCard.RateCardV2AddOnsRequest),
                    nameof(Domain.RateCard.RateCardV2AddOnsResult));
                var request = new Domain.RateCard.RateCardV2AddOnsRequest
                {
                    UserId = userId,
                    PrimaryProdId = 18007,
                    BundleMask = 0,
                    BundleType = Domain.RateCard.BundleType.None,
                };
                try
                {
                    var response = await ApiClient.GetLegacyAddOnsRateCard(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }
        }

        static class IOrderApiAsyncClientCalls
        {
            private static IOrderApiAsyncClient ApiClient;

            static IOrderApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new OrderApiAsyncClient(apiCommand);
            }

            public static async Task UpdateOfflinePaymentStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IOrderApiAsyncClient.UpdateOfflinePaymentStatusAsync),
                    nameof(IOrderApiAsyncClient),
                    nameof(Domain.Orders.UpdateOfflinePaymentStatusRequest),
                    nameof(Domain.Orders.UpdateOfflinePaymentStatusResponse));
                var request = new Domain.Orders.UpdateOfflinePaymentStatusRequest
                {
                    TrxId = 0,
                    Amount = 0,
                    CurrencyCode = "BRL",
                    ProcessorLogID = 0,
                    TrxStatusId = 0
                };
                try
                { 
                    var response = await ApiClient.UpdateOfflinePaymentStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetOfflineTransferDataAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IOrderApiAsyncClient.GetOfflineTransferDataAsync),
                    nameof(IOrderApiAsyncClient),
                    nameof(Domain.Orders.GetOfflineTransferDataRequest),
                    nameof(Domain.Orders.GetOfflineTransferDataResult));
                var request = new Domain.Orders.GetOfflineTransferDataRequest
                {
                    TransactionId = 0,
                };
                try
                {
                    var response = await ApiClient.GetOfflineTransferDataAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task BillEventTicketsAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IOrderApiAsyncClient.BillEventTicketsAsync),
                    nameof(IOrderApiAsyncClient),
                    nameof(Domain.Orders.BillEventTicketsRequest),
                    nameof(Domain.Orders.BillEventTicketsResult));
                var request = new Domain.Orders.BillEventTicketsRequest
                {
                    TrxId = 0,
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.BillEventTicketsAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetImpulseBuyLastPurchaseAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IOrderApiAsyncClient.GetImpulseBuyLastPurchaseAsync),
                    nameof(IOrderApiAsyncClient),
                    nameof(Domain.Orders.ImpulseBuyLastPurchaseRequest),
                    nameof(Domain.Orders.ImpulseBuyLastPurchaseResult));
                var request = new Domain.Orders.ImpulseBuyLastPurchaseRequest
                {
                    UserId = userId,
                    ProdFeatureId = 26
                };
                try
                {
                    var response = await ApiClient.GetImpulseBuyLastPurchaseAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }
        }


        static class ISubscriptionBenefitsApiAsyncClientCalls
        {
            private static ISubscriptionBenefitsApiAsyncClient ApiClient;

            static ISubscriptionBenefitsApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new SubscriptionBenefitsApiAsyncClient(apiCommand);
            }

            public static async Task GetCountOfNewSubscriptionBenefits(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionBenefitsApiAsyncClientCalls.GetCountOfNewSubscriptionBenefits),
                    nameof(ISubscriptionBenefitsApiAsyncClientCalls),
                    nameof(Domain.SubscriptionBenefits.Services.BenefitsStatusRequest),
                    nameof(Domain.SubscriptionBenefits.Services.BenefitsCountNewResponse));
                var request = new Domain.SubscriptionBenefits.Services.BenefitsStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetCountOfNewSubscriptionBenefitsAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetSubscriptionBenefitsStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionBenefitsApiAsyncClientCalls.GetSubscriptionBenefitsStatusAsync),
                    nameof(ISubscriptionBenefitsApiAsyncClientCalls),
                    nameof(Domain.SubscriptionBenefits.Services.BenefitsStatusRequest),
                    nameof(Domain.SubscriptionBenefits.Services.BenefitStatusResponse));
                var request = new Domain.SubscriptionBenefits.Services.BenefitsStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetSubscriptionBenefitsStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

        }

        static class ISubscriptionStatusApiAsyncClientCalls
        {
            private static ISubscriptionStatusApiAsyncClient ApiClient;

            static ISubscriptionStatusApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new SubscriptionStatusApiAsyncClient(apiCommand);
            }

            public static async Task GetDisplayStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetDisplayStatusAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.DisplayUserStatusRequest),
                    $"List<{nameof(Domain.Subscription.DisplayUserStatusResult)}>");
                var request = new Domain.Subscription.DisplayUserStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetDisplayStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetDisplayStatusV2Async(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetDisplayStatusV2Async),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.DisplayUserStatusRequest),
                    nameof(Domain.Subscription.DisplayUserStatusResult));
                var request = new Domain.Subscription.DisplayUserStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetDisplayStatusV2Async(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserStatusPromoInfoAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserStatusPromoInfoAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Promos.UserPromoInfoRequest),
                    nameof(Domain.Promos.UserPromoInfoResult));
                var request = new Domain.Promos.UserPromoInfoRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUserStatusPromoInfoAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserSubscritionStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserSubscriptionStatusAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.UserSubStatusRequest),
                    nameof(Domain.Subscription.UserSubStatusResult));
                var request = new Domain.Subscription.UserSubStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUserSubscriptionStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserSubStatusMessageAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserSubStatusMessageAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.UserSubStatusMessageRequest),
                    nameof(Domain.Subscription.UserSubStatusMessageResult));
                var request = new Domain.Subscription.UserSubStatusMessageRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUserSubStatusMessageAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserStatusDateAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserStatusDateAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.UserStatusDateRequest),
                    nameof(Domain.Subscription.UserStatusDateResult));
                var request = new Domain.Subscription.UserStatusDateRequest { UserId = userId, StatusDt=DateTime.Now };
                try
                {
                    var response = await ApiClient.GetUserStatusDateAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserStatusDateV1Async(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserStatusDateV1Async),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.UserStatusDateRequest),
                    nameof(Domain.Subscription.UserStatusDateResult));
                var request = new Domain.Subscription.UserStatusDateRequest { UserId = userId, StatusDt = DateTime.Now };
                try
                {
                    var response = await ApiClient.GetUserStatusDateV1Async(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserFeaturesAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserFeaturesAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.UserFeatureRequest),
                    nameof(Domain.Subscription.UserFeatureResult));
                var request = new Domain.Subscription.UserFeatureRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUserFeaturesAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserFeaturesLightAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserFeaturesLightAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.UserFeatures.UserFeaturesLightRequest),
                    nameof(Domain.UserFeatures.UserFeaturesLightResult));
                var request = new Domain.UserFeatures.UserFeaturesLightRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUserFeaturesLightAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task CancelUserAsync()
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.CancelUserAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.CancelRequest),
                    nameof(Domain.Subscription.CancelResult));
                var request = new Domain.Subscription.CancelRequest
                {
                    UserID = null,
                    AccountDetailIDList= "172014599",
                    CancelDt = null,
                    CancelTypeID = 0,
                    CSAUserComment = null,
                    EmployeeID = 0,
                    CancelSourceID = 0,
                    SiteToggle = true,
                    TrxIDToNotCancel = null,
                };
                try
                {
                    var response = await ApiClient.CancelUserAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUserBillingDataAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetUserBillingDataAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.GetUserBillingDataRequest),
                    nameof(Domain.Subscription.GetUserBillingDataResult));
                var request = new Domain.Subscription.GetUserBillingDataRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUserBillingDataAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetCurrentPackageAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetCurrentPackageAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.GetCurrentPackageRequest),
                    nameof(Domain.Subscription.GetCurrentPackageResult));
                var request = new Domain.Subscription.GetCurrentPackageRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetCurrentPackageAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetSubscriptionSummaryAsync(int userId)
            {
                PrintApiFunction(
                    nameof(ISubscriptionStatusApiAsyncClient.GetSubscriptionSummaryAsync),
                    nameof(ISubscriptionStatusApiAsyncClient),
                    nameof(Domain.Subscription.UserSubscriptionSummaryRequest),
                    nameof(Domain.Subscription.UserSubscriptionSummaryResult));
                var request = new Domain.Subscription.UserSubscriptionSummaryRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetSubscriptionSummaryAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }
        }

        static class IProductApiAsyncClientCalls
        {
            private static IProductApiAsyncClient ApiClient;

            static IProductApiAsyncClientCalls()
            {
                IAsyncApiCommand apiCommand = new AsyncApiCommand();
                ApiClient = new ProductApiAsyncClient(apiCommand);
            }

            public static async Task MatchMe30DayEligibilityCheck(int userId, int otherUserId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.MatchMe30DayEligibilityCheckAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.MatchMe30DayEligibilityCheckRequest),
                    nameof(Domain.Products.MatchMe30DayEligibilityCheckResult));
                var request = new Domain.Products.MatchMe30DayEligibilityCheckRequest
                {
                    UserId = userId,
                    OtherUserId = otherUserId
                };
                try
                {
                    var response = await ApiClient.MatchMe30DayEligibilityCheckAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task PutMeInUserListAsync(int userId, int otherUserId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.PutMeInUserListAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.PutMeInUserListRequest),
                    nameof(Domain.Products.PutMeInUserListResult));
                var request = new Domain.Products.PutMeInUserListRequest
                {
                    UserId = userId,
                    OtherUserId = otherUserId,
                    TransactionId = 0,
                };
                try
                {
                    var response = await ApiClient.PutMeInUserListAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetTopSpotStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetTopSpotStatusAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.TopSpotStatusRequest),
                    nameof(Domain.Products.TopSpotStatusResult));
                var request = new Domain.Products.TopSpotStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetTopSpotStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task ProvisionAndActivateTopSpotAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.ProvisionAndActivateTopSpotAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.ProvisionAndActivateTopSpotRequest),
                    nameof(Domain.Products.ProvisionAndActivateTopSpotResult));
                var request = new Domain.Products.ProvisionAndActivateTopSpotRequest
                {
                    UserId = userId,
                    Minutes = 60,
                    Activate = false, // TODO: Why is this not respected? 
                };
                try
                {
                    var response = await ApiClient.ProvisionAndActivateTopSpotAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task ActivateUnclaimedImpulseFeatureAsync(int userId, byte luProductFeatureID, int? otherUserId = null)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.ActivateUnclaimedImpulseFeatureAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.ActivateUnclaimedImpulseFeatureRequest),
                    nameof(Domain.Products.ActivateUnclaimedImpulseFeatureResult));
                var request = new Domain.Products.ActivateUnclaimedImpulseFeatureRequest
                {
                    UserId = userId,
                    luProductFeatureID = luProductFeatureID,
                    OtherUserId = otherUserId,
                };
                try
                {
                    var response = await ApiClient.ActivateUnclaimedImpulseFeatureAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }
            public static async Task GetUnitUserStatusPcsAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetUnitUserStatusPcsAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.CSAUserPCSStatusRequest),
                    nameof(Domain.Products.CSAUserPCSStatusResult));
                var request = new Domain.Products.CSAUserPCSStatusRequest { UserId = userId };
                try
                {
                    var response = await ApiClient.GetUnitUserStatusPcsAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUnitUserStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetUnitUserStatusAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.UnitUserStatusRequest),
                    nameof(Domain.Products.UnitUserStatusResult));
                var request = new Domain.Products.UnitUserStatusRequest { UserId = userId, AllowPayPal = true };
                try
                {
                    var response = await ApiClient.GetUnitUserStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetUnitDisplayStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetUnitDisplayStatusAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.UnitDisplayStatusRequest),
                    nameof(Domain.Products.UnitDisplayStatusResult));
                var request = new Domain.Products.UnitDisplayStatusRequest { UserId = userId, EventId = 1, ProdFeatureId = 26 };
                try
                {
                    var response = await ApiClient.GetUnitDisplayStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task GetImpulseFeaturesStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetImpulseFeaturesStatusAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.ImpulseFeatures.Data.GetStatusRequest),
                    nameof(Domain.Products.ImpulseFeatures.Data.GetStatusResult));
                var request = new Domain.Products.ImpulseFeatures.Data.GetStatusRequest
                {
                    UserId = userId,
                    ProductFeature = Domain.Products.ImpulseFeatures.Data.ImpulseFeatureType.SuperLikes
                };
                try
                {
                    var response = await ApiClient.GetImpulseFeaturesStatusAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {apiEx.HttpStatusCode} {apiEx.ErrorCode} {apiEx.Message}");
                }
            }

            public static async Task ActivateImpulseFeatureAsync(int userId, int otherUserId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.ActivateImpulseFeatureAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.Products.ImpulseFeatures.Data.ActivateRequest),
                    nameof(Domain.Products.ImpulseFeatures.Data.ActivateResult));
                var request = new Domain.Products.ImpulseFeatures.Data.ActivateRequest
                {
                    UserId = userId,
                    ProductFeature = Domain.Products.ImpulseFeatures.Data.ImpulseFeatureType.SuperLikes,
                    OtherUserId = otherUserId,
                };
                try
                {
                    var response = await ApiClient.ActivateImpulseFeatureAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }

            public static async Task GetProfileProRedemptionsAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetProfileProRedemptionsAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.ProfileProLite.GetProfileProLiteRedemptionRequest),
                    nameof(Domain.ProfileProLite.ProfileProLiteRedemptionStatus));
                var request = new Domain.ProfileProLite.GetProfileProLiteRedemptionRequest
                {
                    UserId = userId,
                };
                try
                {
                    var response = await ApiClient.GetProfileProRedemptionsAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }

            public static async Task RedeemProfileProAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.RedeemProfileProAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.ProfileProLite.ProfileProLiteRedemption),
                    "void");
                var request = new Domain.ProfileProLite.ProfileProLiteRedemption
                {
                    UserId = userId,
                    RedemptionDate = DateTime.Now,
                    PlatformId = 36,
                    Sid = Guid.NewGuid(),
                };
                try
                {
                    await ApiClient.RedeemProfileProAsync(request);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }

            public static async Task GetDateCoachStatusAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.GetDateCoachStatusAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.DateCoachStatus.Data.DateCoachStatusResponse),
                    "int");
                try
                {
                    await ApiClient.GetDateCoachStatusAsync(userId);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }

            public static async Task ActivateDateCoachAutoRenewalAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.ActivateDateCoachAutoRenewalAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.DateCoachRenewalToggle.Data.DateCoachRenewalToggleResponse),
                    "int");
                try
                {
                    await ApiClient.ActivateDateCoachAutoRenewalAsync(userId);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }

            public static async Task DeactivateDateCoachAutoRenewalAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.DeactivateDateCoachAutoRenewalAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.DateCoachRenewalToggle.Data.DateCoachRenewalToggleResponse),
                    "int");
                try
                {
                    await ApiClient.DeactivateDateCoachAutoRenewalAsync(userId);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }

            public static async Task ToggleDateCoachAutoRenewalAsync(int userId)
            {
                PrintApiFunction(
                    nameof(IProductApiAsyncClient.ToggleDateCoachAutoRenewalAsync),
                    nameof(IProductApiAsyncClient),
                    nameof(Domain.DateCoachRenewalToggle.Data.DateCoachRenewalToggleResponse),
                    "int");
                try
                {
                    await ApiClient.ToggleDateCoachAutoRenewalAsync(userId);
                }
                catch (ApiCommandException apiEx)
                {
                    Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
                }
            }
        }

        private static void PrintApiFunction(string functionName, string interfaceName, string requestType, string resultType)
        {
            Console.WriteLine($"=> {resultType} {interfaceName}.{functionName}({requestType}):");
            Console.WriteLine();
        }

    }
}
