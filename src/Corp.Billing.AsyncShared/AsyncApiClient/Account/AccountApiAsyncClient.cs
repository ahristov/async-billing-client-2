﻿using System;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using System.Threading.Tasks;
using GetPaymentMethodRequest = Corp.Billing.Shared.Domain.Account.GetPaymentMethodRequest;
using GetPaymentMethodResult = Corp.Billing.Shared.Domain.Account.GetPaymentMethodResult;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Account
{
	public class AccountApiAsyncClient : BaseApiAsyncClient, IAccountApiAsyncClient
	{
		private static readonly string GetAccountPaymentMethodV2Url =
			ServiceUrl + ApiPathsV1.Account.Payment.GetAccountPaymentMethodV2Path + "/?";

		public AccountApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public async Task<GetPaymentMethodResult> GetAccountPaymentMethodV2(GetPaymentMethodRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetAccountPaymentMethodV2Url + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<GetPaymentMethodResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}
	}
}
