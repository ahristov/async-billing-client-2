﻿using System;
using System.Threading.Tasks;
using Corp.Billing.Shared.Domain.Account;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Account
{
	public interface IAccountApiAsyncClient
	{
		Task<GetPaymentMethodResult> GetAccountPaymentMethodV2(GetPaymentMethodRequest request,
			TimeSpan? timeout = null);
	}
}
