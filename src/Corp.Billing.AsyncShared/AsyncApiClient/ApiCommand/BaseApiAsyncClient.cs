﻿using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using System;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
	public abstract class BaseApiAsyncClient
	{
		protected IAsyncApiCommand ApiCommand { get; }

		public BaseApiAsyncClient(IAsyncApiCommand apiCommand)
		{
			ApiCommand = apiCommand ?? throw new ArgumentNullException(nameof(apiCommand));
		}

		protected static string ServiceUrl
		{
			get
			{
				return ApiConfiguration.BillingServiceUrl + ApiPathsV1.BasePath + "/";
			}
		}
	}
}
