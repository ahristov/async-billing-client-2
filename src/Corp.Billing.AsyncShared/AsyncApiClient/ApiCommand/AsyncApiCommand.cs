﻿using Corp.Billing.Shared.ApiClient;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Threading;
using System;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
	public class AsyncApiCommand : IAsyncApiCommand
	{
		protected static readonly HttpClient Client;
		private const string JsonMediaType = "application/json";
		private static readonly TimeSpan Timeout;

		static AsyncApiCommand()
		{
			Client = new HttpClient();
			Client.DefaultRequestHeaders.Host = ApiConfiguration.BillingServiceHost;
			Timeout = TimeSpan.FromSeconds(ApiConfiguration.Timeout);
		}

		public Task<ApiCommandResponse<TResponseData>> GetAsync<TResponseData>(string url,
			TimeSpan? timeout = null)
			=> SendAsync<TResponseData>(HttpMethod.Get, url, timeout: timeout ?? Timeout, mediaType: JsonMediaType);

		public Task<ApiCommandResponse<TResponseData>> PostAsync<TResponseData>(string url, object data,
			TimeSpan? timeout = null)
			=> SendAsync<TResponseData>(HttpMethod.Post, url, timeout ?? Timeout, data, mediaType: JsonMediaType);

		public Task<ApiCommandResponse<TResponseData>> PutAsync<TResponseData>(string url, object data,
			TimeSpan? timeout = null)
			=> SendAsync<TResponseData>(HttpMethod.Put, url, timeout ?? Timeout, data, mediaType: JsonMediaType);

		private static CancellationTokenSource GetCancellationTokenSource(TimeSpan timespan)
		{
			return new CancellationTokenSource(timespan);
		}

		private static async Task<ApiCommandResponse<TResponseData>> SendAsync<TResponseData>(HttpMethod httpMethod,
			string url,
			TimeSpan timeout,
			object requestData = null,
			string mediaType = null)
		{
			var httpContent = requestData != null
				? CreateHttpContent(requestData, mediaType)
				: null;

			HttpRequestMessage httpRequestMessage = new HttpRequestMessage
			{
				Content = httpContent,
				Method = httpMethod,
				RequestUri = new Uri(url)
			};

			using (var tokenSource = GetCancellationTokenSource(timeout))
			{
				var httpResponseMessage =
					await Client.SendAsync(httpRequestMessage, tokenSource.Token).ConfigureAwait(false);

				var responseStatusCode = httpResponseMessage.StatusCode;
				var responseMessage = httpResponseMessage.ReasonPhrase;
				var responseContent = httpResponseMessage.Content;
				var responseContentString = string.Empty;

				if (responseContent != null)
					responseContentString = await responseContent.ReadAsStringAsync().ConfigureAwait(false);

				ApiCommandResponse<TResponseData> res;

				if (httpResponseMessage.IsSuccessStatusCode)
				{
					TResponseData responseData = default;

					if (!string.IsNullOrEmpty(responseContentString)) responseData = JsonConvert.DeserializeObject<TResponseData>(
						responseContentString,
						ApiConfiguration.JsonSerializerSettings);

					res = new ApiCommandResponse<TResponseData>
					{
						ResponseStatusCode = responseStatusCode,
						ResponseMessage = responseMessage,
						ResponseData = responseData,
					};
				}
				else
				{
					if (string.IsNullOrEmpty(responseContentString))
						throw new ApiCommandException(responseMessage, responseMessage, 0, responseStatusCode, null);

					var serverErrorResponseData = JsonConvert.DeserializeObject<ServerErrorResponseDTO>(
						responseContentString,
						ApiConfiguration.JsonSerializerSettings);
					throw new ApiCommandException
					(
						serverErrorResponseData?.Message ?? responseMessage,
						serverErrorResponseData?.Title ?? responseMessage,
						serverErrorResponseData?.Code ?? 0,
						responseStatusCode,
						serverErrorResponseData?.ReferenceID
					);
				}

				return res;
			}

		}

		private static HttpContent CreateHttpContent(object data, string mediaType)
		{
			var jsonString = JsonConvert.SerializeObject(data, ApiConfiguration.JsonSerializerSettings);
			var contentBuffer = System.Text.Encoding.UTF8.GetBytes(jsonString);

			var byteContent = new ByteArrayContent(contentBuffer);
			byteContent.Headers.ContentType = new MediaTypeHeaderValue(mediaType);

			return byteContent;
		}

	}
}
