﻿using System.Net;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
	public class ApiCommandResponse<T>
	{
		public T ResponseData { get; set; }
		public HttpStatusCode ResponseStatusCode { get; set; }
		public string ResponseMessage { get; set; }
	}
}
