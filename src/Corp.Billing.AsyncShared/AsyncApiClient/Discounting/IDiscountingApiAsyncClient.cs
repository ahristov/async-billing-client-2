﻿using System;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Threading.Tasks;
using Corp.Billing.Shared.Domain.ProductCatalog.DiscountEngine;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Discounting
{
	public interface IDiscountingApiAsyncClient
	{
		Task<DiscountInfoResult> GetDiscountInfo(PCRulesRequest request, TimeSpan? timeout = null);
	}
}
