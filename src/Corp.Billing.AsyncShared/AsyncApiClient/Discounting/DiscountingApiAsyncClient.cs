﻿using System;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Threading.Tasks;
using Corp.Billing.Shared.Domain.ProductCatalog.DiscountEngine;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Discounting
{
	public class DiscountingApiAsyncClient : BaseApiAsyncClient, IDiscountingApiAsyncClient
	{
		private static readonly string ProductCatalogDiscountOfferUrl = ServiceUrl + ApiPathsV1.ProductCatalog.DiscountEngine.DiscountOffer + "/?";
		public DiscountingApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public async Task<DiscountInfoResult> GetDiscountInfo(PCRulesRequest request, TimeSpan? timeout = null)
		{
			string url = ProductCatalogDiscountOfferUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<DiscountInfoResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}
	}
}
