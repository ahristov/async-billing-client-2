﻿using System;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using Corp.Billing.Shared.Domain.ProfileProLite;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Products
{
	public class ProductApiAsyncClient : BaseApiAsyncClient, IProductApiAsyncClient
	{
		private static readonly string GetMatchMe30DayEligibilityCheckUrl = ServiceUrl + ApiPathsV1.Products.MatchMe.GetMatchMe30DayEligibilityCheck + "/?";
		private static readonly string PutMeInUserListUrl = ServiceUrl + ApiPathsV1.Products.MatchMe.PutMeInUserList;
		private static readonly string GetTopSpotStatusUrl = ServiceUrl + ApiPathsV1.Products.TopSpot.GetTopSpotStatusPath + "/?";
		private static readonly string ProvisionAndActivateTopSpotUrl = ServiceUrl + ApiPathsV1.Products.TopSpot.ProvisionAndActivateTopSpot;
		private static readonly string ActivateUnclaimedImpulseFeatureUrl = ServiceUrl + ApiPathsV1.Products.TopSpot.ActivateUnclaimedImpulseFeature;
		private static readonly string GetUnitUserStatusPcsUrl = ServiceUrl + ApiPathsV1.Products.ProfilePro.GetUnitUserStatusPcsPath + "/?";
		private static readonly string GetUnitUserStatusUrl = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitUserStatusPath + "/?";
		private static readonly string GetUnitDisplayStatusUrl = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitDisplayStatusPath + "/?";
		private static readonly string StatusAndActivateUrl = ServiceUrl + ApiPathsV1.Products.ImpulseFeatures.StatusAndActivate + "/?";
		private static readonly string ProfileProLiteRedemptionUrl = ServiceUrl + ApiPathsV1.ProfileProLite.Redemption + "/?";
		private static readonly string GetDateCoachStatusUrl = ServiceUrl + ApiPathsV1.Products.DateCoach.GetDateCoachStatus + "/?";
		private static readonly string ProductsDateCoachRenewalToggleTypeUrl = ServiceUrl + ApiPathsV1.Products.DateCoach.ToggleDateCoachRenewal;

		public ProductApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public async Task<MatchMe30DayEligibilityCheckResult> MatchMe30DayEligibilityCheckAsync(
			MatchMe30DayEligibilityCheckRequest request, TimeSpan? timeout = null)
		{
			string url = GetMatchMe30DayEligibilityCheckUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<MatchMe30DayEligibilityCheckResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<PutMeInUserListResult> PutMeInUserListAsync(PutMeInUserListRequest request,
			TimeSpan? timeout = null)
		{
			var apiResult = await ApiCommand.PostAsync<PutMeInUserListResult>(PutMeInUserListUrl, request, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<TopSpotStatusResult> GetTopSpotStatusAsync(TopSpotStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetTopSpotStatusUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<TopSpotStatusResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<ProvisionAndActivateTopSpotResult> ProvisionAndActivateTopSpotAsync(
			ProvisionAndActivateTopSpotRequest request, TimeSpan? timeout = null)
		{
			var apiResult = await ApiCommand.PostAsync<ProvisionAndActivateTopSpotResult>(ProvisionAndActivateTopSpotUrl, request, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<ActivateUnclaimedImpulseFeatureResult> ActivateUnclaimedImpulseFeatureAsync(
			ActivateUnclaimedImpulseFeatureRequest request, TimeSpan? timeout = null)
		{
			var apiResult = await ApiCommand.PutAsync<ActivateUnclaimedImpulseFeatureResult>(ActivateUnclaimedImpulseFeatureUrl, request, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<CSAUserPCSStatusResult> GetUnitUserStatusPcsAsync(CSAUserPCSStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUnitUserStatusPcsUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<CSAUserPCSStatusResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UnitUserStatusResult> GetUnitUserStatusAsync(UnitUserStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUnitUserStatusUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UnitUserStatusResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UnitDisplayStatusResult> GetUnitDisplayStatusAsync(UnitDisplayStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUnitDisplayStatusUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UnitDisplayStatusResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<GetStatusResult> GetImpulseFeaturesStatusAsync(GetStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = StatusAndActivateUrl + request.ToQueryString();
			url = url.Replace("{productFeature}", request.ProductFeature.ToString().ToLowerInvariant());
			var apiResult = await ApiCommand.GetAsync<GetStatusResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<ActivateResult> ActivateImpulseFeatureAsync(ActivateRequest request, TimeSpan? timeout = null)
		{
			string url = StatusAndActivateUrl.Replace("{productFeature}", request.ProductFeature.ToString().ToLowerInvariant());
			var apiResult = await ApiCommand.PutAsync<ActivateResult>(url, request, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public Task RedeemProfileProAsync(ProfileProLiteRedemption request, TimeSpan? timeout = null)
		{
			return ApiCommand.PostAsync<ActivateResult>(ProfileProLiteRedemptionUrl, request, timeout);
		}

		public async Task<ProfileProLiteRedemptionStatus> GetProfileProRedemptionsAsync(
			GetProfileProLiteRedemptionRequest request, TimeSpan? timeout = null)
		{
			string url = ProfileProLiteRedemptionUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<ProfileProLiteRedemptionStatus>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<DateCoachStatusResponse> GetDateCoachStatusAsync(int userId, TimeSpan? timeout = null)
		{
			var request = new DateCoachStatusRequest { UserId = userId };
			string url = GetDateCoachStatusUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<DateCoachStatusResponse>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public Task<DateCoachRenewalToggleResponse> ActivateDateCoachAutoRenewalAsync(int userId,
			TimeSpan? timeout = null)
		{
			return ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Activate);
		}

		public Task<DateCoachRenewalToggleResponse> DeactivateDateCoachAutoRenewalAsync(int userId,
			TimeSpan? timeout = null)
		{
			return ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Deactivate);
		}

		public Task<DateCoachRenewalToggleResponse> ToggleDateCoachAutoRenewalAsync(int userId,
			TimeSpan? timeout = null)
		{
			return ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Toggle);
		}

		private async Task<DateCoachRenewalToggleResponse> ToggleAutoRenewalWorker(int userId, DateCoachRenewalToggleRequestType toggleType, TimeSpan? timeout = null)
		{
			var request = new DateCoachRenewalToggleRequest { UserId = userId, ToggleType = toggleType };

			string url = ProductsDateCoachRenewalToggleTypeUrl
				.Replace("{toggleType}", request.ToggleType.ToString().ToLowerInvariant());

			var res = await ApiCommand.PutAsync<DateCoachRenewalToggleResponse>(url, request, timeout).ConfigureAwait(false);
			return res.ResponseData;
		}

	}
}
