﻿using System;
using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System.Threading.Tasks;
using Corp.Billing.Shared.Domain.ProfileProLite;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Products
{
	/// <summary>
	/// This interface is the async version for the following synchronous interfaces:
	/// <see cref="Shared.Domain.Products.IProductApiClient"/>
	/// <see cref="Shared.Domain.Products.ImpulseFeatures"/>
	/// </summary>
	public interface IProductApiAsyncClient
	{
		// MatchMe
		Task<MatchMe30DayEligibilityCheckResult> MatchMe30DayEligibilityCheckAsync(
			MatchMe30DayEligibilityCheckRequest request, TimeSpan? timeout = null);
		Task<PutMeInUserListResult> PutMeInUserListAsync(PutMeInUserListRequest request,
			TimeSpan? timeout = null);
		// Boost
		Task<TopSpotStatusResult> GetTopSpotStatusAsync(TopSpotStatusRequest request, TimeSpan? timeout = null);
		Task<ProvisionAndActivateTopSpotResult> ProvisionAndActivateTopSpotAsync(
			ProvisionAndActivateTopSpotRequest request, TimeSpan? timeout = null);
		Task<ActivateUnclaimedImpulseFeatureResult> ActivateUnclaimedImpulseFeatureAsync(
			ActivateUnclaimedImpulseFeatureRequest request, TimeSpan? timeout = null);
		// ProfilePro
		Task<CSAUserPCSStatusResult> GetUnitUserStatusPcsAsync(CSAUserPCSStatusRequest request,
			TimeSpan? timeout = null);
		// ProfilePro Redemptions
		Task RedeemProfileProAsync(ProfileProLiteRedemption request, TimeSpan? timeout = null);
		Task<ProfileProLiteRedemptionStatus> GetProfileProRedemptionsAsync(GetProfileProLiteRedemptionRequest request,
			TimeSpan? timeout = null);
		// Unit products
		Task<UnitUserStatusResult> GetUnitUserStatusAsync(UnitUserStatusRequest request, TimeSpan? timeout = null);
		Task<UnitDisplayStatusResult> GetUnitDisplayStatusAsync(UnitDisplayStatusRequest request,
			TimeSpan? timeout = null);
		// Impulse features
		Task<GetStatusResult> GetImpulseFeaturesStatusAsync(GetStatusRequest request, TimeSpan? timeout = null);
		Task<ActivateResult> ActivateImpulseFeatureAsync(ActivateRequest request, TimeSpan? timeout = null);
		// Date coach
		Task<DateCoachStatusResponse> GetDateCoachStatusAsync(int userId, TimeSpan? timeout = null);
		Task<DateCoachRenewalToggleResponse> ActivateDateCoachAutoRenewalAsync(int userId, TimeSpan? timeout = null);
		Task<DateCoachRenewalToggleResponse> DeactivateDateCoachAutoRenewalAsync(int userId, TimeSpan? timeout = null);
		Task<DateCoachRenewalToggleResponse> ToggleDateCoachAutoRenewalAsync(int userId, TimeSpan? timeout = null);
	}
}
