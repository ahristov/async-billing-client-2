﻿using System;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Orders;
using System.Threading.Tasks;
using UpdateOfflinePaymentStatusRequest = Corp.Billing.Shared.Domain.Orders.UpdateOfflinePaymentStatusRequest;
using UpdateOfflinePaymentStatusResponse = Corp.Billing.Shared.Domain.Orders.UpdateOfflinePaymentStatusResponse;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Orders
{
	public class OrderApiAsyncClient : BaseApiAsyncClient, IOrderApiAsyncClient
	{
		private static readonly string BillEventTicketsUrl = ServiceUrl + ApiPathsV1.Orders.UnitProducts.BillEventTicketsPath;
		private static readonly string GetImpulseBuyLastPurchaseUrl = ServiceUrl + ApiPathsV1.Orders.UnitProducts.GetImpulseBuyLastPurchase + "/?";
		private static readonly string UpdateOfflineTransactionStatusUrl = ServiceUrl + ApiPathsV1.Orders.Transaction.UpdateOfflineTransactionStatusPath + "/?";

		public OrderApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public async Task<BillEventTicketsResult> BillEventTicketsAsync(BillEventTicketsRequest request,
			TimeSpan? timeout = null)
		{
			var apiResult = await ApiCommand.PostAsync<BillEventTicketsResult>(BillEventTicketsUrl, request, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<ImpulseBuyLastPurchaseResult> GetImpulseBuyLastPurchaseAsync(
			ImpulseBuyLastPurchaseRequest request, TimeSpan? timeout = null)
		{
			string url = GetImpulseBuyLastPurchaseUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<ImpulseBuyLastPurchaseResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<GetOfflineTransferDataResult> GetOfflineTransferDataAsync(
			GetOfflineTransferDataRequest request, TimeSpan? timeout = null)
		{
			string url = UpdateOfflineTransactionStatusUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<GetOfflineTransferDataResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UpdateOfflinePaymentStatusResponse> UpdateOfflinePaymentStatusAsync(
			UpdateOfflinePaymentStatusRequest request, TimeSpan? timeout = null)
		{
			var apiResult = await ApiCommand.PutAsync<UpdateOfflinePaymentStatusResponse>(UpdateOfflineTransactionStatusUrl, request).ConfigureAwait(false);
			return apiResult.ResponseData;
		}
	}
}
