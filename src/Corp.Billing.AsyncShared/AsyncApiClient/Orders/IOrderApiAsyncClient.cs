﻿using System;
using Corp.Billing.Shared.Domain.Orders;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Orders
{
	/// <summary>
	/// This interface is the async version for the following synchronous interfaces:
	/// <see cref="IOrderService"/> 
	/// (<see cref="Shared.ApiClient.Orders.OrderApiClient"/>, skipping legacy functions)
	/// </summary>
	public interface IOrderApiAsyncClient
	{
		// Transaction
		Task<UpdateOfflinePaymentStatusResponse> UpdateOfflinePaymentStatusAsync(
			UpdateOfflinePaymentStatusRequest request, TimeSpan? timeout = null);
		Task<GetOfflineTransferDataResult> GetOfflineTransferDataAsync(GetOfflineTransferDataRequest request,
			TimeSpan? timeout = null);
		// Unit products
		Task<BillEventTicketsResult> BillEventTicketsAsync(BillEventTicketsRequest request, TimeSpan? timeout = null);
		Task<ImpulseBuyLastPurchaseResult> GetImpulseBuyLastPurchaseAsync(ImpulseBuyLastPurchaseRequest request,
			TimeSpan? timeout = null);
	}
}
