﻿using System;
using System.Threading.Tasks;
using Corp.Billing.Shared.Domain.RateCard;
using Corp.Billing.Shared.Domain.RateCardV3.Services;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Catalog
{
	public interface IRateCardApiAsyncClient
	{
		Task<GetExtendedRateCardResponse> GetExtendedRateCard(GetRateCardRequest req, TimeSpan? timeout = null);
		Task<GetRateCardResponse> GetRateCard(GetRateCardRequest req, TimeSpan? timeout = null);
		Task<RateCardV2AddOnsResult> GetLegacyAddOnsRateCard(RateCardV2AddOnsRequest request, TimeSpan? timeout = null);
	}
}
