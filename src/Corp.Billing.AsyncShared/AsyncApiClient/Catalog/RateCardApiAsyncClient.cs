﻿using System;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.RateCard;
using Corp.Billing.Shared.Domain.RateCardV3.Services;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Catalog
{
	public class RateCardApiAsyncClient : BaseApiAsyncClient, IRateCardApiAsyncClient
	{
		private static readonly string GetExtendedRateCardV3Url = ServiceUrl + ApiPathsV1.RateCard.GetExtendedRateCardV3 + "/?";
		private static readonly string GetRateCardV2AddOnsUrl = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2AddOnsPath + "/?";
		private static readonly string GetRateCardV3Url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV3 + "/?";

		public RateCardApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public async Task<GetExtendedRateCardResponse> GetExtendedRateCard(GetRateCardRequest req,
			TimeSpan? timeout = null)
		{
			string url = GetExtendedRateCardV3Url + req.ToQueryString();

			url = url
				.Replace("{catalogType}", req.CatalogTypeParam.ToString().ToLowerInvariant())
				.Replace("{catalogSource}", req.CatalogSourceParam.ToString().ToLowerInvariant());

			var apiResult = await ApiCommand.GetAsync<GetExtendedRateCardResponse>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<RateCardV2AddOnsResult> GetLegacyAddOnsRateCard(RateCardV2AddOnsRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetRateCardV2AddOnsUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<RateCardV2AddOnsResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<GetRateCardResponse> GetRateCard(GetRateCardRequest req, TimeSpan? timeout = null)
		{
			string url = GetRateCardV3Url + req.ToQueryString();

			url = url
				.Replace("{catalogType}", req.CatalogTypeParam.ToString().ToLowerInvariant())
				.Replace("{catalogSource}", req.CatalogSourceParam.ToString().ToLowerInvariant());

			var apiResult = await ApiCommand.GetAsync<GetRateCardResponse>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}
	}
}

