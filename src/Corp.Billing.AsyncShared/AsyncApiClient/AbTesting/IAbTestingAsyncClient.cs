﻿using System;
using System.Threading.Tasks;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.AsyncShared.AsyncApiClient.AbTesting
{
	public interface IAbTestingAsyncClient
	{
		Task<bool> Evaluate(PCRulesRequest request, string testName, TimeSpan? timeout = null);
		Task<EvaluateTestVariablesResult> EvaluateTestVariables(PCRulesRequest request, string testName,
			TimeSpan? timeout = null);

	}
}
