﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
	public class SubscriptionBenefitsApiAsyncClient : BaseApiAsyncClient, ISubscriptionBenefitsApiAsyncClient
	{
		private static readonly string SubscriptionBenefitsCountNewUrl = ServiceUrl + ApiPathsV1.Subscription.SubscriptionBenefitsCountNew + "/?";
		private static readonly string SubscriptionBenefitsUrl = ServiceUrl + ApiPathsV1.Subscription.SubscriptionBenefits + "/?";

		public SubscriptionBenefitsApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req,
			TimeSpan? timeout = null)
		{
			return GetCountOfNewSubscriptionBenefitsAsync(req, null, timeout);
		}

		public async Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req,
			ICollection<int> acceptedFeatureTypes, TimeSpan? timeout = null)
		{
			string url = SubscriptionBenefitsCountNewUrl + req.ToQueryString();

			string aftp = "" + GetAcceptedFeatureTypesParam(acceptedFeatureTypes);
			if (!string.IsNullOrWhiteSpace(aftp))
			{
				url = $"{url}&{aftp}";
			}

			var apiResult = await ApiCommand.GetAsync<BenefitsCountNewResponse>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req,
			TimeSpan? timeout = null)
		{
			return GetSubscriptionBenefitsStatusAsync(req, null, timeout);
		}

		public async Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req,
			ICollection<int> acceptedFeatureTypes, TimeSpan? timeout = null)
		{
			string url = SubscriptionBenefitsUrl + req.ToQueryString();

			string aftp = "" + GetAcceptedFeatureTypesParam(acceptedFeatureTypes);
			if (!string.IsNullOrWhiteSpace(aftp))
			{
				url = url + "&" + aftp;
			}

			var apiResult = await ApiCommand.GetAsync<BenefitStatusResponse>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		private static string GetAcceptedFeatureTypesParam(ICollection<int> acceptedFeatureTypes)
		{
			if (acceptedFeatureTypes == null)
				return string.Empty;

			if (acceptedFeatureTypes.Count < 1)
				return string.Empty;

			var paramName = "acceptedFeatureTypes";
			var paramValue = JsonConvert.SerializeObject(acceptedFeatureTypes);

			var res = paramName.GetNameValueQueryParameterPairs(paramValue);
			return res;
		}

	}
}
