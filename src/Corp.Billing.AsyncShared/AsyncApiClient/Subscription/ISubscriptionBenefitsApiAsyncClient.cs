﻿using System;
using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
	public interface ISubscriptionBenefitsApiAsyncClient
	{
		Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req,
			TimeSpan? timeout = null);
		Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req,
			ICollection<int> acceptedFeatureTypes, TimeSpan? timeout = null);
		Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req,
			TimeSpan? timeout = null);
		Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req,
			ICollection<int> acceptedFeatureTypes, TimeSpan? timeout = null);
	}
}
