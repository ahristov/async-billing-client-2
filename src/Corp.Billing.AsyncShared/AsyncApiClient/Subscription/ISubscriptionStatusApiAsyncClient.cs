﻿using System;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.Subscription;
using Corp.Billing.Shared.Domain.UserFeatures;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
	/// <summary>
	/// This interface is the async version for the following synchronous interfaces:
	/// <see cref="IUserSubscriptionStatus"/>
	/// <see cref="IUserSubscriptionStatusV2"/>
	/// <see cref="Shared.ApiClient.Subscription.UserBillingDataApiClient"/>
	/// <see cref="IUserBillingData"/>
	/// <see cref="Shared.ApiClient.Subscription.IUserSubscriptionSummaryApiClient"/>
	/// </summary>
	public interface ISubscriptionStatusApiAsyncClient
	{
		// This is not needed, use GetDisplayStatus()
		// Task<List<DisplayUserStatusResult>> GetUserStatusBy(int userId, CancellationToken? cancellationToken = null);

		Task<List<DisplayUserStatusResult>> GetDisplayStatusAsync(DisplayUserStatusRequest request,
			TimeSpan? timeout = null);
		Task<DisplayUserStatusResponse> GetDisplayStatusV2Async(DisplayUserStatusRequest request,
			TimeSpan? timeout = null);
		Task<UserPromoInfoResult> GetUserStatusPromoInfoAsync(UserPromoInfoRequest request, TimeSpan? timeout = null);
		Task<UserSubStatusResult> GetUserSubscriptionStatusAsync(UserSubStatusRequest request, TimeSpan? timeout = null);
		Task<UserSubStatusMessageResult> GetUserSubStatusMessageAsync(UserSubStatusMessageRequest request,
			TimeSpan? timeout = null);
		Task<UserStatusDateResult> GetUserStatusDateAsync(UserStatusDateRequest request, TimeSpan? timeout = null);
		Task<UserStatusDateResult> GetUserStatusDateV1Async(UserStatusDateRequest request, TimeSpan? timeout = null);
		Task<UserFeatureResult> GetUserFeaturesAsync(UserFeatureRequest request, TimeSpan? timeout = null);
		Task<UserFeaturesLightResult> GetUserFeaturesLightAsync(UserFeaturesLightRequest request, TimeSpan? timeout = null);
		Task<CancelResult> CancelUserAsync(CancelRequest request, TimeSpan? timeout = null);
		Task<GetUserBillingDataResult> GetUserBillingDataAsync(GetUserBillingDataRequest request, TimeSpan? timeout = null);
		Task<GetCurrentPackageResult> GetCurrentPackageAsync(GetCurrentPackageRequest request, TimeSpan? timeout = null);
		Task<UserSubscriptionSummaryResult> GetSubscriptionSummaryAsync(UserSubscriptionSummaryRequest request, TimeSpan? timeout = null);
	}
}
