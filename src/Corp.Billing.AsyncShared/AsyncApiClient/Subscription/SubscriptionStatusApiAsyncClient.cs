﻿using System;
using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.Subscription;
using Corp.Billing.Shared.Domain.UserFeatures;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
	public class SubscriptionStatusApiAsyncClient : BaseApiAsyncClient, ISubscriptionStatusApiAsyncClient
	{
		private static readonly string SubscriptionFeaturesActivationUrl = ServiceUrl + ApiPathsV1.Subscription.CancelProduct;
		private static readonly string GetCurrentPackageUrl = ServiceUrl + ApiPathsV1.Subscription.GetCurrentPackage + "/?";
		private static readonly string GetDisplayStatusPathUrl = ServiceUrl + ApiPathsV1.Subscription.GetDisplayStatusPath + "/?";
		private static readonly string GetDisplayStatusPathV2Url = ServiceUrl + ApiPathsV1.Subscription.GetDisplayStatusPathV2 + "/?";
		private static readonly string GetSubscriptionSummaryUrl = ServiceUrl + ApiPathsV1.Subscription.GetSubscriptionSummary + "/?";
		private static readonly string GetUserBillingDataUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserBillingData + "/?";
		private static readonly string GetUserFeaturesUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserFeaturesPath + "/?";
		private static readonly string GetUserFeaturesLightUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserFeaturesLightPath + "/?";
		private static readonly string GetUserStatusDateUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserStatusDatePath + "/?";
		private static readonly string GetUserStatusDateV1Url = ServiceUrl + ApiPathsV1.Subscription.GetUserStatusDateV1 + "/?";
		private static readonly string GetUserPromoInfoUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserPromoInfoPath + "/?";
		private static readonly string GetUserSubStatusUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserSubStatusPath + "/?";
		private static readonly string GetUserSubStatusMessageUrl = ServiceUrl + ApiPathsV1.Subscription.GetUserSubStatusMessagePath + "/?";

		public SubscriptionStatusApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

		public async Task<CancelResult> CancelUserAsync(CancelRequest request, TimeSpan? timeout = null)
		{
			var apiResult = await ApiCommand.PostAsync<CancelResult>(SubscriptionFeaturesActivationUrl, request, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<GetCurrentPackageResult> GetCurrentPackageAsync(GetCurrentPackageRequest request, TimeSpan? timeout = null)
		{
			string url = GetCurrentPackageUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<GetCurrentPackageResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<List<DisplayUserStatusResult>> GetDisplayStatusAsync(DisplayUserStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetDisplayStatusPathUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<List<DisplayUserStatusResult>>(url, timeout);
			return apiResult.ResponseData;
		}

		public async Task<DisplayUserStatusResponse> GetDisplayStatusV2Async(DisplayUserStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetDisplayStatusPathV2Url + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<DisplayUserStatusResponse>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserSubscriptionSummaryResult> GetSubscriptionSummaryAsync(UserSubscriptionSummaryRequest request, TimeSpan? timeout = null)
		{
			string url = GetSubscriptionSummaryUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserSubscriptionSummaryResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<GetUserBillingDataResult> GetUserBillingDataAsync(GetUserBillingDataRequest request, TimeSpan? timeout = null)
		{
			string url = GetUserBillingDataUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<GetUserBillingDataResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserFeatureResult> GetUserFeaturesAsync(UserFeatureRequest request, TimeSpan? timeout = null)
		{
			string url = GetUserFeaturesUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserFeatureResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserFeaturesLightResult> GetUserFeaturesLightAsync(UserFeaturesLightRequest request, TimeSpan? timeout = null)
		{
			string url = GetUserFeaturesLightUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserFeaturesLightResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserStatusDateResult> GetUserStatusDateAsync(UserStatusDateRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUserStatusDateUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserStatusDateResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserStatusDateResult> GetUserStatusDateV1Async(UserStatusDateRequest request, TimeSpan? timeout = null)
		{
			string url = GetUserStatusDateV1Url + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserStatusDateResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserPromoInfoResult> GetUserStatusPromoInfoAsync(UserPromoInfoRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUserPromoInfoUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserPromoInfoResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserSubStatusResult> GetUserSubscriptionStatusAsync(UserSubStatusRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUserSubStatusUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserSubStatusResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}

		public async Task<UserSubStatusMessageResult> GetUserSubStatusMessageAsync(UserSubStatusMessageRequest request,
			TimeSpan? timeout = null)
		{
			string url = GetUserSubStatusMessageUrl + request.ToQueryString();
			var apiResult = await ApiCommand.GetAsync<UserSubStatusMessageResult>(url, timeout).ConfigureAwait(false);
			return apiResult.ResponseData;
		}
	}
}
