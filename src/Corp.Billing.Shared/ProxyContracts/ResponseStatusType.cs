﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public enum ResponseStatusType
    {
        Undefined,
        Success,
        Error,
        AccountLockFailed,
    }
}
