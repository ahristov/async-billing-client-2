﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class CreditCardWithCardinalParamsPaymentRequestMessage : CreditCardPaymentRequestMessage
    {
        public string IPAddress
        {
            get { return Get<string>("IPAddress"); }
            set { this["IPAddress"] = value; }
        }

        // Cardinal amount (it is the MAX from initial order amount and the renewal amount)
        public int AmountInCents
        {
            get { return Get<int>("AmountInCents"); }
            set { this["AmountInCents"] = value; }
        }

        public int CurrencyCodeNumber
        {
            get { return Get<int>("CurrencyCodeNumber"); }
            set { this["CurrencyCodeNumber"] = value; }
        }

        public string Recurring
        {
            get { return Get<string>("Recurring"); }
            set { this["Recurring"] = value; }
        }

        public int RecurringFrequency
        {
            get { return Get<int>("RecurringFrequency"); }
            set { this["RecurringFrequency"] = value; }
        }

    }
}
