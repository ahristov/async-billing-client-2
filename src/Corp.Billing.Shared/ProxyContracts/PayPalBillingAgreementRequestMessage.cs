﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class PayPalBillingAgreementRequestMessage : PaymentProxyRequestMessage
    {
        public PayPalBillingAgreementRequestMessage()
        {
            CommandType = PaymentProxyCommandType.PayPalBillingAgreement;
        }

        public string Token
        {
            get { return Get<string>("Token"); }
            set { this["Token"] = value; }
        }
    }
}
