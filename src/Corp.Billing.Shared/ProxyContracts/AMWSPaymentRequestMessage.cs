﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class AMWSPaymentRequestMessage : OnlinePaymentRequestMessage
    {
        public AMWSPaymentRequestMessage() : base()
        {
            CommandType = PaymentProxyCommandType.AMWSOrderProcessing;
        }

        public AMWSPaymentRequestMessage(Dictionary<string, object> from) : base(from)
        {
            CommandType = PaymentProxyCommandType.AMWSOrderProcessing;
        }

        public string AMWSBillingAgreementId
        {
            get { return Get<string>("AMWSBillingAgreementId"); }
            set { this["AMWSBillingAgreementId"] = value; }
        }

        public bool AMWSConsentStatus
        {
            get { return Get<bool>("AMWSConsentStatus"); }
            set { this["AMWSConsentStatus"] = value; }
        }

    }
}
