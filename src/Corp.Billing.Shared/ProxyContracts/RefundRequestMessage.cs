﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class RefundRequestMessage : PaymentProxyRequestMessage
    {
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public int TransactionId { get; set; }
    }
}
