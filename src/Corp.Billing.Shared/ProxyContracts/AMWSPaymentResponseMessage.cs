﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class AMWSPaymentResponseMessage : OnlinePaymentResponseMessage
    {
        public AMWSPaymentResponseMessage()
        {
            this.ReturnValue = ReturnPaymentCodes.DEFAULT;
        }
    }
}
