﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class ApplePayPaymentResponseMessage : OnlinePaymentResponseMessage
    {
        public ApplePayPaymentResponseMessage()
        {
            this.ReturnValue = ReturnPaymentCodes.DEFAULT;
        }

        public string TransactionAuthorizationCode
        {
            get { return Get<string>("TransactionAuthorizationCode"); }
            set { this["TransactionAuthorizationCode"] = value; }
        }

    }
}
