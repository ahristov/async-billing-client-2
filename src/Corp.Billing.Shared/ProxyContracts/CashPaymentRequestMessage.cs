﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class CashPaymentRequestMessage : OfflinePaymentRequestMessage
    {
        public string CheckNumber
        {
            get { return Get<string>("CheckNumber"); }
            set { this["CheckNumber"] = value; }
        }

        public string RoutingNumber
        {
            get { return Get<string>("RoutingNumber"); }
            set { this["RoutingNumber"] = value; }
        }
    }
}
