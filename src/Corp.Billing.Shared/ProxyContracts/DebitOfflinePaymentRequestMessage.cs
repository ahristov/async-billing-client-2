﻿using Corp.Billing.Shared.Domain.Account;
using System;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class DebitOfflinePaymentRequestMessage : OfflinePaymentRequestMessage, IBankAccount
    {

        public Banks Bank
        {
            get
            {
                var sBank = Get<String>("Bank");
                if (string.IsNullOrEmpty(sBank))
                    return Banks.UNKNOWN;

                return (Banks)Enum.Parse(typeof(Banks), sBank, false); 
            }
            set { 
                this["Bank"] = value; 
            }
        }

        public int BankNumber
        {
            get { return (int) Bank; }
        }

        public string BankBranch
        {
            get { return Get<String>("BankBranch"); }
            set { this["BankBranch"] = value; }
        }

        public string BankAccount
        {
            get { return Get<String>("BankAccount"); }
            set { this["BankAccount"] = value; }
        }

        public string BankAccountCheckDigit
        {
            get { return Get<String>("BankAccountCheckDigit"); }
            set { this["BankAccountCheckDigit"] = value; }
        }

        public string Cpf
        {
            get { return Get<String>("Cpf"); }
            set { this["Cpf"] = value; }
        }

        public int TrxID { get; set; }
    }

    public enum Banks
    {
        UNKNOWN = 0,
        BRAD = 237,
        HSBC = 399,
        ITAU = 341,
        SBRA = 353
    }
}
