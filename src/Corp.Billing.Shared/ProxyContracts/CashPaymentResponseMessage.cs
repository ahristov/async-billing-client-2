﻿using System;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class CashPaymentResponseMessage : OfflinePaymentResponseMessage
    {
        public string OrderString
        {
            get { return Get<String>("OrderString"); }
            set { this["OrderString"] = value; }
        }
    }
}
