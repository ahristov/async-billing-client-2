﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class ApplePayPaymentRequestMessage : CreditCardPaymentRequestMessage
    {
        public ApplePayPaymentRequestMessage() : base()
        {
            CommandType = PaymentProxyCommandType.ApplePayOrderProcessing;
        }

        public ApplePayPaymentRequestMessage(Dictionary<string, object> from) : base(from)
        {
            CommandType = PaymentProxyCommandType.ApplePayOrderProcessing;
        }

        public string ApplePaymentPayload
        {
            get { return Get<string>("ApplePaymentPayload"); }
            set { this["ApplePaymentPayload"] = value; }
        }

        public string ApplePayMerchantId
        {
            get { return Get<string>("ApplePayMerchantId"); }
            set { this["ApplePayMerchantId"] = value; }
        }



        // --
        // The rest will be decrypted from the encrypted Payment data
        //
        //
        //      ApplePayOnlineCryptogram
        //      ApplePayAccountExpirationDate
        //
        //      CardNumberClear
        //      CcYear
        //      CcMonth
        //
        // --


        public string ApplePayOnlineCryptogram
        {
            get { return Get<string>("ApplePayOnlineCryptogram"); }
            set { this["ApplePayOnlineCryptogram"] = value; }
        }

        public DateTime ApplePayAccountExpirationDate
        {
            get { return Get<DateTime>("ApplePayAccountExpirationDate"); }
            set { this["ApplePayAccountExpirationDate"] = value; }
        }

    }
}
