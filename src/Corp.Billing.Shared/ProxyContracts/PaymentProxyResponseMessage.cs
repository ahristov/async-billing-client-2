﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class PaymentProxyResponseMessage : BaseProxyResponseMessage
    {
        public int AcctOrderId
        {
            get { return this.Get<int>("AcctOrderId"); }
            set { this["AcctOrderId"] = value; }
        }

        public int TrxId
        {
            get { return this.Get<int>("TrxId"); }
            set { this["TrxId"] = value; }
        }

        public string XmlMatchEventTicketsPurchased
        {
            get { return this.Get<string>("XmlMatchEventTicketsPurchased"); }
            set { this["XmlMatchEventTicketsPurchased"] = value; }
        }
    }
}
