﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class ResponseError
    {
        public ResponseErrorType ErrorType { get; set; }

        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDetails { get; set; }

    }
}
