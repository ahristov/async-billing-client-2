﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class PayPalBillingAgreementTokenRequestMessage : PaymentProxyRequestMessage
    {
        public PayPalBillingAgreementTokenRequestMessage()
        {
            CommandType = PaymentProxyCommandType.GetPayPalToken;
        }

        public string CountryCode
        {
            get { return Get<string>("CountryCode"); }
            set { this["CountryCode"] = value; }
        }

        public string WebUrl
        {
            get { return Get<string>("WebUrl"); }
            set { this["WebUrl"] = value; }
        }

        public string ImageUrl
        {
            get { return Get<string>("ImageUrl"); }
            set { this["ImageUrl"] = value; }
        }

        public string ProductDescription
        {
            get { return Get<string>("ProductDescription"); }
            set { this["ProductDescription"] = value; }
        }

        public string ReturnUrl
        {
            get { return Get<string>("ReturnUrl"); }
            set { this["ReturnUrl"] = value; }
        }

        public string CancelUrl
        {
            get { return Get<string>("CancelUrl"); }
            set { this["CancelUrl"] = value; }
        }

        public byte? ClientIp1
        {
            get { return Get<byte?>("ClientIp1"); }
            set { this["ClientIp1"] = value; }
        }

        public byte? ClientIp2
        {
            get { return Get<byte?>("ClientIp2"); }
            set { this["ClientIp2"] = value; }
        }

        public byte? ClientIp3
        {
            get { return Get<byte?>("ClientIp3"); }
            set { this["ClientIp3"] = value; }
        }

        public byte? ClientIp4
        {
            get { return Get<byte?>("ClientIp4"); }
            set { this["ClientIp4"] = value; }
        }
    }
}
