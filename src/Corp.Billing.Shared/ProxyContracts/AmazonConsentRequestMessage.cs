﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class AmazonConsentRequestMessage : PaymentProxyRequestMessage
    {
        public string BillingAgreementId
        {
            get { return this.Get<string>("BillingAgreementId"); }
            set { this["BillingAgreementId"] = value; }
        }
    }
}