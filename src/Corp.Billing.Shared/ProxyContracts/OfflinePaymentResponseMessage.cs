﻿using System;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class OfflinePaymentResponseMessage : OrderPaymentResponseMessage
    {
        public string AuthorizeUrl
        {
            get { return Get<String>("AuthorizeUrl"); }
            set { this["AuthorizeUrl"] = value; }
        }
    }
}
