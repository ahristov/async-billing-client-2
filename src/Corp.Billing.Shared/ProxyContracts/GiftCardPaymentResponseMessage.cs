﻿using Corp.Billing.Shared.Domain.Payments.Transactions;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class GiftCardPaymentResponseMessage : OrderPaymentResponseMessage
    {
        public GiftCardPaymentResponseMessage()
        {
            ReturnValue = ReturnPaymentCodes.DEFAULT;
        }

        public string TransactionAuthorizationCode
        {
            get { return Get<string>("TransactionAuthorizationCode"); }
            set { this["TransactionAuthorizationCode"] = value; }
        }

        public TransactionStatus TransactionStatus
        {
            get { return Get<TransactionStatus>("TransactionStatus"); }
            set { this["TransactionStatus"] = value; }
        }

        public TransactionStatusReason TransactionStatusReason
        {
            get { return Get<TransactionStatusReason>("TransactionStatusReason"); }
            set { this["TransactionStatusReason"] = value; }
        }
    }
}
