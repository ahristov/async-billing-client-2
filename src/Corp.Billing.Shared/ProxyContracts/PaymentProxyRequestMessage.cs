﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class PaymentProxyRequestMessage : BaseProxyRequestMessage
    {
        public PaymentProxyRequestMessage() { }
        public PaymentProxyRequestMessage(Dictionary<string, object> from) : base(from) { }

        public PaymentProxyCommandType CommandType
        {
            get { return GetEnum<PaymentProxyCommandType>("CommandType"); }
            set { this["CommandType"] = ToEnum<PaymentProxyCommandType>(value + ""); }
        }


        public byte SiteCode
        {
            get { return this.Get<byte>("SiteCode"); }
            set { this["SiteCode"] = value; }
        }

        public short UrlCode
        {
            get { return this.Get<short>("UrlCode"); }
            set { this["UrlCode"] = value; }
        }


        public int UserId
        {
            get { return this.Get<int>("UserId"); }
            set { this["UserId"] = value; }
        }

        [JsonIgnore]
        public bool HasUserId
        {
            get
            {
                if (!this.ContainsKey("UserId"))
                    return false;

                return this.UserId != 0;
            }
        }

        public string DtlCode
        {
            get { return this.Get<string>("DtlCode"); }
            set { this["DtlCode"] = value; }
        }

        public int ProcessorTypeId
        {
            get { return this.Get<int>("ProcessorTypeId"); }
            set { this["ProcessorTypeId"] = value; }
        }

        public string ShoppingCartId
        {
            get { return this.Get<string>("ShoppingCartId"); }
            set { this["ShoppingCartId"] = value; }
        }
    }
}
