﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public static class ReturnPaymentCodeOffsets
    {
        public const int PROCESS_TRX = 44;
        public const int UPDATE_STATUS = 66;
        public const int PTCCERROR = 110; // SP bilProcessTxr return codes for failed CC payment with offset added +110
    }
}
