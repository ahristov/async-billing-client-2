﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class AdyenNotificationResponseMessage
    {
        public const string DEFAULT_ACCEPTED_RESPONSE = "[accepted]";

        public string notificationResponse { get; set; }
    }
}
