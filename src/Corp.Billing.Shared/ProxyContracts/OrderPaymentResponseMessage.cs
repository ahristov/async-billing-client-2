﻿using Corp.Billing.Shared.Domain.PaymentV3.Data.Response;
using System;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class OrderPaymentResponseMessage : PaymentProxyResponseMessage
    {
        public DateTime PaidThru
        {
            get { return Get<DateTime>("PaidThru"); }
            set { this["PaidThru"] = value; }
        }

        public long Features
        {
            get { return Get<long>("Features"); }
            set { this["Features"] = value; }
        }

        public long ProdAttributeMask
        {
            get { return Get<long>("ProdAttributeMask"); }
            set { this["ProdAttributeMask"] = value; }
        }

        public int TrxStatusId
        {
            get { return Get<int>("TrxStatusId"); }
            set { this["TrxStatusId"] = value; }
        }

        public int? TrxStatusIdForFee
        {
            get { return Get<int?>("TrxStatusIdForFee"); }
            set { this["TrxStatusIdForFee"] = value; }
        }

        public bool AwardedComplimentary
        {
            get { return Get<bool>("AwardedComplimentary"); }
            set { this["AwardedComplimentary"] = value; }
        }

        // Events related

        public int? AccountDetailId
        {
            get { return Get<int?>("AccountDetailId"); }
            set { this["AccountDetailId"] = value; }
        }

        public byte? EventStatus
        {
            get { return Get<byte?>("EventStatus") ; }
            set { this["EventStatus"] = value; }
        }

        public bool FraudDetected
        {
            get { return Get<bool>("FraudDetected"); }
            set { this["FraudDetected"] = value; }
        }

        public string TransactionAuthorizationCode
        {
            get { return Get<string>("TransactionAuthorizationCode"); }
            set { this["TransactionAuthorizationCode"] = value; }
        }

        public decimal? TransactionAmount
        {
            get { return Get<decimal?>("TransactionAmount"); }
            set { this["TransactionAmount"] = value; }
        }

        public string TransactionISOCurrencyCode
        {
            get { return Get<string>("TransactionISOCurrencyCode"); }
            set { this["TransactionISOCurrencyCode"] = value; }
        }
        public decimal? RefundAmount
        {
            get { return Get<decimal?>("RefundAmount"); }
            set { this["RefundAmount"] = value; }
        }

        public PerformedActionType PerformedAction
        {
            get { return GetEnum<PerformedActionType>("PerformedAction"); }
            set { this["PerformedAction"] = ToEnum<PerformedActionType>(value + ""); }
        }
    }
}
