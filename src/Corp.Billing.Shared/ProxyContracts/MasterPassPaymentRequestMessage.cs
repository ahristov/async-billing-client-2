﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class MasterPassPaymentRequestMessage : CreditCardWithExternalWalletPaymentRequestMessage
    {
        public MasterPassPaymentRequestMessage()
        {
            CommandType = PaymentProxyCommandType.VisaCheckoutOrderProcessing;
        }

        public string CheckoutResourceUrl
        {
            get { return Get<string>("CheckoutResourceUrl"); }
            set { this["CheckoutResourceUrl"] = value; }
        }

        public string OauthToken
        {
            get { return Get<string>("OauthToken"); }
            set { this["OauthToken"] = value; }
        }

        public string OauthVerifier
        {
            get { return Get<string>("OauthVerifier"); }
            set { this["OauthVerifier"] = value; }
        }


    }
}
