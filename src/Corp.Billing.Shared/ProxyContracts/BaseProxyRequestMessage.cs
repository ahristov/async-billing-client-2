﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public abstract class BaseProxyRequestMessage : BaseProxyMessage
    {
        public BaseProxyRequestMessage() { }
        public BaseProxyRequestMessage(Dictionary<string, object> from) : base(from) { }


        public byte PlatformId
        {
            get { return Get<byte>("PlatformId"); }
            set { this["PlatformId"] = value; }
        }
    }
}
