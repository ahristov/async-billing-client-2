﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public enum ResponseErrorType
    {
        Undefined,
        ValidationError,
        CreditCardServiceError,
        PayPalServiceErrorError,
    }
}
