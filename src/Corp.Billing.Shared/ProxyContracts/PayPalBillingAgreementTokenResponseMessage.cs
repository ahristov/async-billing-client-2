﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class PayPalBillingAgreementTokenResponseMessage : BaseProxyResponseMessage
    {
        public string Token
        {
            get { return Get<string>("Token"); }
            set { this["Token"] = value; }
        }

        public string ApprovalUrl
        {
            get { return Get<string>("ApprovalUrl"); }
            set { this["ApprovalUrl"] = value; }
        }
    }
}
