﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class BoletoOfflinePaymentResponseMessage : OfflinePaymentResponseMessage
    {
       
        public  int Unit 
        {
                get { return Get<int>("Unit"); }
                set { this["Unit"] = value; }
        }

        public  int Total
        {
                get { return Get<int>("Total"); }
                set { this["Total"] = value; }
        }
        public String CompanyName
        {
            get { return Get<String>("CompanyName"); }
            set { this["CompanyName"] = value; }
        }
        public String CompanyBankNumber
        {
            get { return Get<String>("CompanyBankNumber"); }
            set { this["CompanyBankNumber"] = value; }
        }
        public String CompanyRoudingNumber 
        {
            get { return Get<String>("CompanyRoudingNumber"); }
            set { this["CompanyRoudingNumber"] = value; }
        }
        public String CompanyAccountNumber
        {
            get { return Get<String>("CompanyAccountNumber"); }
            set { this["CompanyAccountNumber"] = value; }
        }
        public String Signature
        {
            get { return Get<String>("Signature"); }
            set { this["Signature"] = value; }
        }
        public int ShoppingId
        {
            get { return Get<int>("SHOPPINGID"); }
            set { this["SHOPPINGID"] = value; }
        }


        public string Description
        {
            get { return Get<String>("Description"); }
            set { this["Description"] = value; }
        }


        public decimal Amount
        {
            get { return Get<Decimal>("Amount"); }
            set { this["Amount"] = value; }
        }

        public decimal AmountWithNoDecimals
        {
            get { return ((int)(Amount * 100)); }
            
        }

        public String FormatedAmount
        {
            get { return string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", Amount); }
        }

        public DateTime DueDate
        {
            get { return Get<DateTime>("DueDate"); }
            set { this["DueDate"] = value; }
        }

        public DateTime ProcessDate
        {
            get { return Get<DateTime>("ProcessDate"); }
            set { this["ProcessDate"] = value; }
        }

        public DateTime ExpireDate
        {
            get { return Get<DateTime>("ExpireDate"); }
            set { this["ExpireDate"] = value; }
        }

        public String FullName
        {
            get { return Get<String>("FullName"); }
            set { this["FullName"] = value; }
        }

        public String Address
        {
            get { return Get<String>("Address"); }
            set { this["Address"] = value; }
        }

        public String City
        {
            get { return Get<String>("City"); }
            set { this["City"] = value; }
        }

        public String State
        {
            get { return Get<String>("State"); }
            set { this["State"] = value; }
        }

        public String ZipCode
        {
            get { return Get<String>("ZipCode"); }
            set { this["ZipCode"] = value; }
        }

        public String CPF
        {
            get { return Get<String>("CPF"); }
            set { this["CPF"] = value; }
        }


    }
}
