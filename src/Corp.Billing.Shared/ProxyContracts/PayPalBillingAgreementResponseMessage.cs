﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class PayPalBillingAgreementResponseMessage : BaseProxyResponseMessage
    {
        public string Payer
        {
            get { return Get<string>("Payer"); }
            set { this["Payer"] = value; }
        }

        public string FirstName
        {
            get { return Get<string>("FirstName"); }
            set { this["FirstName"] = value; }
        }

        public string LastName
        {
            get { return Get<string>("LastName"); }
            set { this["LastName"] = value; }
        }

        public string PayerCountry
        {
            get { return Get<string>("PayerCountry"); }
            set { this["PayerCountry"] = value; }
        }

        public string PayerId
        {
            get { return Get<string>("PayerId"); }
            set { this["PayerId"] = value; }
        }

        public string PayerStatus
        {
            get { return Get<string>("PayerStatus"); }
            set { this["PayerStatus"] = value; }
        }

        public string BillingAddressStatus
        {
            get { return Get<string>("BillingAddressStatus"); }
            set { this["BillingAddressStatus"] = value; }
        }

        public string BillingAddressStreet1
        {
            get { return Get<string>("BillingAddressStreet1"); }
            set { this["BillingAddressStreet1"] = value; }
        }

        public string BillingAddressStreet2
        {
            get { return Get<string>("BillingAddressStreet2"); }
            set { this["BillingAddressStreet2"] = value; }
        }

        public string BillingAddressCity
        {
            get { return Get<string>("BillingAddressCity"); }
            set { this["BillingAddressCity"] = value; }
        }

        public string BillingAddressState
        {
            get { return Get<string>("BillingAddressState"); }
            set { this["BillingAddressState"] = value; }
        }

        public string BillingAddressCountry
        {
            get { return Get<string>("BillingAddressCountry"); }
            set { this["BillingAddressCountry"] = value; }
        }

        public string BillingAddressZip
        {
            get { return Get<string>("BillingAddressZip"); }
            set { this["BillingAddressZip"] = value; }
        }

        public string BillingAgreementId
        {
            get { return Get<string>("BillingAgreementId"); }
            set { this["BillingAgreementId"] = value; }
        }
    }
}
