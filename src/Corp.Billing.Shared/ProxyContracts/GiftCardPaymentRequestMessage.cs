﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class GiftCardPaymentRequestMessage : CreditCardPaymentRequestMessage
    {
        public GiftCardPaymentRequestMessage()
        {
            CommandType = PaymentProxyCommandType.GiftCardOrderProcessingV2;
        }
    }
}
