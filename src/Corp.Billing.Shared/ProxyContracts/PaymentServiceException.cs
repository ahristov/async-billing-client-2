﻿using System;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class PaymentServiceException : Exception
    {
        public PaymentServiceException(string errMsg) : base(errMsg)
        { }
    }
}
