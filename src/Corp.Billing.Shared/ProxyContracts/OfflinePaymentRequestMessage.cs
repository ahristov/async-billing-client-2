﻿using System;


namespace Corp.Billing.Shared.ProxyContracts
{
    public class OfflinePaymentRequestMessage : OrderPaymentRequestMessage
    {
        public string UserEmail
        {
            get { return Get<string>("UserEmail"); }
            set { this["UserEmail"] = value; }
        }

        public int TrxId
        {
            get { return Get<int>("TrxID"); }
            set { this["TrxID"] = value; }
        }

        public int? ReferenceId
        {
            get { return Get<int?>("ReferenceID", (v) => v > 0); }
            set { this["ReferenceID"] = value; }
        }

        public StatusType Status
        {
            get { return GetEnum<StatusType>("Status"); }
            set { this["Status"] = value; }
        }

        public enum StatusType
        {
            Success = 1,
            Failure = 2,
            PendingOfflineTransfer = 14, 
            PendingOfflineTransferProvisionedAdvance = 15,
            CancelOfflineTransferProvisionedInAdvance = 16
        }

        public decimal? Amount
        {
            get { return Get<decimal?>("Amount", (v) => v > 0); }
            set { this["Amount"] = value; }
        }

        public string CurrencyCode
        {
            get { return Get<string>("CurrencyCode"); }
            set { this["CurrencyCode"] = value; }
        }

        public string ReturnCode
        {
            get { return Get<string>("ReturnCode"); }
            set { this["ReturnCode"] = value; }
        }

    }
}
