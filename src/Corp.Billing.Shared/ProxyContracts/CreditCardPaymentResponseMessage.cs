﻿using Corp.Billing.Shared.Subscription.Payment;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class CreditCardPaymentResponseMessage : OnlinePaymentResponseMessage
    {
        public CreditCardPaymentResponseMessage()
        {
            this.ReturnValue = ReturnPaymentCodes.DEFAULT;
        }

        public PaymentDefinitions.CreditCardTypes PaymentMethodCardType { get; set; }
        public string PaymentMethodLastFourDigits { get; set; }

    }
}
