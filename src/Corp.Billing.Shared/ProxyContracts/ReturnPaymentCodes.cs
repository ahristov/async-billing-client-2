﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public static class ReturnPaymentCodes
    {
        public const int DEFAULT = -2; // Initialized but never used

        public const int SODLERROR = -1;
        public const int SOFAILEDTOGETALOCK = -3;

        public const int VERISIGNSUCCESS = 0;
        public const int VERISIGNCOMMERROR = 3;
        public const int VERISIGNHARDFAILURE = 1;
        public const int VERISIGNHARDFAILUREBADACCT = 2;

        public const int PTSUCCESS = 44;

        public const int USDLERROR = 85;
        public const int USVERISIGNSUCCESS = 100;
        public const int USVERISIGNCOMMERROR = 103;
        public const int USVERISIGNHARDFAILURE = 101;
        public const int USVERISIGNHARDFAILUREBADACCT = 102;
        public const int SODOUBLECLICKSUCCESS = 36;

        public const int SOSUCCESS = 0;
        public const int PTCOMMERROR = 53;
        public const int PTVERISIGNOFFINAPPLOBJ = 54;
        public const int PTBPPSUCCEEDEDVERISIGNFAILED = 55;

        public const int PTCCERROR_INSUFFICIENTFUNDS = 111;
        public const int PTCCERROR_SECURITYCODE = 112;
        public const int PTCCERROR_EXPIRATIONDATE = 113;

    }
}
