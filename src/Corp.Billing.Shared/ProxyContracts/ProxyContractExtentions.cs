﻿using System;
using System.Text.RegularExpressions;

namespace Corp.Billing.Shared.ProxyContracts
{
    public static class ProxyContractExtentions
    {

        public static TResponseMessage As<TResponseMessage>(this BaseProxyMessage original, Func<TResponseMessage> instante)
            where TResponseMessage : BaseProxyMessage
        {
            var result = instante();
            foreach (var kvp in original)
                result[kvp.Key] = kvp.Value;
            return result;
        }


        public static TEnum ToEnum<TEnum>(this string s)
            where TEnum : struct
        {
            if (string.IsNullOrEmpty(s))
                return default(TEnum);

            int i;
            if (int.TryParse(s, out i))
            {
                return (TEnum)Enum.ToObject(typeof(TEnum), i);
            }

            return (TEnum)Enum.Parse(typeof(TEnum), s, true);
        }


        public static bool TryParseGuid(this string s, out Guid result)
        {
            result = Guid.Empty;

            if (string.IsNullOrEmpty(s))
                return false;

            s = s.Replace("-", string.Empty).Replace("{", string.Empty).Replace("}", string.Empty);

            Regex r = new Regex("[0-9A-Fa-f]{32}");

            if (!r.IsMatch(s))
                return false;

            result = new Guid(s);
            return true;
        }
    }
}
