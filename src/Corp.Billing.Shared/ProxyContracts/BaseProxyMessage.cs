﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public abstract class BaseProxyMessage : Dictionary<string, object>
    {
        public BaseProxyMessage()
            : base(StringComparer.OrdinalIgnoreCase)
        { }
        public BaseProxyMessage(Dictionary<string, object> from) : base(from) { }


        public T Get<T>(string key)
        {
            return Get<T>(key, null);
        }

        public T Get<T>(string key, Func<T, bool> test)
        {
            if (!ContainsKey(key))
                return default(T);

            object val;
            if (TryGetValue(key, out val))
            {
                if (val != null)
                {
                    T result;

                    if (typeof (T) == typeof (Guid?))
                    {
                        var guid = GetNullableGuid(val.ToString());
                        result = guid.HasValue ? (T) (object)guid.Value : default(T);
                    }
                    else
                    {
                        Type t = typeof (T);
                        t = Nullable.GetUnderlyingType(t) ?? t;
                        result = (T)Convert.ChangeType(val, t);
                    }

                    if (test != null)
                        result = test(result) ? result : default(T);

                    return result;
                }
            }

            return default(T);
        }

        private Guid? GetNullableGuid(string s)
        {
            Guid result;
            if (!s.TryParseGuid(out result))
                return default(Guid?);
            return result;
        }

        public TEnum GetEnum<TEnum>(string key)
            where TEnum : struct
        {
            return Get<string>(key).ToEnum<TEnum>();
        }

        protected TEnum ToEnum<TEnum>(string s)
            where TEnum : struct
        {
            return s.ToEnum<TEnum>();
        }
    }

}
