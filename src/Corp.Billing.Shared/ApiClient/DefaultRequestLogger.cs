﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Corp.Billing.Shared.ApiClient
{
    public class DefaultRequestHelper : IApiClientRequestHelper
    {
        public class LogDetails
        {
            public string Url { get; set; }
            public string Request { get; set; }
            public string Response { get; set; }
            public DateTime CreatedAt { get; set; }
            public TimeSpan Duration { get; set; }
        }

        public Action<LogDetails> OnLog { get; set; }
        public Action<WebHeaderCollection> OnProcessRequestHeaders { get; set; }

        public DefaultRequestHelper()
        {
        }

        public virtual void LogRequest(string url, DateTime createdAt, TimeSpan duration, string request, string response)
        {
            if (this.OnLog != null)
                this.OnLog(new LogDetails() { Url = url, Request = request, Response = response, CreatedAt = createdAt, Duration = duration });
        }

        public void ProcessRequestHeaders(WebHeaderCollection headers)
        {
            if (this.OnProcessRequestHeaders != null)
                this.OnProcessRequestHeaders(headers);
        }
    }
}
