﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.ProvisionIAP.Services;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Provision
{
    public class ProvisionIAPClient : BaseApiClient, IProvisionIAP
    {
        public ProvisionIAPClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public ProvisionIAPResponse ProvisionReceipt(ProvisionIAPRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.Provision.ProvisionIAP;

            url = url
                .Replace("{receiptSource}", req.ReceiptSource.ToString().ToLowerInvariant())
                .Replace("{receiptFlow}", req.ReceiptFlow.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestPostJson(url, req);

            var res = JsonConvert.DeserializeObject<ProvisionIAPResponse>(apiResult);
            return res;
        }
    }
}
