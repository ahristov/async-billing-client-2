﻿using Corp.Billing.Shared.Domain.Orders;

namespace Corp.Billing.Shared.ApiClient.Transaction
{
    public interface ITransactionApiClient
    {
        TransactionDetailsResult GetTransactionDetails(TransactionDetailsRequest request);
    }
}
