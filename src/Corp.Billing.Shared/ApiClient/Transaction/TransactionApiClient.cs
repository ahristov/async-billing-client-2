﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Orders;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Transaction
{
    public class TransactionApiClient : BaseApiClient, ITransactionApiClient
    {
        public TransactionApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public TransactionDetailsResult GetTransactionDetails(TransactionDetailsRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.TransactionDetails + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<TransactionDetailsResult>(apiResult);
            return result;
        }
    }
}
