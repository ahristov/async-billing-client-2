﻿namespace Corp.Billing.Shared.ApiClient.Base
{
    public interface IApiClient
    {
        string ServiceUrl { get; }
    }
}
