﻿using Corp.Billing.Shared.ApiPaths;

namespace Corp.Billing.Shared.ApiClient.Base
{
    public abstract class BaseApiClient : IApiClient
    {
        protected readonly ApiCommand ApiSvc;

        protected BaseApiClient(ApiCommand apiSvc)
        {
            ApiSvc = apiSvc;
        }

        public string ServiceUrl
        {
            get
            {
                return ApiConfiguration.BillingServiceUrl + ApiPathsV1.BasePath + "/";
            }
        }
    }
}
