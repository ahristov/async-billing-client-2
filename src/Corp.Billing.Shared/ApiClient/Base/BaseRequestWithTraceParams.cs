﻿namespace Corp.Billing.Shared.ApiClient.Base
{
    public class BaseRequestWithTraceParams : IHasTraceParams
    {
        public TraceRequestParams.LevelType TraceLevel { get; set; }

        public TraceRequestParams.VerbosityType TraceVerbosity { get; set; }
    }
}
