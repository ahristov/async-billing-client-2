﻿namespace Corp.Billing.Shared.ApiClient.Base
{
    public class TraceRequestParams
    {
        public enum LevelType
        {
            None,
            Error,
            Debug,
        }

        public enum VerbosityType
        {
            Minimal,
            General,
            Verbose,
        }

    }
}
