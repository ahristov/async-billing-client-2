﻿using Corp.Billing.Shared.Domain.Site;
using System.Collections.Generic;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Site
{
    public class SiteFeaturesApiClient : BaseApiClient, ISiteFeaturesService
    {
        public SiteFeaturesApiClient(ApiCommand apiSvc) 
            : base(apiSvc)
        { }


        public IList<SiteFeatures.GetSiteFeaturesItem> GetSiteFeaturesAll()
        {
            string url = ServiceUrl + ApiPathsV1.Site.GetSiteFeaturesAll;
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<List<SiteFeatures.GetSiteFeaturesItem>>(apiResult);
            return result;
        }


        public IList<SiteFeatures.GetSiteFeaturesItem> GetSiteFeatures(SiteFeatures.GetSiteFeaturesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Site.GetSiteFeatures + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<List<SiteFeatures.GetSiteFeaturesItem>>(apiResult);
            return result;
        }

    }
}
