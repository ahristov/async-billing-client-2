﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Fraud;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Fraud
{
    public class FraudApiClient : BaseApiClient, IFraudService
    {
        public FraudApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }


        // Fraud

        public LogFraudEventResult LogFraudEvent(LogFraudEventRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Fraud.LogFraudEvent;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<LogFraudEventResult>(apiResult);
            return result;
        }

        public SubAttributeResult GetFraudProperties(SubAttributeRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Fraud.GetFraudProperties + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<SubAttributeResult>(apiResult);
            return result;
        }
    }
}
