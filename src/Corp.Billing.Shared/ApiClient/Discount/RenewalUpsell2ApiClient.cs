﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.Discount;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Discount
{
    public class RenewalUpsell2ApiClient : BaseApiClient, IRenewalUpsell2ApiClient
    {
        public RenewalUpsell2ApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public RenewalUpsell2Result ApplyRenewalUpsell(RenewalUpsell2Request request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RenewalUpsell2;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<RenewalUpsell2Result>(apiResult);
            return result;
        }
    }
}
