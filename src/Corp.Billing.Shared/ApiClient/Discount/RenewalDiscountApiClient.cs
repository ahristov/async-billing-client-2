﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Discount;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Discount
{
    public class RenewalDiscountApiClient : BaseApiClient, IRenewalDiscountService
    {
        public RenewalDiscountApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }


        // Renewal

        public RenewalDiscountResult GetRenewalDiscountMessaging(RenewalDiscountRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.GetRenewalDiscountMessagingPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RenewalDiscountResult>(apiResult);
            return result;
        }

        public RenewalDiscountResultRNT GetRenewalDiscountMessagingRnt(RenewalDiscountRequestRNT request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.GetRenewalDiscountMessagingRntPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RenewalDiscountResultRNT>(apiResult);
            return result;
        }

        public RedeemRenewalDiscountResult RedeemRenewalDiscount(RedeemRenewalDiscountRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RedeemRenewalDiscountPath;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<RedeemRenewalDiscountResult>(apiResult);
            return result;
        }

        public RedeemRenewalDiscountResultRNT RedeemRenewalDiscountRnt(RedeemRenewalDiscountRequestRNT request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RedeemRenewalDiscountRntPath;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<RedeemRenewalDiscountResultRNT>(apiResult);
            return result;
        }

    }
}
