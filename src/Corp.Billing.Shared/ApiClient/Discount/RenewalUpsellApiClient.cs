﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Discount;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Discount
{
    public class RenewalUpsellApiClient : BaseApiClient, IRenewalUpsellApiClient
    {
        public RenewalUpsellApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public GetRenewalUpsellResult GetRenewalUpsell(GetRenewalUpsellRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RenewalUpsell + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetRenewalUpsellResult>(apiResult);
            return result;
        }

        public ApplyRenewalUpsellResult ApplyRenewalUpsell(ApplyRenewalUpsellRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RenewalUpsell;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<ApplyRenewalUpsellResult>(apiResult);
            return result;
        }
    }
}
