﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Discount;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Discount
{
    public class GuaranteeDiscountApiClient : BaseApiClient, IGuaranteeDiscountService
    {
        public GuaranteeDiscountApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        // Guarantee

        public RedeemGuaranteeResult RedeemGuarantee(RedeemGuaranteeRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Guarante.RedeemGuarantee;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<RedeemGuaranteeResult>(apiResult);
            return result;
        }
    }
}
