﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Discount;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Discount
{
    public class RenewalUpsellV2ApiClient : BaseApiClient, IRenewalUpsellV2ApiClient
    {
        public RenewalUpsellV2ApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public GetRenewalUpsellV2Result GetRenewalUpsell(GetRenewalUpsellV2Request request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RenewalUpsellV2 + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetRenewalUpsellV2Result>(apiResult);
            return result;
        }

        public ApplyRenewalUpsellV2Result ApplyRenewalUpsell(ApplyRenewalUpsellV2Request request)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Renewal.RenewalUpsellV2;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<ApplyRenewalUpsellV2Result>(apiResult);
            return result;

        }


    }
}
