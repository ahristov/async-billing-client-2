﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.VisaCheckout;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.VisaCheckout
{
    public class VisaCheckoutApiClient : BaseApiClient, IVisaCheckoutService
    {
        public VisaCheckoutApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        /// <summary>
        /// Updates the payment information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// 
        /// <example>
        /// Usage example:
        /// <code>
        ///     VisaCheckoutApiClient api = new VisaCheckoutApiClient(new ApiCommand());
        ///     var result = api.UpdatePaymentInfo(new UpdatePaymentInfoRequest()
        ///     {
        ///         CallId = "6946340194196736601",
        ///         CurrencyCode = "USD",
        ///         Total = 12.08,
        ///     });
        /// </code>
        /// </example>
        public UpdatePaymentInfoResponse UpdatePaymentInfo(UpdatePaymentInfoRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.VisaCheckout.Wallet.UpdatePaymentInfo;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<UpdatePaymentInfoResponse>(apiResult);
            return result;
        }

        /// <summary>
        /// Decrypts the payment data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// 
        /// <example>
        /// Usage example:
        /// <code>
        ///     VisaCheckoutApiClient api = new VisaCheckoutApiClient(new ApiCommand());
        ///     var result = api.DecryptPaymentData(new DecryptPaymentDataRequest
        ///     {
        ///         EncKey = "XTCGY55BG29TPkR/9G4YmqDmbhbgMXvpZVZ5zPrOjPa96fw0GDtOaStvAVplnbT2rpKS+nWEhayYtiV85DgZKZaX9p+/A2/r3nm8nQkNbPTRPgtQzt9wMl60BaJFGivz",
        ///         EncPaymentData = "mM5dZn9wnIlOIb284sUFGP2TKTupPa95N1sA3W7fABe4VgUHoBXGXGW+/RGGp75jszzMi6GH39v4sH8MtaAQAvgoYq+DWgio8fs6w4Itf9WX3VfWi1ef/a7XPnAau6a5fz2awy7QvBoMvM7pE5VgMwF4fZEsIfzS0vueKp9kKhZDRAxJKlgkD3WuJyh3d3baBHYlo19kAQEOnyHvp3kX4Iem0gcFGhLOP84ShOh8QOJxOJfCM4aduD9OnOaaCyu6o3PgF7ywV48AQ3TSkKCiDEN/j9KxGKaOM3eIPkFHSeJ/i/nM2PzTjuYIcRS/PyAtfpbYmSCxX+DxApJJQQ07ghQ7NAyqhjtyDEq2gvjtB0sqlTwy5U9+iLcr+1PD0Iq/l1NqF6q/suYOD+c1AUZu3hP6c84F6B7DpTvniJLg+UxiXzSxMCnUs0Zyyf7ySt6si8cEPghXi7J46g9oJ669oWCrod3zPQAvNTtkrcMFaZMLnqSq6KxaBKpy7enF5JbJtUoMd08tNTA97e2j4Oook5+MDzTTrckvgpije7ieDXB+ZS2cSJpKXRE33mEb0qPc/tCuYZfVWB/6yEvSJ6rqTykwNAAn7OhuRwIOQ3RhzhoZiEdMIGlF2dbEY54kjVW/V2425tIHMRJCbAPC2kIb9cgIR4vz2++UKphabTkLRh9kdEVpE4FifMfaDRMI/2OtIIGzNWSzSj1GUMncSB4UMKjONm1EtS8v9scVa3ui6douJuumbWytVJ5J0RKxrFPpWluRFXuwY+ftoEGrUwtijQaaK9tHDVWs2dle5NXGr/eKwYOK6PE51A9coIO3aTSwrPDtgbpFiztGKjrJgPrxftjst9SxQN2uX4FYIXhK4hXk5Qstgt6TH+ZFZYaZ5ccvmSc/uC88Gs1DgaiE38PnyqjGPKDf5QYkuhEMiD1Bs2pE4Xmbr6WBItmiAuIMbpLaXDpZH9byzOlQRrPaDMd2vYcfbhpHY4JyJl0VFaPRuogDdScGSZdOrPaIBpZ3eX79GoO2cTI/v+AAZ4GXuen/1LEX3Yqeb9WE2rXbbHkiARr4tv5c6QDqWyk+NAFXUcuPVA8aBdUkSKXF4cnIyn2zaaj1PHumDcHbtQ0OrOKBY+E0dNYpssRyHMeIyo0Cyz3mMvFZTlkLQIqrzxzswOrJP+aNAMtarWrsTwPAeNwTKCdRAkjXqZR4wvoioyMTT0yl8TjqwHrY4lXEivSdi62xgtVqxtzi9JbX6FrBTUfV6BD2mivDaRO9lewGHYh+AFPsQIbmeZQPZfTN32U6KlDFVUrhYyXh1eIPKIa1jF3h4dAA+wLNGr186dcVZQP1YIdakJI7Tt7pVEzcY8ySDIfsgaAtDklP6dC/IZG+VUO1qZaJjvEImq3r70a82bAxyrDjx+/4NaOm8STC/41rJvBfLLAx5U2l6cPGL9R6Ex1brO1PGIu4GzeaZY2Z9Lp+DQL7y/Nc4IjxMvH/hBT1I6C+YBD8pEx/Mcfvjr344B89bwoom7xHckF3mc2NqYXtv6K0m6mgZ1DcSM3iea5Uc83IGpaR90X1LvsBHjjtR6UezAGiuakueR0GL7PhnLcUde5xGhy7WX12EVM93VXFDSgxkicukiKDfp3ItqlGPGqO9/JEw12YqVKel9AycfPa8/yXd3fDiHSYUEeqRWIGorVtrVG5UHzQnI8YBW/cTW5nXOnhZyVoMHRM2gw1JStqocD8bBEv8PrZ2kxl2ytrql26A8e93dgjv6xVf2/p8NjhkvY=",
        ///     });
        /// </code>
        /// </example>
        public DecryptPaymentDataResponse DecryptPaymentData(DecryptPaymentDataRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.VisaCheckout.Wallet.DecryptPaymentData;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<DecryptPaymentDataResponse>(apiResult);
            return result;
        }
    }
}
