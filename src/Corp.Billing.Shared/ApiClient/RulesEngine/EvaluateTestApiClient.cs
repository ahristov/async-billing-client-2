﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.RulesEngine
{
    public class EvaluateTestApiClient : BaseApiClient, IEvaluateTestService
    {
        public EvaluateTestApiClient(ApiCommand apiSvc)
            : base(apiSvc) { }

        public bool Evaluate(PCRulesRequest request, string testName)
        {
            string url = ServiceUrl + ApiPathsV1.RulesEngine.EvaluateTest.Replace("{testName}", testName) + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);
            var result = JsonConvert.DeserializeObject<bool>(apiResult);
            return result;
        }

        public EvaluateTestVariablesResult EvaluateTestVariables(PCRulesRequest request, string testName)
        {
            string url = ServiceUrl + ApiPathsV1.RulesEngine.EvaluateTestVariables.Replace("{testName}", testName) + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);
            var result = JsonConvert.DeserializeObject<EvaluateTestVariablesResult>(apiResult);
            return result;
        }
    }
}
