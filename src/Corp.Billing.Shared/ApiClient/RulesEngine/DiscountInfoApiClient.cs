﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.DiscountEngine;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.RulesEngine
{
    public class DiscountInfoApiClient : BaseApiClient, IDiscountOfferService
    {
        public DiscountInfoApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public DiscountInfoResult GetDiscountInfo(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.DiscountEngine.DiscountOffer + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<DiscountInfoResult>(apiResult);
            return result;
        }
    }
}
