﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.RateCard;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.RateCard
{
    public class RateCardV2ApiClient : BaseApiClient, IRateCardV2Service
    {
        public RateCardV2ApiClient(ApiCommand apiSvc) 
            : base(apiSvc)
        { }

        public RateCardV2BaseResult GetBaseRateCard(RateCardV2BaseRequest request) // Gets PCRulesRequest
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2BasePath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardV2BaseResult>(apiResult);
            return result;
        }

        public RateCardV2BaseResult GetAddOnsBundleRateCard(RateCardV2AddOnsBundleRequest request) // Gets PCRulesRequest
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2AddOnsBundlePath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardV2BaseResult>(apiResult);
            return result;
        }


        public RateCardV2AddOnsResult GetAddOnsRateCard(RateCardV2AddOnsRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2AddOnsPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardV2AddOnsResult>(apiResult);
            return result;
        }

        public RateCardV2PowerUpsResult GetPowerUpsRateCard(RateCardV2PowerUpsRequest request) // Gets PCRulesRequest
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2PowerUpsPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardV2PowerUpsResult>(apiResult);
            return result;
        }

        public RateCardV2UnitProductsResult GetUnitRateCard(RateCardV2UnitProductsRequest request) // PCRulesRequest
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2UnitPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardV2UnitProductsResult>(apiResult);
            return result;
        }

        public RateCardV2FeatureCompareResult GetFeatureCompare(RateCardV2FeatureCompareRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2FeatureComparePath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardV2FeatureCompareResult>(apiResult);
            return result;
        }
    }
}
