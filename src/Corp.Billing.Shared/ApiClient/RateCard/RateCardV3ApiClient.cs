﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Newtonsoft.Json;
using RcSvc = Corp.Billing.Shared.Domain.RateCardV3.Services;

namespace Corp.Billing.Shared.ApiClient.RateCard
{
    public class RateCardV3ApiClient : BaseApiClient, RcSvc.IRateCardGetter
    {
        public RateCardV3ApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public RcSvc.GetRateCardResponse GetRateCard(RcSvc.GetRateCardRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV3 + "/?" + req.ToQueryString();

            url = url
                .Replace("{catalogType}", req.CatalogTypeParam.ToString().ToLowerInvariant())
                .Replace("{catalogSource}", req.CatalogSourceParam.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<RcSvc.GetRateCardResponse>(apiResult);
            return res;
        }

        public RcSvc.GetExtendedRateCardResponse GetExtendedRateCard(RcSvc.GetRateCardRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetExtendedRateCardV3 + "/?" + req.ToQueryString();

            url = url
                .Replace("{catalogType}", req.CatalogTypeParam.ToString().ToLowerInvariant())
                .Replace("{catalogSource}", req.CatalogSourceParam.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<RcSvc.GetExtendedRateCardResponse>(apiResult);
            return res;
        }
    }
}
