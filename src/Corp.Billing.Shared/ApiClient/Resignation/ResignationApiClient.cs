﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Resignation;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Resignation
{
    public class ResignationApiClient : BaseApiClient, IResignationService
    {
        public ResignationApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        // Resignation

        public ResignStatusResult GetResignStatus(ResignStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Resignation.GetResignStatus + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<ResignStatusResult>(apiResult);
            return result;
        }

        public ResignResult Resign(ResignRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Resignation.Resign;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<ResignResult>(apiResult);
            return result;
        }


        // Free trials

        public FreeTrialCancelResult CancelFreeTrial(FreeTrialCancelRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Resignation.FreeTrial.CancelFreeTrial;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<FreeTrialCancelResult>(apiResult);
            return result;
        }

        public FreeTrialReinstateResult ReinstateFreeTrial(FreeTrialReinstateRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Resignation.FreeTrial.ReactivateFreeTrial;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<FreeTrialReinstateResult>(apiResult);
            return result;
        }

    }
}
