﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.Cardinal;
using Corp.Billing.Shared.ApiClient;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Cardinal
{
    public class CardinalApiClient : BaseApiClient, ICardinalService
    {
        public CardinalApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public CardinalLookUpResult LookUp(CardinalLookUpRequestData request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.CreditCard.CardinalLookUp;
            var apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<CardinalLookUpResult>(apiResult);
            return result;
        }
    }
}
