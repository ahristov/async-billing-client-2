﻿using RequestContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events;
using ResponseContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Response;

namespace Corp.Billing.Shared.ApiClient.PaymentV3
{
    public interface IEventsPaymentApiClient
    {
        ResponseContracts.PaymentResponse ProcessPayment(RequestContracts.PaymentRequest request);
        ResponseContracts.PaymentResponse ProcessPayment(RequestContracts.RequestWithSecuredPaymentToken request);
        ResponseContracts.PaymentResponse ProcessPayment(RequestContracts.RequestWithPaymentMethodToken request);
        ResponseContracts.PaymentResponse ProcessPaymentWithDefaultPaymentMethod(RequestContracts.RequestWithDefaultPaymentMethod request);
    }
}
