﻿using Corp.Billing.Shared.Domain.PaymentV3.Data.Request;
using Corp.Billing.Shared.Domain.PaymentV3.Data.Response;

namespace Corp.Billing.Shared.ApiClient.PaymentV3
{
    public interface IOrderPaymentApiClient
    {
        OrderPaymentResponse ProcessPayment(OrderPaymentRequest request);
    }
}
