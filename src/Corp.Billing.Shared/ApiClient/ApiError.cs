﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace Corp.Billing.Shared.ApiClient
{
    public class ApiError
    {
        public int? Code { get; set; }
        public Guid? ReferenceID { get; set; }

        public string Title { get; set; }
        public string Message { get; set; }

        public Newtonsoft.Json.Linq.JObject Details { get; set; }

        public HttpStatusCode? HttpStatusCode { get; set; }

        [JsonIgnore]
        public Exception RelatedException { get; set; }
    }
}
