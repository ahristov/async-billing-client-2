﻿using System;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.GiftCards;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public class GiftCardPaymentApiClient : BaseApiClient, IGiftCardPaymentService
    {
        public GiftCardPaymentApiClient(ApiCommand apiSvc) : base(apiSvc) { }

        public GiftCardEligibilityResult GetEligibility(int userId)
        {
            string url = $"{ServiceUrl}{ApiPathsV1.GiftCard.GetGiftCardEligibilityPath}/?userId={userId}";

            string apiResult = ApiSvc.WebRequestGet(url);
            return JsonConvert.DeserializeObject<GiftCardEligibilityResult>(apiResult);
        }

        public GiftCardProcessResult ProcessCard(GiftCardPaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.GiftCard.GiftCardProcessPath;

            string apiResult = ApiSvc.WebRequestPostJson(url, request);
            return JsonConvert.DeserializeObject<GiftCardProcessResult>(apiResult);

        }
    }
}
