﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using Corp.Billing.Shared.Domain.Orders;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public class DebitPaymentApiClient : BaseApiClient, IDebitPaymentService
    {
        public const string dateFormat = "MMddyyyy";
        
        public DebitPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public virtual OfflinePaymentResponseMessage Authorize(DebitOfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.Debito.DebitoPayment + "/" + ApiPathsV1.Account.Payment.Debito.DebitoPaymentAuth;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }

        public virtual OfflinePaymentResponseMessage ProcessPayment(OfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.Debito.DebitoPayment + "/" + ApiPathsV1.Account.Payment.Debito.DebitoPaymentProcess;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }

        public virtual SearchForDebitPendingTrxResult GetPendingTransactions(DateTime startAt, DateTime endAt, int bank)
        {
            string parameters = "?" + "startAt=" + startAt.ToString(dateFormat) + "&endAt=" + endAt.ToString(dateFormat) + "&bank=" + bank;
            
            string url = ServiceUrl + ApiPathsV1.Account.Payment.Debito.DebitoPayment + "/" + ApiPathsV1.Account.Payment.Debito.DebitoPendingTransactions + parameters;
            string apiResult = ApiSvc.WebRequestGet(url);
            
            var result = JsonConvert.DeserializeObject<SearchForDebitPendingTrxResult>(apiResult);
            return result;
        }
    }
}
