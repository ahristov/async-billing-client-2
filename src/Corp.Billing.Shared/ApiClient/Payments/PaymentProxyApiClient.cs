﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public class PaymentProxyApiClient : BaseApiClient, IPaymentProxyService
    {
        public PaymentProxyApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public PaymentProxyResponseMessage Authorize(PaymentProxyRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Proxy.AuthorizeProxy;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<PaymentProxyResponseMessage>(apiResult);
            return result;
        }

        public PaymentProxyResponseMessage ProcessPayment(PaymentProxyRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Proxy.PaymentProxy;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<PaymentProxyResponseMessage>(apiResult);
            return result;
        }

        public RefundResponseMessage ProcessRefund(RefundRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Proxy.RefundProxy;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<RefundResponseMessage>(apiResult);
            return result;
        }
    }
}
