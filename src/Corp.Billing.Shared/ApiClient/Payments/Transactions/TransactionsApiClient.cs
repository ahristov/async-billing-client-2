﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.Domain.Payments.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;

namespace Corp.Billing.Shared.ApiClient.Payments.Transactions
{
    public class TransactionsApiClient : BaseApiClient, ITransactionsApiClient
    {
        public TransactionsApiClient()
            : base(new ApiCommand())
        { }

        public IEnumerable<JObject> SearchByProcessorReference(int processorID, string processorReference)
        {
            UriBuilder uri = new UriBuilder(this.ServiceUrl + ApiPathsV1.Payment.Transactions.Search);
            uri.Query = string.Format("p={0}&r={1}", processorID, processorReference);
            string apiResult = ApiSvc.WebRequestGet(uri.ToString());
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<JObject>>(apiResult);
            return result;
        }
    }
}
