﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public class PayPalPaymentApiClient : BaseApiClient, IPayPalPaymentService
    {
        public PayPalPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public PayPalPaymentResponseMessage ProcessPayment(PayPalPaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Proxy.PaymentProxy;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<PayPalPaymentResponseMessage>(apiResult);
            return result;
        }

        public PayPalBillingAgreementTokenResponseMessage CreateBillingAgreementToken(PayPalBillingAgreementTokenRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.PayPal.CreateBillingAgreementToken;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<PayPalBillingAgreementTokenResponseMessage>(apiResult);
            return result;
        }

        public PayPalBillingAgreementResponseMessage CreateBillingAgreement(PayPalBillingAgreementRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.PayPal.CreateBillingAgreement;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<PayPalBillingAgreementResponseMessage>(apiResult);
            return result;
        }
    }
}
