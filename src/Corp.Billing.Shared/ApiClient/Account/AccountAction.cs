﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Account
{
    public enum AccountActionType
    {
        ReRegister = 1,
        PurgeVersion = 2
    }

    public class AccountAction
    {
        public int AccountID { get; set; }
        public AccountActionType ActionType { get; set; }
        public List<int> TargetEncryptionVersions { get; set; }
    }
}
