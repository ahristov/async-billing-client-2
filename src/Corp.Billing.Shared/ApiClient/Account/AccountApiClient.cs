﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Account;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Xml.Linq;
using System.Linq;

namespace Corp.Billing.Shared.ApiClient.Account
{
    public class AccountApiClient : BaseApiClient, IAccountService
    {
        public AccountApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }


        // Payment 

        public Domain.Account.AcctPaymentMethResult GetAccountPaymentMethod(Domain.Account.AcctPaymentMethRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.GetAccountPaymentMethodPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<Domain.Account.AcctPaymentMethResult>(apiResult);
            return result;
        }

        public Domain.Account.GetPaymentMethodResult GetAccountPaymentMethodV2(Domain.Account.GetPaymentMethodRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.GetAccountPaymentMethodV2Path + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<Domain.Account.GetPaymentMethodResult>(apiResult);
            return result;
        }

        public Dictionary<string, string> GetExternalTokens(int accountID)
        {
            string url = ServiceUrl + ApiPathsV1.Account.GetExternalTokensPath.Replace("{id}", accountID.ToString());
            var apiResult = ApiSvc.WebRequestGet(url);
            if (string.IsNullOrEmpty(apiResult))
                throw new Exception("Unable to retreive response from Billing API");
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(apiResult);
            return result;
        }

        public Domain.Account.AcctPaymentMethodUpdateResult UpdateAccountPaymentMethod(Domain.Account.AcctPaymentMethodUpdateRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.UpdateAccountPaymentMethodPath;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<Domain.Account.AcctPaymentMethodUpdateResult>(apiResult);
            return result;
        }

        // UpdateAccountPaymentMethodV2Path
        public Domain.Account.AcctPaymentMethodUpdateResult UpdateAccountPaymentMethodV2(Domain.Account.AcctPaymentMethodUpdateRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.UpdateAccountPaymentMethodV2Path;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<Domain.Account.AcctPaymentMethodUpdateResult>(apiResult);
            return result;
        }

        public UpdatePaymentMethodFirstAndLastNameResult UpdatePaymentMethodFirstAndLastName(UpdatePaymentMethodFirstAndLastNameRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.UpdatePaymentMethodFirstAndLastName;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<UpdatePaymentMethodFirstAndLastNameResult>(apiResult);
            return result;
        }


        public Domain.Account.UserPaymentInfoResult GetUserPaymentInfo(Domain.Account.UserPaymentInfoRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.GetPaymentInfoPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<Domain.Account.UserPaymentInfoResult>(apiResult);
            return result;
        }


        // Lock

        public LockAccountResult LockAccount(LockAccountRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Lock.LockAccountPath;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<LockAccountResult>(apiResult);
            return result;
        }

        public ReleaseAccountLockResult ReleaseAccountLock(ReleaseAccountLockRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Lock.ReleaseAccountLockPath;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<ReleaseAccountLockResult>(apiResult);
            return result;
        }

        public int Register(int accountTypeID, string accountNumber)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Register;

            try
            {
                var request = new
                {
                    accountTypeID,
                    accountNumber
                };

                var response = ApiSvc.WebRequestPostJson(url, request);

                XElement el = XElement.Parse(response);

                var resultValue = el.Elements().Where(e => e.Name.LocalName.Equals("TokenId", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                if (resultValue == null || resultValue.IsEmpty)
                    throw new Exception("Unable to locate AccountID in response");

                var result = Convert.ToInt32(resultValue.Value);

                return result;
            }
            catch (ApiException)
            {
                throw;
            }
        }

        public void ReRegister(int accountID)
        {
            string url = ServiceUrl + ApiPathsV1.Account.ReRegister.Replace("{id}", accountID.ToString());

            try
            {
                ApiSvc.WebRequestPost(url, string.Empty);
            }
            catch (ApiException apiEx)
            {
                if (apiEx.ApiError.Code == 30010)
                {
                    throw new DuplicateAccountApiException(apiEx);
                }

                throw;
            }
        }

        // Purge

        public void PurgeAccount(int accountID, DateTime markedForPurgeAt)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Purge.PurgeAccount + accountID + "?markedForPurgeAt=" + markedForPurgeAt;
            ApiSvc.WebRequestDelete(url);
        }

        public void PurgeAccount(int accountID, int newAccountID, DateTime markedForPurgeAt)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Purge.PurgeAccount + accountID + "?markedForPurgeAt=" + markedForPurgeAt + "&newAccountID=" + newAccountID;
            ApiSvc.WebRequestDelete(url);
        }

        public void PurgeAccountVersion(int accountID, int encryptionVersion)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Purge.PurgeAccount + accountID + "?encryptionVersion=" + encryptionVersion;
            ApiSvc.WebRequestDelete(url);
        }

        public IEnumerable<AccountPurgeItem> GetAccountsPendingPurge(int? batchSize)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Purge.GetPendingAccounts;

            if (batchSize != null && batchSize.HasValue)
            {
                url += "?batchSize=" + batchSize.Value;
            }

            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<AccountPurgeItem[]>(apiResult);

            return result;
        }

        public IEnumerable<AccountAction> GetPendingAccountActions(int? batchSize)
        {
            string url = ServiceUrl + ApiPathsV1.Account.GetPendingActions;

            if (batchSize != null && batchSize.HasValue)
            {
                url += "?batchSize=" + batchSize.Value;
            }

            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<AccountAction[]>(apiResult);

            return result;
        }

        // Bill Credit
        public BilCreditResponse BilCredit(BilCreditRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.AccountBilCredit;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<BilCreditResponse>(apiResult);
            return result;
        }
    }
}
