﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Account
{
    public class DuplicateAccountInfo
    {
        public int RequestedAccountID { get; set; }
        public int FoundAccountID { get; set; }
    }
}
