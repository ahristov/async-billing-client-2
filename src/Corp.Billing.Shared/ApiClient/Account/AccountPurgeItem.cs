﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Account
{
    public class AccountPurgeItem
    {
        public int AccountID { get; set; }
        public DateTime MarkedForPurgeAt { get; set; }
    }
}
