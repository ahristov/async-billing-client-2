﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Account
{
    public class DuplicateAccountApiException : ApiException
    {
        public DuplicateAccountInfo AccountInfo { get; set; }
        public DuplicateAccountApiException(ApiException ex) : base(ex.ApiError)
        {
            try
            {
                this.AccountInfo = this.ApiError.Details.ToObject<DuplicateAccountInfo>();
            }
            catch { }
        }
    }
}
