﻿using System;
using System.Net;

namespace Corp.Billing.Shared.ApiClient.StandardApiHeaders
{
    public static class ApiHeadersSetter
    {
        public static void SetApiHeaders(WebHeaderCollection headers)
        {
            if (ApiConfiguration.StandardHeadersResolver == null)
                return;

            try
            {
                var apiHeaders = ApiConfiguration.StandardHeadersResolver.GetApiHeaderValues();
                if (apiHeaders != null)
                {
                    if (apiHeaders.OriginCodeBaseId.HasValue)
                        headers.Add(ApiHeaderParameterNames.OriginCodeBaseId, apiHeaders.OriginCodeBaseId.ToString());
                    if (apiHeaders.OriginPlatformId.HasValue)
                        headers.Add(ApiHeaderParameterNames.OriginPlatformId, apiHeaders.OriginPlatformId.ToString());
                    if (apiHeaders.SessionId.HasValue)
                        headers.Add(ApiHeaderParameterNames.SessionId, apiHeaders.SessionId.ToString());
                    if (apiHeaders.UserId.HasValue)
                        headers.Add(ApiHeaderParameterNames.UserId, apiHeaders.UserId.ToString());
                    if (!string.IsNullOrWhiteSpace(apiHeaders.UserAgentString))
                        headers.Add(ApiHeaderParameterNames.UserAgentString, apiHeaders.UserAgentString.ToString());
                    if (!string.IsNullOrWhiteSpace(apiHeaders.Community))
                        headers.Add(ApiHeaderParameterNames.Community, apiHeaders.Community.ToString());
                    if (apiHeaders.ReferenceId.HasValue)
                        headers.Add(ApiHeaderParameterNames.ReferenceId, apiHeaders.ReferenceId.ToString());
                }
            }
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debug.WriteLine("Api Headers: Setter Error:" + ex.ToString());
            }
        }
    }
}
