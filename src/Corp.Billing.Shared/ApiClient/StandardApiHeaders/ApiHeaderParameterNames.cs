﻿namespace Corp.Billing.Shared.ApiClient.StandardApiHeaders
{
    public static class ApiHeaderParameterNames
    {
        /// <summary>
        /// Code base ID of the app calling the Billing API
        /// </summary>
        public static string OriginCodeBaseId { get { return "api-origincodebaseid"; } }

        /// <summary>
        /// Legacy Code base ID of the app calling the Billing API
        /// </summary>
        public static string LegacyOriginCodeBaseId { get { return ApiClient.ApiCommand.OriginCodeBaseIdHeaderName; } }

        /// <summary>
        /// Platform ID of the app calling the Billing API
        /// </summary>
        public static string OriginPlatformId { get { return "api-originplatformid"; } }

        /// <summary>
        /// Session ID of the session on the app calling the Billing API
        /// </summary>
        public static string SessionId { get { return "api-sessionid"; } }

        /// <summary>
        /// User ID of the user of the app calling the Billing API
        /// </summary>
        public static string UserId { get { return "api-customer"; } }

        /// <summary>
        /// User agent string from the browser of the user accessing the app calling the Billing API
        /// </summary>
        public static string UserAgentString { get { return "api-useragentstring"; } }

        /// <summary>
        /// Concatenated SiteCode-UrlCode from the session on the app calling the Billing API
        /// </summary>
        public static string Community { get { return "api-community"; } }


        /// <summary>
        /// Reference ID to chain the calls from service to service.
        /// </summary>
        public static string ReferenceId { get { return "api-referenceid";  } }
    }
}
