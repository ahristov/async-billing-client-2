﻿namespace Corp.Billing.Shared.ApiClient.StandardApiHeaders
{
    public class DefaultApiHeaderValuesResolver: IApiHeaderValuesResolver
    {
        public virtual ApiHeaderValues GetApiHeaderValues()
        {
            return new ApiHeaderValues();
        }
    }
}
