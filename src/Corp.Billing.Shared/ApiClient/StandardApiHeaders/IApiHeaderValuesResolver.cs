﻿namespace Corp.Billing.Shared.ApiClient.StandardApiHeaders
{
    public interface IApiHeaderValuesResolver
    {
        ApiHeaderValues GetApiHeaderValues();
    }
}
