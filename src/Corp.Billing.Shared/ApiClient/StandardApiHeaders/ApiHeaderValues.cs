﻿using System;

namespace Corp.Billing.Shared.ApiClient.StandardApiHeaders
{
    public class ApiHeaderValues
    {
        /// <summary>
        /// Code base ID of the app calling the Billing API
        /// </summary>
        public int? OriginCodeBaseId { get; set; }

        /// <summary>
        /// Legacy Code base ID of the app calling the Billing API
        /// </summary>
        public int? LegacyOriginCodeBaseId { get; set; }

        /// <summary>
        /// Platform ID of the app calling the Billing API
        /// </summary>
        public byte? OriginPlatformId { get; set; }

        /// <summary>
        /// Session ID of the session on the app calling the Billing API
        /// </summary>
        public Guid? SessionId { get; set; }

        /// <summary>
        /// User ID of the user of the app calling the Billing API
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// User agent string from the browser of the user accessing the app calling the Billing API
        /// </summary>
        public string UserAgentString { get; set; }

        /// <summary>
        /// Concatenated SiteCode-UrlCode from the session on the app calling the Billing API
        /// </summary>
        public string Community { get; set; }

        /// <summary>
        /// Reference ID to chain the calls from service to service.
        /// </summary>
        public Guid? ReferenceId { get; set; }
    }
}
