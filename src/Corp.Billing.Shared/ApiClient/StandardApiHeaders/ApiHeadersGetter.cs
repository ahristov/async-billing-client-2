﻿using System;
using System.Collections.Specialized;

namespace Corp.Billing.Shared.ApiClient.StandardApiHeaders
{
    public class ApiHeadersGetter
    {
        private readonly NameValueCollection _httpHeaders;
        public ApiHeadersGetter(NameValueCollection httpHeaders)
        {
            if (httpHeaders == null)
                throw new ArgumentNullException(nameof(httpHeaders));

            _httpHeaders = httpHeaders;
        }

        public virtual ApiHeaderValues GetApiHeaders()
        {
            var res = new ApiHeaderValues();

            GetOriginCodeBaseId(res);
            GetLegacyOriginCodeBaseId(res);
            GetOriginPlatformId(res);
            GetSessionId(res);
            GetUserId(res);
            GetUserAgentString(res);
            GetCommunity(res);
            GetReferenceId(res);

            return res;
        }

        private void GetOriginCodeBaseId(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.OriginCodeBaseId, out val))
                return;

            int res;
            if (!int.TryParse(val, out res))
                return;

            data.OriginCodeBaseId = res;
        }

        private void GetLegacyOriginCodeBaseId(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.LegacyOriginCodeBaseId, out val))
                return;

            int res;
            if (!int.TryParse(val, out res))
                return;

            data.LegacyOriginCodeBaseId = res;
        }

        private void GetOriginPlatformId(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.OriginPlatformId, out val))
                return;

            byte res;
            if (!byte.TryParse(val, out res))
                return;

            data.OriginPlatformId = res;
        }

        private void GetSessionId(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.SessionId, out val))
                return;

            Guid res;
            if (!Guid.TryParse(val, out res))
                return;

            data.SessionId = res;
        }

        private void GetUserId(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.UserId, out val))
                return;

            int res;
            if (!int.TryParse(val, out res))
                return;

            data.UserId = res;
        }

        private void GetUserAgentString(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.UserAgentString, out val))
                return;

            data.UserAgentString = val;
        }

        private void GetCommunity(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.Community, out val))
                return;

            data.Community = val;
        }

        private void GetReferenceId(ApiHeaderValues data)
        {
            string val;
            if (!TryGetValue(ApiHeaderParameterNames.ReferenceId, out val))
                return;

            Guid res;
            if (!Guid.TryParse(val, out res))
                return;

            data.ReferenceId = res;
        }

        private bool TryGetValue(string headerName, out string headerValue)
        {
            try
            {
                if (_httpHeaders != null)
                {
                    headerValue = _httpHeaders.Get(headerName);
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debug.WriteLine("Api Headers: Getter Error:" + ex.ToString());
            }

            headerValue = null;
            return false;
        }
    }
}
