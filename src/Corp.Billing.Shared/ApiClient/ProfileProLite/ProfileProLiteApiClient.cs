﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.ProfileProLite;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ApiClient.ProfileProLite
{
    public class ProfileProLiteApiClient : BaseApiClient, IProfileProRedemptionService
    {
        public ProfileProLiteApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public ProfileProLiteRedemptionStatus GetRedemptions(int userId)
        {
            string url = ServiceUrl + ApiPathsV1.ProfileProLite.Redemption + "/?userId=" + userId;
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<ProfileProLiteRedemptionStatus>(apiResult);

            return result;
        }

        public void Redeem(ProfileProLiteRedemption request)
        {
            string url = ServiceUrl + ApiPathsV1.ProfileProLite.Redemption;
            ApiSvc.WebRequestPostJson(url, request);
        }
    }
}
