﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Chargebacks;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Chargebacks
{
    public class AmericanExpressChargebackApiClient : BaseApiClient
    {
        public AmericanExpressChargebackApiClient(ApiCommand apiSvc) : base(apiSvc)
        {
        }

        public AmericanExpressChargebackResult PerformChargeback(AmericanExpressChargebackRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Chargebacks.PerformChargeback;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<AmericanExpressChargebackResult>(apiResult);
            return result;
        }
    }
}
