﻿namespace Corp.Billing.Shared.ApiClient.ApiCommands
{
    public interface IGetKeyCommand
    {
        string GetCurrentKey(string groupName, int id);
        string GetCurrentKey(string groupName);
    }
}
