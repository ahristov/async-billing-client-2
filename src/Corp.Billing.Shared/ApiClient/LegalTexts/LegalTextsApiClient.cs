﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.LegalTexts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.LegalTexts
{
    public class LegalTextsApiClient : BaseApiClient, ILegalTextsGetter
    {

        public LegalTextsApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public LegalTextsResponse GetLegalTexts(LegalTextsRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.Legal.LegalTexts + "/?" + req.ToQueryString();

            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<LegalTextsResponse>(apiResult);
            return result;
        }
    }
}
