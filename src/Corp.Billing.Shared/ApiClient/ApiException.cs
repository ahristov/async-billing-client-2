﻿using System;
using System.Net;

namespace Corp.Billing.Shared.ApiClient
{
    public class ApiException : Exception
    {
        public ApiError ApiError { get; protected set; }

        public ApiException(ApiError error)
            : this(error, null)
        { }

        public ApiException(ApiError error, HttpWebResponse httpRes)
            : base(error.Message, error.RelatedException)
        {
            this.ApiError = error;

            if (httpRes != null) { this.ApiError.HttpStatusCode = httpRes.StatusCode; }
        }

        public static ApiException CreateTimeoutException()
        {
            var res = new ApiException(new ApiError()
            {
                HttpStatusCode = HttpStatusCode.RequestTimeout,
                Message = "Request Timeout",
            });
            return res;
        }
    }
}
