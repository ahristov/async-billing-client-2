﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Subscription;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Subscription
{
    public class UserBillingDataApiClient : BaseApiClient, IUserBillingData
    {
        public UserBillingDataApiClient(ApiCommand apiSvc) 
            : base(apiSvc)
        { }

        public GetUserBillingDataResult GetUserBillingData(GetUserBillingDataRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserBillingData + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetUserBillingDataResult>(apiResult);
            return result;
        }

        public GetCurrentPackageResult GetCurrentPackage(GetCurrentPackageRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetCurrentPackage + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetCurrentPackageResult>(apiResult);
            return result;
        }

    }
}
