﻿using Corp.Billing.Shared.Domain.Subscription;

namespace Corp.Billing.Shared.ApiClient.Subscription
{
    public interface IUserSubscriptionSummaryApiClient
    {
        UserSubscriptionSummaryResult GetSubscriptionSummary(UserSubscriptionSummaryRequest request);
    }
}
