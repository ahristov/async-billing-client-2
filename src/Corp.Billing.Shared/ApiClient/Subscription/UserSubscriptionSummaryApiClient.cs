﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Subscription;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Subscription
{
    public class UserSubscriptionSummaryApiClient : BaseApiClient, IUserSubscriptionSummaryApiClient
    {
        public UserSubscriptionSummaryApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public UserSubscriptionSummaryResult GetSubscriptionSummary(UserSubscriptionSummaryRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetSubscriptionSummary + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserSubscriptionSummaryResult>(apiResult);
            return result;
        }
    }
}
