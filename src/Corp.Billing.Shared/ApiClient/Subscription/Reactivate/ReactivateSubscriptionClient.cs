﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.Subscription.Reactivate.Services;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Subscription.Reactivate
{
    public class ReactivateSubscriptionClient : BaseApiClient, IReactivateSubscriptionClient
    {
        public ReactivateSubscriptionClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public ReactivateSubscriptionResponse ReactivateSubscription(ReactivateSubscriptionRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.ReactivateSubscription;

            string apiResult = ApiSvc.WebRequestPutJson(url, req);

            var res = JsonConvert.DeserializeObject<ReactivateSubscriptionResponse>(apiResult);
            return res;
        }
    }
}
