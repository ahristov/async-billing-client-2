﻿using System.Collections.Generic;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.Subscription;
using Newtonsoft.Json;
using Corp.Billing.Shared.Domain.UserFeatures;

namespace Corp.Billing.Shared.ApiClient.Subscription
{
    public class UserSubscriptionApiClient : BaseApiClient, IUserSubscriptionStatus, IUserSubscriptionStatusV2
    {
        public UserSubscriptionApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public List<DisplayUserStatusResult> GetUserStatusBy(int userId)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/userStatus/?userid=" + userId.ToString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<List<DisplayUserStatusResult>>(apiResult);
            return result;
        }



        public List<DisplayUserStatusResult> GetDisplayStatus(DisplayUserStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetDisplayStatusPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<List<DisplayUserStatusResult>>(apiResult);
            return result;
        }

        public DisplayUserStatusResponse GetDisplayStatusV2(DisplayUserStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetDisplayStatusPathV2 + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<DisplayUserStatusResponse>(apiResult);
            return result;
        }

        public UserPromoInfoResult GetUserStatusPromoInfo(UserPromoInfoRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserPromoInfoPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserPromoInfoResult>(apiResult);
            return result;
        }


        public UserSubStatusResult GetUserSubscritionStatus(UserSubStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserSubStatusPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserSubStatusResult>(apiResult);
            return result;
        }

        public UserSubStatusMessageResult GetUserSubStatusMessage(UserSubStatusMessageRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserSubStatusMessagePath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);
            var result = JsonConvert.DeserializeObject<UserSubStatusMessageResult>(apiResult);

            return result;
        }

        public UserStatusDateResult GetUserStatusDate(UserStatusDateRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserStatusDatePath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserStatusDateResult>(apiResult);
            return result;
        }

        public UserStatusDateResult GetUserStatusDateV1(UserStatusDateRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserStatusDateV1 + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserStatusDateResult>(apiResult);
            return result;
        }

        public UserFeatureResult GetUserFeatures(UserFeatureRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserFeaturesPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserFeatureResult>(apiResult);
            return result;
        }

        public UserFeaturesLightResult GetUserFeaturesLight(UserFeaturesLightRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserFeaturesLightPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UserFeaturesLightResult>(apiResult);
            return result;
        }


        public CancelResult CancelUser(CancelRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.CancelProduct;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<CancelResult>(apiResult);
            return result;
        }

    }
}
