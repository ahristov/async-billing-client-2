﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Files
{
    public class FileListingItem
    {
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
