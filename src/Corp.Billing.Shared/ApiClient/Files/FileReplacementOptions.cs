﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Files
{
    public class ReplacementOptions
    {
        public int? LineLength { get; set; }

        public string LineDelimiter { get; set; }

        public FieldPosition[] FieldPositions { get; set; }

        public ReplacementOptions()
        {
            this.LineLength = null;
            this.LineDelimiter = null;
            this.FieldPositions = null;
        }
    }

    public class FieldPosition
    {
        public FieldPosition(int index, int length)
        {
            Index = index;
            Length = length;
        }

        public int Index { get; set; }
        public int Length { get; set; }
    }

}
