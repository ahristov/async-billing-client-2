﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using Corp.Billing.Shared.Domain.DateCoachStatus.Data;

namespace Corp.Billing.Shared.ApiClient.DateCoach
{
    public interface IDateCoachApiClient
    {
        /// <summary>
        /// Activates the automatic renewal.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        DateCoachRenewalToggleResponse ActivateAutoRenewal(int userId);

        /// <summary>
        /// Deactivates the automatic renewal.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        DateCoachRenewalToggleResponse DeactivateAutoRenewal(int userId);

        /// <summary>
        /// Gets the date coach status.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        DateCoachStatusResponse GetDateCoachStatus(int userId);

        /// <summary>
        /// Toggles the automatic renewal.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        DateCoachRenewalToggleResponse ToggleAutoRenewal(int userId);
    }
}