﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.DateCoach
{
    public class DateCoachApiClient : BaseApiClient, IDateCoachApiClient
    {
        public DateCoachApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        /// <summary>
        /// Gets the date coach status.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>The ate coach status.</returns>
        public virtual DateCoachStatusResponse GetDateCoachStatus(int userId)
        {
            var request = new DateCoachStatusRequest { UserId = userId };

            string url = ServiceUrl + ApiPathsV1.Products.DateCoach.GetDateCoachStatus + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<DateCoachStatusResponse>(apiResult);
            return result;
        }

        /// <summary>
        /// Activates the automatic renewal of the date coach.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>result status of the activation operation incl. the new date coach status.</returns>
        public virtual DateCoachRenewalToggleResponse ActivateAutoRenewal(int userId)
        {
            var res = ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Activate);
            return res;
        }

        /// <summary>
        /// Deactivates the automatic renewal of the date coach.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Result status of the deactivation operation incl. the new date coach status.</returns>
        public virtual DateCoachRenewalToggleResponse DeactivateAutoRenewal(int userId)
        {
            var res = ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Deactivate);
            return res;
        }

        /// <summary>
        /// Toggles the automatic renewal - make it opposite on whatever it currently is.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Result status of the toggle operation incl. the new date coach status.</returns>
        public virtual DateCoachRenewalToggleResponse ToggleAutoRenewal(int userId)
        {
            var res = ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Toggle);
            return res;
        }


        private DateCoachRenewalToggleResponse ToggleAutoRenewalWorker(int userId, DateCoachRenewalToggleRequestType toggleType)
        {
            var request = new DateCoachRenewalToggleRequest { UserId = userId, ToggleType = toggleType };

            string url = ServiceUrl + ApiPathsV1.Products.DateCoach.ToggleDateCoachRenewal;
            url = url
                .Replace("{toggleType}", request.ToggleType.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var res = JsonConvert.DeserializeObject<DateCoachRenewalToggleResponse>(apiResult);
            return res;
        }

    }
}
