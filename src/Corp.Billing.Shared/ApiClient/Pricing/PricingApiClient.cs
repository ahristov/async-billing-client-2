﻿using System;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Pricing;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Pricing
{
    public class PricingApiClient : BaseApiClient, IPricingService
    {
        public PricingApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }


        // Pricing

        public PriceGroupCBSAResult GetPriceGroupCbsa(PriceGroupCBSARequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Pricing.GetPriceGroupCbsaPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PriceGroupCBSAResult>(apiResult);
            return result;
        }

        public GetUserScoreDataResult GetUserPricingScore(GetUserScoreDataRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Pricing.GetUserScoreData + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetUserScoreDataResult>(apiResult);
            return result;
        }

        /// <summary>
        /// Deprecated. Use <see cref="GetUserPricingScore"/> instead.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Obsolete]
        public GetUserScoreDataResult GetUserScoreData(GetUserScoreDataRequest request) { return GetUserPricingScore(request); }

        public BillCurrencyConversionRatesResult GetBillCurrencyConversionRates()
        {
            string url = ServiceUrl + ApiPathsV1.Pricing.GetBillCurrencyConversionRates;
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<BillCurrencyConversionRatesResult>(apiResult);
            return result;
        }
    }
}
