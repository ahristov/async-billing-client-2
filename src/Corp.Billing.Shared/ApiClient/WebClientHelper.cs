﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Corp.Billing.Shared.ApiClient
{
    public static class WebRequestExtensions
    {
        public static HttpWebResponse GetHttpWebResponse(this WebRequest request, out bool wasWebException)
        {
            wasWebException = false;

            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            WebResponse response = null;
            try
            {
                response = request.GetResponse();
            }
            catch (WebException wEx)
            {
                wasWebException = true;

                if (wEx.Status == WebExceptionStatus.Timeout)
                    throw ApiException.CreateTimeoutException();


                response = wEx.Response;
            }
            catch
            {
                response = null;
            }

            return (HttpWebResponse)response;
        }

        private static void CopyStreamTo(MemoryStream source, Stream destination)
        {
            byte[] buffer = new byte[Math.Min(source.Length, 81920)];
            int count;
            while ((count = source.Read(buffer, 0, buffer.Length)) != 0)
                destination.Write(buffer, 0, count);
        }

        public static string ReadResponseAsString(this WebRequest webRequest, MemoryStream requestContent)
        {
            if (webRequest == null)
                throw new ArgumentNullException("webRequest");
            if (webRequest.RequestUri == null)
                throw new ArgumentNullException("webRequest.RequestUri");

            string response = null;
            string requestForLog = string.Empty;
            string responseForLog = null;
            DateTime start = DateTime.Now;
            bool wasWebException = false;

            if (requestContent == null)
                requestContent = new MemoryStream(0);

            using (requestContent)
            {
                string requestBody = null;

                if (requestContent.Length > 0)
                {
                    using (var webStream = webRequest.GetRequestStream())
                    {
                        requestContent.Seek(0, SeekOrigin.Begin);
                        CopyStreamTo(requestContent, webStream);
                    }

                    try
                    {
                        requestContent.Seek(0, SeekOrigin.Begin);
                        using (var reader = new StreamReader(requestContent))
                        {
                            requestBody = reader.ReadToEnd();
                        }
                    }
                    catch { }
                }

                requestForLog = GetRequestHeader(webRequest);
                if (!string.IsNullOrEmpty(requestBody))
                {
                    requestForLog += "\r\n" + requestBody;
                }

                try
                {
                    using (HttpWebResponse webResponse = webRequest.GetHttpWebResponse(out wasWebException))
                    {
                        if (webResponse != null)
                        {
                            responseForLog = GetResponseHeader(webResponse);

                            try
                            {
                                using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                                {
                                    response = reader.ReadToEnd();
                                    responseForLog += "\r\n" + response;
                                }
                            }
                            catch (ProtocolViolationException)
                            {
                                // No Response
                                response = null;
                            }

                            if (wasWebException)
                            {
                                var error = ReadApiError(response);
                                if (error != null)
                                {
                                    throw new ApiException(error, webResponse);
                                }

                                throw new WebException(GetExceptionDebugMessage(webRequest, webResponse));
                            }
                        }
                    }
                }
                finally
                {
                    TryLog(webRequest.RequestUri.ToString(), start, DateTime.Now - start, requestForLog, responseForLog);
                }
            }

            return response;
        }

        private static string GetRequestHeader(WebRequest request)
        {
            if (request == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();
            sb.Append(request.Method);
            sb.Append(" ");
            sb.Append(ProcessEndPointUrl(request.RequestUri.ToString()));
            if (request is HttpWebRequest)
            {
                sb.Append(" HTTP/");
                sb.Append((request as HttpWebRequest).ProtocolVersion.ToString(2));
            }
            sb.AppendLine();

            if (request.Headers != null && request.Headers.Count > 0)
            {
                foreach (var h in request.Headers.AllKeys)
                {
                    sb.Append(h);
                    sb.Append(": ");
                    sb.AppendLine(ProcessEndPointUrl(request.Headers[h]));
                }
            }

            return sb.ToString();
        }
        private static string GetResponseHeader(HttpWebResponse response)
        {
            if (response == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("HTTP/{0} {1} {2}", response.ProtocolVersion.ToString(2), (int)response.StatusCode, response.StatusCode.ToString()));

            if (response.Headers != null && response.Headers.Count > 0)
            {
                foreach (var h in response.Headers.AllKeys)
                {
                    sb.Append(h);
                    sb.Append(": ");
                    sb.AppendLine(ProcessEndPointUrl(response.Headers[h]));
                }
            }

            return sb.ToString();
        }

        private static string ProcessEndPointUrl(string endPointUrl)
        {
            try
            {
                if (!Uri.IsWellFormedUriString(endPointUrl, UriKind.RelativeOrAbsolute))
                    return endPointUrl;

                UriBuilder url = new UriBuilder(endPointUrl);
                if (!string.IsNullOrEmpty(url.Password))
                {
                    url.Password = new string('X', url.Password.Length);
                }

                if (!string.IsNullOrEmpty(url.Query))
                {
                    var queryParams = System.Web.HttpUtility.ParseQueryString(url.Query);
                    foreach (var name in queryParams.AllKeys)
                    {
                        var value = queryParams.Get(name);
                        if (string.IsNullOrWhiteSpace(value))
                            continue;

                        if (value.Contains(":") && Uri.IsWellFormedUriString(value, UriKind.Absolute))
                        {
                            var uri = new UriBuilder(value);
                            if (!string.IsNullOrEmpty(uri.Password))
                            {
                                uri.Password = new string('X', uri.Password.Length);
                            }
                            queryParams.Set(name, uri.Uri.AbsoluteUri);
                        }
                    }
                    var method = queryParams.GetType().GetMethod("ToString", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, new Type[] { typeof(bool) }, null);
                    if (method != null)
                    {
                        var result = method.Invoke(queryParams, new object[] { false });
                        if (result != null)
                        {
                            url.Query = result as string;
                        }
                    }

                    return url.ToString();
                }
            }
            catch { }

            return endPointUrl;
        }

        public static void TryLog(string url, DateTime start, TimeSpan duration, string request, string response)
        {
            try
            {
                if (ApiConfiguration.RequestHelper != null)
                    ApiConfiguration.RequestHelper.LogRequest(url, start, duration, request, response);
            }
            catch { }
        }

        public static void ValidateResponse(this WebRequest webRequest, MemoryStream requestContent)
        {
            string result = webRequest.ReadResponseAsString(requestContent);
        }

        private static string GetExceptionDebugMessage(WebRequest webRequest, HttpWebResponse webResponse)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("The remote server returned an error.");

            if (webRequest != null && webRequest.RequestUri != null)
                sb.AppendFormat("The remote server returned an error. Request Uri: {0}", webRequest.RequestUri.AbsoluteUri);

            if (webResponse != null)
                sb.AppendFormat("HTTP {0}: {1}", webResponse.StatusCode, webResponse.StatusDescription);

            return sb.ToString();
        }

        public static ApiException GetApiException(this WebException exception)
        {
            if (exception == null)
                return null;

            var webResponse = exception.Response;
            ApiError error = ReadApiError(webResponse);
            if (error != null)
            {
                error.RelatedException = exception;
                return new ApiException(error, webResponse as HttpWebResponse);
            }

            return null;
        }

        public static ApiError ReadApiError(WebResponse webResponse)
        {
            try
            {
                if (webResponse != null)
                {
                    return ReadApiError(webResponse.GetResponseStream());
                }
            }
            catch { }

            return null;
        }

        public static ApiError ReadApiError(Stream responseStream)
        {
            try
            {
                using (var responseReader = new StreamReader(responseStream))
                {
                    return ReadApiError(responseReader.ReadToEnd());
                }
            }
            catch { }

            return null;
        }

        public static ApiError ReadApiError(string response)
        {
            try
            {
                var error = JsonConvert.DeserializeObject<ApiError>(response);
                if (error != null)
                {
                    return error;
                }
            }
            catch { }

            return null;
        }

        public static void ThrowIfApiError(this WebException exception)
        {
            ApiException apiEx = exception.GetApiException();
            if (apiEx != null)
                throw apiEx;
        }

    }
}
