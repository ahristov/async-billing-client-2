﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.SaveOffers.Data;
using Corp.Billing.Shared.Domain.SaveOffers.Services;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.SaveOffers
{
    public class SaveOffersApiClient : BaseApiClient, ISaveOfferService
    {
        public SaveOffersApiClient(ApiCommand apiSvc) 
            : base(apiSvc)
        { }

        public GetSaveOfferEligibilityResult GetSaveOfferEligibility(GetSaveOfferEligibilityRequest request, SaveOfferFlowType saveOfferFlow)
        {
            string url = ServiceUrl + ApiPathsV1.Discount.Offer.SaveOffer + "/?" + request.ToQueryString();

            url = url.Replace("{saveOfferFlow}", saveOfferFlow.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<GetSaveOfferEligibilityResult>(apiResult);
            return res;
        }
    }
}
