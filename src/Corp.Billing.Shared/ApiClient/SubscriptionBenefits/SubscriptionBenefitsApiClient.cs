﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ApiClient.SubscriptionBenefits
{
    public class SubscriptionBenefitsApiClient : BaseApiClient, ISubscriptionBenefitsApiClient
    {
        public SubscriptionBenefitsApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public BenefitsCountNewResponse GetCountOfNewSubscriptionBenefits(BenefitsStatusRequest req)
        {
            return GetCountOfNewSubscriptionBenefits(req, null);
        }

        public BenefitStatusResponse GetSubscriptionBenefitsStatus(BenefitsStatusRequest req)
        {
            return GetSubscriptionBenefitsStatus(req, null);
        }

        public BenefitsCountNewResponse GetCountOfNewSubscriptionBenefits(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.SubscriptionBenefitsCountNew + "/?" + req.ToQueryString();

            string aftp = ""+GetAcceptedFeatureTypesParam(acceptedFeatureTypes);
            if (!string.IsNullOrWhiteSpace(aftp))
            {
                url = $"{url}&{aftp}";
            }

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<BenefitsCountNewResponse>(apiResult);
            return res;
        }

        public BenefitStatusResponse GetSubscriptionBenefitsStatus(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.SubscriptionBenefits + "/?" + req.ToQueryString();

            string aftp = "" + GetAcceptedFeatureTypesParam(acceptedFeatureTypes);
            if (!string.IsNullOrWhiteSpace(aftp))
            {
                url = $"{url}&{aftp}";
            }

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<BenefitStatusResponse>(apiResult);
            return res;
        }

        private static string GetAcceptedFeatureTypesParam(ICollection<int> acceptedFeatureTypes)
        {
            if (acceptedFeatureTypes == null)
                return string.Empty;

            if (acceptedFeatureTypes.Count < 1)
                return string.Empty;

            var paramName = "acceptedFeatureTypes";
            var paramValue = JsonConvert.SerializeObject(acceptedFeatureTypes);

            var res = paramName.GetNameValueQueryParameterPairs(paramValue);
            return res;
        }

    }
}
