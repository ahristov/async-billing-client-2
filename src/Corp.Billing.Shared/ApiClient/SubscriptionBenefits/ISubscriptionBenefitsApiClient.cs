﻿using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ApiClient.SubscriptionBenefits
{
    public interface ISubscriptionBenefitsApiClient
    {
        BenefitsCountNewResponse GetCountOfNewSubscriptionBenefits(BenefitsStatusRequest req);
        BenefitStatusResponse GetSubscriptionBenefitsStatus(BenefitsStatusRequest req);
        BenefitsCountNewResponse GetCountOfNewSubscriptionBenefits(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes);
        BenefitStatusResponse GetSubscriptionBenefitsStatus(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes);
    }
}
