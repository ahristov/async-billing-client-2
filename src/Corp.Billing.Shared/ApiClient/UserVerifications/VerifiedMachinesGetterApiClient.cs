﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.UserVerifications;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.UserVerifications
{
    public class VerifiedMachinesGetterApiClient : BaseApiClient, IVerifiedMachinesGetterApiClient
    {
        public VerifiedMachinesGetterApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public GetVerifiedMachinesResult GetVerifiedMachines(GetVerifiedMachinesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.UserVerifications.VerifiedMachines + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetVerifiedMachinesResult>(apiResult);
            return result;
        }
    }
}
