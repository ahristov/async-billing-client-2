﻿using Corp.Billing.Shared.Domain.UserVerifications;

namespace Corp.Billing.Shared.ApiClient.UserVerifications
{
    public interface IVerifiedMachinesGetterApiClient
    {
        GetVerifiedMachinesResult GetVerifiedMachines(GetVerifiedMachinesRequest req);
    }
}
