﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Products.PowerUps;
using Corp.Billing.Shared.Domain.Products.PowerUps.Data;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Products
{
    public class PowerUpsPurchaseEligibilityApiClient : BaseApiClient, IPowerUpsPurchaseEligibilityService
    {
        public PowerUpsPurchaseEligibilityApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public PowerUpsPurchaseEligibilityResponse GetPowerUpsPurchaseEligibility(PowerUpsPurchaseEligibilityRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.Products.PowerUps.PowerUpsPurchaseEligibility + "/?" + req.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<PowerUpsPurchaseEligibilityResponse>(apiResult);
            return res;
        }
    }
}
