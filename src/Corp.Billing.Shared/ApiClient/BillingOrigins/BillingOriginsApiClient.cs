﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.BillingOrigins.Data;
using Newtonsoft.Json;
using System;

namespace Corp.Billing.Shared.ApiClient.BillingOrigins
{
    public class BillingOriginsApiClient : BaseApiClient, IBillingOriginsApiClient
    {
        public BillingOriginsApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public void SetBillingOrigins(SetBillingOriginsRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.BillingOrigins.BillingOriginsPath;
            ApiSvc.WebRequestPostJson(url, req);
        }

        public BillingOriginsData GetBillingOriginData(int userId, Guid sessionId)
        {
            string url = ServiceUrl + ApiPathsV1.BillingOrigins.BillingOriginsPathWithSession + "/?userId=" + userId;
            url = url.Replace("{sessionId}", sessionId.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<BillingOriginsData>(apiResult);
            return res;
        }
    }
}
