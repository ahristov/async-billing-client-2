﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;

namespace Corp.Billing.Shared.Manage
{
    public class RedisCacheApiClient : BaseApiClient, IRedisCacheApiClient
    {
        public RedisCacheApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public void DeleteCache(int userId)
        {
            string url = ServiceUrl + ApiPathsV1.Manage.RedisCacheAllUserData + $"/?userId={userId}";
            ApiSvc.WebRequestDelete(url);
            return;
        }
    }
}
