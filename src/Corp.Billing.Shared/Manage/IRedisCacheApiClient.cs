﻿namespace Corp.Billing.Shared.Manage
{
    public interface IRedisCacheApiClient
    {
        void DeleteCache(int userId);
    }
}
