﻿using Corp.Billing.Shared.Domain.Products;
using System;

namespace Corp.Billing.Shared.Domain.RateCard
{
    public static class RateCardExtensions
    {
        public static long FeatureToBundleBitMask(this byte luProdFeatureId)
        {
            return (long) Math.Pow(2, luProdFeatureId - 1);
        }

        public static bool BundleBitMaskIncludesFeature(this long bundleMask, byte luProdFeatureId)
        {
            return ((long) Math.Pow(2, luProdFeatureId - 1) & bundleMask) > 0;
        }
        public static bool BundleBitMaskIncludesFeature(this long bundleMask, ProductFeatureType prodFeature)
        {
            return ((long)Math.Pow(2, (int)prodFeature - 1) & bundleMask) > 0;
        }
    }
}
