﻿namespace Corp.Billing.Shared.Domain.RateCard
{
    public interface IRateCardV2Service
    {
        RateCardV2BaseResult GetBaseRateCard(RateCardV2BaseRequest request);
        RateCardV2BaseResult GetAddOnsBundleRateCard(RateCardV2AddOnsBundleRequest request);

        RateCardV2AddOnsResult GetAddOnsRateCard(RateCardV2AddOnsRequest request);
        RateCardV2PowerUpsResult GetPowerUpsRateCard(RateCardV2PowerUpsRequest request);
        RateCardV2UnitProductsResult GetUnitRateCard(RateCardV2UnitProductsRequest request);

        RateCardV2FeatureCompareResult GetFeatureCompare(RateCardV2FeatureCompareRequest request);
        
    }
}
