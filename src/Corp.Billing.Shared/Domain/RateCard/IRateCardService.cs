﻿namespace Corp.Billing.Shared.Domain.RateCard
{
    public interface IRateCardService
    {
        // Rate card

        RateCardBaseResult GetBaseRateCard(RateCardBaseRequest request);
        RateCardAddOnResult GetAddOnRateCard(RateCardAddOnRequest request);
        UnitRateCardResult GetUnitRateCard(UnitRateCardRequest request);
    }
}
