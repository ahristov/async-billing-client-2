﻿namespace Corp.Billing.Shared.Domain.RateCard.Services
{
    public interface IRateCardKeyGetterCacheOnly : IRateCardKeyGetter
    { }
}
