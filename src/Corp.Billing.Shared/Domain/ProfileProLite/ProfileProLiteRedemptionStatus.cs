﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProfileProLite
{
    [Serializable]
    public class ProfileProLiteRedemptionStatus
    {
        public bool CanRedeem { get; set; }

        public ICollection<ProfileProLiteRedemption> Redemptions { get; set; }

        public ProfileProLiteRedemptionStatus()
        {
            Redemptions = new List<ProfileProLiteRedemption>();
        }
    }
}
