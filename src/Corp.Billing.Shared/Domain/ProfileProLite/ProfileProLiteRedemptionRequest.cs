﻿namespace Corp.Billing.Shared.Domain.ProfileProLite
{
    public class GetProfileProLiteRedemptionRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }
}
