﻿using Newtonsoft.Json;
using System;

namespace Corp.Billing.Shared.Domain.ProfileProLite
{
    public class ProfileProLiteRedemption
    {
        [JsonProperty(Order = 1)]
        public int UserId { get; set; }
        [JsonProperty(Order = 2)]
        public DateTime RedemptionDate { get; set; }
        [JsonProperty(Order = 3)]
        public int PlatformId { get; set; }
        [JsonProperty(Order = 4)]
        public Guid Sid { get; set; }
    }
}
