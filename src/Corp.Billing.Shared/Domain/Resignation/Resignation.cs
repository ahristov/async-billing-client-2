﻿using System;

namespace Corp.Billing.Shared.Domain.Resignation
{
    public class FreeTrialCancelRequest
    {
        public int UserID { get; set; }
    }

    public class FreeTrialCancelResult
    {
        public int ReturnValue { get; set; }
    }

    public class FreeTrialReinstateRequest
    {
        public int UserID { get; set; }
    }

    public class FreeTrialReinstateResult
    {
        public int ReturnValue { get; set; }
    }

    public class ResignStatusRequest : IApiRequestWithQueryString
    {
        public int UserID { get; set; }
    }

    public class ResignStatusResult
    {
        public int ReturnValue { get; set; }
        public bool CanResign { get; set; }
        public bool CanBeRefunded { get; set; }
        public string BaseCode { get; set; }
        public byte StateCode { get; set; }
        public short RenewalCount { get; set; }
        public long ProdAttributeMask { get; set; }
        public int AccountDetailID { get; set; }
        public short ProdDetailID { get; set; }
    }

    [Serializable]
    public class ResignRequest
    {
        public short? StateCode { get; set; }
        public int UserId { get; set; }
        public string Handle { get; set; }
        public int? AcctDtlId { get; set; }
        public DateTime? BeginDt { get; set; }
        public string PymtType { get; set; }
        public string ResignType { get; set; }
        public bool? Refund { get; set; }
        public string AnswerIDs { get; set; }
        public string AnswerText { get; set; }
        public string AnswerOther { get; set; }
        public int? EmployeeId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string StateAbbreviation { get; set; }
        public short? CountryCode { get; set; }
        public string PostalCode { get; set; }
        public short? ResignCode { get; set; }
        public int? Return { get; set; }
        public string ConfirmationId { get; set; }
        public string CSAUserComment { get; set; }
        public string Msg { get; set; }
        public int? LuCancelTypeId { get; set; }
        public short? NetPromoterResponse { get; set; }
        public int? SurveyPageCode { get; set; }
        public string HandleMet { get; set; }
    }

    [Serializable]
    public class ResignResult
    {
        public short ResignCode { get; set; }
        public int Return { get; set; }
        public string ConfirmationId { get; set; }
        public string Msg { get; set; }
    }
}
