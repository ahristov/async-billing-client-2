﻿namespace Corp.Billing.Shared.Domain.Resignation
{
    public interface IResignationService
    {
        // Resignation

        ResignStatusResult GetResignStatus(ResignStatusRequest request);
        ResignResult Resign(ResignRequest request);

        // Free trials

        FreeTrialCancelResult CancelFreeTrial(FreeTrialCancelRequest request);
        FreeTrialReinstateResult ReinstateFreeTrial(FreeTrialReinstateRequest request);
        
    }
}
