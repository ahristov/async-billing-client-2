﻿using Corp.Billing.Shared.Domain.Promos;

namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    public class GetRateCardResponse
    {
        public Data.RateCard RateCard { get; set; }
        public Data.CatalogType CatalogType { get; set; }
        public Data.CatalogSource CatalogSource { get; set; }

        public GetRateCardResponse()
        {
            RateCard = new Data.RateCard();
        }
    }
}
