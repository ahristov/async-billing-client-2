﻿using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.RateCard;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    [Serializable]
    public class GetProdTokensToSellableUnitsResponse: GetProdTokensToSellableResponseBase
    {
        public IList<RateCardV2UnitProductsItem> UnitProducts { get; set; }

        public GetProdTokensToSellableUnitsResponse()
            : base()
        {
            UnitProducts = new List<RateCardV2UnitProductsItem>();
        }
    }
}
