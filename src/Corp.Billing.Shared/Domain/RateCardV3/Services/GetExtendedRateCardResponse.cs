﻿using Corp.Billing.Shared.Domain.Promos;

namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    public class GetExtendedRateCardResponse
    {
        public Data.ExtendedRateCard RateCard { get; set; }
        public GetPromoDataResultV2 PromoData { get; set; }
        
        public int SubUserState { get; set; }
        public bool HasPhotoOnFirstRCView { get; set; }

        public byte? OriginPlatformId { get; set; }
        public string OriginUserAgentString { get; set; }
        public int? OriginAreaId { get; set; }

        public Data.CatalogType CatalogType { get; set; }
        public Data.CatalogSource CatalogSource { get; set; }

        public GetExtendedRateCardResponse()
        {
            RateCard = new Data.ExtendedRateCard();
            PromoData = new GetPromoDataResultV2();
        }
    }
}
