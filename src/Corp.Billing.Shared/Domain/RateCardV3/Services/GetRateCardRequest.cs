﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Corp.Billing.Shared.Domain.RateCard;

namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    public class GetRateCardRequest : PCRulesRequest
    {
        public int? RateCardKey { get; set; }
        private string _promoId;
        public new string PromoId
        {
            get
            {
                return _promoId;
            }
            set
            { 
                _promoId = value;
                base.PromoId = value;
            }
        }
        public int? RateCardOverrideKey { get; set; }
        public int? OtherUserId { get; set; }
        public Data.RateCardType? RateCardType { get; set; }
        public Products.ProductFeatureType? PrimaryFeature { get; set; }
        public bool ApplyPromoToUnitProducts { get; set; }

        private byte? _monthsToCompare;
        public byte MonthsToCompare
        {
            get { return _monthsToCompare.NormalizeMonthValue(); }
            set { _monthsToCompare = value; }
        }

        public GetRateCardRequest()
        {
            MonthsToCompare = 1;
        }
    }
}
