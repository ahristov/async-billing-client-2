﻿using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.RateCard;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    [Serializable]
    public class GetProdTokensToSellableResponse: GetProdTokensToSellableResponseBase
    {
        public IList<RateCardV2AddOnsItem> AddOnProducts { get; set; }
        public bool IsMembershipUpgrade { get; set; }

        public GetProdTokensToSellableResponse()
            : base()
        {
            AddOnProducts = new List<RateCardV2AddOnsItem>();
        }
    }
}
