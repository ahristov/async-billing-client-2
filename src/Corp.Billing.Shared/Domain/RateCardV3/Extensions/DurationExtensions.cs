﻿using Corp.Billing.Shared.Domain.Products;

namespace Corp.Billing.Shared.Domain.RateCardV3.Extensions
{
    public static class DurationExtensions
    {
        public static Duration CreateDuration(this DurationUnitType unit, decimal length)
        {
            return new Duration
            {
                Unit = unit,
                Length = length,
            };
        }
    }
}
