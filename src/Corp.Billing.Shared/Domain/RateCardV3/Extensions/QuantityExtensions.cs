﻿using Corp.Billing.Shared.Domain.Products;

namespace Corp.Billing.Shared.Domain.RateCardV3.Extensions
{
    public static class QuantityExtensions
    {
        public static Quantity CreateQuantity(this QuantityUnitType unit, decimal amount)
        {
            return new Quantity
            {
                Unit = unit,
                Amount = amount,
            };
        }
    }
}
