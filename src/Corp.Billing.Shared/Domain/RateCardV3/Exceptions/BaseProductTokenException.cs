﻿using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.RateCardV3.Exceptions
{
    public abstract class BaseProductTokenException : Exception
    {
        public BaseProductTokenException(int userId, string productToken)
        {
            UserId = userId;
            ProductToken = productToken;
        }

        public int UserId { get; set; }
        public string ProductToken { get; set; }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
