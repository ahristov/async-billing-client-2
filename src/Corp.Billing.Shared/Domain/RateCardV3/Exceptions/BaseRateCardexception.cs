﻿using Corp.Billing.Shared.Domain.RateCardV3.Services;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.RateCardV3.Exceptions
{
    public abstract class BaseRateCardexception : Exception
    {
        public BaseRateCardexception(GetRateCardRequest req)
        {
            Request = req;
        }

        public GetRateCardRequest Request { get; set; }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }

        public static string DbgString(GetRateCardRequest req)
        {
            return string.Format("userId={0},catalogType={1},catalogSource={2},platformId={3},sessionId={4}; ratecardKey={5}",
                req.UserId,
                req.CatalogTypeParam,
                req.CatalogSourceParam,
                req.PlatformId,
                req.SessionId,
                req.RateCardKey);
        }
    }
}