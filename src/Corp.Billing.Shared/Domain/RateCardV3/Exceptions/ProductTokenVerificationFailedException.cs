﻿using Corp.Billing.Shared.Facilities;
using System.Net;

namespace Corp.Billing.Shared.Domain.RateCardV3.Exceptions
{
    public class ProductTokenVerificationFailedException: BaseProductTokenException
    {
        public ProductTokenVerificationFailedException(int userId, string productToken)
            : base(userId, productToken)
        {
            this.AddData(new
            {
                userId,
                productToken,
            });
        }

        public override HttpStatusCode HttpStatusCode { get { return HttpStatusCode.BadRequest; } }

        public override int ErrorCode => (int)ExceptionErrorCodes.ProductTokenVerificationFailed;
        public override string ErrorMessage => ErrorMessages.PRODUCT_TOKEN_VERIFICATION_FAILED;
    }
}
