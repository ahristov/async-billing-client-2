﻿namespace Corp.Billing.Shared.Domain.RateCardV3.Exceptions
{
    public static class ErrorMessages
    {
        public const string UNABLE_TO_RESOLVE_RATECARD_KEY = "Unable to resolve rate card key";
        public const string UNABLE_TO_LOAD_RATECARD_XML = "Unable to load rate card xml";
        public const string UNABLE_TO_RESOLVE_RATECARD_OVERRIDE_KEY = "Unable to resolve rate card override key";
        public const string UNABLE_TO_LOAD_RATECARD_OVERRIDE_XML = "Unable tp load rate card override xml";
        public const string PRODUCT_TOKEN_EXPIRED = "Product token has expired";
        public const string PRODUCT_TOKEN_VERIFICATION_FAILED = "Product token verification failed";
    }
    


    public enum ExceptionErrorCodes : int
    {
        Undefined = 0,
        UnableToResolveRateCardKey = 1,
        DidNotLoadRateCardXml = 2,
        UnableToResolveRateCardOverrideKey = 3,
        DidNotLoadRateCardOverrideXml = 4,
        ProductTokenExpired = 1005,
        ProductTokenVerificationFailed = 1006,
    }
}
