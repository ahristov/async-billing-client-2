﻿using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class RateCardTypeData: ICloneable
    {
        public int RateCardType { get; set; }
        public string RateCardTypeName => ((RateCardType)RateCardType).ToString();

        public RateCardTypeData()
        { }

        public RateCardTypeData(RateCardType rateCardType)
        {
            RateCardType = (int)rateCardType;
        }

        public RateCardTypeData Clone()
        {
            return this.MemberwiseClone() as RateCardTypeData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
