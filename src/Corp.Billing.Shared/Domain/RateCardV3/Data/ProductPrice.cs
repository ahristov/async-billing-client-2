﻿using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class ProductPrice : ICloneable
    {
        public decimal PreDiscountedPrice { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        
        public decimal SalesPrice { get; set; }

        public decimal TaxRate { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalCost { get; set; }

        public ProductPrice Clone()
        {
            return this.MemberwiseClone() as ProductPrice;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
