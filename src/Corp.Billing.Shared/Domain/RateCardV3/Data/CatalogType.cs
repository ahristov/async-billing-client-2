﻿namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public enum CatalogType : int
    {
        Default = 0,
        InApp = 1,
    }
}
