﻿namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public enum CatalogSource : int
    {
        Default = 0,
        Apple = 1,
    }
}
