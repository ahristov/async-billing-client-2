﻿using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class SavingsToOneMonth: ICloneable
    {
        public decimal MonthlyPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PercentageSavings { get; set; }
        public SavingsToOneMonth Clone()
        {
            return this.MemberwiseClone() as SavingsToOneMonth;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
