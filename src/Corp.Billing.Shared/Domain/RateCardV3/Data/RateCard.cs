﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public class RateCard
    {
        public IList<Product> Products { get; set; }
        public int? RateCardKey { get; set; }
        public Guid RateCardRefId { get; set; }

        public RateCard()
        {
            Products = new List<Product>();
            RateCardRefId = Guid.NewGuid();
        }
    }
}
