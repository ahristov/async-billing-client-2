﻿using Corp.Billing.Shared.Domain.RateCard;
using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class BundleInformation: ICloneable
    {
        public byte BundleType { get; set; }
        public string BundleTypeName => ((BundleType)this.BundleType).ToString();

        public int? BundleId { get; set; }

        public long BundleMask { get; set; }

        public BundleInformation Clone()
        {
            return this.MemberwiseClone() as BundleInformation;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
