﻿namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public enum RateCardType : int
    {
        Undefined = 0,
        Standard = 1,
        Upgrade = 2,
        Upsell = 3,
        PowerUp = 4,
        Unit = 5,
        UnitRenewal = 6,
        RushHour = 7,
    }
}
