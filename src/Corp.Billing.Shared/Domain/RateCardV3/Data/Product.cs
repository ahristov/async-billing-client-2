﻿using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.RateCard;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public class Product
    {
        public string ExternalProductId { get; set; }
        public ProductFeatureType PrimaryFeature { get; set; }
        public IList<ProductFeatureType> Features { get; set; }
        public Duration Duration { get; set; }
        public Quantity Quantity { get; set; }
        public IList<RateCardType> CanPurchaseAs { get; set; }
        public BundleType BundleType { get; set; }

        public Product()
        {
            Features = new List<ProductFeatureType>();
            Duration = new Duration();
            Quantity = new Quantity();
            CanPurchaseAs = new List<RateCardType>();
            BundleType = BundleType.None;
        }
    }
}
