﻿using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class CurrentPackagePriceData : ICloneable
    {
        public decimal Price { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal PriceWithTax { get; set; }

        public CurrentPackagePriceData Clone()
        {
            return this.MemberwiseClone() as CurrentPackagePriceData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
