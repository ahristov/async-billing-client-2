﻿using Corp.Billing.Shared.Domain.Installlments;
using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Facilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    /// <summary>
    /// TODO: 
    /// - RateCardV2FeatureCompare
    /// - RateCardV2FeatureCompare vs SavingsToNonDiscountedOneMonth?
    /// - Promo information
    /// - Legal texts 
    /// </summary>
    /// <seealso cref="Corp.Billing.Shared.Domain.RateCardV3.Data.Product" />
    [Serializable]
    public class ExtendedProduct : ICloneable
    {
        public string EntityId { get; set; }
        public string ParentEntityId { get; set; }

        public int ProductId { get; set; }

        public short? FreeDays { get; set; }
        public DateTime BeginDt { get; set; }
        public short? InitialDays { get; set; }
        public short? RenewalDays { get; set; }

        public ProductPrice InitialPrice { get; set; }
        public ProductPrice RenewalPrice { get; set; }

        public ProductPrice InitialPricePerPeriod { get; set; }
        public ProductPrice RenewalPricePerPeriod { get; set; }

        public ProductPrice InitialPricePerUnit{ get; set; }
        public ProductPrice RenewalPricePerUnit { get; set; }

        public string ISOCurrencyCode { get; set; }
        public decimal TaxRate { get; set; }

        public BundleInformation BundleInformation { get; set; }

        /// <summary>
        /// Gets or sets the savings to one month non discounted.
        /// </summary>
        /// <remarks>
        /// Applies to: base (more than one months)
        /// </remarks>
        /// <value>
        /// The savings to one month non discounted.
        /// </value>
        public SavingsToOneMonth SavingsToNonDiscountedOneMonth { get; set; }

        public IList<ProductSavingsData> ProductSavings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is best value.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is best value; otherwise, <c>false</c>.
        /// </value>
        public bool IsBestValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this products is a guarantee product.
        /// </summary>
        /// <remarks>
        /// Applies to: base, addon.
        /// </remarks>
        /// <value>
        ///   <c>true</c> if this product is a guarantee product; otherwise, <c>false</c>.
        /// </value>
        public bool IsGuarantee { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this product is part of a bundle.
        /// </summary>
        /// <remarks>
        /// Applies to: base, addon.
        /// </remarks>
        /// <value>
        ///   <c>true</c> if this product is part of a bundle; otherwise, <c>false</c>.
        /// </value>
        public bool IsBundled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is a non-bundled base package and if is is selected, 
        /// an addons page has to be presented to the user to individually add addon products to the purchase.
        /// </summary>
        /// <remarks>
        /// Appplies to: base (non-bindled only base package).
        /// </remarks>
        /// <value>
        ///   <c>true</c> if addons page has to be shown upon selection of this base non-bundled package; otherwise, <c>false</c>.
        /// </value>
        public bool ShowAddOnPage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this product is a free trial.
        /// </summary>
        /// <remarks>
        /// Applies to: base (free trial base product)
        /// </remarks>
        /// <value>
        ///   <c>true</c> if this product is free trial; otherwise, <c>false</c>.
        /// </value>
        public bool IsFreeTrial { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this product is a delay capture. Only applies to add-ons.
        /// </summary>
        /// <remarks>
        /// Applies to: base. addon.
        /// </remarks>
        /// <value>
        ///   <c>true</c> if this product is delay capture; otherwise, <c>false</c>.
        /// </value>
        public bool IsDelayCapture { get; set; }

        public string ExternalProductId { get; set; }
        public byte PrimaryFeature { get; set; }
        public string PrimaryFeatureName => ((ProductFeatureType)this.PrimaryFeature).ToString();
        public IList<ProductFeatureData> Features { get; set; }
        public DurationData Duration { get; set; }
        public DurationData RenewalDuration { get; set; }
        public QuantityData Quantity { get; set; }
        public IList<RateCardTypeData> CanPurchaseAs { get; set; }

        public bool IsMembershipUpgradeProduct => CanPurchaseAs != null && CanPurchaseAs.Count == 1 && CanPurchaseAs.Any(x => x.RateCardType == (int)RateCardType.Upgrade);

        public RefundAmountData RefundAmount { get; set; }
        public DurationData RefundDuration { get; set; }
        public CurrentPackagePriceData CurrentPackagePrice { get; set; }
        public CurrentPackagePriceData CurrentPackagePricePerPeriod { get; set; }
        public DurationData CurrentPackageDuration { get; set; }
        public IDictionary<short, IList<InstallmentDetailTable>> Installments { get; set; }

        public ExtendedProduct()
            : base()
        {
            Features = new List<ProductFeatureData>();
            Duration = new DurationData();
            RenewalDuration = new DurationData();
            Quantity = new QuantityData();
            CanPurchaseAs = new List<RateCardTypeData>();
            ProductSavings = new List<ProductSavingsData>();
            Installments = new Dictionary<short, IList<InstallmentDetailTable>>();
        }

        public ExtendedProduct Clone()
        {
            var res = this.MemberwiseClone() as ExtendedProduct;
            if (res == null)
                return res;

            res.EntityId = this.EntityId.CloneString();
            res.ParentEntityId = this.ParentEntityId.CloneString();
            res.InitialPrice = this.InitialPrice.CloneTyped();
            res.RenewalPrice = this.RenewalPrice.CloneTyped();
            res.InitialPricePerPeriod = this.InitialPricePerPeriod.CloneTyped();
            res.RenewalPricePerPeriod = this.RenewalPricePerPeriod.CloneTyped();
            res.InitialPricePerUnit = this.InitialPricePerUnit.CloneTyped();
            res.RenewalPricePerUnit = this.RenewalPricePerUnit.CloneTyped();
            res.ISOCurrencyCode = this.ISOCurrencyCode.CloneString();
            res.BundleInformation = this.BundleInformation.CloneTyped();
            res.SavingsToNonDiscountedOneMonth = this.SavingsToNonDiscountedOneMonth.CloneTyped();
            res.ProductSavings = this.ProductSavings.CloneTypedCollection().ToList();
            res.Features = this.Features.CloneTypedCollection().ToList();
            res.Duration = this.Duration.CloneTyped();
            res.RenewalDuration = this.RenewalDuration.CloneTyped();
            res.Quantity = this.Quantity.CloneTyped();
            res.CanPurchaseAs = this.CanPurchaseAs.CloneTypedCollection().ToList();
            res.RefundAmount = this.RefundAmount.CloneTyped();
            res.RefundDuration = this.RefundDuration.CloneTyped();
            res.CurrentPackagePrice = this.CurrentPackagePrice.CloneTyped();
            res.CurrentPackagePricePerPeriod = this.CurrentPackagePricePerPeriod.CloneTyped();
            res.CurrentPackageDuration = this.CurrentPackageDuration.CloneTyped();

            return res;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
