﻿using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class RefundAmountData : ICloneable
    {
        public decimal RefundAmount { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal RefundAmountWithTax { get; set; }

        public RefundAmountData Clone()
        {
            return this.MemberwiseClone() as RefundAmountData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
