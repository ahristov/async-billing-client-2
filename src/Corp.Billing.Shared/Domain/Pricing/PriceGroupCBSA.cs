﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Pricing
{
    public class PriceGroupCBSARequest : IApiRequestWithQueryString
    {
        public int UserID { get; set; }
        public string PromoID { get; set; }

        public PriceGroupCBSARequest()
        {
            PromoID = Corp.Billing.Shared.Domain.Consts.DefaultPromoId;
        }
    }
    public class PriceGroupCBSAResult
    {
        public int ReturnValue { get; set; }
        public int PriceGroupID { get; set; }
        public int CBSACode { get; set; }
        public Guid TestingGuid { get; set; }
    }

    public class GetUserScoreDataRequest : IApiRequestWithQueryString
    {
        public int UserID { get; set; }
    }

    [Serializable]
    public class GetUserScoreDataResult
    {
        public int ReturnValue { get; set; }
        public int Age { get; set; }
        public bool HasPhoto { get; set; }
        public float Score { get; set; }
    }

    public class BillCurrencyConversionRatesResult
    {
        public IEnumerable<BillCurrencyRate> BillCurrencyRates { get; set; }
    }

    public class BillCurrencyRate
    {
        public string CurrencyAbbreviation { get; set; }
        public string Description { get; set; }
        public decimal? ForeignPerDollar { get; set; }
        public decimal? DollarPerForeign { get; set; }
        public short CountryCode { get; set; }
    }
}
