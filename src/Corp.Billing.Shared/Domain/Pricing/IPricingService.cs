﻿namespace Corp.Billing.Shared.Domain.Pricing
{
    public interface IPricingService : IUserPricingScoreGetter, IPriceGroupGetter
    {
        // Pricing

        
        BillCurrencyConversionRatesResult GetBillCurrencyConversionRates();
    }
}
