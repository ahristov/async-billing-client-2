﻿namespace Corp.Billing.Shared.Domain.Pricing
{
    public interface IPriceGroupGetter
    {
        PriceGroupCBSAResult GetPriceGroupCbsa(PriceGroupCBSARequest request);
    }
}
