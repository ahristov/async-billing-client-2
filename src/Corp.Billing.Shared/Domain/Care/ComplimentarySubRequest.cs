﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corp.Billing.Shared.Domain.Care
{
    public class ComplimentarySubRequest
    {
        public int UserId { get; set; }
        public byte ReasonId { get; set; }
        public short DaysRequested { get; set; }
        public int EmployeeId { get; set; }
        public short ProductList { get; set; }
        public string Comment { get; set; }    //Optional
        public string RequesterDepartment { get; set; } //Optional
    }

    public class ComplimentarySubResponse
    {
        public int ReturnCode { get; set; }
    }

    public class CompDaysDetailIdResponse
    {
        public short ProductDetailId { get; set; } 
        public int ReturnCode { get; set; }
    }

    public class CompDaysDetailIdRequest
    {
        public short Days { get; set; }
        public int UserId { get; set; }
        /// <summary>
        /// Optional
        /// </summary>
        public string PromoId { get; set; }
    }

    public class EligibleFreeAddOnsRequest
    {
        public int UserId { get; set; }
    }

    public class FreeAddonProduct
    {
        public short ProdDtlId { get; set; }
        public string Description { get; set; }
        public short InitialDays { get; set; }
        public short RenewalDays { get; set; }
        public bool IsProvisioned { get; set; }
        public bool IsFreeAddOn { get; set; }
        public byte luProdFeatureId { get; set; }
    }

    public class EligibleFreeAddOnsResponse
    {
        public int ReturnCode { get; set; }
        public List<FreeAddonProduct> Products { get; set; }
    }

    public class ProcessFreeAddOnRequest
    {
        public int UserId { get; set; }
        public int EmployeeId { get; set; }
        public string ProdDtlDaysList { get; set; }
        public int CrCode { get; set; }
    }

    public class ProcessFreeAddOnResponse
    {
        public int ReturnCode { get; set; }
    }

    ///////////////////////////////////////////////////////////
    ///  PCV2 Below
    ///////////////////////////////////////////////////////////

    public class ImpulseAndRenewableCompProduct
    {
        public Int16 ProductId { get; set; }
        public string ProductDesccription { get; set; }
        public bool IsProvisioned { get; set; }
    }

    public class BundledCompProduct
    {
        public int ProductBundleId { get; set; }
        public string ProductBundleDescription { get; set; }
        public string ProductFeatureList { get; set; }
        public bool IncludesValueAddedFeatures { get; set; }
    }

    public class EligibleCompProducts
    {
        public List<ImpulseAndRenewableCompProduct> NonBundledProducts { get; set; }
        public List<BundledCompProduct> BundledProducts { get; set; }
    }

    public class CompOrderRequest
    {
        public int UserId { get; set; }

        //Only set ProductId or ProductBundleId, never both
        public int ProductId { get; set; }
        public int ProductBundleId { get; set; }

        //maximum 365
        public Int16 ComplimentaryDays { get; set; }

        public Int16 EmployeeId { get; set; }

        public Int16 CrCode { get; set; }
        public string Comment { get; set; }
    }

    public class CompOrderResponse
    {
        public int ReturnCode { get; set; }
        public string ResponseMessage { get; set; }
    }

}
