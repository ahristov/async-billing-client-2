﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace Corp.Billing.Shared.Domain.Care
{
    public class TransactionSearchResults
    {
        public IEnumerable<TransactionSearchResultItem> SearchResults { get; set; }
    }

    public class TransactionSearchResultItem
    {
        public decimal FeeAmount { get; set; }

        public decimal GrossAmount { get; set; }

        public decimal NetAmount { get; set; }

        public string Currency { get; set; }

        public string Payer { get; set; }

        public string PayerDisplayName { get; set; }

        public string Status { get; set; }

        public DateTime Timestamp { get; set; }

        public string Timezone { get; set; }

        public string TransactionID { get; set; }

        public string Type { get; set; }

        public int UserID { get; set; }

        public int PaymentID { get; set; }
    }

    public class TransactionSearchRequest
    {
        public int TransactionProcessorType { get; set; }
        public TransactionSearchType SearchType { get; set; }

        public string CurrencyCode { get; set; }
        public double Amount { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string TransactionReference { get; set; }
        public string OrderReference { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }

    public enum TransactionSearchType
    {
        PayPalGetTransactionDetails = 20,
        PayPalTransactionSearch = 21
    }
}
