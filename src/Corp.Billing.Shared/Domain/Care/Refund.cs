﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Corp.Billing.Shared.Domain.Care
{
    public class CSACalcRefundRequest
    {
        public int UserID { get; set; }
        public string AcctDtlList { get; set; }
        public string RefundType { get; set; }
        public short CRCode { get; set; }
        public int EmployeeID { get; set; }
        public string Comment { get; set; }
        public Guid? RunId { get; set; }
        public byte Units { get; set; }
        public int? TrxID { get; set; }
    }

    public class CSACalcRefundResult
    {
        public int ReturnValue { get; set; }
        public int RefundID { get; set; }
        public decimal RefundTotal { get; set; }
        public short StateTermRefundCount { get; set; }
        public List<RefundProdDetail> ProductList { get; set; }

    }

    public class RefundProdDetail
    {
        public int StgRefundID { get; set; }
        public string TrxType { get; set; }
        public string TrxReason { get; set; }
        public int AppliedTrxID { get; set; }
        public int AcctDtlID { get; set; }
        public string ProdID { get; set; }
        public string ProdDtlID { get; set; }
        public string Product { get; set; }
        public string Description { get; set; }
        public string ISOCurrencyCode { get; set; }
        public bool IsBaseFeature { get; set; }
        public string CurrBeginDt { get; set; }
        public string CurrEndDt { get; set; }
        public string NewBeginDt { get; set; }
        public string NewEndDt { get; set; }
        public string SalesPrice { get; set; }
        public string InitialDays { get; set; }
        public string RefundAmt { get; set; }
        public string RefundDays { get; set; }
        public string DelayCapture { get; set; }
        public string EmployeeID { get; set; }
        public string RefundRequestDt { get; set; }
        public string TaxAmt { get; set; }
        public int PPPartialIssued { get; set; }
    }

    public class CSARefundSubmitRequest
    {
        public string RefundIDList { get; set; }
        public string ResignType { get; set; }
        public int EmployeeID { get; set; }
        public int CancelTypeID { get; set; }
        public string CSAUserComment { get; set; }
        public short ResignCode { get; set; }
        public byte LiveRefund { get; set; }
        public bool? SkipActivity { get; set; }
        public Guid? RunID { get; set; }
    }

    public class CSARefundSubmitResult
    {
        public int ReturnValue { get; set; }
        public string Message { get; set; }
        public string ConfirmationID { get; set; }
        public bool RefundBase { get; set; }
        public byte LiveRefund { get; set; }
    }

    public class CSAChbRefundRequest
    {
        public int OrigTrxID { get; set; }
        public int EmployeeID { get; set; }
        public string Comment { get; set; }
        public short CRCode { get; set; }
        public byte TrxTypeID { get; set; }
        public bool KickOff { get; set; }
        public string ChbSeqNo { get; set; }
        public int? ChbReasonCode { get; set; }
        public byte? Debug { get; set; }
        public string InternalAnalysis { get; set; }
        public string Contact { get; set; }
        public byte? ResolveTrx { get; set; }
        public short? ActivityID { get; set; }
        public string ChbReasonCodeChar { get; set; }
        public Guid? RunID { get; set; }
        public decimal? ChgBakAmt { get; set; }
        public short? UsageCode { get; set; }
        public int ProcessorLogID { get; set; }
    }

    public class CSAChbRefundResult
    {
        public int ReturnValue { get; set; }
        public string ConfirmationID { get; set; }
        public int TrxID { get; set; }
    }

    public class CSAInsAbuseRefundRequest
    {
        public string UserIDList { get; set; }
        public short ReasonID { get; set; }
        public int EmployeeID { get; set; }
    }

    public class MassBlockerRequest
    {
        public List<UserBlockList> UserAndReasonList { get; set; }
        public int EmployeeId { get; set; }
    }

    public class UserBlockList
    {
        public int UserId { get; set; }
        public int ReasonId { get; set; }
    }

    public class GenericResult
    {
        public int ReturnValue { get; set; }
    }

    public class CSAUnitStageRefundRequest
    {
        public int UserID { get; set; }
        public string AcctDtlList { get; set; }
        public short CRCode { get; set; }
        public int EmployeeID { get; set; }
        public bool SupervisoryOverride { get; set; }
        public bool Debug { get; set; }
        public string Comment { get; set; }
    }

    public class CSAUnitStageRefundResult
    {
        public int ReturnValue { get; set; }
        public string RefundIDList { get; set; } 
    }

    public class PayPalTrxInfoRequest
    {
        public string TransactionId { get; set; }
    }

    public class PayPalTrxInfoResponse
    {
        public int TransactionId { get; set; }
        public int UserId { get; set; }
        public int ReturnValue { get; set; }
    }

    public enum RefundType
    {
        Regular = 1,
        Historical = 2,
        Chargeback = 3,
        ChargePerUnit = 4, //CPU such as PCS, Stealth, TopSpot
        Offline = 5
    }

    //Stage Refund
    public class StageRefundRequest
    {
        //common fields
        public int RefundType { get; set; } //{1,2,3} 1=Regular; 2=Historical/Chargeback; 3=Chargeback; 4=UnitProduct; 5=Offline
        public int RefundPortion { get; set; } // {1,2} 1=full refund; 2=partial refund
        public int EmployeeId { get; set; } //employee id 
        public string Comment { get; set; } //passed in from interface based on context; usually pre-filled and not exposed to agent for freeform

        //Regular refunds
        public string RefundIdList { get; set; } //comma delim list; corresponds to refundId(s) in BillingData..stgRefund
        public string ResignType { get; set; } // {B,K} B=Resign or Cancel; K=Resign and Block <--need to evaluate along with ResignCode
        public byte LiveRefund { get; set; } // {0,1} 0=staged; 1=no staging
        public short ResignCode { get; set; } //BillingData..luCrCode
        public Guid? RunId { get; set; } //if != null, then we suppress returning PayPal transaction list, if present <-- weird logic
        public bool? SkipActivity { get; set; } // {0,1} 0=write activity record; 1=suppress
        public int CancelTypeId { get; set; } //1 Full Refund and Resignation, 2 Feature Cancellation, 3 Batch Billing Failure, 4 System cancel, 5 auto-cancel comp

        //Historical / Chargeback
        public int? ChbReasonCode { get; set; } //usually 0 for refunds, numeric for chargebacks
        public int OrigTrxId { get; set; } //the sale transaction id to be refunded or charged back
        public short CrCode { get; set; } // usually 112 from CSA, could vary from other callers
        public byte TrxTypeId { get; set; } // {4,9,15} 4 = CHB, 9 HRF, 15 ADJ
        public bool KickOff { get; set; } //true will block user, false will not

        //chargeback only fields
        public string ChbSeqNo { get; set; } //not used for refunds
        public string InternalAnalysis { get; set; } //50-char free form field for chargebacks
        public string Contact { get; set; } // {"Y","N"} Indicates if a user has been contacted

        //unit stage refund only
        public int UserId { get; set; } //only used for Unit Stage Refunds
        public string AcctDtlList { get; set; } //only for Unit Stage Refunds

        //seldom used fields
        public byte? ResolveTrx { get; set; } // {0,1} 0=enforce check for active product; 1=bypass check
        public byte? Debug { get; set; } // 0 always
        public string ChbReasonCodeChar { get; set; } //not used from CSA front end for any operation; Used by bchProcessAutoChargebackLog
        public short? ActivityId { get; set; } //usually null, but if present overrides 
        public decimal? ChgBakAmt { get; set; } //not used from CSA front end
        public short? UsageCode { get; set; } //not used from CSA front end
        public int ProcessorLogId { get; set; } //not used from CSA front end; appears to be used like a local variable inside of [bilRefundChbTrxv3]
        
    }

    public class StageRefundResult
    {
        public int ReturnValue { get; set; } //success of the staging operation
        
        //Regular
        public string Message { get; set; }
        public string ConfirmationID { get; set; }
        public bool RefundBase { get; set; }
        public byte LiveRefund { get; set; }

        //Chargeback
        public int TrxID { get; set; }

        //unit stage refund
        public string RefundIDList { get; set; } 
    }

    //Get Refund Batch
    public class RefundBatchRequest 
    {
        public int BatchSize { get; set; }
    }

    public class PendingRefundItem
    {
        public int TransactionProcessorType { get; set; }   //c.f. the TransactionProcessorType enum
        public string PayPalTransactionId { get; set; }     //PayPal use only
        public int TransactionId { get; set; }              //this is the refund transaction id
        public string AccountNumber { get; set; }           //clear text account number...deprecated
        public decimal TotalAmount { get; set; }            //this is the refund amount
        public int RefundPortion { get; set; }              // {1,2} : 1 = full (F)ull, 2 = partial (C)onversion
        public int EmployeeId { get; set; }
        public int UserId { get; set; }
        public short SiteCode { get; set; }
        public string IsoCurrencyCode { get; set; }
        public short? UrlCode { get; set; }

        //v2 properties
        public int PaymentToken { get; set; } // the tokenized account number
        public int OriginalTransactionId { get; set; }
        public decimal OriginalAmount { get; set; } 
        public int StageRefundId { get; set; }
        public Guid? LockId { get; set; }
        public string BaseCode { get; set; }  
        public string DetailCode { get; set; }
        public int ProcessorLogId { get; set; }
    }
    
    public class RefundBatchResult
    {
        public List<PendingRefundItem> PendingRefunds { get; set; }
        public int BatchReturnCode { get; set; }
    }

    public class PerformRefundResult 
    {
        [DefaultValue(-1)]
        public int RefundResult { get; set; }  // {0,1,5,8} 0 = Success, 1 = General Failure, 5, error in bilUpdateStatus, 8 = Error releasing matchlock
        public string CorrelationId { get; set; }  //PayPal Only
    }

    public enum RefundResultCodes
    {
        NotSet = -1,
        Success = 0,
        GeneralFailure = 1,
        ErrorInBilUpdateStatus = 5,
        ErrorReleasingMatchLock = 8
    }

    public class RefundFinalizeRequest
    {
        public int RefundTransactionId { get; set; }
        public int Result { get; set; }
        public int VerisignCount { get; set; }
        public int ProcessorLogId { get; set; }
        public int UserId { get; set; }
        public Guid LockId { get; set; }
        public int RefundId { get; set; }
    }

    public class RefundFinalizeResponse
    {
        public int ReturnValue { get; set; }
    }

}
