﻿namespace Corp.Billing.Shared.Domain.Care
{
    public class CSAUpdateStatusRequest
    {
        public int? UserId { get; set; }
        public int TrxID { get; set; }
        public short? Result { get; set; }
        public int? AcctPymtMethID { get; set; }
        public int? VerisignCount { get; set; }
        public int? ProcessorLogID { get; set; }
        public int? LuTrxStatusID { get; set; }
    }

    public class CSAUpdateStatusResult
    {
        public int ReturnValue { get; set; }
        public int TrxStatusID { get; set; }
        public int ResultType {get;set;}
        public int EventAcctDtlID { get; set; }
        public byte EventStatus { get; set; }
        public bool AwardedComplimentary { get; set; }
        public int EcardSubmitStatus { get; set; }

    }

    public class CSACashCalcRequest
    {
        public int UserID { get; set; }
        public decimal PayAmt { get; set; }
        public byte PState { get; set; }
        public short PCountry { get; set; }
        public string PromoID { get; set; }
    }

    public class CSACashCalcResult
    {
        public int ReturnValue { get; set; }
        public short ProdDtlID { get; set; }
        public short CashDays { get; set; }
        public decimal CashPrice { get; set; }

    }
}
