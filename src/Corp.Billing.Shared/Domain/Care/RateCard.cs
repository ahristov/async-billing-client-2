﻿using System;
using System.Collections.Generic;


namespace Corp.Billing.Shared.Domain.Care
{

    public class CSARateCardAddOnRequest
    {
        public int UserID { get; set; }
        public string ProdDtlIDList { get; set; }
        public short UrlCode { get; set; }
        public short PState { get; set; }
        public short PCountry { get; set; }
        public bool IsCashOrder { get; set; }
        public string PromoID { get; set; }
        public short CashDays { get; set; }
        public decimal CashPrice { get; set; }
    }

    public class CSARateCardAddOnResult
    {
        public int ReturnValue { get; set; }
        public int ProcReturn { get; set; }
        public int UserID { get; set; }
        public short StateCode { get; set; }
        public short CountryCode { get; set; }
        public int BrandID { get; set; }
        public short Age { get; set; }
        public short AccountOrderModID { get; set; }
        public DateTime OldUserStatusDate { get; set; }
        public short GeoStateTaxID {get; set;}
        public List<BaseList> BaseFeatureList { get; set; }
        public FeatureItem FreeTrialItem { get; set; }
    }

    public class BaseList
    {
        public short ProdDtlID { get; set; }
        public DateTime BeginDt { get; set; }
        public short InitialDays { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal RenewalPrice { get; set; }
        public short RenewalDays { get; set; }
        public bool IsFreeTrial { get; set; }
        public byte Months { get; set; }
        public decimal InitialTax { get; set; }
        public decimal RenewalTax { get; set; }
        public string Descr { get; set; }
        public short FreeDays { get; set; }
        public string OrderString { get; set; }
        public string ISOCurrencyCode { get; set; }
        public bool BundlePreOrdered { get; set; }
        public byte? ProdFeatureID { get; set; }
        public List<FeatureItem> AddOnFeatures { get; set; }
    }
    public class FeatureItem
    {
        public short ProdDtlID { get; set; }
        public DateTime BeginDt { get; set; }
        public short InitialDays { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal RenewalPrice { get; set; }
        public short RenewalDays { get; set; }
        public bool IsFreeTrial { get; set; }
        public byte Months { get; set; }
        public decimal InitialTax{ get; set; }
        public decimal RenewalTax { get; set; }
        public string Descr { get; set; }
        public short FreeDays { get; set; }
        public string OrderString { get; set; }
        public string ISOCurrencyCode { get; set; }
        public bool BundlePreOrdered { get; set; }
        public byte? ProdFeatureID { get; set; }
    }
    public class CSARateCardRequest
    {
        public int UserID { get; set; }
        public string PromoID { get; set; }
        public int? ProdDtlID { get; set; }
        public int? EventID { get; set; }
        
    }
    public class CSARateCardResult
    {
        public short AccountOrderModID { get; set; }
        public DateTime OldUserStatusDate { get; set; }
        public int ReturnValue { get; set; }
        public List<UnitRateCardItem> RateCardItems { get; set; }
    }

    public class UnitRateCardItem
    {
        public int ProdDtlID { get; set; }
        public byte luProdFeatureID { get; set; }
        public byte Units { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal TaxAmt { get; set; }
        public string ISOCurrencyCode { get; set; }
        public string OrderString { get; set; }
        public short ImpulseMinutes { get; set; }		
    }
    public class CSAEligibleAddonCountResult
    {
        public int ReturnValue { get; set; }
        public int UserID { get; set; }
        public short AccountOrderModID { get; set; }
        public DateTime OldUserStatusDate { get; set; }
        public short GeoStateTaxID { get; set; }
        public List<CSAOrderDetailResult> Details { get; set; }
        public int AddOnCount { get; set; }
    }
}
