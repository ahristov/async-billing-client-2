﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Corp.Billing.Shared.Domain.Care
{
    public class UserSearchResults
    {
        public IEnumerable<UserSearchResultItem> SearchResults { get; set; }
        public int? SelectCount { get; set; }
    }

    public class UserSearchResultItem
    {
        public int? UserId { get; set; }
        public string Handle { get; set; }
        public string Handle2 { get; set; }
        public string Email { get; set; }
        public string UsersName { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string PostalCode { get; set; }
        public byte? LoginDisabled { get; set; }
        public int? SiteCode { get; set; }
    }

    public enum SearchFamily
    {
        User = 1,
        GiftSubscription = 2,
        NonRegEmail = 3 //deprecated, do not use - delete after 6/1/2018
    }

    public enum UserSearchType
    {
        Handle = 1,
        Name = 2,
        Email = 3,
        TransactionId = 4,
        Check = 5,
        MoneyOrder = 6,
        OrderNumber = 7,
        RedemptionKey = 8,
        UserId = 9,
        Phone = 10,
        FullCreditCard = 11,
        PartialCreditCard = 12,
        MatchTalkPhone = 13,
        Debito = 14,
        NonRegEmail = 15,   //deprecated, do not use - delete after 6/1/2018
        ConfirmationNumber = 16,
        GiftPurchaserId = 17,
        ProcessorReference = 18,
        ARN = 19
    }

    public abstract class BaseSearchCriteria
    {
        public abstract SearchFamily SearchFamily { get; }
        public UserSearchType SearchType { get; set; }
    }

    public class UserSearchCriteria : BaseSearchCriteria
    {
        public override SearchFamily SearchFamily { get { return SearchFamily.User; } }

        public int StartRowIndex { get; set; }
        public int? MaximumRows { get; set; }
        public short? SiteCode { get; set; }
        public short? SiteCodeGroup { get; set; }
        public string Handle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? UserId { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public int? OrderNumber { get; set; }
        public string RedemptionKey { get; set; }
        public int? TrxId { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string BankIdentification { get; set; }
        public string LastFour { get; set; }
        public string CreditCardNumber { get; set; }
        public string Receipt { get; set; }
        public string ProcessorReference { get; set; }
        public int? ProcessorID { get; set; }
        public int? EmployeeId { get; set; }
        public string ARN { get; set; }
    }

    public class GiftSubscriptionSearchCriteria : BaseSearchCriteria
    {
        public override SearchFamily SearchFamily { get { return SearchFamily.GiftSubscription; } }

        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ConfirmationNumber { get; set; }
        public int? UserId { get; set; }
        public string CreditCardNumber { get; set; }
        public string BankIdentification { get; set; }
        public string LastFour { get; set; }
    }

}
