﻿namespace Corp.Billing.Shared.Domain.Care
{
    public class UserActivityRequest
    {
        public short ActivityId { get; set; }
        public int UserId { get; set; }
        public string Comment { get; set; }
        public int EmployeeId { get; set; }
    }

    public class UserActivityResult
    {
        public int ReturnValue { get; set; }
    }
}
