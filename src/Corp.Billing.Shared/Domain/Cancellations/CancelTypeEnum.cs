﻿namespace Corp.Billing.Shared.Domain.Cancellations
{
    public enum CancelTypeEnum : int
    {
        Active = 0,
        FullCancellation = 1,
        FeatureCancellation = 2,
        SystemCancellation = 3,
        SystemCancellationOldSubWhenStackingASub = 4,
        AutoCancelCompOrder = 5,
        AutoCancelNewOrdersAssociatedWithNonRenewablePymtMeth = 6,
        AutoCancelFreeTrialRecord = 7,
        FullCancellationChargeback = 8,
        TemporaryCancellationForBillFailure = 9,
        FullCancellationUserKickedOffSite = 10,
        FailedToBill = 11,
        AutoCancelRefundedAcctDtls = 12,
        AutoCancelRenewalDueToGuaranteeEligibility = 13,
        AutoCancelChargePerUnitOrders = 14,
        CancelChargePerUnitServiceRendered = 15,
        CanceledUserWasMovedToMEETIC = 16,
        FailedToRenewWithin10DaysOfAppleExpirationDate = 17,
        CancelOfflineTransferNotReceived = 18,
        OfflineTransferPendingReceipt = 19,
        SystemCancelFullPackageRenewalConvertTo30DaySubscriptionLatAmOnly = 20,
        CancelDueToAppleIAPPurchaseNotification = 21,
        OfflineTransferPendingBDCOnHold = 22,
        AutoCancelAppleIAPSandboxOrder = 23,
    }
}
