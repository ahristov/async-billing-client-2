﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.Domain.Messaging
{
    public class InsertENSEMailRequest
    {
        public string Email { get; set; }
        public int? SenderUserId { get; set; }
        public int RecipientUserId { get; set; }
        
        
        public short EmailTypeId { get; set; }
        public string VarData { get; set; }
        
        
        public short SiteCode { get; set; }
        public string CultureCode { get; set; }
        public Dictionary<string, string> ReplacementTokens { get; set; }
    }

    public class InsertENSEmailResponse
    {
        public int ReturnValue { get; set; }
    }

    public enum EmailType : short
    {
        Unknown = 0,
        SubConfirmation = 19,
        DomesticCancelMembership = 133,
        DomesticSubReactivation = 134,
        DomesticStackedSubscription = 178,
        DomesticAddOnPurchase = 179,
        DomesticFreeTrialConfirmation = 180,
        DomesticGiftCertificateRedemptionPurchase = 181,
        Domestic50PercentOffNextRenewal = 182,
        PaymentNotProcessed = 371,
        TopSpotConfirmationEmail = 489,
        GCPendingTransaction = 510,
        StealthModeConfirmationEmail = 506,
        EmailVerificationEmail = 514,

        LatAmFailedOfflinePayment = 572,
    }
}
