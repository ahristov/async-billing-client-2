﻿namespace Corp.Billing.Shared.Domain.Messaging
{
    public class EmailSavedNotSentRequest : IApiRequestWithQueryString
    {
        public int SenderUserId { get; set; }
    }

    public class EmailSavedNotSentResponse
    {
        public int ReturnValue { get; set; }
        public int SendCount { get; set; }
    }
}
