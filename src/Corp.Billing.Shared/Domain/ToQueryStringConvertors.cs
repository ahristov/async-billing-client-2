﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Corp.Billing.Shared.Domain
{
    public static class ToQueryStringExtensions
    {
        /// <summary>
        /// Converts name and value(s) into escaped pair(s) for making an URL query string parameter.
        /// </summary>
        /// <param name="paramName">The name of the parameter</param>
        /// <param name="paramValues">One or many values of the parameter</param>
        /// <returns>String representing the query string parameter</returns>
        /// <example>
        /// var qs = "pName".GetNameValueQueryParameterPairs(1, 2, 3); // qs is "qs=1&qs=2&qs=3"
        /// 
        /// </example>
        public static string GetNameValueQueryParameterPairs(this string paramName, params object[] paramValues)
        {
            if (string.IsNullOrWhiteSpace(paramName) || paramValues == null || paramValues.Length < 1) {
                return string.Empty;
            }

            List<string> nameValuePairs = new List<string>();
            var escapedName = Uri.EscapeDataString(("" + paramName).ToCamelCase());

            foreach (var paramValue in paramValues)
            {
                var escapedValue = Uri.EscapeDataString("" + paramValue);
                nameValuePairs.Add($"{escapedName}={escapedValue}");
            }

            var res = String.Join("&", nameValuePairs);
            return res;
        }

        public static string ToQueryString(this IApiRequestWithQueryString request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Get all non-null properties on the object

            bool first = true;

            StringBuilder requestBuilder = new StringBuilder();
            foreach (var x in request.GetType().GetProperties())
            {
                if (!x.CanRead) continue;

                var value = x.GetValue(request, null);
                if (value == null) continue;

                //Append & separator
                if (first)
                    first = false;
                else
                    requestBuilder.Append("&");


                var valueString = GetValueForQueryString(value);

                var escapedName = Uri.EscapeDataString(x.Name.ToCamelCase());
                var escapedValue = Uri.EscapeDataString(valueString);
                requestBuilder.AppendFormat("{0}={1}", escapedName, escapedValue);
            }

            return requestBuilder.ToString();
        }

        /// <summary>
        /// Stolen from Newtonsoft Json Serializer decompiled code
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToCamelCase(this string s)
        {
            if (string.IsNullOrEmpty(s) || !char.IsUpper(s[0]))
                return s;
            char[] chArray = s.ToCharArray();
            for (int index = 0; index < chArray.Length; ++index)
            {
                bool flag = index + 1 < chArray.Length;
                if (index <= 0 || !flag || char.IsUpper(chArray[index + 1]))
                    chArray[index] = char.ToLower(chArray[index], CultureInfo.InvariantCulture);
                else
                    break;
            }
            return new string(chArray);
        }

        private static string GetValueForQueryString(object value)
        {
            string valueString;
            if (value is DateTime)
            {
                valueString = ((DateTime) value).ToString("s");
            }
            else
            {
                valueString = value.ToString();
            }
            return valueString;
        }

        public static string GetUrl(this IApiRequestWithQueryString request, string prefix, string path)
        {
            return prefix + path + request.ToQueryString();
        }

        public static string GetUrl(this IApiRequest request, string prefix, string path)
        {
            return prefix + path;
        }
    }
}
