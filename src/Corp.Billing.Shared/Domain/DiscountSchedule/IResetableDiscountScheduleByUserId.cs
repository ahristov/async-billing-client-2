﻿using Corp.Billing.Shared.Facilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Corp.Billing.Shared.Domain.DiscountSchedule
{
    public interface IResetableDiscountScheduleByUserId: IDiscountScheduleByUserId, IResetableCache<int>
    {
    }
}
