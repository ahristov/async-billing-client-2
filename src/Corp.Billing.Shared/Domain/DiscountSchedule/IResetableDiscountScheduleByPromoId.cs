﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.DiscountSchedule
{
    public interface IResetableDiscountScheduleByPromoId: IDiscountScheduleByPromoId, IResetableCache<string>
    {
    }
}
