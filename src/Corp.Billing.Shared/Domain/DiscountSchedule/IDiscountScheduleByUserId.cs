﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.DiscountSchedule
{
    public interface IDiscountScheduleByUserId
    {
        ICollection<string> GetDiscountScheduleByUserId(int userId);
    }
}
