﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserBillingDataGetter : IResetableCache<int>
    {
        GetUserBillingDataResult GetUserBillingData(GetUserBillingDataRequest request);
    }

    public interface IUserBillingDataGetterNoHttpCache : IUserBillingDataGetter { }
}
