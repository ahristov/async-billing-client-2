﻿namespace Corp.Billing.Shared.Domain.Subscription.Extentions
{
    public static class SubscriptionPackageTypeExtentions
    {
        public static void TemporaryFixGoldAndPlatinumAsPremium(
            this IHasSubscriptionPackageType subPackageData)
        {
            if (subPackageData != null)
            {
                if (subPackageData.SubscriptionPackageType == (int)SubscriptionPackageTypeEnum.LatamGoldPackage
                    || subPackageData.SubscriptionPackageType == (int)SubscriptionPackageTypeEnum.LatamPlatinumPackage)
                {
                    subPackageData.SubscriptionPackageType = (int)SubscriptionPackageTypeEnum.PremiumPackage;
                }
            }
        }
    }
}
