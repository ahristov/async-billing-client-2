﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.Subscription.Extentions
{
    public static class DisplayStatusExtentions
    {
        public static bool IsAutoRenewalOn(this IEnumerable<DisplayUserStatusResult> displayStatus)
            => IsAutoRenewalOn(displayStatus, DateTime.Now);

        public static bool IsAutoRenewalOn(this IEnumerable<DisplayUserStatusResult> displayStatus, DateTime now)
        {
            if (displayStatus == null)
                return true;

            var res = displayStatus.Any(x => x.IsBaseFeature
                && x.BeginDate <= now
                && x.CancelDate > DateTime.MinValue
                && x.CancelDate <= now
                && x.Renewable
                && !x.Billable.GetValueOrDefault()) == false;

            return res;
        }
    }
}
