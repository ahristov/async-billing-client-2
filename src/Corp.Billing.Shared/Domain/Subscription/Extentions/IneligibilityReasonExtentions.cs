﻿using System.Linq;
using IneligibilityReasonType = Corp.Billing.Shared.Domain.Subscription.UserSubStatusResult.IneligibilityReasonType;

namespace Corp.Billing.Shared.Domain.Subscription.Extentions
{
    public static class IneligibilityReasonExtentions
    {
        public static bool IneligibilityReasonIsOfflinePayment(this IneligibilityReasonType ineligibilityReason)
        {
            return ineligibilityReason == IneligibilityReasonType.OfflinePaymentMethod; ;
        }

        public static bool IsEligibleToPurchaseSubscription(this IneligibilityReasonType ineligibilityReason)
        {
            return ineligibilityReason == IneligibilityReasonType.Eligible; ;
        }

        public static bool IsEligibleForSaveOffers(this IneligibilityReasonType ineligibilityReason)
        {
            var res = new IneligibilityReasonType[]
            {
                IneligibilityReasonType.UserInFreeTrial,
                IneligibilityReasonType.Failed15MinuteRule,
                IneligibilityReasonType.ExceedStackedSubRule,
            }.Contains(ineligibilityReason) == false;

            return res;
        }

    }
}
