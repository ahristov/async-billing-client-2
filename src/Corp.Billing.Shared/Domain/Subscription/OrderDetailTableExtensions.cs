﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Corp.Billing.Shared.Domain.RateCard;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public static class OrderDetailTableExtensions
    {
        public static List<OrderDetailTable> ToOrderDetailTable(this IList<RateCardV2AddOnsItem> addOns)
        {
            List<OrderDetailTable> res = addOns.Select(addOn => new OrderDetailTable()
            {
                ProdBundleId = addOn.BundleId,
                ProdId = addOn.ProdId,
                ProdFeatureId = addOn.LuProdFeatureId,
                BeginDate = addOn.BeginDt,
                DelayCapture = addOn.DelayCapture,
                ISOCurrencyCode = addOn.ISOCurrencyCode,
                InitialDays = addOn.InitialDays,
                RenewalDays = addOn.RenewalDays,
                SalesPrice = addOn.InitialAmt,
                RenewalPrice = addOn.RenewalAmt,
                FullPrice = addOn.PreDiscountInitialAmt, // PC2: NOTE: This is the rate card price prior any discounts,
                IsFreeTrial = addOn.IsFreeTrial,
            }).ToList();

            return res;
        }
    }
}
