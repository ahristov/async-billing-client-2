﻿using Corp.Billing.Shared.Domain.Installlments;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public class OrderCreateRequest
    {
        public int UserId { get; set; }

        public int PaymentMethodId { get; set; }

        public IList<OrderDetailTable> OrderDetailTable { get; set; }
        public IList<InstallmentDetailTable> InstallmentTable { get; set; }

        public Guid LockId { get; set; }

        public int SubBrandId { get; set; }

        public int PriceGrpId { get; set; }

        public int? CbsaCode { get; set; }

        public string PostalCode { get; set; }

        public int CohortIdPromotion { get; set; }

        public string PromoId { get; set; }

        public int CohortIdRateCard { get; set; }
        public int RateCardKey { get; set; }
        public int? RateCardOverrideKey { get; set; }

        public int CohortIdRateCardTemplate { get; set; }
        public int RateCardTemplateKey { get; set; }


        public Guid? Sid { get; set; }
        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }

        public byte? TrxSrcId { get; set; }
        public int EmployeeId { get; set; }

        public string Comment { get; set; }

        public short? CRCode { get; set; }

        public bool IsComplimentary { get; set; }

        public bool IsOneClickEligible { get; set; }

        public bool IsOneClickOrder { get; set; }

        public bool IsUpsell { get; set; }

        public bool UpgradeWithRefund { get; set; }

        public bool IsSaveOffer { get; set; }

        public bool IsInstallment { get; set; }
        public bool VisaCheckout { get; set; }
        public bool MasterPass { get; set; }
        public bool IsGiftCard { get; set; }

        public int? CardinalLogId { get; set; }

        public int? EventId { get; set; }
        public int? OtherUserId { get; set; }

        public string PhoneNumberClear { get; set; }


        public int? AcctOrderId { get; set; }

        public bool IsAppleIAPSandbox { get; set; }

        public string AuthenticationValue { get; set; }

        public Guid? RateCardRefId { get; set; }
    }


    public enum OrderCreateResultType
    {
        SqlException = -1,
        Success = 0,

        MissingOrInvalidInputParameter = 10,
        MissingOrInvalidOrderDetails = 20,
        LockIdDoesNotExists = 30,
        UnknownProcessorForSpecifiedPaymentMethod = 40,
        ExceptoinInBilCreateTrx = 50,
    }


    public class OrderCreateResult
    {
        public int AcctOrderId { get; set; }
        public int TrxId { get; set; }

        public decimal LocalTotalAmountIncludingTax { get; set; }

        public byte TrxCategoryId { get; set; }
        public byte TrxTypeId { get; set; }

        public byte? TrxProcessorID { get; set; }

        public int ReturnValue { get; set; }


        public OrderCreateResultType CreateResult
        {
            get { return (OrderCreateResultType)ReturnValue; }
        }
    }


}
