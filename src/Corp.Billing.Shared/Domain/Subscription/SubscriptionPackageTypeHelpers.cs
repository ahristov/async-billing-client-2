﻿using Corp.Billing.Shared.Subscription.Product;
using System;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public static class SubscriptionPackageTypeHelpers
    {
        const byte MIN_PREMIUM_MONTHS = 3;

        public static SubscriptionPackageTypeEnum GetProductSubscriptionPackageType(
            byte productMonths,
            int productBundleId)
        {
            if (productMonths <= 0)
                return SubscriptionPackageTypeEnum.NonSubscriber;

            ProductBundleEnum productBundle = ProductBundleEnum.None;

            if (Enum.IsDefined(typeof(ProductBundleEnum), productBundleId))
                productBundle = (ProductBundleEnum)productBundleId;

            var res = productBundle.GetSubscriptionPackageType();
            
            if (res == SubscriptionPackageTypeEnum.PremiumPackage
                && productMonths < MIN_PREMIUM_MONTHS)
            {
                res = SubscriptionPackageTypeEnum.StandardPackage; // Even if iOS has 1 month bundle, we provision value added features as for standard.
            }

            return res;
        }

        public static SubscriptionPackageTypeEnum GetCurrentSubscriptionPackageType(
            bool isSubscriber,
            bool isCurrentPackageBundle,
            byte packageMonths,
            bool packageHasValueAdd,
            int? prodBundleId)
        {
            packageMonths = packageMonths < 0 
                ? (byte)0
                : packageMonths;

            // non subs

            if (!isSubscriber)
                return SubscriptionPackageTypeEnum.NonSubscriber;

            // comp subs w/o prod bundle

            if (packageMonths == 0 && !prodBundleId.HasValue)
                return (isCurrentPackageBundle && packageHasValueAdd)
                    ? SubscriptionPackageTypeEnum.PremiumPackage
                    : SubscriptionPackageTypeEnum.StandardPackage;

            // paid subs

            ProductBundleEnum productBundle = ProductBundleEnum.None;
            int prodBundleIdValue = prodBundleId.GetValueOrDefault();

            if (Enum.IsDefined(typeof(ProductBundleEnum), prodBundleIdValue))
                productBundle = (ProductBundleEnum)prodBundleIdValue;

            var res = productBundle.GetSubscriptionPackageType();

            if (res == SubscriptionPackageTypeEnum.PremiumPackage
                && packageMonths > 0
                && packageMonths < MIN_PREMIUM_MONTHS)
            {
                res = SubscriptionPackageTypeEnum.StandardPackage; // Even if iOS has 1 month bundle, we provision value added features as for standard.
            }

            return res;
        }
    }
}
