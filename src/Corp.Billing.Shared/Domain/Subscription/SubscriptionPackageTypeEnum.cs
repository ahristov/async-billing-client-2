﻿namespace Corp.Billing.Shared.Domain.Subscription
{
    public enum SubscriptionPackageTypeEnum : byte
    {
        NonSubscriber = 0,
        StandardPackage = 1,
        PremiumPackage = 2,
        ElitePackage = 3,
        LatamGoldPackage = 4,
        LatamPlatinumPackage = 5,
    }
}
