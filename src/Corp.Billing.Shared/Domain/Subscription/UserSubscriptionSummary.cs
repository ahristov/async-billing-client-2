﻿using System;

namespace Corp.Billing.Shared.Domain.Subscription
{
	[Serializable]
    public class UserSubscriptionSummaryResult: IHasSubscriptionPackageType
	{
		public byte SubscriptionPackageType { get; set; }
		public string SubscriptionPackageTypeName => ((SubscriptionPackageTypeEnum)SubscriptionPackageType).ToString();
		public byte SubscriptionsSummaryState { get; set; }
		public string SubscriptionsSummaryStateName => ((SubscriptionsSummaryStateEnum)SubscriptionsSummaryState).ToString();
		public DateTime? EndDate { get; set; }
		public int Months { get; set; }
		public bool IsBundle { get; set; }
		public bool IsIap { get; set; }
		public bool IsAutoRenewActive { get; set; }
		public bool CanOneClickResub { get; set; }
		public bool CanPurchaseBase { get; set; }
		public bool CanPurchaseAddOn { get; set; }
		public bool IsFreeTrial { get; set; }
		public bool IsComplimentary { get; set; }
		public bool IsCharter { get; set; }
		public bool IsInECheckReviewPeriod { get; set; }
		public bool IsEligibleForDiscountedEvents { get; set; }
		public bool IsGuaranteedSubscriber { get; set; }
		public bool IsEligibleToUpgradeToPremium { get; set; }
		public DateTime? CurrentSubscriptionBeginDate { get; set; }
	}

	public class UserSubscriptionSummaryRequest: IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }
}
