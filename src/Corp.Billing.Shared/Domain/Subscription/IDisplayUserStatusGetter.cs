﻿using Corp.Billing.Shared.Facilities;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IDisplayUserStatusGetter : IResetableCache<int>
    {
        DisplayUserStatusResponse SubDisplayStatusv2(DisplayUserStatusRequest request);
    }
}
