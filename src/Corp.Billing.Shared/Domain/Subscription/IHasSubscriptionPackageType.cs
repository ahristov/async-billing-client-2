﻿namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IHasSubscriptionPackageType
    {
        byte SubscriptionPackageType { get; set; }
    }
}
