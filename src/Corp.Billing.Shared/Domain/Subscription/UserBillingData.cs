﻿using Corp.Billing.Shared.Facilities;
using System;
using System.Linq;

namespace Corp.Billing.Shared.Domain.Subscription
{

    public class PaymentMethodIURequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public int PaymentMethodToken { get; set; }
        public string PaymentMethodDtlCode { get; set; }
        public string BankIdentification { get; set; }
        public string AccountNumberLastFour { get; set; }
        public string DebitoBranch { get; set; }
        public byte? ExpirationMonth { get; set; }
        public short? ExpirationYear { get; set; }
        public string LastCheckNumber { get; set; }
        public bool SetInactive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public byte? StateCode { get; set; }
        public short? CountryCode { get; set; }
        public string PostalCode { get; set; }
        public bool UpdateDefaultPaymentMethod { get; set;  }

        public byte? SubmittedMethodHours { get; set; }
    }

    public class PaymentMethodIUResult
    {
        public int PaymentMethodId { get; set; }
        public int? SubmittedMethodCount { get; set; }
        public int ReturnValue { get; set; }
    }


    public class GetUserBillingDataRequest : IApiRequestWithQueryString, IResetableCacheRequest
    {
        public int UserId { get; set; }
        public bool ResetCache { get; set; }
    }

    public class GetCurrentPackageRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class GetCurrentPackageResult: ICurrentPackageResult, IHasSubscriptionPackageType
    {
        public bool IsCurrentSubscriber { get; set; }
        public bool IsFreeTrial { get; set; }
        public byte? CurrentPackageMonths { get; set; }
        public bool IsCurrentPackageBundle { get; set; }
        public bool IsGuaranteeSubscriber { get; set; }
        public byte SubscriptionPackageType { get; set; }
        public string SubscriptionPackageTypeName => ((SubscriptionPackageTypeEnum)this.SubscriptionPackageType).ToString();
        public bool IsCurrentPackageAppleIAP { get; set; }
        public bool IsEligibleForDiscountedEvents { get; set; }
        public bool IsEligibleForHappyHour { get; set; }
        public DateTime? CurrentPackageBeginDate { get; set; }
        public DateTime? CurrentPackageLastRenewDate { get; set; }
    }

    [Serializable]
    public class GetUserBillingDataResult
    {
        public enum SubscriberStateType
        {
            NeverSubscribed = 0,
            PreviousSubscriber = 1,
            CurrentSubscriber = 2,
        }

        public int UserId { get; set; }
        public string Handle { get; set; }
        public short SiteCode { get; set; }
        public string Email { get; set; }
        public byte GenderGenderSeek { get; set; }
        public short CountryCode { get; set; }
        public byte StateCode { get; set; }
        public string PostalCode { get; set; }
        public DateTime SignupDt { get; set; }
        public DateTime LoginDt { get; set; }
        public DateTime PrevLoginDt { get; set; }
        public DateTime Birthday { get; set; }
        public short UrlCode { get; set; }
        public int BrandId { get; set; }
        public string PromoId { get; set; }
        public Guid TestingGuid { get; set; }
        public int PriceGrpId { get; set; }
        public int CBSACode { get; set; }

        public void SetSubscriberState(byte? val)
        {
            val = val ?? 0;
            if (Enum.IsDefined(typeof(SubscriberStateType), (int)val))
                SubscriberState = (SubscriberStateType)val;
            else
                SubscriberState = SubscriberStateType.NeverSubscribed;
        }

        public SubscriberStateType SubscriberState { get; set; }

        public DateTime? FirstSubDate { get; set; }
        public byte? CurrentPackageMonths { get; set; }
        public bool? IsCurrentPackageBundle { get; set; }
        public bool IsGuaranteeSubscriber { get; set; }
        public bool? IsCurrentPackageAppleIAP { get; set; }
        public byte? LastPackageMonths { get; set; }
        public bool? IsInstallment { get; set; }
        public long? NextRenewalFeatures { get; set; }
        public DateTime? NextRenewalDate { get; set; }
        public short? NextRenewalDays { get; set; }
        public decimal? NextRenewalPrice { get; set; }
        public string NextISOCurrencyCode { get; set; }
        public int? DefaultPaymentMethodID { get; set; }
        public string DefaultPaymentMethodType { get; set; }
        public string DefaultPaymentMethodBIN { get; set; }
        public string LastISOCurrencyCode { get; set; }
        public int? ProdBundleId { get; set; }
        public DateTime? CurrentPackageBeginDate { get; set; }
        public DateTime? CurrentPackageLastRenewDate { get; set; }
        public string ExternalProductId { get; set; }
        public byte? FTPkgMonths { get; set; }
        public int? FTPkgBundleID { get; set; }
        public int? CurrentPackageType { get; set; }
        public int ReturnValue { get; set; }


        public byte Gender
        {
            get
            {
                var res = (byte) ((new[] {1, 2}).Any(i => i == GenderGenderSeek) ? 1 : 2); // 3,4
                return res;
            }
        }

        public int Age { get { return GetAge(Birthday.Date); } }

        private int GetAge(DateTime day)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - day.Year;
            if (day > today.AddYears(-age))
                age--;
            return age;
        }

        public bool IsCurrentPackageValueAdd { get; set; }

    }
}
