﻿using System;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public class CancelRequest
    {
        public int? UserID { get; set; }
        public string AccountDetailIDList { get; set; }
        public DateTime? CancelDt { get; set; }
        public int CancelTypeID { get; set; }
        public string CSAUserComment { get; set; }
        public int EmployeeID { get; set; }
        public byte CancelSourceID { get; set; }
        public bool SiteToggle { get; set; }
        public int? TrxIDToNotCancel { get; set; }
    }

    public class CancelResult
    {
        public int ReturnValue { get; set; }
    }
}
