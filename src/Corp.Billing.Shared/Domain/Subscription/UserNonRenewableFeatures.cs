﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public class GetUserNonRenewableFeaturesRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class GetUserNonRenewableFeaturesResult
    {
        public DateTime? BaseFeatureEndDt { get; set; }
        public ICollection<NonRenewableFeature> Features { get; set; }
        public ICollection<SubscriptionPaymentTransaction> SubscriptionPayments { get; set; }

        public GetUserNonRenewableFeaturesResult()
        {
            Features = new List<NonRenewableFeature>();
            SubscriptionPayments = new List<SubscriptionPaymentTransaction>();
        }


        [Serializable]
        public class NonRenewableFeature
        {
            public int AcctDtlID { get; set; }
            public byte ProdFeatureID { get; set; }
            public DateTime EndDt { get; set; }
        }

        [Serializable]
        public class SubscriptionPaymentTransaction
        {
            /// <summary>
            /// Id of the transaction.
            /// </summary>
            public int TrxId { get; set; }
            /// <summary>
            /// Type of the transaction. New=1, Renewal=2, Bill delay capture=8.
            /// </summary>
            public byte TrxTypeId { get; set; }
            /// <summary>
            /// Date of the transaction.
            /// </summary>
            public DateTime? StatusDt { get; set; }
        }
    }
}
