﻿using Corp.Billing.Shared.Domain.Subscription.Reactivate.Data;

namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Services
{
    public class ReactivateSubscriptionRequest
    {
        public int UserId { get; set; }
        public CancelSourceType CancelSource { get; set; }
        public int EmployeeId { get; set;}

        public static ReactivateSubscriptionRequest CreateRequestForSiteReactivate(int userId)
        {
            return new ReactivateSubscriptionRequest
            {
                UserId = userId,
                CancelSource = CancelSourceType.SiteAccountSettings,
                EmployeeId = -1,
            };
        }
        public static ReactivateSubscriptionRequest CreateRequestForCSAReactivate(int userId)
        {
            return new ReactivateSubscriptionRequest
            {
                UserId = userId,
                CancelSource = CancelSourceType.CSA,
            };
        }
    }
}
