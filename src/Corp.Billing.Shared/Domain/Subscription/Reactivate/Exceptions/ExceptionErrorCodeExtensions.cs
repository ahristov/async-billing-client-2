﻿using System.Net;
using System.Reflection;

namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Exceptions
{
    public static class ExceptionErrorCodeExtensions
    {
        public static HttpStatusCode GetHttpStatusCode(this ExceptionErrorCode errorCode)
        {
            FieldInfo fi = errorCode.GetType().GetField(errorCode.ToString());

            ExceptionHttpCodeAttribute[] attributes =
                (ExceptionHttpCodeAttribute[])fi.GetCustomAttributes(typeof(ExceptionHttpCodeAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].HttpStatusCode;
            else
                return ExceptionHttpCodeAttribute.HTTPCODE_400_BadRequest;
        }

        public static string GetHttpResponseMessage(this ExceptionErrorCode errorCode)
        {
            FieldInfo fi = errorCode.GetType().GetField(errorCode.ToString());

            ExceptionMessageAttribute[] attributes =
                (ExceptionMessageAttribute[])fi.GetCustomAttributes(typeof(ExceptionMessageAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Message;
            else
                return string.Empty;
        }

    }
}
