﻿using System;
using System.ComponentModel;

namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Exceptions
{
    [AttributeUsage(AttributeTargets.All)]
    public class ExceptionMessageAttribute : Attribute
    {
        public static readonly DescriptionAttribute Default = new DescriptionAttribute();

        public string Message { get; private set; }

        public ExceptionMessageAttribute(string message)
        {
            Message = message;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ExceptionMessageAttribute other = obj as ExceptionMessageAttribute;

            return (other != null) && (string.Equals(other.Message, this.Message, StringComparison.InvariantCultureIgnoreCase));
        }

        public override int GetHashCode()
        {
            return this.Message.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return (this.Equals(Default));
        }

    }
}
