﻿namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Exceptions
{
    public enum ExceptionErrorCode
    {
        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_500_InternalServerError)]
        [ExceptionMessage("Error processing the request")]
        Undefined = 0,

        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_400_BadRequest)]
        [ExceptionMessage("Invalid User ID")]
        InvalidUserId = 1010,

        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_422_UnprocessableEntity)]
        [ExceptionMessage("User not found")]
        UserNotFound = 1020,

        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_422_UnprocessableEntity)]
        [ExceptionMessage("Not a subscriber")]
        NotSubscriber = 1030,

        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_422_UnprocessableEntity)]
        [ExceptionMessage("Automatic renewal is active")]
        AutomaticRenewalIsActive = 1040,

        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_500_InternalServerError)]
        [ExceptionMessage("Internal error reactivating subscription")]
        InternalReactivateError = 2010,

        [ExceptionHttpCode(ExceptionHttpCodeAttribute.HTTPCODE_500_InternalServerError)]
        [ExceptionMessage("Internal error reinstantiating free trial")]
        ReinstantiateFreeTrialError = 2020,
    }
}
