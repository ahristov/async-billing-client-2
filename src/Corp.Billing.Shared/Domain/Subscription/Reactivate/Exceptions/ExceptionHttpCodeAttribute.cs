﻿using System;
using System.ComponentModel;
using System.Net;

namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Exceptions
{
    [AttributeUsage(AttributeTargets.All)]
    public class ExceptionHttpCodeAttribute : Attribute
    {
        public const HttpStatusCode HTTPCODE_500_InternalServerError = HttpStatusCode.InternalServerError;
        public const HttpStatusCode HTTPCODE_400_BadRequest = HttpStatusCode.BadRequest;
        public const HttpStatusCode HTTPCODE_422_UnprocessableEntity = (HttpStatusCode)422;

        public static readonly DescriptionAttribute Default = new DescriptionAttribute();

        public HttpStatusCode HttpStatusCode { get; private set; }

        public ExceptionHttpCodeAttribute(HttpStatusCode statusCode)
        {
            HttpStatusCode = statusCode;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ExceptionHttpCodeAttribute other = obj as ExceptionHttpCodeAttribute;

            return (other != null) && (other.HttpStatusCode == this.HttpStatusCode);
        }

        public override int GetHashCode()
        {
            return HttpStatusCode.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return (this.Equals(Default));
        }

    }
}
