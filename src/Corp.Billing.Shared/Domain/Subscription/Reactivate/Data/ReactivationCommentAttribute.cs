﻿using System;
using System.ComponentModel;

namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Data
{
    [AttributeUsage(AttributeTargets.All)]
    public class ReactivationCommentAttribute : Attribute
    {
        public const string COMMENT_GENERIC = "Account restivated";
        public const string COMMENT_CSA_REACTIVATED = "Subscription reactivated from CSA";
        public const string COMMENT_SITE_REACTIVATED = "User reactivated subscription on-site";

        public static readonly DescriptionAttribute Default = new DescriptionAttribute();

        public string  Comment { get; private set; }

        private ReactivationCommentAttribute()
        {
            Comment = COMMENT_GENERIC;
        }

        public ReactivationCommentAttribute(string comment)
            : this()
        {
            Comment = comment ?? Comment;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ReactivationCommentAttribute other = obj as ReactivationCommentAttribute;

            return (other != null) && string.Equals(other.Comment, this.Comment, StringComparison.InvariantCultureIgnoreCase);
        }

        public override int GetHashCode()
        {
            return Comment.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return (this.Equals(Default));
        }
    }
}
