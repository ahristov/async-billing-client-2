﻿namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Data
{
    public enum CancelSourceType : byte
    {
        [ReactivationComment(ReactivationCommentAttribute.COMMENT_GENERIC)]
        Unknown = 0,

        [ReactivationComment(ReactivationCommentAttribute.COMMENT_CSA_REACTIVATED)]
        CSA = 1,

        [ReactivationComment(ReactivationCommentAttribute.COMMENT_SITE_REACTIVATED)]
        SiteAccountSettings = 2,
    }
}
