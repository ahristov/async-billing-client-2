﻿using Corp.Billing.Shared.Domain.Subscription.Extentions;
using Corp.Billing.Shared.Facilities;
using Corp.Billing.Shared.Subscription.Payment;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public class UserSubStatusRequest : IApiRequestWithQueryString, IResetableCacheRequest
    {
        public int UserId { get; set; }
        public string OneClickCurrencyCode { get; set; }
        public bool? LogOneClickEligibility { get; set; }
        public Guid? SID { get; set; }
        public bool IsVerified { get; set; }
        public bool ResetCache { get; set; }

        public bool IsOneClickCurrencyCodeSpecified()
        {
            return ("" + OneClickCurrencyCode).Trim().Length == 3;
        }
    }

    [Serializable]
    public class UserSubStatusResult
    {
        public int UserId { get; set; }
        public DateTime MaxBaseEndDtFTC { get; set; }
        public virtual DateTime MaxBaseEndDt { get; set; }
        public virtual int DefaultAcctPymtMethID { get; set; }
        public int SubUserState { get; set; }
        public bool CanPurchaseBase { get; set; }
        public bool CanPurchaseAddOn { get; set; }
        public virtual bool CanOneClickResub { get; set; }
        public virtual byte IneligibilityReason { get; set; }
        public virtual byte LastSubMonths { get; set; }
        public string LastProdFeatureList { get; set; }
        public bool CanPurchaseGuarantee { get; set; }
        public int URLCurrency { get; set; }
        public DateTime? LastFTBeginDt { get; set; }
        public bool LastSubBundled { get; set; }
        public int LastSubBundleId { get; set; }
        public DateTime? LastTakenPromoDt { get; set; }
        public bool IsInstallment { get; set; }

        /// <summary>
        /// Gets or sets the offline payment method.
        /// 
        /// Current Potential Values:
        ///     - BO  :  Boleto
        ///     - DB  :  Debito
        ///     - DM  :  DineroMail
        /// 
        /// </summary>
        /// <value>
        /// The offline payment method.
        /// </value>
        public string OfflinePaymentMethod { get; set; }


        /// <summary>
        /// Transaction ID if the last transaction is offline.
        /// </summary>
        public int? OfflinePaymentTrxID { get; set; }
        public bool IsVerified { get; set; }

        public int ReturnValue { get; set; }

        public bool IsSubscriber { get { return MaxBaseEndDtFTC >= DateTime.Now; } }


        // Sub user status evaluations

        public UserSubStatusType UserSubState => (UserSubStatusType)SubUserState;
        public bool NeverSubscribed => UserSubState.IsNeverSubscribed();
        public bool IsFreeTrialUser => UserSubState.IsFreeTrialUser();
        public bool IsExpiredFreeTrialUser => UserSubState.IsExpiredFreeTrialUser();
        public bool IsPendingOfflineSubscrber => UserSubState.IsPendingOfflineSubscrber();
        public bool IsCharterMember => UserSubState.IsCharterMember();
        public bool IsPastSubscriber => UserSubState.IsPastSubscriber();
        public bool IsFailedRenewal => UserSubState.IsFailedRenewal();
        public bool IsResignedCurentSub => UserSubState.IsResignedCurentSub();
        public bool IsIAPSubscriber => UserSubState.IsIAPSubscriber();
        public bool IsCompUser => UserSubState.IsCompUser();
        public bool IsInECheckReviewPeriod => UserSubState.IsInECheckReviewPeriod();

        public enum UserSubStatusType
        {
            Error = -1,
            InvalidUserId = 1,              // Invalid User
            NeverSubscribed = 2,            // Never Had Site Access (Not Used By Site)
            CharterMember = 3,              // Charter Member
            FreeTrial = 4,                  // in the Free Trial period (UserAccount PTD is null or in the past)
            CompAccount = 5,                // CURRENT Comp Account (Not Used By Site)
            ResignedPastSub = 6,            // EXPIRED Resigned Subscription (Not Used By Site)
            CurrentSubscriber = 7,          // CURRENT Subscription
            PastSubscriber1 = 8,            // EXPIRED Subscription Failed Renewal - Paid w/ credit card and failed renewal w/in last 60 days
            PastSubscriber2 = 9,            // EXPIRED Subscription Retry In Process - failed renewal
            ResignedCurrentSub = 10,        // CURRENT Resigned Subscription
            ResignedFreeTrial = 11,         // User resigned while in Free Trial period
            ExpiredCompAccount = 12,        // EXPIRED Comp Account
            ExpiredFreeTrial = 13,          // EXPIRED Free-Trial RESIGNED Prior To Conversion
            CurrentAipSub = 15,             // CURRENT Apple In-App active Sub
            PendingSubscription = 16,       // PENDING Subscription - Offline Payment Method
            ExpiredAipSub = 17,             // EXPIRED Subscription Apple In App
            ECheckReviewPeriod = 14,        // E-Check Review Period
            ExpiredFreeTrialOther = 101,    // All Other EXPIRED Free-Trial Scenarios
            ExpiredSubscription = 102       // All Other EXPIRED Subscription Scenarios
        }

        // Ineligibility status

        public IneligibilityReasonType SubIneligibilityReason => (IneligibilityReasonType)IneligibilityReason;
        public bool IneligibilityReasonIsOfflinePayment => SubIneligibilityReason.IneligibilityReasonIsOfflinePayment();
        public bool IsEligibleToPurchaseSubscription => SubIneligibilityReason.IsEligibleToPurchaseSubscription();
        public bool IsEligibleForSaveOffers => SubIneligibilityReason.IsEligibleForSaveOffers();

        public enum IneligibilityReasonType : byte
        {
            Eligible = 0,
            InvalidUserId = 1,
            ExceedStackedSubRule = 2,
            Failed15MinuteRule = 3,
            UserInFreeTrial = 4,
            ActiveGuaranteeSubscription = 5,
            ReviewDelyCapture = 6,          // Do not allow purchase of base feature
            OfflinePaymentMethod = 8,       // Do not allow purchase of base feature
        }

        public bool IsOfflineDirectDebit
        {
            get
            {
                return (OfflinePaymentMethod + "").Equals(PaymentDefinitions.CardTypes.Debito, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public SubscriptionPackageTypeEnum GetLastSubPackageType() => SubscriptionPackageTypeHelpers.GetProductSubscriptionPackageType(LastSubMonths, LastSubBundleId);
        public byte LastSubPackageType => (byte)GetLastSubPackageType();

    }





    public class UserStatusDateRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public DateTime? StatusDt { get; set; }
    }

    [Serializable]
    public class UserStatusDateResult
    {
        public int ReturnValue { get; set; }
        public DateTime StatusDate { get; set; }
    }

    public class UserSubStatusMessageRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class UserSubStatusMessageResult
    {
        public int ReturnValue { get; set; }
        public long SubscriberStatusMask { get; set; }
        public byte BaseFeaturePurchaseCnt { get; set; }
        public byte LastBaseFeatureRenCnt { get; set; }
    }

    public class UserFeatureRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class UserFeatureResult
    {
        public int ReturnValue { get; set; }
        public long Features { get; set; }
        public long ProdAttributeMask { get; set; }
        public DateTime MaxBaseEndDt { get; set; }
    }

    [Serializable]
    public class DisplayUserStatusRequest : IApiRequestWithQueryString, IResetableCacheRequest
    {
        public int UserId { get; set; }
        public bool ResetCache { get; set; }
    }

    [Serializable]
    public class DisplayUserStatusResponse
    {
        public IList<DisplayUserStatusResult> DisplayUserStatus { get; set; }
        public IList<DisplayInstallmentsScheduleResult> DisplayInstallmentsSchedule { get; set; }

        public DisplayUserStatusResponse()
        {
            DisplayUserStatus = new List<DisplayUserStatusResult>();
            DisplayInstallmentsSchedule = new List<DisplayInstallmentsScheduleResult>();
        }
    }

    [Serializable]
    public class DisplayInstallmentsScheduleResult
    {
        public long PaymentSchedule { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string ISOCurrencyCode { get; set; }
        public byte Status { get; set; }
        public decimal AmountWithTax { get; set; }
        public int InstallmentTypeID { get; set; }

        public DisplayInstallmentsScheduleResult()
        {
            ISOCurrencyCode = "USD";
        }
    }

    [Serializable]
    public class DisplayUserStatusResult
    {
        public int ReturnValue { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public string Descr { get { return Description; } set { Description = value; } }
        public decimal SalesPrice { get; set; }
        public decimal RenewalPrice { get; set; }
        public DateTime BeginDate { get; set; }
        [JsonIgnore]
        public DateTime BeginDt { get { return BeginDate; } set { BeginDate = value; } }
        public DateTime EndDate { get; set; }
        [JsonIgnore]
        public DateTime EndDt { get { return EndDate; } set { EndDate = value; } }
        public bool? Billable { get; set; }
        public bool Renewable { get; set; }
        public int AcctDtlID { get; set; }
        public bool LinksToFreeTrial { get; set; }
        public bool IsBaseFeature { get; set; }
        public bool RequiresBaseFeature { get; set; }
        public short ProdDtlID { get; set; }
        public short RenewalCnt { get; set; }
        public long ProdAttributeMask { get; set; }
        public DateTime CancelDate { get; set; }
        [JsonIgnore]
        public DateTime CancelDt { get { return CancelDate; } set { CancelDate = value; } }
        public string ISOCurrencyCode { get; set; }
        public string CurrencyCulture { set; get; }
        public byte ProdFeatureID { get; set; }
        [JsonIgnore]
        public byte luProdFeatureID { get { return ProdFeatureID;  } set { ProdFeatureID = value; } }
        public byte Months { get; set; }
        public int? OfflinePaymentMethodID { get; set; }
        public string ExternalProductId { get; set; }
        public int LastTrxID { get; set; }
    }
}
