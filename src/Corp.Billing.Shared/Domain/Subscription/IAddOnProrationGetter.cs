﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IAddOnProrationGetter : IResetableCache<int>
    {
        GetFeatureDaysToProrateResult GetFeatureDaysToProrate(GetFeatureDaysToProrateRequest request);
    }
}
