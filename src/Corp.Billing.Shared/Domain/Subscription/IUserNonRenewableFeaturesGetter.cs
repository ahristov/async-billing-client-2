﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserNonRenewableFeaturesGetter : IResetableCache<int>
    {
        GetUserNonRenewableFeaturesResult GetUserNonRenewableFeatures(GetUserNonRenewableFeaturesRequest request);
    }
}
