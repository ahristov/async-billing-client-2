﻿using System;

namespace Corp.Billing.Shared.Domain.Subscription
{
    /// <summary>
    /// Holds one item from the order detail table.
    /// </summary>
    /// <remarks>
    /// See: Corp.Billing.Data.DataAccess.Subscription.OrderCreateRepo.CreateOrder(), not all properties get passed to the database.
    /// </remarks>
    [Serializable]
    public class OrderDetailTable
    {
        public int ProdId { get; set; }
        public int ProdBundleId { get; set; }
        public byte ProdFeatureId { get; set; }
        public DateTime BeginDate { get; set; }
        public bool DelayCapture { get; set; }
        public short InitialDays { get; set; }
        public decimal SalesPrice { get; set; }
        public short RenewalDays { get; set; }
        public decimal RenewalPrice { get; set; }
        public decimal FullPrice { get; set; }
        public string ISOCurrencyCode { get; set; }

        public bool IsFreeTrial { get; set; }

        public byte? Months { get; set; }
        public int? Weeks { get; set; }
        public bool BundlePreOrdered { get; set; }

        const byte serviceFeeFeatureId = 24;
        public bool IsServiceFee
        {
            get { return this.ProdFeatureId == serviceFeeFeatureId; }
        }

    }
}
