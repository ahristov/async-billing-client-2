﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{

    public class OrderValidateRequest
    {
        public int UserId { get; set; }
        public int PaymentMethodId { get; set; }
        public IList<OrderDetailTable> OrderDetailTable { get; set; }

        public Guid? Sid { get; set; }
        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }

        public int EventId { get; set; }
        public int OtherUserId { get; set; }
        public bool UpgradeWithRefund { get; set; }


        public OrderValidateRequest()
        {
            this.OrderDetailTable = new List<OrderDetailTable>();
        }
    }

    public enum OrderValidateResultType
    {
        SqlException = -1,
        Success = 0,

        // Unable To Process Order
        MissingOrInvalidInputParameters = 10,
        OrderDetailWasNotProvidedForValidation = 20,

        // Failed To Validate Order
        InvalidEventIdSpecified = 25,
        InvalidMatchMeUserPair = 30,
        UnratedMatchMeExists = 40,
        RatedMatchMeExistsWithinLast30Days = 50,
        OneOrMoreActiveOrUnclaimedExists = 60,
        OneOrMoreUnresolvedOrCommErrorExists = 70,
        AddOnOnlyOrderIncludesCurrentlyProvisioned = 80,
        OneOrMoreAddOnRequiresProvisionedBase = 85,
        PaymentMethodHasBeenBlocked = 90,
        PaymentMethodFlaggedAsInactiveOrExpired = 100,
        PaymentMethodUsedToProcessMultipleOrdersLast14Days = 110,
        DuplicateOfPreviouslySubmittedOrder = 120,
        UnableToAcquireLock = 130,

        // Base feature purchase is not allowed
        UserIDNotSpecified = 201,
        BaseFeatureStackedSubExists = 202,
        UserPurchasedBaseFeatureWithinLast15Minutes = 203,
        UserHasTransactionInProcess = 204,
        UserHasActiveSixMonthGuaranteeSubscription = 205,
        UserHasBaseFeatureInDelayCaptureState = 206,
        UserIsAnActiveIAPSubscriber = 207,
        UserHasPendingSubscriptionOfOfflinePayment = 208,

    }

    public class OrderValidateResult
    {
        public Guid? LockId { get; set; }

        public int ReturnValue { get; set; }

        public OrderValidateResultType ValidateResult
        {
            get { return (OrderValidateResultType) ReturnValue; }
        }
    }

}
