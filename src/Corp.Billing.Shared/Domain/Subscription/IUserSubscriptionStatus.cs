﻿using System.Collections.Generic;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.UserFeatures;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserSubscriptionStatus
    {
        List<DisplayUserStatusResult> GetUserStatusBy(int userId);
        List<DisplayUserStatusResult> GetDisplayStatus(DisplayUserStatusRequest request);
        UserPromoInfoResult GetUserStatusPromoInfo(UserPromoInfoRequest request);
        UserSubStatusResult GetUserSubscritionStatus(UserSubStatusRequest request);
        UserSubStatusMessageResult GetUserSubStatusMessage(UserSubStatusMessageRequest request);
        UserStatusDateResult GetUserStatusDate(UserStatusDateRequest request);
        UserStatusDateResult GetUserStatusDateV1(UserStatusDateRequest request);
        UserFeatureResult GetUserFeatures(UserFeatureRequest request);
        UserFeaturesLightResult GetUserFeaturesLight(UserFeaturesLightRequest request);
        CancelResult CancelUser(CancelRequest request);
    }

    public interface IUserSubscriptionStatusV2
    {
        DisplayUserStatusResponse GetDisplayStatusV2(DisplayUserStatusRequest request);
    }
}
