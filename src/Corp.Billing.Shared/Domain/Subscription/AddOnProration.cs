﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public class GetFeatureDaysToProrateRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }


    [Serializable]
    public class GetFeatureDaysToProrateResult
    {
        public int ReturnValue { get; set; }

        public IList<GetFeatureDaysToProrateItem> Items;
        public IList<GetFeatureDaysToProrateRefundItem> RefundItems;


        public GetFeatureDaysToProrateResult()
        {
            Items = new List<GetFeatureDaysToProrateItem>();
            RefundItems = new List<GetFeatureDaysToProrateRefundItem>();
        }
    }

    [Serializable]
    public class GetFeatureDaysToProrateItem
    {
        public byte FeatureId { get; set; }
        public DateTime? PaidThroughDate { get; set; }
        public byte PackageMonths { get; set; }
        public short PackageDays { get; set; }
        public short RenewalDays { get; set; }
        public int DaysToProrate { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte? CancelTypeId { get; set; }
        public string ISOCurrencyCode { get; set; }
        public decimal? LastPaymentPrice { get; set; }
        public bool IsLastPaymentPriceDiscounted { get; set; }
        public decimal? NextPaymentPrice { get; set; }
        public bool IsNextPaymentPriceDiscounted { get; set; }

    }

    [Serializable]
    public class GetFeatureDaysToProrateRefundItem
    {
        public byte luProdFeatureID { get; set; }
        public decimal? RefundAmt { get; set; }
        public short? RefundDays { get; set; }
    }
}
