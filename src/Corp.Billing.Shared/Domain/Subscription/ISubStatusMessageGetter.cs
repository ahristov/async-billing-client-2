﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface ISubStatusMessageGetter : IResetableCache<int>
    {
        UserSubStatusMessageResult SubStatusMessageV0(UserSubStatusMessageRequest request);
    }
}
