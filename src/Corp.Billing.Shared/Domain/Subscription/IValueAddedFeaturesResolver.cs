﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IValueAddedFeaturesResolver
    {
        IList<ValueAddedFeaturesTable> ResolveValueAddedFeatures(int userId, IList<OrderDetailTable> orderDetailTable);
    }
}
