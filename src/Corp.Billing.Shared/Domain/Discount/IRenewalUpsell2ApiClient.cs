﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public interface IRenewalUpsell2ApiClient
    {
        RenewalUpsell2Result ApplyRenewalUpsell(RenewalUpsell2Request request);
    }
}
