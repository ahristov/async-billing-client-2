﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public interface IRenewalUpsellV2ApiClient
    {
        GetRenewalUpsellV2Result GetRenewalUpsell(GetRenewalUpsellV2Request request);
        ApplyRenewalUpsellV2Result ApplyRenewalUpsell(ApplyRenewalUpsellV2Request request);
    }
}
