﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public interface IRenewalUpsellApiClient
    {
        GetRenewalUpsellResult GetRenewalUpsell(GetRenewalUpsellRequest request);
        ApplyRenewalUpsellResult ApplyRenewalUpsell(ApplyRenewalUpsellRequest request);
    }
}
