﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public enum RenewalUpsellResponseCodeType
    {
        Undefined,
        Success,
        Error,
    }
}
