﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Discount
{
    // Base classes and compositions


    [Serializable]
    public class RenewalAmount
    {
        public decimal RenewalPrice { get; set; }
        public decimal RenewalTax { get; set; }
        public decimal RenewalCost { get; set; }
    }

    [Serializable]
    public class RenewalAmounts
    {
        public RenewalAmount OldRenewalPrice { get; set; }
        public RenewalAmount NewRenewalPrice { get; set; }

        public bool HasTax { get; set; }

        public RenewalAmounts()
        {
            OldRenewalPrice = new RenewalAmount();
            NewRenewalPrice = new RenewalAmount();
        }
    }


    public class RenewalDiscountRequestBase
    {
        public int UserId { get; set; }
        public decimal PercentDiscount { get; set; }
    }

    [Serializable]
    public class RenewalDiscountResultBase
    {
        // Common properties

        public byte PackageMonths { get; set; }
        public RenewalAmounts DiscountAmounts { get; set; }
        public string ISOCurrencyCode { get; set; }

        /// <summary>
        /// Resignation: Includes tax; RNT: Includes tax
        /// </summary>
        public decimal NewRenewalPrice { get; set; }

        /// <summary>
        /// Resignation: Does not include tax; RNT: Includes tax
        /// </summary>
        public decimal OldRenewalPrice { get; set; }
        public decimal PercentDiscount { get; set; }


        public RenewalDiscountResultBase()
        {
            DiscountAmounts = new RenewalAmounts();
        }

    }


    // Get Offer at Resignation //////////////////////////////////////////////////////////////////

    public class RenewalDiscountRequest : RenewalDiscountRequestBase, IApiRequestWithQueryString
    { }


    [Serializable]
    public class RenewalDiscountResult : RenewalDiscountResultBase
    {
        public int AcctdtlId { get; set; }
        public DateTime RenewalDate { get; set; }
        public DateTime? TakeDate { get; set; }


        public RenewalDiscountResult()
        { }
    }


    // Get Offer RNT //////////////////////////////////////////////////////////////////


    public class RenewalDiscountRequestRNT : RenewalDiscountRequestBase, IApiRequestWithQueryString
    { }

    [Serializable]
    public class RenewalDiscountResultRNT : RenewalDiscountResultBase
    {
        /// <summary>
        /// 0: Got response from the SP;
        /// 1: There has been an exception with code 50000 in the SP, that means "Not eilgible"
        /// </summary>
        /// <value>
        /// The return value.
        /// </value>
        public int ReturnValue { get; set; }

        public List<RenewalDiscountItem> Items { get; set; }

        public RenewalDiscountResultRNT()
            : base()
        {
            Items = new List<RenewalDiscountItem>();
        }

        public static RenewalDiscountResultRNT RenewalDiscountResultRntForNotEligible()
        {
            return new RenewalDiscountResultRNT()
            {
                ReturnValue = 1,
            };
        }
    }

    [Serializable]
    public class RenewalDiscountItem
    {
        public int AcctDtlID { get; set; }
        public byte luProdFeatureID { get; set; }
        public byte Months { get; set; }
        public DateTime CancelDt { get; set; }
        public DateTime EndDt { get; set; }
        public decimal OldRenewalPrice { get; set; }
        public decimal NewRenewalPrice { get; set; }
    }



    // Redeem //////////////////////////////////////////////////////////////////


    public class RedeemRenewalDiscountRequest
    {
        public int UserId { get; set; }
        public decimal PercentDiscount { get; set; }
    }

    public class RedeemRenewalDiscountResult
    {
        public int ReturnValue { get; set; }
    }

    public class RedeemRenewalDiscountRequestRNT
    {
        public int UserId { get; set; }
        public decimal PercentDiscount { get; set; }
        public decimal NewRenewalPrice { get; set; }
    }

    public class RedeemRenewalDiscountResultRNT
    {
        public int ReturnValue { get; set; }
    }


    // Redeem Guarantee //////////////////////////////////////////////////////////////////

    public class RedeemGuaranteeResult
    {
        public int ReturnValue { get; set; }
    }

    public class RedeemGuaranteeRequest
    {
        public int UserID { get; set; }
        public bool RedeemGuarantee { get; set; }
        public int? EmployeeID { get; set; }
    }
}
