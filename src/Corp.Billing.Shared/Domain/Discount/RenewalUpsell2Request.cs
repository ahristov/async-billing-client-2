﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.Discount
{
    public class RenewalUpsell2Request : PCRulesRequest
    {
        public int? RateCardKey { get; set; }
        public int BaseProductId { get; set; }
        public long BundleProductMask { get; set; }
        public bool IsInstallments { get; set; }

        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }
    }
}
