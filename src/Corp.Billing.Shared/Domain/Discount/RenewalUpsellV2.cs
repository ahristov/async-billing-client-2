﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Corp.Billing.Shared.Domain.Subscription;
using System;

namespace Corp.Billing.Shared.Domain.Discount
{

    public class GetRenewalUpsellV2Request : PCRulesRequest
    {
        private int _daysBeforeRenewal;

        public int DaysBeforeRenewal
        {
            get
            {
                return (_daysBeforeRenewal <= 0)
                    ? RenewalUpsellDefaults.DefaultDaysBeforeRenewal
                    : _daysBeforeRenewal;
            }
            set { _daysBeforeRenewal = value; }
        }

        public int? RateCardKey { get; set; }

    }

    [Serializable]
    public class GetRenewalUpsellV2Result : GetRenewalUpsellResult
    {
        public GetRenewalUpsellV2Result()
        { }

        public GetRenewalUpsellV2Result(GetUserBillingDataResult billingData)
            : base(billingData)
        { }



        public int? RateCardKey { get; set; }
        public string PricingRuleId { get; set; }
        public decimal TaxRate { get; set; }
    }


    public class ApplyRenewalUpsellV2Request : PCRulesRequest
    {
        public int? RateCardKey { get; set; }

        private int _daysBeforeRenewal;
        public int DaysBeforeRenewal
        {
            get
            {
                return (_daysBeforeRenewal <= 0)
                    ? RenewalUpsellDefaults.DefaultDaysBeforeRenewal
                    : _daysBeforeRenewal;
            }
            set { _daysBeforeRenewal = value; }
        }

        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }
    }

    [Serializable]
    public class ApplyRenewalUpsellV2Result
    {
        public RenewalUpsellResponseCodeType ResponseCode { get; set; }

        public int? RateCardKey { get; set; }
        public string PricingRuleId { get; set; }
        public decimal TaxRate { get; set; }
    }


}
