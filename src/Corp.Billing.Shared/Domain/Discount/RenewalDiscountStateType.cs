﻿using System;

namespace Corp.Billing.Shared.Domain.Discount
{
    // AcceptedRenewalDiscountIsApplied
    // AcceptedRenewalDiscountIsNotAppliedYet
    // CurrentSubWithCanceledAutoRenewal

    [Flags]
    public enum RenewalDiscountStateType : int
    {
        None = 0, // 0x0000,

        /// <summary>
        /// One Or More Save Offer(s) Have Been Taken In The Past (May Not Be Current Subscription).
        /// </summary>
        AcceptedRenewalDiscountPreviously = 1,

        /// <summary>
        /// No Active Features. No discounts to return or calculate.
        /// </summary>
        NotCurrentSub = 2,

        /// <summary>
        /// User Currently Subscribed - Active Renewable Feature(s). Use this for Resignation offer.
        /// </summary>
        CurrentSubWithActiveAutoRenewal = 4,

        /// <summary>
        /// User Currently Subscribed - Active Cancelled Feature(s). Use this for RNT offer.
        /// </summary>
        CurrentSubWithCanceledAutoRenewal = 8,

        /// <summary>
        /// Renewal Discount Has Been Applied To Active Feature(s) In The Past.
        /// </summary>
        AcceptedRenewalDiscountIsApplied = 16,

        /// <summary>
        /// Previously Applied Renewal Discounts Have Not Yet Renewed.
        /// </summary>
        AcceptedRenewalDiscountIsNotAppliedYet = 32,
    }


    public static class RenewalDiscountStateTypeExtensions
    {
    }
}
