﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public enum RenewalDiscountOfferType : byte
    {
        Undefined = 0,
        ResignationDiscount = 1,
        ResignedNotTerminatedDiscount = 2,
    }
}
