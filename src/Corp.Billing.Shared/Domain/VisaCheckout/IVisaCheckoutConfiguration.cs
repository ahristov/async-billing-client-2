﻿namespace Corp.Billing.Shared.Domain.VisaCheckout
{
    public interface IVisaCheckoutConfiguration
    {
        string VisaCheckoutSharedSecret { get; }
        string VisaCheckoutApiKey { get; }

        string VisaCheckoutWalletBaseServiceUrl { get; }
        string VisaCheckoutWalletHostName { get; }

        string VisaCheckoutProxyUrl { get; }
        int VisaCheckoutTimeout { get; }
    }
}
