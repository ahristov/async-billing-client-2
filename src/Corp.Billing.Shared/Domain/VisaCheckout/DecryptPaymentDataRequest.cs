﻿namespace Corp.Billing.Shared.Domain.VisaCheckout
{
    public class DecryptPaymentDataRequest
    {
        public string EncKey { get; set; }
        public string EncPaymentData { get; set; }
    }
}
