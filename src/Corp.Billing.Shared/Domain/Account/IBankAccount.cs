﻿using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Account
{
    public interface IBankAccount
    {
        Banks Bank { get; set; }
        int BankNumber { get; }
        string BankBranch { get; set; }
        string BankAccount { get; set; }
        string BankAccountCheckDigit { get; set; }
        string Cpf { get; set; }
    }
}
