﻿using Corp.Billing.Shared.ProxyContracts;
using System;

namespace Corp.Billing.Shared.Domain.Account
{
    public static class BankAccountParsers
    {
        public static string ToAccountNumberString(this IBankAccount bankAccount)
        {
            var accountNumber = String.Concat(
                bankAccount.BankNumber, "|",
                bankAccount.BankBranch, "|",
                bankAccount.BankAccount, "|",
                bankAccount.BankAccountCheckDigit, "|",
                bankAccount.Cpf);

            return accountNumber;
        }

        public static Boolean ParseAccountNumber(this IBankAccount bankAccount, string accountNumber)
        {
            string[] accountData = (accountNumber ?? string.Empty).Split('|');
            if (accountData.Length > 0)
            {
                int bankNo;
                if (int.TryParse(accountData[0], out bankNo))
                    bankAccount.Bank = (Banks)bankNo;
                else
                    return false;

                if (accountData.Length > 1)
                    bankAccount.BankBranch = accountData[1];
                if (accountData.Length > 2)
                    bankAccount.BankAccount = accountData[2];
                if (accountData.Length > 3)
                    bankAccount.BankAccountCheckDigit = accountData[3];
                if (accountData.Length > 4)
                    bankAccount.Cpf = accountData[4];

                return true;
            }

            return false;
        }
    }
}
