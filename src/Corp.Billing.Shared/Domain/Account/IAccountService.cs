﻿using Corp.Billing.Shared.ApiClient.Account;
using System;
using System.Collections.Generic;
namespace Corp.Billing.Shared.Domain.Account
{
    public interface IAccountService
    {
        // Payment 

        AcctPaymentMethResult GetAccountPaymentMethod(AcctPaymentMethRequest request);
        GetPaymentMethodResult GetAccountPaymentMethodV2(GetPaymentMethodRequest request);
        AcctPaymentMethodUpdateResult UpdateAccountPaymentMethod(AcctPaymentMethodUpdateRequest request);
        AcctPaymentMethodUpdateResult UpdateAccountPaymentMethodV2(AcctPaymentMethodUpdateRequest request);
        UpdatePaymentMethodFirstAndLastNameResult UpdatePaymentMethodFirstAndLastName(UpdatePaymentMethodFirstAndLastNameRequest request);
        UserPaymentInfoResult GetUserPaymentInfo(UserPaymentInfoRequest request);

        // Lock

        LockAccountResult LockAccount(LockAccountRequest request);
        ReleaseAccountLockResult ReleaseAccountLock(ReleaseAccountLockRequest request);

        // Bill Credit
        BilCreditResponse BilCredit(BilCreditRequest request);

        void PurgeAccount(int accountID, DateTime markedForPurgeAt);
        void PurgeAccount(int accountID, int newAccountID, DateTime markedForPurgeAt);
        void PurgeAccountVersion(int accountID, int encryptionVersion);

        Dictionary<string, string> GetExternalTokens(int accountID);
        int Register(int accountTypeID, string accountNumber);
        void ReRegister(int accountID);
        IEnumerable<AccountPurgeItem> GetAccountsPendingPurge(int? batchSize);
        IEnumerable<AccountAction> GetPendingAccountActions(int? batchSize);

    }
}
