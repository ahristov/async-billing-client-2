﻿namespace Corp.Billing.Shared.Domain.Account
{
    public class BilCreditRequest: IApiRequest
    {
        public int? UserId { get; set; }
        public short? CRCode { get; set; }
        public short CompDays { get; set; }
        public int? EmployeeId { get; set; }
        public string TrxSrc { get; set; }
        public string Comment { get; set; }
        public bool IsDebit { get; set; }
        public bool ReadOnly { get; set; }
    }
}
