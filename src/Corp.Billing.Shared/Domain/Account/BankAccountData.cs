﻿using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Account
{
    public class BankAccountData : IBankAccount
    {
        public Banks Bank { get; set; }

        public int BankNumber
        {
            get { return (int)Bank; }
        }

        public string BankBranch { get; set; }
        public string BankAccount { get; set; }
        public string BankAccountCheckDigit { get; set; }
        public string Cpf { get; set; }
    }
}
