﻿using Corp.Billing.Shared.ApiClient.Files;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Core.Services.Files
{
    public interface IFileApiClient
    {
        void Push(Uri destination, string contents, System.Text.Encoding encoding);
        void Push(Uri destination, string contents);
        void PushAndRename(Uri destination, Uri renameDestination, string contents, System.Text.Encoding encoding);
        void PushAndRename(Uri destination, Uri renameDestination, string contents);

        PulledFile<TAccount> Pull<TAccount>(Uri source, ReplacementOptions options);
        PulledFile<TAccount> Pull<TAccount>(Uri source);
        void Move(Uri source, Uri destination);
        void Copy(Uri source, Uri destination);
        void Delete(Uri source);
        IEnumerable<FileListingItem> GetDirectoryListing(Uri directory);
    }
}
