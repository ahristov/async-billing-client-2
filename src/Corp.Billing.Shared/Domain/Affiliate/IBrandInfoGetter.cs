﻿namespace Corp.Billing.Shared.Domain.Affiliate
{
    public interface IBrandInfoGetter
    {
        BrandInfoResult GetBrandInfo(BrandInfoRequest request);
    }
}
