﻿namespace Corp.Billing.Shared.Domain.Affiliate
{
    public class BrandInfoRequest : IApiRequestWithQueryString
    {
        public int BrandId { get; set; }
    }

    public class BrandInfoResult : IApiResponse
    {
        public int ReturnValue { get; set; }
        public int BillingId { get; set; }
        public string Channel { get; set; }
        public string ChannelName { get; set; }
        public string BrandNonBrandRollUp { get; set; }
        public byte LowConvertingChannel { get; set; }
    }

}

