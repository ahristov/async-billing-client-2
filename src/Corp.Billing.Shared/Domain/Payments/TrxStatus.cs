﻿namespace Corp.Billing.Shared.Domain.Payments
{
    public enum TrxStatus
    {
        NotProcessed = 0,
        Success = 1,
        Failure = 2,
        InRenewal = 3,
        InRefundCommFailure = 4,
        InDelayCapture = 5,
        InCommErrorRetry = 7,
        SystemCancelled = 8,
        ECheckqueue = 9,
        ReviewDelayCapture = 10,
        NewSubFailRetry = 13,
        PendingOfflineTransfer = 14
    }
}
