﻿namespace Corp.Billing.Shared.Domain.Payments.Transactions
{
    /// <summary>
    /// Duplicate of Services.Common.Payment.Entities.TransactionStatus
    /// </summary>
    public enum TransactionStatus
    {
        Unknown = 0,
        Approved = 1,
        Declined = 2
    }
}
