﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.Domain.Payments.Transactions
{
    public interface ITransactionsApiClient
    {
        IEnumerable<JObject> SearchByProcessorReference(int processorID, string processorReference);
    }
}
