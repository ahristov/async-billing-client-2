﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Payments
{
    public interface IBoletoPaymentService
    {
        OfflinePaymentResponseMessage Authorize(OfflinePaymentRequestMessage request);
        OfflinePaymentResponseMessage ProcessPayment(OfflinePaymentRequestMessage request);
        string GetInfo(int trxId);
    }
}
