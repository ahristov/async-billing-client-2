﻿using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Payments
{
    public interface IPaymentProxyService
    {
        PaymentProxyResponseMessage ProcessPayment(PaymentProxyRequestMessage request);
    }
}
