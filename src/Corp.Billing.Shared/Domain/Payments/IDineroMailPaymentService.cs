﻿using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public interface IDineroMailPaymentService
    {
        OfflinePaymentResponseMessage Authorize(OfflinePaymentRequestMessage request);
        OfflinePaymentResponseMessage ProcessPayment(DineroMailOfflinePaymentRequestMessage request);
        OfflinePaymentResponseMessage ProcessPaymentNotification(OfflinePaymentRequestMessage request);
    }
}