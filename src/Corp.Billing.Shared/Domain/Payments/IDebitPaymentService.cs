﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Corp.Billing.Shared.ProxyContracts;
using Corp.Billing.Shared.Domain.Orders;

namespace Corp.Billing.Shared.Domain.Payments
{
    public interface IDebitPaymentService
    {
        OfflinePaymentResponseMessage Authorize(DebitOfflinePaymentRequestMessage request);
        OfflinePaymentResponseMessage ProcessPayment(OfflinePaymentRequestMessage request);
        SearchForDebitPendingTrxResult GetPendingTransactions(DateTime startAt, DateTime endAt, int bank);
    }
}
