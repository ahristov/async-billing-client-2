﻿using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Payments
{
    public interface IAMWSPaymentApiClient
    {
        AMWSPaymentResponseMessage Consent(AMWSPaymentRequestMessage request);
    }
}
