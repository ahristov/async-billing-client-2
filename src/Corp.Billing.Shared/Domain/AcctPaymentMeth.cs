﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain
{
    public class AcctPaymentMethRequest : IApiRequestWithQueryString
    {
        public int AcctPymtMethID { get; set; }
    }
    public class AcctPaymentMethResult
    {
        public int ReturnValue { get; set; }
        public string  BaseCode { get; set; }
		public string DtlCode { get; set; }
		public DateTime? CreateDt { get; set; }
        public DateTime? InactiveDt { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string FullName { get; set; }
        public string  Address1 { get; set; }
        public string  Address2 { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string PostalCode { get; set; }
        public string AcctNum { get; set; }
        public byte CCMonth { get; set; }
        public short CCYear { get; set; }
        public string CheckNum { get; set; }
        public string DriverLic { get; set; }
        public string DLStateAbv { get; set; }
        public string DLCountryAbv { get; set; }
        public int EmployeeID { get; set; }
        public string CardStartDt { get; set; }
        public string CardIssueNum { get; set; }
        public string PhoneNumberClear { get; set; }
    }

    public class AcctPaymentMethodUpdateRequest
    {
        public int AcctPymtMethId { get; set; }
        public string DtlCode { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }

        public string PostalCode { get; set; }
        public byte? CcMonth { get; set; }
        public short? CcYear { get; set; }
        public string CheckNum { get; set; }
        public string DriverLic { get; set; }
        public string DlStateAbv { get; set; }
        public string DlCountryAbv { get; set; }
        public int EmployeeId { get; set; }
        public string AcctNumClear { get; set; }
        // @WriteDefault
        public string CardStartDt { get; set; }
        public string CardIssueNum { get; set; }
        public string PhoneNumberClear { get; set; }
        // @OverrideValues
    }

    public class AcctPaymentMethodUpdateResult
    {
        public int ReturnValue { get; set; }
    }

    public class BillingAddressRequest
    {
        public int PaymentMethodID { get; set; }
    }

    public class BillingAddressResult
    {
        public int ReturnValue { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string StateAbbreviation { get; set; }
        public string CountryAbbreviation { get; set; }
        public string PostalCode { get; set; }
    }

    public class UserPaymentInfoRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    public class UserPaymentInfoResult
    {
        public int ProcessorLogId { get; set; }
        public byte OrderStatus { get; set; }
        public string PaymentReference { get; set; }
        public DateTime? StatusDate { get; set; }
        public string SwiftCode { get; set; }
        public string SpecialId { get; set; }
        public string AdditionalReference { get; set; }
        public string AccountHolder { get; set; }
        public string ExternalReference { get; set; }
        public string BankName { get; set; }
        public byte? EffortId { get; set; }
        public byte? AttemptId { get; set; }
        public string CountryDescription { get; set; }
        public string City { get; set; }
        public string BankAccountNumber { get; set; }
        public string FormActionURL { get; set; }
        public decimal? OrderAmount { get; set; }
        public int ReturnValue { get; set; }
    }


    public class GetPaymentMethodRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public int? PaymentMethodId { get; set; }
    }

    public class GetPaymentMethodResult
    {
        public int ReturnValue { get; set; }

        public IList<GetPaymentMethodItem> Items { get; set; }

        public GetPaymentMethodResult()
        {
            Items = new List<GetPaymentMethodItem>();
        }
    }

    public class GetPaymentMethodItem
    {
        public int PaymentMethodId { get; set; }
        public bool IsDefaultPaymentMethod { get; set; }
        public bool IsActivePaymentMethod { get; set; }
        public int PaymentMethodToken { get; set; }
        public string PaymentMethodTypeDescr { get; set; }
        public string BaseCode { get; set; }
        public string DtlCode { get; set; }
        public string BankIdentification { get; set; }
        public string AccountNumberLastFour { get; set; }
        public byte ExpirationMonth { get; set; }
        public short ExpirationYear { get; set; }
        public string LastCheckNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string PostalCode { get; set; }
    }


    public class BoletoInfoResult
    {
        public string FullName { get; set; }
        public decimal OrderAmount { get; set; }
        public DateTime ExpireDate { get; set; }
        public int ReturnValue { get; set; }

    }

    public class BoletoInfoRequest
    {
        public int TrxId { get; set; }


    }

    public class UpdateOfflinePaymentStatusRequest
    {
        public int TrxId { get; set; }
        public int TrxStatusId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class UpdateOfflinePaymentStatusResponse
    {
        public int ReturnValue { get; set; }

    }
}
