﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.UserVerifications
{
    public interface IVerifiedMachinesGetter: IResetableCache<int>
    {
        GetVerifiedMachinesResult GetVerifiedMachines(int userId);
    }
}
