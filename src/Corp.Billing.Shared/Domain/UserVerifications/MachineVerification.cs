﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.UserVerifications
{
    [Serializable]
    public class GetVerifiedMachinesRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class GetVerifiedMachinesResult
    {
        public IList<VerifiedMachineData> VerifiedMachines { get; set; }
        public int DbReturnValue { get; set; }

        public GetVerifiedMachinesResult()
        {
            VerifiedMachines = new List<VerifiedMachineData>();
        }
    }

    [Serializable]
    public class VerifiedMachineData
    {
        public Guid MachineId { get; set; }
        public int VerifyMethodId { get; set; }
    }

}
