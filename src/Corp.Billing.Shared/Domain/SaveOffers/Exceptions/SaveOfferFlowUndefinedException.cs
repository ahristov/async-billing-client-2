﻿using Corp.Billing.Shared.Domain.SaveOffers.Services;

namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public class SaveOfferFlowUndefinedException : BaseSaveOfferException
    {
        public SaveOfferFlowUndefinedException() : base() {}
        public SaveOfferFlowUndefinedException(BaseSaveOfferRequest request) : base(request) { }
        public override int ErrorCode { get { return (int)ExceptionErrorCodes.SaveOfferFlowUndefined; } }
        public override string ErrorMessage { get { return string.Format("SaveOffers: Save offer flow undefined"); } }

    }
}
