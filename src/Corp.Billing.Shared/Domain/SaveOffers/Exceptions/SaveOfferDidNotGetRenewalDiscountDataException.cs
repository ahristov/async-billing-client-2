﻿using Corp.Billing.Shared.Domain.SaveOffers.Services;

namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public class SaveOfferDidNotGetRenewalDiscountDataException : BaseSaveOfferException
    {
        public SaveOfferDidNotGetRenewalDiscountDataException() : base() { }
        public SaveOfferDidNotGetRenewalDiscountDataException(BaseSaveOfferRequest request) : base(request) { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.DidNotGetRenewalDiscountData; } }
        public override string ErrorMessage { get { return string.Format("SaveOffers: Did not get renewal discount message from DB"); } }
    }
}
