﻿using Corp.Billing.Shared.Domain.SaveOffers.Services;

namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public class SaveOfferUserNotFoundException : BaseSaveOfferException
    {
        public SaveOfferUserNotFoundException() : base() { }
        public SaveOfferUserNotFoundException(BaseSaveOfferRequest request) : base(request) { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.UserNotFoundError; } }
        public override string ErrorMessage { get { return string.Format("SaveOffers: User not found"); } }
    }
}
