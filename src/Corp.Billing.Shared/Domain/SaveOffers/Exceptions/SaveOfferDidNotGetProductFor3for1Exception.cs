﻿using Corp.Billing.Shared.Domain.SaveOffers.Services;

namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public class SaveOfferDidNotGetProductFor3for1Exception : BaseSaveOfferException
    {
        public SaveOfferDidNotGetProductFor3for1Exception() : base() { }
        public SaveOfferDidNotGetProductFor3for1Exception(BaseSaveOfferRequest request) : base(request) { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.DidNotGetProductFor3for1; } }
        public override string ErrorMessage { get { return string.Format("SaveOffers: Rate card did not return product for 3for1 offer"); } }
    }
}