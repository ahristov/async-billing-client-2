﻿namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public enum ExceptionErrorCodes
    {
        Undefined = 0,

        UserNotFoundError = 1010,
        SaveOfferInitializationError = 1020,
        SaveOfferFlowUndefined = 1030,
        DidNotGetRenewalDiscountData = 1040,
        DidNotGetProductFor3for1 = 1050,
    }
}
