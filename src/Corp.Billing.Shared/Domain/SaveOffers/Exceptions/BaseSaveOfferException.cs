﻿using Corp.Billing.Shared.Domain.SaveOffers.Services;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public abstract class BaseSaveOfferException : Exception
    {
        public BaseSaveOfferException()
        { }

        public BaseSaveOfferException(BaseSaveOfferRequest request)
        {
            Request = request;
        }

        public BaseSaveOfferRequest Request { get; set; }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
