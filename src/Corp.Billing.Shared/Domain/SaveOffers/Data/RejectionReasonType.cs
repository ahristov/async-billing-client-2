﻿namespace Corp.Billing.Shared.Domain.SaveOffers.Data
{
    public enum RejectionReasonType : int
    {
        Undefined,

        SiteCodeNotEligible,
        CountryCodeNotEligible,
        NotCurrentSubscriber,
        NeverSubscribed,
        LastPackageMonthsNotEligibleForSaveOffer,
        CannotPurchaseBase,
        StatusNotEligibleForSaveOffers,
        AutoRenewalIsOff,
        AutoRenewalIsOn,
        BaseFeaturePurchaseCntIsNotOne,
        DidNotFindAppropriateBaseFeature,
        ExpirationDateIsInThePast,
        ExpirationDateTooFar,
        CurrentSubPackageLengthDoesNotQualify,
        CannotOneClickPurchaseStackSub,
        LastBaseFeatureRenewedModeThanOnce,
        NotEligibleFor3For1DiscountPromo,
        PriceGroupAppliesToRenewals,
        LastBaseFeatureHasRenewed,
        DiscountMessagingPriceIsNotValid,
        DiscountMessagingShowsIsNotEligible,
        AlreadyAcceptedRenewalDiscountOffer,
        InvalidRequestRateCardKey,
        InvalidPostalCode,
        ElitePackageCurrentSubscriberDoesNotQualifiForDiscountedRenewals,
        IsInstallmentSubscriber,
    }
}
