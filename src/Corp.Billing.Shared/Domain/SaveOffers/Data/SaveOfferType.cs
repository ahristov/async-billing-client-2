﻿namespace Corp.Billing.Shared.Domain.SaveOffers.Data
{
    public enum SaveOfferType
    {
        Undefined,
        SaveOffer3for1,
        SaveOfferRenewalDiscount,
    }
}
