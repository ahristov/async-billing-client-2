﻿namespace Corp.Billing.Shared.Domain.SaveOffers.Data
{
    public enum SaveOfferFlowType
    {
        Undefined,
        ResignationFlow,
        TerminationFlow
    }
}
