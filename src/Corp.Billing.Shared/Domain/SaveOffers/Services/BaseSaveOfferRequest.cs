﻿using System;

namespace Corp.Billing.Shared.Domain.SaveOffers.Services
{
    [Serializable]
    public abstract class BaseSaveOfferRequest
    {
        public int UserId { get; set; }
        public int RateCardKey { get; set; }
    }
}
