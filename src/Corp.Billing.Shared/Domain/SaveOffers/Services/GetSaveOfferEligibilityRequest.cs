﻿using Corp.Billing.Shared.Domain.SaveOffers.Data;
using System;

namespace Corp.Billing.Shared.Domain.SaveOffers.Services
{
    [Serializable]
    public class GetSaveOfferEligibilityRequest : BaseSaveOfferRequest, IApiRequestWithQueryString
    { }
}
