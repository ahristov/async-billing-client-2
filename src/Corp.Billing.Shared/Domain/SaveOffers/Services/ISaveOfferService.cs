﻿using Corp.Billing.Shared.Domain.SaveOffers.Data;

namespace Corp.Billing.Shared.Domain.SaveOffers.Services
{
    public interface ISaveOfferService
    {
        GetSaveOfferEligibilityResult GetSaveOfferEligibility(GetSaveOfferEligibilityRequest request, SaveOfferFlowType saveOfferFlow);
    }
}
