﻿using System.Reflection;

namespace Corp.Billing.Shared.Domain.Products.IneligibilityReasons
{
    public static class IneligibilityReasonTools
    {
        public static PowerUps.Data.PowerUpPurchaseIneligibilityType? GetPowerUpPurchaseIneligibilityType(
            this ImpulseFeatures.Data.PurchaseIneligibilityReasonType impulseFeatureIneligibilityReason)
        {
            FieldInfo fi = impulseFeatureIneligibilityReason.GetType().GetField(impulseFeatureIneligibilityReason.ToString());
            CorrespondingPowerUpIneligibilityAttribute[] attributes =
                (CorrespondingPowerUpIneligibilityAttribute[])fi.GetCustomAttributes(typeof(CorrespondingPowerUpIneligibilityAttribute), false);

            return (attributes?.Length > 0)
                ? attributes[0].PurchaseIneligibility
                : (PowerUps.Data.PowerUpPurchaseIneligibilityType?)null;
        }
    }
}
