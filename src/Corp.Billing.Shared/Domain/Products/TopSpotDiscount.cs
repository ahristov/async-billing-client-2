﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.Domain.Products
{
    public class TopSpotDiscount
    {
        public class GetTopSpotDiscountRequest
        {
            public int UserId { get; set; }
        }

        public class GetTopSpotDiscountResult
        {
            public bool Eligible { get; set; }
        }

    }
}
