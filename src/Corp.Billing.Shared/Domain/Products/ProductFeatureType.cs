﻿using Corp.Billing.Shared.Domain.Products.ProductFeatures;

namespace Corp.Billing.Shared.Domain.Products
{
    public enum ProductFeatureType : byte
    {
        Undefined = 0,
        EmailCommunication = 1,
        ActiveProfile = 2,
        HighlightedProfile = 3,
        DateCoach = 4,
        FirstImpressions = 5,
        Voice = 6,
        PayForResponse = 7,
        MindFindBind = 8,
        [CanSaleAsPowerUp]
        EmailReadNotification = 9,
        MindFindBindSavings = 10,
        PlatinumServicesSavings = 11,
        [CanSaleAsPowerUp]
        MatchPhone = 12,
        MatchMobile = 13,
        SugarDaddyEmailToken = 14,
        RemoteApplicationEmailToken = 15,
        ProfileConsultingStandAloneProduct = 16,
        ProfileConsultingSubscriptionRequired = 17,
        Platinum = 18,
        PlatinumPackageSavings = 19,
        ChemistrySubscription = 20,
        [FeatureProductId(18070)]
        MatchEventMemberTickets = 21,
        [FeatureProductId(18071)]
        MatchEventGuestTicketsMale = 22,
        [FeatureProductId(18072)]
        MatchEventGuestTicketsFemale = 23,
        ActivationFee = 24,
        PremiumBundleSavings = 25,
        [CanSaleAsPowerUp][IsImpulseFeature(ImpulseFeatures.Data.ImpulseFeatureType.TopSpot)]
        TopSpot = 26,
        [CanSaleAsPowerUp][IsImpulseFeature(ImpulseFeatures.Data.ImpulseFeatureType.Undercover)]
        Stealth = 27,
        [CanSaleAsPowerUp]
        ReplyForFree = 28,
        [CanSaleAsPowerUp]
        PrivateMode = 29,
        MatchMe = 30,
        LoveToken = 31,
        SpeedVerify = 32,
        Intuition = 33,
        Invisibility = 34,
        Magnetism = 35,
        SuperStrength = 36,
        MatchIQ = 37,
        MatchVerification = 38,
        [CanSaleAsPowerUp][IsImpulseFeature(ImpulseFeatures.Data.ImpulseFeatureType.SuperLikes)]
        SuperLikes =39,
    }
}
