﻿using System;

namespace Corp.Billing.Shared.Domain.Products
{
    /// <summary>
    /// This has to be deprecated and removed as dependency in Match8.
    /// For future changes use `IProductApiClient`.
    /// </summary>
    [Obsolete]
    public interface IProductService
    {
        // MatchMe

        MatchMe30DayEligibilityCheckResult MatchMe30DayEligibilityCheck(MatchMe30DayEligibilityCheckRequest request);
        PutMeInUserListResult PutMeInUserList(PutMeInUserListRequest request);

        // TopSpot

        TopSpotStatusResult GetTopSpotStatus(TopSpotStatusRequest request);
        ActivateUnclaimedImpulseFeatureResult ActivateUnclaimedImpulseFeature(ActivateUnclaimedImpulseFeatureRequest request);

        // ProfilePro

        CSAUserPCSStatusResult GetUnitUserStatusPcs(CSAUserPCSStatusRequest request);

        // Unit products

        UnitUserStatusResult GetUnitUserStatus(UnitUserStatusRequest request);
        UnitDisplayStatusResult GetUnitDisplayStatus(UnitDisplayStatusRequest request);

    }

}
