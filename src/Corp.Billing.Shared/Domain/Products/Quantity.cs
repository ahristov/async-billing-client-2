﻿namespace Corp.Billing.Shared.Domain.Products
{
    public class Quantity
    {
        public decimal Amount { get; set; }
        public QuantityUnitType Unit { get; set; }
    }
}
