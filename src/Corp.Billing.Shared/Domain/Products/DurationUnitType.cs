﻿namespace Corp.Billing.Shared.Domain.Products
{
    public enum DurationUnitType
    {
        Undefined,
        Second,
        Minute,
        Hour,
        Day,
        Week,
        Month,
        Year,
    }
}
