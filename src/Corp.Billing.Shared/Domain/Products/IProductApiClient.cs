﻿namespace Corp.Billing.Shared.Domain.Products
{
    public interface IProductApiClient
    {
        // MatchMe

        MatchMe30DayEligibilityCheckResult MatchMe30DayEligibilityCheck(MatchMe30DayEligibilityCheckRequest request);
        PutMeInUserListResult PutMeInUserList(PutMeInUserListRequest request);

        // TopSpot

        TopSpotStatusResult GetTopSpotStatus(TopSpotStatusRequest request);
        ProvisionAndActivateTopSpotResult ProvisionAndActivateTopSpot(ProvisionAndActivateTopSpotRequest request);
        ActivateUnclaimedImpulseFeatureResult ActivateUnclaimedImpulseFeature(ActivateUnclaimedImpulseFeatureRequest request);

        // ProfilePro

        CSAUserPCSStatusResult GetUnitUserStatusPcs(CSAUserPCSStatusRequest request);

        // Unit products

        UnitUserStatusResult GetUnitUserStatus(UnitUserStatusRequest request);
        UnitDisplayStatusResult GetUnitDisplayStatus(UnitDisplayStatusRequest request);

    }

}
