﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Corp.Billing.Shared.Domain.Products
{
    [AttributeUsage(AttributeTargets.All)]
    public class FeatureProductIdAttribute : Attribute
    {
        public int ProdId { get; private set; }

        public FeatureProductIdAttribute(int prodId)
        {
            ProdId = prodId;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            FeatureProductIdAttribute other = obj as FeatureProductIdAttribute;

            return (other != null) && other.ProdId == this.ProdId;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return this.ProdId == 0;
        }

    }
}
