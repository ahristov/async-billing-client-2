﻿using Corp.Billing.Shared.Domain.ProvisionIAP;
using Corp.Billing.Shared.Domain.RateCard;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products
{
    public interface IBoostWithRffService
    {
        bool GetIsInBoostWithRffAbTest(int userId);
        void SwitchBoostWithRffProductId(IEnumerable<RateCardV2UnitProductsItem> products, int userId);
        void SwitchBoostWithRffProductId(IEnumerable<OrderProductTable> products, int userId);
        int? GetBoostProductId(int minutes, int userId);
    }
}
