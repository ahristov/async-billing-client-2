﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System;

namespace Corp.Billing.Shared.Domain.Products.ProductFeatures
{
    [AttributeUsage(AttributeTargets.All)]
    public class IsImpulseFeatureAttribute : Attribute
    {
        public ImpulseFeatureType ImpulseFeature { get; private set; }

        public IsImpulseFeatureAttribute(ImpulseFeatureType impulseFeature)
        {
            ImpulseFeature = impulseFeature;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            IsImpulseFeatureAttribute other = obj as IsImpulseFeatureAttribute;

            return (other != null) && other.ImpulseFeature == this.ImpulseFeature;
        }
        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => ImpulseFeature == ImpulseFeatureType.Undefined;
    }
}
