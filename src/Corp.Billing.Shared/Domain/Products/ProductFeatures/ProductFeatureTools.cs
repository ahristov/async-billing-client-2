﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Corp.Billing.Shared.Domain.Products.ProductFeatures
{
    public static class ProductFeatureTools
    {
        public static bool CanSaleAsPowerUp(this ProductFeatureType productFeature)
        {
            FieldInfo fi = productFeature.GetType().GetField(productFeature.ToString());
            if (fi == null)
                return false;

            CanSaleAsPowerUpAttribute[] attributes =
                (CanSaleAsPowerUpAttribute[])fi.GetCustomAttributes(typeof(CanSaleAsPowerUpAttribute), false);
            return attributes?.Length > 0;
        }

        public static IEnumerable<ProductFeatureType> GetAllProductFeatureTypes() => System.Enum.GetValues(typeof(ProductFeatureType)).Cast<ProductFeatureType>();

        public static IEnumerable<ProductFeatureType> GetPowerUpFeatureTypes(this IEnumerable<ProductFeatureType> pfts) => pfts.Where(pft => pft.CanSaleAsPowerUp());
        public static IEnumerable<ProductFeatureType> GetPowerUpFeatureTypes() => GetAllProductFeatureTypes().GetPowerUpFeatureTypes();
        
        public static ImpulseFeatureType? GetImpulseFeatureType(this ProductFeatureType productFeature)
        {
            FieldInfo fi = productFeature.GetType().GetField(productFeature.ToString());
            if (fi == null)
                return null;

            IsImpulseFeatureAttribute[] attributes =
                (IsImpulseFeatureAttribute[])fi.GetCustomAttributes(typeof(IsImpulseFeatureAttribute), false);

            return (attributes?.Length > 0)
                ? attributes[0].ImpulseFeature
                : (ImpulseFeatureType?)null;
        }
    }
}
