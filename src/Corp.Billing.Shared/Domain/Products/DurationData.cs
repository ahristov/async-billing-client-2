﻿using System;

namespace Corp.Billing.Shared.Domain.Products
{
    [Serializable]
    public class DurationData: ICloneable
    {
        public decimal Length { get; set; }
        public int DurationUnit { get; set; }
        public string DurationUnitName => ((DurationUnitType)DurationUnit).ToString();

        public DurationData()
        { }

        public DurationData(Duration duration)
        {
            Length = duration.Length;
            DurationUnit = (int)duration.Unit;
        }
        public DurationData Clone()
        {
            return this.MemberwiseClone() as DurationData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
