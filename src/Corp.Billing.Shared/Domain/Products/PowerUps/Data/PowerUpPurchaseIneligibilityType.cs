﻿namespace Corp.Billing.Shared.Domain.Products.PowerUps.Data
{
    public enum PowerUpPurchaseIneligibilityType: int
    {
        Eligible = 0,
        NotEligible = 1,
        NonSubscriber = 2,
        SubStatusCannotPurchaseAddOn = 3,
        AlreadyProvisionedFeature = 4,
        UserIsInFreeTrial = 5,
        IsCurrentIAPSubscriber = 6,
        CompUser = 7,
        RateCardDoesNotContainThisProduct = 8,
        IsCurrentRenewingFeatureSubscriber = 9,
        IsCurrentNonRenewingFeatureSubscriber = 10,
        HasUnlimitedDateCoach = 11,
    }
}
