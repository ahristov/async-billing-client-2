﻿namespace Corp.Billing.Shared.Domain.Products.PowerUps.Data
{
    public class PowerUpFeature
    {
        public byte FeatureId { get; set; }
        public string FeatureName => ((ProductFeatureType)FeatureId).ToString();
        public bool CanPurchase { get; set; }
        public int IneligibilityReason { get; set; }
        public string IneligibilityReasonName => ((PowerUpPurchaseIneligibilityType)IneligibilityReason).ToString();

        public static PowerUpFeature CreateInstanceEligible(ProductFeatureType productFeature)
        {
            return new PowerUpFeature
            {
                FeatureId = (byte)productFeature,
                CanPurchase = true,
                IneligibilityReason = (int)PowerUpPurchaseIneligibilityType.Eligible,
            };
        }

        public static PowerUpFeature CreateInstanceNotEligible(ProductFeatureType productFeature)
        {
            return new PowerUpFeature
            {
                FeatureId = (byte)productFeature,
                CanPurchase = false,
                IneligibilityReason = (int)PowerUpPurchaseIneligibilityType.NotEligible,
            };
        }
    }
}
