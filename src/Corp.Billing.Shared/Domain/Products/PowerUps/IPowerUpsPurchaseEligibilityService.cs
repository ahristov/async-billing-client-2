﻿using Corp.Billing.Shared.Domain.Products.PowerUps.Data;

namespace Corp.Billing.Shared.Domain.Products.PowerUps
{
    public interface IPowerUpsPurchaseEligibilityService
    {
        PowerUpsPurchaseEligibilityResponse GetPowerUpsPurchaseEligibility(PowerUpsPurchaseEligibilityRequest request);
    }
}
