﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Exceptions
{
    public class InvalidProductFeatureException : BaseImpulseFeaturesException
    {
        public InvalidProductFeatureException(GetStatusRequest req)
            : base(req)
        { }

        public InvalidProductFeatureException(ActivateRequest req)
            : base(req)
        { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.InvalidProductFeature; } }

        public override string ErrorMessage
        {
            get
            {
                return string.Format("Feature {0} is not defined in {1}", 
                    ProductFeature, typeof(PurchasedUnitFeatureType).Name);
            }
        }
    }
}
