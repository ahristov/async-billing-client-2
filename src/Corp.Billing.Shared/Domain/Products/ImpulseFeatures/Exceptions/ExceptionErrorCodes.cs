﻿namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Exceptions
{
    public enum ExceptionErrorCodes : byte
    {
        Undefined = 0,

        UserDoesNotExist = 100,
        InvalidProductFeature = 110,

        DbErrorActivatingUnClaimedImpulseFeature = 120,
    }
}
