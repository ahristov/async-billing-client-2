﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Exceptions
{
    public class UserNotFoundException : BaseImpulseFeaturesException
    {
        public UserNotFoundException(GetStatusRequest req)
            : base(req)
        { }

        public UserNotFoundException(ActivateRequest req)
            : base(req)
        { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.UserDoesNotExist; } }

        public override string ErrorMessage { get { return string.Format("User {0} not found", UserId); } }
    }
}
