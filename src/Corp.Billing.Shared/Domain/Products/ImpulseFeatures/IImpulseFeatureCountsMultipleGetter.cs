﻿using Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount;
using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    [System.Obsolete("Use IImpulseFeatureCountsGetter instead")]
    public interface IImpulseFeatureCountsMultipleGetter : IResetableCache<int>
    {
        GetImpulseFeatureLogCountsMultipleResult GetImpulseFeatureLogCountsMultiple(GetPurchasedUnitsCountRequest request);
    }
}
