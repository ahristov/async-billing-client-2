﻿using Corp.Billing.Shared.Facilities;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    public interface IImpulseFeatureCountsGetter : IResetableCache<int>
    {
        /// <summary>
        /// Retrieves all features for the user along with free/paid and activated/unactivated status counts.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<GetImpulseFeatureCountsResult> GetImpulseFeatureCounts(int userId);
    }
}
