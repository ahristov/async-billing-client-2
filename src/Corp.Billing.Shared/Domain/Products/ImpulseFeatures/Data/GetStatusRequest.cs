﻿namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data
{
    public class GetStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public ImpulseFeatureType ProductFeature { get; set; }
    }
}
