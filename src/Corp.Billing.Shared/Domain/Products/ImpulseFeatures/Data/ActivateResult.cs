﻿namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data
{
    public class ActivateResult : ImpulseFeatureStatus
    {
        public bool Success { get; set; }

        public ActivateResult()
        { }

        public ActivateResult(ImpulseFeatureStatus status)
        {
            ProductFeature = status.ProductFeature;
            CanRedeem = status.CanRedeem;
            FreeNotActivatedCount = status.FreeNotActivatedCount;
            PaidNotActivatedCount = status.PaidNotActivatedCount;
            CanRedeemUntil = status.CanRedeemUntil;
            TotalFreeCount = status.TotalFreeCount;
            TotalPaidCount = status.TotalPaidCount;
            CanPurchase = status.CanPurchase;
            CanPayWithOneClickPaymentMethod = status.CanPayWithOneClickPaymentMethod;
            PurchaseIneligibilityReason = status.PurchaseIneligibilityReason;
            MostRecentActivationDt = status.MostRecentActivationDt;
        }
    }
}
