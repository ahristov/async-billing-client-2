﻿using System;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data
{
    /// <summary>
    /// Contains data for the status of one impulse feature.
    /// </summary>
    [Serializable]
    public class ImpulseFeatureStatus
    {
        /// <summary>
        /// The product feature.
        /// </summary>
        public ImpulseFeatureType ProductFeature { get; set; }

        /// <summary>
        /// Can the user redeem this feature?
        /// SuperLike: Only true, when is current sub and TotalUnActivatedCount > 0
        /// TopSpot: Only true, when TotalUnActivatedCount > 0
        /// </summary>
        public bool CanRedeem { get; set; }


        /// <summary>
        /// Count of the free not yet activated units.
        /// </summary>
        public int FreeNotActivatedCount { get; set; }

        /// <summary>
        /// Count of the paid not yet activated units.
        /// </summary>
        public int PaidNotActivatedCount { get; set; }

        /// <summary>
        /// A total of the free and paid not yet activated units. 
        /// </summary>
        public int TotalNotActivatedCount => FreeNotActivatedCount + PaidNotActivatedCount;


        /// <summary>
        /// When does the redemption expire.
        /// SuperLike: Always null.
        /// TopSpot: Always null.
        /// </summary>
        public DateTime? CanRedeemUntil { get; set; }

        /// <summary>
        /// How many total free count or impulse features are in the database for this user.
        /// </summary>
        public int TotalFreeCount { get; set; }

        /// <summary>
        /// How many total free count or impulse features are in the database for this user.
        /// </summary>
        public int TotalPaidCount { get; set; }

        /// <summary>
        /// How many total count or impulse features are in the database for this user.
        /// </summary>
        public int TotalCount => TotalFreeCount + TotalPaidCount;

        /// <summary>
        /// Is the user eligible to purchase this feature.
        /// </summary>
        public bool CanPurchase { get; set; }

        /// <summary>
        /// Gets or sets if the user has 1-click MOP.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the user has 1-click MOP, false otherwise</c>.
        /// </value>
        public virtual bool CanPayWithOneClickPaymentMethod { get; set; }

        /// <summary>
        /// Gets or sets the purchase ineligibility reason. Corresponds to the enumeration <see cref="PurchaseIneligibilityReasonType"/>.
        /// </summary>
        /// <value>
        /// The purchase ineligibility reason.
        /// </value>
        public byte PurchaseIneligibilityReason { get; set; }
        public string PurchaseIneligibilityReasonName => ((PurchaseIneligibilityReasonType)this.PurchaseIneligibilityReason).ToString();

        public DateTime? MostRecentActivationDt { get; set; }
    }
}
