﻿using Corp.Billing.Shared.Domain.Products.IneligibilityReasons;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data
{
    public enum PurchaseIneligibilityReasonType: byte
    {
        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.Eligible)]
        Eligible = 0,

        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.NotEligible)]
        NotEligible = 1,

        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.NonSubscriber)]
        NonSubscriber = 2,

        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.IsCurrentRenewingFeatureSubscriber)]
        IsCurrentRenewingFeatureSubscriber = 3,

        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.IsCurrentNonRenewingFeatureSubscriber)]
        IsCurrentNonRenewingFeatureSubscriber = 4,

        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.IsCurrentIAPSubscriber)]
        IsCurrentIAPSubscriber = 5,

        [CorrespondingPowerUpIneligibility(PowerUps.Data.PowerUpPurchaseIneligibilityType.HasUnlimitedDateCoach)]
        HasUnlimitedDateCoach = 6,
    }
}
