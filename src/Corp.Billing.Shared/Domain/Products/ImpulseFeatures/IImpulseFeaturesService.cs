﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    public interface IImpulseFeaturesService
    {
        GetStatusResult GetImpulseFeaturesStatus(GetStatusRequest req);
        ActivateResult ActivateImpulseFeature(ActivateRequest req);
    }
}
