﻿using Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount;
using System;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    /// <summary>
    /// Corresponding to "FeatureCountsByDate" 1st recordset.
    /// </summary>
    [Serializable]
    public class ImpulseFeatureLogRow
    {
        public DateTime DateAcquired { get; set; }
        public int LuProdFeatureID  { get; set; }
        public PurchasedUnitFeatureType UnitFeatureTypeVal
        {
            get
            {
                if (!Enum.IsDefined(typeof(PurchasedUnitFeatureType), LuProdFeatureID))
                    return PurchasedUnitFeatureType.Undefined;
                return (PurchasedUnitFeatureType)LuProdFeatureID;
            }
        }
        public bool IsUsed { get; set; }
        public int AcquireType { get; set; }
        public ImpulseFeatureAcquireType AcquireTypeVal
        {
            get
            {
                if (!Enum.IsDefined(typeof(ImpulseFeatureAcquireType), AcquireType))
                    return ImpulseFeatures.ImpulseFeatureAcquireType.Undefined;

                return (ImpulseFeatureAcquireType)AcquireType;
            }
        }
        public bool IsRefunded { get; set; }
        public int Count { get; set; }
    }
}
