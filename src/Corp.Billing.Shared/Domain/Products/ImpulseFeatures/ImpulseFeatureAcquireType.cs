﻿namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    public enum ImpulseFeatureAcquireType
    {
        Undefined = -1,
        Purchased = 0,
        Complimentary = 1,
    }
}
