﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    [Serializable]
    public class GetImpulseFeatureLogCountsMultipleResult
    {
        public int ReturnValue { get; set; }
        public IList<ImpulseFeatureLogRow> Rows { get; set; }
        public IList<TotalFeatureCountsRow> UserTotals { get; set; }
        public IList<ImpulseFeatureDailyRow> DailyRows { get; set; }

        public GetImpulseFeatureLogCountsMultipleResult()
        {
            Rows = new List<ImpulseFeatureLogRow>();
            UserTotals = new List<TotalFeatureCountsRow>();
            DailyRows = new List<ImpulseFeatureDailyRow>();
        }
    }
}
