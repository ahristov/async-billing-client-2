﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    [Serializable]
    public class GetImpulseFeatureCountsResult
    {
        public ImpulseFeatureType ProdFeatureId { get; set; }

        public int FreeNotActivatedNotExpired { get; set; }
        public int FreeNotActivatedExpired { get; set; }
        public int FreeNotActivatedFuture { get; set; }
        public int FreeActivated { get; set; }
        public int FreeTotal { get; set; }

        public int PaidNotActivatedNotExpired { get; set; }
        public int PaidNotActivatedExpired { get; set; }
        public int PaidNotActivatedFuture { get; set; }
        public int PaidActivated { get; set; }
        public int PaidTotal { get; set; }
        public DateTime? MostRecentActivationDt { get; set; }
    }
}
