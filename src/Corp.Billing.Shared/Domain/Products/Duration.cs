﻿namespace Corp.Billing.Shared.Domain.Products
{
    public class Duration
    {
        public decimal Length { get; set; }
        public DurationUnitType Unit { get; set; }
    }
}
