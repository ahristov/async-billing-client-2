﻿namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public enum PurchasedUnitFeatureType : int
    {
        Undefined = 0,
        DateCoach = 4,
        TopSpot = 26,
        Undercover = 27,
        ProfileProStandAlone = 16,
        SuperLikes = 39,
    }
}
