﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public class UnitProductDailyCounts
    {
        public PurchasedUnitFeatureType Feature { get; set; }

        public List<UnitProductCountsForOneDay> Days { get; set; }


        public UnitProductDailyCounts()
        {
            Days = new List<UnitProductCountsForOneDay>();
        }
    }
}
