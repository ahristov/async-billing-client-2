﻿namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public interface IPurchasedUnitsCount
    {
        GetPurchasedUnitsCountResult GetPurchasedUnitsCount(GetPurchasedUnitsCountRequest request);
    }
}
