﻿using Corp.Billing.Shared.Facilities;
using System;

namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public class GetPurchasedUnitsCountRequest : IApiRequestWithQueryString, IResetableCacheRequest
    {
        public int UserId { get; set; }
        public DateTime StartDt { get; set; }
        public DateTime EndDt { get; set; }
        public PurchasedUnitFeatureType? Feature { get; set; }
        public bool ResetCache { get; set; }
    }
}
