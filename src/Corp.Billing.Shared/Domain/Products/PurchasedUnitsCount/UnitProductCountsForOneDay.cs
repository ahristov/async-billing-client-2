﻿using System;

namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public class UnitProductCountsForOneDay
    {
        public DateTime Day { get; set; }

        /// <summary>
        /// Activated during the day, free and paid included, refunds excluded
        /// </summary>
        public int CountActivatedUnits { get; set; }

        /// <summary>
        /// Reserve at end of day, free and paid included, refunds excluded
        /// </summary>
        public int CountUnitsInReserveAtEndOfDay { get; set; }

        /// <summary>
        /// Purchased during the day, paid only (free excluded), refunds excluded
        /// </summary>
        public int CountPurchasedPaidUnits { get; set; }

        /// <summary>
        /// Purchased during the day, free only (paid excluded), refunds excluded
        /// </summary>
        public int CountPurchasedComplimentaryUnits { get; set; }

    }
}
