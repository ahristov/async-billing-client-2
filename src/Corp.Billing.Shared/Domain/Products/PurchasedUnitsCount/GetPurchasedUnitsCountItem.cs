﻿namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public class GetPurchasedUnitsCountItem
    {
        public PurchasedUnitFeatureType Feature { get; set; }

        /// <summary>
        /// Count of purchased (non-free) units. They may or may not be used.
        /// </summary>
        public int CountPurchasedUnits { get; set; }
    }
}
