﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount
{
    public class GetPurchasedUnitsCountResult
    {
        public int TotalFreeCount { get; set; }

        public int TotalPaidCount { get; set; }

        public int TotalCount => TotalFreeCount + TotalPaidCount;


        /// <summary>
        /// Gets or sets the purchased units.
        /// </summary>
        /// <value>
        /// The purchased units.
        /// </value>
        public List<GetPurchasedUnitsCountItem> PurchasedUnits { get; set; }

        /// <summary>
        /// Gets or sets the daily counts.
        /// IMPORTANT: It is only returning data when specific feature is requested!
        /// It will return an empty array if <see cref="GetPurchasedUnitsCountRequest.Feature"/> is not set to actual product feature.
        /// </summary>
        /// <value>
        /// The daily counts.
        /// </value>
        public List<UnitProductDailyCounts> DailyCounts { get; set; }

        public GetPurchasedUnitsCountResult()
        {
            PurchasedUnits = new List<GetPurchasedUnitsCountItem>();
            DailyCounts = new List<UnitProductDailyCounts>();
        }
    }
}
