﻿namespace Corp.Billing.Shared.Domain.DateCoachStatus.Exceptions
{
    public enum ExceptionErrorCodes : byte
    {
        Undefined = 0,

        InvalidUserId = 100,
    }
}
