﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.DateCoachStatus.Data
{
    [Serializable]
    public class DateCoachUserSessionInfoDataResult
    {
        public string CurrentPlan { get; set; }
        public byte? SessionsPermonth { get; set; }
        public DateTime? RenewalDate { get; set; }
        public decimal? RenewalAmt { get; set; }
        public byte? RenewalDays { get; set; }
        public int? RenewingAcctDtlId { get; set; }
        public DateTime? CancelDt { get; set; }
        public short TotalPurchaseCount { get; set; }
        public bool IsUnlimitedDateCoach { get; set; }

        public IList<RemainingSessionsData> RemainingSessions { get; set; }

        public int ReturnValue { get; set; }


        public DateCoachUserSessionInfoDataResult()
        {
            RemainingSessions = new List<RemainingSessionsData>();
        }

        [Serializable]
        public class RemainingSessionsData
        {
            public int SessionsRemaining { get; set; }
            public DateTime? ExpirationDt { get; set; }
        }

        public bool IsRenewalCancelled => this.CancelDt.HasValue;
    }
}
