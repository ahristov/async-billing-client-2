﻿using Corp.Billing.Shared.Facilities;
using System;

namespace Corp.Billing.Shared.Domain.DateCoachStatus.Data
{
    [Serializable]
    public class DateCoachUserSessionInfoDataRequest : IResetableCacheRequest
    {
        public int UserId { get; set; }
        public bool ResetCache { get; set; }
    }
}
