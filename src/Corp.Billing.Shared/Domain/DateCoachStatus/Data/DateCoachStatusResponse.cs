﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.DateCoachStatus.Data
{
    [Serializable]
    public class DateCoachStatusResponse
    {
        public byte CountOfSessionsPerRenewal { get; set; }

        public bool IsRenewalPlan { get; set; }
        public byte? RenewalDays { get; set; }
        public DateTime? NextRenewalDate { get; set; }
        public decimal? NextRenewalPrice { get; set; }

        public bool IsRenewalCancelled { get; set; }
        public DateTime? RenewalCancellationDate { get; set; }
        public short TotalPurchaseCount { get; set; }
        public bool IsUnlimitedDateCoach { get; set; }

        /// <summary>
        /// Is the user eligible to purchase this feature.
        /// </summary>
        public bool CanPurchase { get; set; }

        /// <summary>
        /// Gets or sets the purchase ineligibility reason. Corresponds to the enumeration <see cref="PurchaseIneligibilityReasonType"/>.
        /// </summary>
        /// <value>
        /// The purchase ineligibility reason.
        /// </value>
        public byte PurchaseIneligibilityReason { get; set; }
        public string PurchaseIneligibilityReasonName => ((PurchaseIneligibilityReasonType)this.PurchaseIneligibilityReason).ToString();

        public IEnumerable<RemainingSessionData> RemainingSession { get; set; }

        public DateCoachStatusResponse()
        {
            RemainingSession = new List<RemainingSessionData>();
        }

        [Serializable]
        public class RemainingSessionData
        {
            public DateTime? ExpirationDate { get; set; }
            public int CountRemainingSessions { get; set; }
        }
    }

}
