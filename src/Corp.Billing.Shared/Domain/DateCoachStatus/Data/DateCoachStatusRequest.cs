﻿using System;

namespace Corp.Billing.Shared.Domain.DateCoachStatus.Data
{
    [Serializable]
    public class DateCoachStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }
}
