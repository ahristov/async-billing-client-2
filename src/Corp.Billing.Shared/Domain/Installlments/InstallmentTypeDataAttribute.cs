﻿using System;

namespace Corp.Billing.Shared.Domain.Installlments
{
    public class InstallmentTypeDataAttribute : Attribute
    {
        public short InstallmentFrequency { get; }
        public byte NumberOfInstallments { get; }
        public bool IsActive { get; }
        public string Description { get; set; }

        public InstallmentTypeDataAttribute(
            short installmentFrequency,
            byte numberOfInstallments,
            bool isActive,
            string description)
        {
            InstallmentFrequency = installmentFrequency;
            NumberOfInstallments = numberOfInstallments;
            IsActive = isActive;
            Description = description;
        }
        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            InstallmentTypeDataAttribute other = obj as InstallmentTypeDataAttribute;
            return (other != null)
                && other.InstallmentFrequency == this.InstallmentFrequency
                && other.NumberOfInstallments == this.NumberOfInstallments
                && other.IsActive == this.IsActive;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => this.InstallmentFrequency <= 0;
    }
}
