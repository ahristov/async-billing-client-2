﻿using System;

namespace Corp.Billing.Shared.Domain.Installlments
{
    [Serializable]
    public class InstallmentDetailTable : InstallmentData
    {
        public short InstallmentTypeId { get; set; }
        public short ProdDays { get; set; }
        public byte LuProdFeatureId { get; set; }
        public decimal TaxRate { get; set; }
        public byte luGeoTaxTypeID { get; set; }
        public int GeoTaxID { get; set; }

    }
}
