﻿
// Do not modify, this is generated from T4 template

using System;

namespace Corp.Billing.Shared.Domain.Installlments
{
    [Serializable]
    public enum InstallmentTypeEnum
    {
        /// <summary>
        /// 1000 - Billed Every 14 Days
        /// </summary>
        [InstallmentTypeData(14, 4, true, "Billed Every 14 Days")]
        BilledEvery14Days = 1000,
    }
}

