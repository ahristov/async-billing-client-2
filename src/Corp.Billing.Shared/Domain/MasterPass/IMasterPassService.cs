﻿using System;
using System.Collections.Generic;
using Corp.Billing.Shared.Subscription.Payment;

namespace Corp.Billing.Shared.Domain.MasterPass
{
    public interface IMasterPassService
    {
        GetCheckoutDataResult GetCheckoutData(GetCheckoutDataRequest request);
        void LogTransaction(LogTransactionRequestV7 request);
        void LogTransaction(LogTransactionRequest request);
    }


    public class LogTransactionRequestV7 : IApiRequestWithQueryString
    {
        public string TransactionId { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public bool PaymentSuccessful { get; set; }
        public string PaymentCode { get; set; }
        public DateTime PaymentDate { get; set; }
        public string ShoppingCartId { get; set; }
        public int? MatchTrxId { get; set; }
    }

    public class LogTransactionRequest : IApiRequestWithQueryString
    {
        public string MasterPassTransactionId { get; set; }
        public string ApprovalCode { get; set; }
        public string TransactionStatus { get; set; }
        public string ISOCurrency { get; set; }
        public decimal OrderAmount { get; set; }
    }


    //

    public class GetCheckoutDataRequest : IApiRequestWithQueryString
    {
        public string CheckoutResourceUrl { get; set; }
        public string OAuthToken { get; set; }
        public string OAuthVerifier { get; set; }
        public string CartId { get; set; }
    }


    public class GetCheckoutDataResult
    {
        public string PlainCreditCardNumber { get; set; }
        public PaymentDefinitions.CreditCardTypes CardType { get; set; }
        public short CardExpMonth { get; set; }
        public short CardExpYear { get; set; }

        public string MasterPassTransactionId { get; set; }

        public string GetDtlCode()
        {
            return PaymentDefinitions.DtlCodeFromCardType(this.CardType);
        }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }

        public string WalletId { get; set; }
    }

    //


    public class GenerateOAuthTokenRequest
    {
        public string OriginUrl { get; set; }
        public string CallBackUrl { get; set; }
        public string ISOCurrency { get; set; }
        public IList<ShoppingCartItem> ShoppingCartItems { get; set; }

        public GenerateOAuthTokenRequest()
        {
            ShoppingCartItems = new List<ShoppingCartItem>();
        }
    }

    public class GenerateOAuthTokenResult
    {
        public string OAuthToken { get; set; }
    }


    public class ShoppingCartItem
    {
        public string Description { get; set; }
        public decimal Value { get; set; }

    }


}
