﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Newtonsoft.Json;
using System;

namespace Corp.Billing.Shared.Domain.MasterPass
{
    public class MasterPassApiClient : BaseApiClient, IMasterPassService
    {
        public MasterPassApiClient(ApiCommand apiSvc) 
            : base(apiSvc)
        { }

        [Obsolete]
        public GenerateOAuthTokenResult GenerateOAuthToken(GenerateOAuthTokenRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.MasterPass.Wallet.OAuthToken;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<GenerateOAuthTokenResult>(apiResult);
            return result;
        }

        public GetCheckoutDataResult GetCheckoutData(GetCheckoutDataRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.MasterPass.Wallet.Checkout + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetCheckoutDataResult>(apiResult);
            return result;
        }

        public void LogTransaction(LogTransactionRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.MasterPass.Wallet.Log + "/?" + request.ToQueryString();
            ApiSvc.WebRequestGet(url);
        }

        public void LogTransaction(LogTransactionRequestV7 request)
        {
            throw new NotImplementedException();
        }
    }
}
