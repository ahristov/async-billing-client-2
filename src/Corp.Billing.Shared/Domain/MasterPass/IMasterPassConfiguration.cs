﻿using System.Security.Cryptography.X509Certificates;

namespace Corp.Billing.Shared.Domain.MasterPass
{

    public interface IMasterPassConfiguration
    {
        string MasterPassConsumerKey { get; }

        string MasterPassApiHostUrl { get; }
        bool MasterPassApiIsSandbox { get; }
        string MasterPassKeystorePath { get; }
        string MasterPassKeystorePassword { get; }
        string MasterPassCheckoutId { get; }
    }
}
