﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Promos
{
    public interface IUserPromoInfoGetter : IResetableCache<int>
    {
        UserPromoInfoResult GetUserPromoInfo(UserPromoInfoRequest request);
    }

    public interface IUserPromoInfoGetterNoHttpCache : IUserPromoInfoGetter { }
}
