﻿using System.Linq;

namespace Corp.Billing.Shared.Domain.Promos
{
    public static class PromoExtensions
    {
        public static string NormalizePromoId(this string promoId)
        {
            var res = (promoId ?? string.Empty).Trim();
            return string.IsNullOrEmpty(res) ? Corp.Billing.Shared.Domain.Consts.DefaultPromoId : res;
        }

        public static int[] GetFeaturesFromList(this string promoFeaturesList) => ("" + promoFeaturesList)
            .Trim()
            .Split(new char[] { ',' })
            .Select(s => (int.TryParse(s, out int pf) ? (int?)pf : null))
            .Where(pf => pf.HasValue)
            .Select(pf => pf.Value)
            .ToArray();

        public static bool PromoApplicableToFeature(this string promoFeaturesList, byte featureId)
        {
            int[] features = GetFeaturesFromList(promoFeaturesList);
            var res = features.Length <= 0 || features.Any(f => f == featureId);
            return res;
        }

        public static bool IsDefaultPromo(this string promoId)
        {
            return string.Equals(
                NormalizePromoId(promoId),
                Consts.DefaultPromoId,
                System.StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
