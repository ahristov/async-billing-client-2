﻿using System.Linq;

namespace Corp.Billing.Shared.Domain.Promos
{
    public static class PromoToRenewals
    {
        private static int[] PRC_GRP = new int[] { 68, 69, 72 };

        public static bool PromoAppliesToRenewal(this int priceGroupId)
        {
            return PRC_GRP.Any(x => x == priceGroupId);
        }

        public static bool PromoAppliesToRenewal(this string priceGroupId)
        {
            priceGroupId = ("" + priceGroupId).Trim();
            return PRC_GRP.Any(x => string.Equals(x.ToString(), priceGroupId, System.StringComparison.InvariantCultureIgnoreCase));
        }

    }
}
