﻿namespace Corp.Billing.Shared.Domain.Promos
{
    public interface IPromoService
    {
        // Promos

        PromoEligibilityResult GetPromoEligibility(PromoEligibilityRequest request);
        PromoValidateRedemptionKeyResult ValidateRedemptionKey(PromoValidateRedemptionKeyRequest request);
        PromoValidationResult GetValidation(PromoValidationRequest request);
        PromoGetInfoResult GetInfo(PromoGetInfoRequest request);
        GetPromoDataResult GetPromoData(GetPromoDataRequest request);

        // Gifts

        GiftRedeemResult RedeemGift(GiftRedeemRequest request);
        GiftValidateKeysResult ValidateGiftKeys(GiftValidateKeysRequest request);
    }
}
