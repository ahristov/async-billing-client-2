﻿using System;
using System.Collections.Generic;
using Corp.Billing.Shared.Domain.Orders;
using Corp.Billing.Shared.Subscription.Promotion;

namespace Corp.Billing.Shared.Domain.Promos
{
    public class PromoEligibilityRequest : IApiRequestWithQueryString
    {
        public string PromoID { get; set; }
        public int? UserID { get; set; }
        public short? StateCode { get; set; }
        public short? CountryCode { get; set; }
        public int? BrandID { get; set; }
        public short? UrlCodeIn { get; set; }
        public byte? Age { get; set; }
        public bool? LogPromo { get; set; }
        public bool? SkipTimeToRedeem { get; set; }

    }
    public class PromoEligibilityResult
    {
        public int ReturnValue { get; set; }
        public virtual PromoEligibilityCode EligibilityCode { get; set; }
        public string DefaultPromoId { get; set; }
        public OfferType LuPromoOfferType { get; set; }
        public byte? DefaultPackageMonths { get; set; }
        public bool? IsDefaultPackageBundled { get; set; }
        public bool? SkipRateCard { get; set; }
        public short PromoGrpID { get; set; }
        public bool? ShowPromoTerms { get; set; }
        public string PromoName { get; set; }
    }

    public class UserPromoInfoRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class UserPromoInfoResult
    {
        public int ReturnValue { get; set; }
        public string PromoID { get; set; }
        public int BrandID { get; set; }
        public short URLCode { get; set; }
        public string DefaultBrandPromoID { get; set; }
        public DateTime DefaultPromoTestExpire { get; set; }
        public short SiteCode { get; set; }
    }

    public enum OfferType : byte
    {
        None = 1,
        FreeTrial = 2,
        TimeAdded = 3,
        PercentOff = 4,
        DollarsOff = 5,
        Sweepstake = 6,
        SaveSub = 7,
        PaidTrial = 8,
        FreeAddOn = 9,
        FreeDaysNS = 10,
        RenewalDct = 11,
        AddonBndl = 12,
        EventDscnt = 13,
        FlatInit = 14
    }

    public enum PromoEligibilityCode
    {
        Success = 0,
        UnspecifiedError = 1,
        PromoIDCouldNotResolve = 2,
        InvalidPromoName = 3,
        Expired = 4,
        HasNotStarted = 5,
        RedemptionPeriodExpired = 6,
        UserHasPreviousFreeTrial = 7,
        PromoNameOrIDRequired = 8,
        NotUsed = 9,
        InvalidEligibilityID = 10,
        UrlCodeExluded = 11,
        UrlCodeNotFound = 12,
        CountryIsExcluded = 13,
        CountryNoAllowedInList = 14,
        CountryRequired = 15,
        StateExcluded = 16,
        StateNotAllowedInList = 17,
        StateRequire = 18,
        NotValidForCurrentSubscribers = 19,
        UserIDRequired = 20,
        UserHasTakeRecentFreeTrial = 21,
        PhotoRequired = 22,
        ProfileRequired = 23,
        ProfileOrPhotoRestriction = 24,
        InvalidCCType = 25,
        InvalidPaymentType = 26,
        InvalidAge = 27,
        AgeIsRequired = 28,
        MustBeASubscriber = 29,
        InvliadSubscriber = 30,
        BrandIDExcluded = 31,
        BrandIDNotAllowedInList = 32,
        BrandIDOrBillingIDRequired = 33,
        FailedToLogPromo = 34,
        UserHasRecentPaidTrial = 35,
        BillingIDIsExcluded = 36,
        BillingIDNotFound = 37,
        NotEligibleForDefaultPromos = 38,
        CurrencyRestrictions = 39,
        ByInvitationOnly = 40
    }

    public class PromoGetInfoRequest : IApiRequestWithQueryString
    {
        public string PromoID { get; set; }
    }

    public class PromoGetInfoResult
    {
        public string PromoName { get; set; }
        public string Description { get; set; }
        public DateTime CreateDt { get; set; }
        public DateTime StartDt { get; set; }
        public DateTime EndDt { get; set; }
        public string HeaderHTML { get; set; }
        public string ColorHTML { get; set; }
        public bool ShowCheck { get; set; }
        public short luPromoEligibilityID { get; set; }
        public short PromoGroupID { get; set; }
        public DiscountOfferType PromoOfferType { get; set; }
        public short DefaultProdDtlID { get; set; }
        public short TimeToRedeem { get; set; }
        public short DefaultTemplateID { get; set; }
        public string DiscountDescription { get; set; }
        public short DiscountAmount { get; set; }
        public decimal DiscountPercentage { get; set; }
        public short FreeDays { get; set; }
        public int PromoCap { get; set; }
        public int PromoCapCounter { get; set; }
        public AccountPaymentMethodType AccountPaymentMethodType { get; set; }
        public char BaseCode { get; set; }
        public short FreeTrialDays { get; set; }
        public bool ShowPromoTerms { get; set; }
        public int ReturnValue { get; set; }

        private string _prodFeatureList = string.Empty;
        public string ProdFeatureList
        {
            get { return _prodFeatureList; }
            set { _prodFeatureList = ("" + value).Trim(); }
        }
        public bool SkipRateCard { get; set; }
        public int[] PromoFeatures => ProdFeatureList.GetFeaturesFromList();
    }

    public enum AccountPaymentMethodType : byte
    {
        NotApplicable = 0,
        AmericanExpress = 1,
        DinersClub = 2,
        Discover = 3,
        JCB = 4,
        MasterCard = 5,
        Visa = 6,
        OnlineCheck = 7,
        Check = 8,
        Cash = 9,
        MoneyOrder = 12,
        ECheck = 15,
        GiftSubscriptionRedemption = 16,
        SwitchMaestro = 17,
        PayPal = 18,
        ZeusConversionWithoutPaymentInformation = 19,
        DanalTeledit = 20,
        GCBankTransfer = 21,
        AppleIAP = 22,
        GCKonbini = 23,
        Tokens = 24
    }

    public class PromoValidateRedemptionKeyRequest
    {
        public string RedemptionKey { get; set; }
        public string PromoID { get; set; }
        public int UserID { get; set; }
    }

    public class PromoValidateRedemptionKeyResult
    {
        public PromotionDefinitions.PromoValidateRedemptionKeyCode ReturnValue { get; set; }
    }

    public class PromoValidationRequest : IApiRequestWithQueryString
    {
        public string Keyword { get; set; }
    }

    public class PromoValidationResult
    {
        public string PromoID;
        public PromotionDefinitions.PromoValidationCode ReturnValue { get; set; }
        public byte DisplayPromoDetails { get; set; }
        public byte DisplaySiteNav { get; set; }
        public bool RateCardPromo { get; set; }
        public OfferType OfferType { get; set; }
        public DateTime? PromoStartDt { get; set; }
        public DateTime? PromoEndDt { get; set; }
        public List<short> QuestionIds { get; set; }
        public List<short> TextQuestionIds { get; set; }
    }
}
