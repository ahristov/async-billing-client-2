﻿using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.Promos
{
    public class GetPromoDataRequest : IApiRequestWithQueryString
    {
        public string PromoId { get; set; }
    }


    public class GetPromoDataResultBase
    {
        public int ReturnValue { get; set; }

        public string PromoId { get; set; }
        public byte PromoOfferTypeId { get; set; }
        public short InitialDaysExtension { get; set; }
        public decimal DefaultDiscountPrct { get; set; }
        public decimal DefaultDiscountAmt { get; set; }
        public short? FreeTrialProductId { get; set; }
        public short? FreeTrialDays { get; set; }
        public IList<GetPromoDataItem> Items { get; set; }

        private string _prodFeatureList = string.Empty;
        public string ProdFeatureList
        {
            get { return _prodFeatureList; }
            set { _prodFeatureList = ("" + value).Trim(); }
        }
        public bool SkipRateCard { get; set; }
        
        public bool IsFlatRatePromo { get; set; }
        public byte FlatRateProductMonths { get; set; }
        public bool FlatRateProductIsPremium { get; set; }
        public decimal FlatRateProductAmount { get; set; }


        public int[] PromoFeatures => ProdFeatureList.GetFeaturesFromList();

        public bool OnlyAppliesToAddOns => PromoFeatures != null
                    && PromoFeatures.Length > 0
                    && PromoFeatures.Contains(1) == false;

        public GetPromoDataResultBase()
        {
            Items = new List<GetPromoDataItem>();
        }
    }


    public class GetPromoDataResult : GetPromoDataResultBase
    {
        public OfferType OfferType
        {
            get { return (OfferType) this.PromoOfferTypeId; }
        }

        public GetPromoDataResult()
            : base()
        { }
    }

    public class GetPromoDataResultV2 : GetPromoDataResultBase
    {
        public string PromoOfferTypeName => ((OfferType)this.PromoOfferTypeId).ToString();

        public GetPromoDataResultV2()
            : base()
        { }

        public static GetPromoDataResultV2 CreateInstance(GetPromoDataResultBase other)
        {
           var res = new GetPromoDataResultV2
            {
                ReturnValue = other.ReturnValue,
                PromoId = other.PromoId,
                PromoOfferTypeId = other.PromoOfferTypeId,
                InitialDaysExtension = other.InitialDaysExtension,
                DefaultDiscountPrct = other.DefaultDiscountPrct,
                DefaultDiscountAmt = other.DefaultDiscountAmt,
                FreeTrialProductId = other.FreeTrialProductId,
                FreeTrialDays = other.FreeTrialDays,
                ProdFeatureList = other.ProdFeatureList,
                Items = other.Items,
            };

            return res;
        }
    }

    public class GetPromoDataItem
    {
        public byte Months { get; set; }
        public bool IsBundled { get; set; }
        public decimal? DiscountPrct { get; set; }
        public decimal? DiscountAmt { get; set; }
        public decimal? FlatRateAmt { get; set; }
    }
}
