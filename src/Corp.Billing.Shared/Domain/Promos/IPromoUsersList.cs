﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Promos
{
    public interface IPromoUsersList
    {
        ICollection<int> GetPromoUsers(string promoId);
    }
}
