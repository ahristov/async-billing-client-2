﻿using System;

namespace Corp.Billing.Shared.Domain.Promos
{
    public class GiftRedeemRequest
    {
        public int GiftTransactionId { get; set; }
        public int UserId { get; set; }
        public string PromoId { get; set; }
        public byte? luTransactionSourceId { get; set; }
        public int SubBrandId { get; set; }
        public bool? IsComplimentary { get; set; }
        public short? EmployeeId { get; set; }
        public string Comment { get; set; }
        public short? CRCode { get; set; }
        public int? AccountPaymentMethodId { get; set; }
        public bool? IsRefund { get; set; }
        public Guid SID { get; set; }
        public byte ClientIP1 { get; set; }
        public byte ClientIP2 { get; set; }
        public byte ClientIP3 { get; set; }
        public byte ClientIP4 { get; set; }
        public short? SubscribeTextID { get; set; }
        public string DetailCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public byte? P_State { get; set; }
        public short? P_Country { get; set; }
        public string PostalCode { get; set; }
        public byte? CCMonth { get; set; }
        public short? CCYear { get; set; }
        public string CheckNumber { get; set; }
        public string DriverLic { get; set; }
        public string DLStateAbbreviation { get; set; }
        public string DLCountryAbbreviation { get; set; }
        public string AccountNumberClear { get; set; }
        public bool? OverrideSubOrderRules { get; set; }

        public GiftRedeemRequest()
        {
            PromoId = Corp.Billing.Shared.Domain.Consts.DefaultPromoId;
        }
    }

    public class GiftRedeemResult
    {
        public int AccountOrderId { get; set; }
        public int TransactionId { get; set; }
        public int ReturnValue { get; set; }
    }

    public class GiftValidateKeysRequest : IApiRequestWithQueryString
    {
        public int GiftTransactionId { get; set; }
        public string RedemptionKey { get; set; }
    }

    public class GiftValidateKeysResult
    {
        public byte ValidationCode { get; set; }
        public int ReturnValue { get; set; }
    }
}
