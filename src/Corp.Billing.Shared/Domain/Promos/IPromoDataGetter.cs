﻿namespace Corp.Billing.Shared.Domain.Promos
{
    public interface IPromoDataGetter
    {
        GetPromoDataResult GetPromoData(GetPromoDataRequest request);
    }
}
