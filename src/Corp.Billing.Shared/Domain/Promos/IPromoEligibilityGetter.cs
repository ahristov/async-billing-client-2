﻿namespace Corp.Billing.Shared.Domain.Promos
{
    public interface IPromoEligibilityGetter
    {
        PromoEligibilityResult GetPromoEligibility(PromoEligibilityRequest request);
    }
}
