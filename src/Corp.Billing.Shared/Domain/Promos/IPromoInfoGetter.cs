﻿namespace Corp.Billing.Shared.Domain.Promos
{
    public interface IPromoInfoGetter
    {
        PromoGetInfoResult GetPromoInfo(PromoGetInfoRequest request);
    }
}
