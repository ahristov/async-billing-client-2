﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Corp.Billing.Shared.Domain.Chargebacks
{
    public class AmericanExpressChargebackRequest : ChargebackRequest
    {
        public override ChargebackProviderType Provider
        {
            get { return ChargebackProviderType.AmericanExpress; }
            set { }
        }
        public int FileId { get; set; }
        public string SENumb { get; set; }
        public string CMAcctNumberLastFour { get; set; }
        public string CurrentCaseNumber { get; set; }
        public string CurrentActionNumber { get; set; }
        public string PreviousCaseNumber { get; set; }
        public string Resolution { get; set; }
        public DateTime DateOfAdjustment { get; set; }
        public DateTime DateOfCharge { get; set; }
        public string CaseType { get; set; }
        public string LOCNumb { get; set; }
        public string CBReasonCode { get; set; }
        public decimal CBAmount { get; set; }
        public string CBAdjustmentNumber { get; set; }
        public string CBResolutionAdjNumber { get; set; }
        public string MerchantOrderNum { get; set; }
        public decimal BilledAmount { get; set; }
        public string SOCInvoiceNumber { get; set; }
        public string ROCInvoiceNumber { get; set; }
        public decimal ForeignAmount { get; set; }
        public string Currency { get; set; }
        public string CMOrigAcctNumLastFour { get; set; }
        public string CMOrigName { get; set; }
    }

    public class AmericanExpressChargebackResult : ChargebackResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
