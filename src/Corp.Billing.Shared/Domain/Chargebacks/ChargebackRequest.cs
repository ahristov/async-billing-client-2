﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Chargebacks
{
    public enum ChargebackProviderType
    {
        AmericanExpress = 1
    }

    [JsonConverter(typeof(ChargebackRequestConverter))]
    public abstract class ChargebackRequest
    {
        public abstract ChargebackProviderType Provider { get; set; }

        private class ChargebackRequestConverter : JsonCreationConverter<ChargebackRequest>
        {
            protected override ChargebackRequest Create(Type objectType, JObject jObject)
            {
                var provider = (ChargebackProviderType)jObject.Value<int>("Provider");
                switch (provider)
                {
                    case ChargebackProviderType.AmericanExpress:
                        return new AmericanExpressChargebackRequest();
                    default:
                        throw new NotSupportedException("ChargebackProviderType " + provider + " is not supported");
                }
            }
        }
    }

    public abstract class ChargebackResponse
    {
    }
}
