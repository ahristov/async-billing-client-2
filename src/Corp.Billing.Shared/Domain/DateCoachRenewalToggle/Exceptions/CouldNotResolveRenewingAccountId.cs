﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public class CouldNotResolveRenewingAccountId : BaseDateCoachRenewalToggleException
    {
        public CouldNotResolveRenewingAccountId(DateCoachRenewalToggleRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.CouldNotResolveRenewingAccountId;

        public override string ErrorMessage => "Could not resolve renewing account detail ID";
    }
}
