﻿namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public enum ExceptionErrorCodes : byte
    {
        Undefined = 0,

        InvalidUserId = 100,
        CouldNotResolveRenewingAccountId = 101,
        RenewalIsAlreadyDeactivated = 120,
        RenewalIsCurrentlyActive = 130,
        DatabaseErrorActivatingRenewal = 140,
        DatabaseErrorDeactivatingRenewal = 141,
    }
}
