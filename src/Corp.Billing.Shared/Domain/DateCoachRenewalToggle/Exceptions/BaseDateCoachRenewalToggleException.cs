﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public abstract class BaseDateCoachRenewalToggleException : Exception
    {
        public int UserId { get; private set; }

        public BaseDateCoachRenewalToggleException(DateCoachRenewalToggleRequest request)
        {
            UserId = request.UserId;
        }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
