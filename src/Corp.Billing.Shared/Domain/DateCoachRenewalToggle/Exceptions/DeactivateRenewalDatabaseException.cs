﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public class DeactivateRenewalDatabaseException : BaseDateCoachRenewalToggleException
    {
        public DeactivateRenewalDatabaseException(DateCoachRenewalToggleRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.DatabaseErrorDeactivatingRenewal;

        public override string ErrorMessage => "Database error deactivating renewal";
    }
}
