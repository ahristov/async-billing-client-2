﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using System.Net;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public class InvalidUserIdException : BaseDateCoachRenewalToggleException
    {
        public InvalidUserIdException(DateCoachRenewalToggleRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.InvalidUserId;

        public override string ErrorMessage => "Invalid user ID";

        public override HttpStatusCode HttpStatusCode => HttpStatusCode.BadRequest;
    }
}
