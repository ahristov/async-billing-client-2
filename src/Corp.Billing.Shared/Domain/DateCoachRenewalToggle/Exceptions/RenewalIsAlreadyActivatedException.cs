﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public class RenewalIsCurrentlyActiveException : BaseDateCoachRenewalToggleException
    {
        public RenewalIsCurrentlyActiveException(DateCoachRenewalToggleRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.RenewalIsCurrentlyActive;

        public override string ErrorMessage => "Date coach is currently active";
    }
}
