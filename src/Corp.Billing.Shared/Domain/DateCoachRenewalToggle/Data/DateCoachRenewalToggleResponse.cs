﻿using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using System;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data
{
    [Serializable]
    public class DateCoachRenewalToggleResponse
    {
        public DateCoachRenewalToggleResultType ToggleResult { get; set; }
        public int DbReturnValue { get; set; }
        public DateCoachStatusResponse FeatureStatus { get; set; }

        public DateCoachRenewalToggleResponse()
        {
            FeatureStatus = new DateCoachStatusResponse();
        }
    }
}
