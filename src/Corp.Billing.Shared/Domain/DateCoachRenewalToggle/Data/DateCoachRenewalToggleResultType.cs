﻿namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data
{
    public enum DateCoachRenewalToggleResultType : byte
    {
        Undefined=0,
        RenewalActivated=1,
        RenewalDecativated=2,
        Error=100,
    }
}
