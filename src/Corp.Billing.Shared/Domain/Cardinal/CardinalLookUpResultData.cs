﻿namespace Corp.Billing.Shared.Domain.Cardinal
{
    public class CardinalLookUpResultData
    {
        public string Cavv { get; set; }
        public string EciFlag { get; set; }
        public string Enrolled { get; set; }
        public string XId { get; set; }
        public int? CardinalLogId { get; set; }
    }
}
