﻿namespace Corp.Billing.Shared.Domain.Cardinal
{
    public interface ICardinalService
    {
        CardinalLookUpResult LookUp(CardinalLookUpRequestData request);
    }
}
