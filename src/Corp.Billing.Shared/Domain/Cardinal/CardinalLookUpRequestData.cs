﻿namespace Corp.Billing.Shared.Domain.Cardinal
{
    public class CardinalLookUpRequestData
    {
        public CardinalBrowserData Browser { get; set; }
        public CardinalUserData User { get; set; }
        public CardinalPaymentData Payment { get; set; }

        public CardinalLookUpRequestData()
        {
            Browser = new CardinalBrowserData();
            User = new CardinalUserData();
            Payment = new CardinalPaymentData();
        }
    }
}
