﻿namespace Corp.Billing.Shared.Domain.Cardinal
{
    public class CardinalBrowserData
    {
        /// <summary>
        /// Gets the user agent string (HTTP_USER_AGENT).
        /// </summary>
        /// <value>
        /// The user agent string.
        /// </value>
        public string UserAgent { get; set; }

        /// <summary>
        /// Gets the browser accept header (HTTP_ACCEPT).
        /// </summary>
        /// <value>
        /// The browser accept header.
        /// </value>
        public string BrowserHttpAcceptHeader { get; set; }

        /// <summary>
        /// Gets the remote ip address.
        /// </summary>
        /// <value>
        /// The remote ip address.
        /// </value>
        public string IpAddress { get; set; }

    }
}
