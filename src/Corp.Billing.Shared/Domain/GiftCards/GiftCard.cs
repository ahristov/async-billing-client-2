﻿namespace Corp.Billing.Shared.Domain.GiftCards
{
    public enum GiftCardIneligibilityType
    {
        Unknown = 0,
        Eligible = 1,
        UserIsAlreadySub = 2,
        InvalidSiteCode = 3,
        InvalidCountryCode = 4,
        UserInFreeTrial = 5,
        PendingOfflineSubscriber = 6,
        CharterMember = 7,
        IneligibleToPurchaseSub = 8   // User has some other reason they cannot have a subscription
    }

    public enum GiftCardProcessingErrorCode
    {
        UnknownError = 0,
        Success = 1,
        UserIneligible = 2,
        EligibilityCheckFailed = 3, // Unable to determine whether the user is eligible
        FailedToValidateOrder = 4,  // Most commonly a result of the 15-minute retry rule
        PaymentCommError = 5,
        CardDeclined_Other = 100,
        CardDeclined_SecurityCode = 101,
        CardDeclined_Expiration = 102,
        CardDeclined_Validation = 103,
        CardDeclined_InsufficientFunds = 104
    }

    public class GiftCardEligibilityResult
    {
        public bool UserIsEligible { get; set; }
        public GiftCardIneligibilityType IneligibilityType { get; set; }
    }

    public class GiftCardProcessResult
    {
        public GiftCardEligibilityResult Eligibility { get; set; }
        public GiftCardProcessingErrorCode ErrorCode { get; set; }
    }
}
