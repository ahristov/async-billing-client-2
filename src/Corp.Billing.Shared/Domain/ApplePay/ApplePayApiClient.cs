﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;

namespace Corp.Billing.Shared.Domain.ApplePay
{
    public class ApplePayApiClient : BaseApiClient, IApplePayService
    {
        public ApplePayApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public string ValidateMerchant(ValidateMerchantRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ApplePay.ValidateMerchant;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);
            return apiResult;
        }
    }
}
