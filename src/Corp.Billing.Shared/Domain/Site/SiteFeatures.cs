﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Site
{
    public class SiteFeatures
    {
        public class GetSiteFeaturesRequest : IApiRequestWithQueryString
        {
            public short SiteCode { get; set; }
        }

        public class GetSiteFeaturesResult
        {
            public int ReturnValue { get; set; }
            public IList<GetSiteFeaturesItem> Items;

            public GetSiteFeaturesResult()
            {
                Items = new List<GetSiteFeaturesItem>();
            }
        }

        public class GetSiteFeaturesItem
        {
            public short SiteCode { get; set; }
            public byte FeatureId { get; set; }
            public string FeatureDescr { get; set; }

            public long BitMaskValue { get; set; }
            public bool IsBaseFeature { get; set; }
            public bool RequiresBaseFeature { get; set; }

            public bool IsSoldPerUnit { get; set; }
        }
    }
}
