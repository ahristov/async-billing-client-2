﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Site
{
    public class SitePaymentMatrix
    {
        public class GetSitePaymentMatrixRequest : IApiRequestWithQueryString
        {
            public short SiteCode { get; set; }
            public short UrlCode { get; set; }
            public short? CountryCode { get; set; }
            public string ISOCurrencyCode { get; set; }

            /// <summary>
            /// Send `true` to include not active payment methods (Active=0, like "E-Check" for SiteCode=1)
            /// Note: Site should pass `false`.
            /// </summary>
            /// <value>
            /// Value of the flag.
            /// </value>
            public bool IncudeNotActive { get; set; }


            /// <summary>
            /// Send `true` to include not applicable payment service categories (PaymentServiceCategoryID = 0, like "Cash" for SiteCode=1)
            /// Note: Site should pass `false`.
            /// </summary>
            /// <value>
            /// Value of the flag.
            /// </value>
            public bool IncludeNotApplicable { get; set; }

            /// <summary>
            /// Send `true` to include internal payment processors (PaymentProcessorId = 1, like "Check" and "Money Order" for SiteCode=1)
            /// Note: Site should pass `false`. CSA should pass `true`.
            /// </summary>
            /// <value>
            /// Value of the flag.
            /// </value>
            public bool IncludeInternal { get; set; }
        }

        public class GetSitePaymentMatrixResult
        {
            public int ReturnValue { get; set; }
            public IList<GetSitePaymentMatrixItem> Items;

            public GetSitePaymentMatrixResult()
            {
                Items = new List<GetSitePaymentMatrixItem>();
            }
        }

        public class GetSitePaymentMatrixItem
        {
            public short SiteCode { get; set; }
            public short? UrlCode { get; set; }
            public string UrlCodeDescriprion { get; set; }
            public short? CountryCode { get; set; }
            public string CountryName { get; set; }
            public string PresentationCurrencyCode { get; set; }
            public string SettlementCurrencyCode { get; set; }
            public byte? PaymentServiceCategoryId { get; set; }
            public string PaymentServiceCategoryDescription { get; set; }
            public byte? PaymentProcessorId { get; set; }
            public string PaymentProcessorDescription { get; set; }
            public byte? PaymentMethodTypeId { get; set; }
            public string PaymentMethodTypeDescription { get; set; }
            public string DtlCode { get; set; }
            public bool? Active { get; set; }
            public string BaseCode { get; set; }
        }
    }
}
