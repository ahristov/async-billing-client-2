﻿namespace Corp.Billing.Shared.Domain.Site
{
    public interface ISalesTaxRatesService
    {
        SalesTaxRates.GetSalesTaxRatesResponse GetSalesTaxRates(SalesTaxRates.GetSalesTaxRatesRequest request);

        SalesTaxRates.GetSalesTaxRatesResponse GetSalesTaxRates(SalesTaxRates.GetSalesTaxRatesByUserRequest request);
    }
}
