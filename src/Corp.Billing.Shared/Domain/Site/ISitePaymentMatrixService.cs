﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Site
{
    public interface ISitePaymentMatrixService
    {
        IList<SitePaymentMatrix.GetSitePaymentMatrixItem> GetSitePaymentMatrix(SitePaymentMatrix.GetSitePaymentMatrixRequest request);
        IList<SitePaymentMatrix.GetSitePaymentMatrixItem> GetSitePaymentMatrixAll();
    }
}
