﻿using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.Site
{
    public static class SiteFeaturesExtensions
    {
        public static IList<SiteFeatures.GetSiteFeaturesItem> Filter(
            this IList<SiteFeatures.GetSiteFeaturesItem> items,
            SiteFeatures.GetSiteFeaturesRequest request)
        {
            var result = items.Where(e => e.SiteCode == request.SiteCode).ToList();
            return result;
        }
    }
}
