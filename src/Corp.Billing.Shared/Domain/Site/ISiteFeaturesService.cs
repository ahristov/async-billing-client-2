﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Site
{
    public interface ISiteFeaturesService
    {
        IList<SiteFeatures.GetSiteFeaturesItem> GetSiteFeaturesAll();
        IList<SiteFeatures.GetSiteFeaturesItem> GetSiteFeatures(SiteFeatures.GetSiteFeaturesRequest request);
    }
}
