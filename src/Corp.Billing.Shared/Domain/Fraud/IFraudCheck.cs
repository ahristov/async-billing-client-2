﻿using System;

namespace Corp.Billing.Shared.Domain.Fraud
{
    public interface IFraudCheck
    {
        bool CheckForFraud(int userId, Guid? sessionId, string paymentMethodHash, int paymentMethodToken);
        bool CheckForFraud(int userId, Guid? sessionId, string paymentMethodHash, int paymentMethodToken, Action<int> fraudLogger);
    }
}
