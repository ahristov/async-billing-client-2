﻿using System;

namespace Corp.Billing.Shared.Domain.Fraud
{
    public class LogFraudEventRequest
    {
        public int luFraudEventTypeId { get; set; }
        public int UserId { get; set; }
        public Guid SId { get; set; }
        public byte ClientIP1 { get; set; }
        public byte ClientIP2 { get; set; }
        public byte ClientIP3 { get; set; }
        public byte ClientIP4 { get; set; }
        public string AccountNumberClear { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHash { get; set; }
        public string CSAComment { get; set; }
        public short CSAActivity_Type { get; set; }
        public string AccountHash2 { get; set; }
    }

    public class LogFraudEventResult
    {
        public int ReturnValue { get; set; }
    }


    public class LogFraudEventRequestV1
    {
        public int UserId { get; set; }
        public int? PaymentMethodToken { get; set; }

        public Guid SId { get; set; }
        public byte ClientIP1 { get; set; }
        public byte ClientIP2 { get; set; }
        public byte ClientIP3 { get; set; }
        public byte ClientIP4 { get; set; }

        public int luFraudEventTypeId { get; set; }

        public string CSAComment { get; set; }

        // public short CSAActivity_Type { get; set; }
    }



    public class SubAttributeRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    public class SubAttributeResult
    {
        public bool IsWhiteListed { get; set; }
        public byte MOP { get; set; }
        public int Value { get; set; }
        public bool IsRecentSub { get; set; }
        public bool LookAtScoreResetOnly { get; set; }
        public int ReturnValue { get; set; }
    }
}
