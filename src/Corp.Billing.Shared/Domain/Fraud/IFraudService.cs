﻿namespace Corp.Billing.Shared.Domain.Fraud
{
    public interface IFraudService
    {
        // Fraud

        LogFraudEventResult LogFraudEvent(LogFraudEventRequest request);
        SubAttributeResult GetFraudProperties(SubAttributeRequest request);
    }
}
