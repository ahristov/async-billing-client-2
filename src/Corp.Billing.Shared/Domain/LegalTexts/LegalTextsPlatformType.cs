﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public enum LegalTextsPlatformType
    {
        Undefined,
        DesktopWeb,
        MobileWeb
    }
}
