﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public enum LegalTextDiscountType
    {
        None,
        FreeTrial,
        InitialDiscount,
        RenewalDiscount
    }
}
