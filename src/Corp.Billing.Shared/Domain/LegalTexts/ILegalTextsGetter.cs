﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public interface ILegalTextsGetter
    {
        LegalTextsResponse GetLegalTexts(LegalTextsRequest req);
    }
}
