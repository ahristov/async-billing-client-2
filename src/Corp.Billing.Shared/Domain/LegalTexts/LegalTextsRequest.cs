﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public class LegalTextsRequest : IApiRequestWithQueryString, ILegalTextRequest
    {
        public int? UserId { get; set; }

        public string CultureCode { get; set; }
        public LegalTextPlacementType PlacementType { get; set; }
        public LegalTextOrderType OrderType { get; set; }
        public LegalTextDiscountType DiscountType { get; set; }
        public Shared.Subscription.Payment.PaymentDefinitions.WalletType WalletType { get; set; }

        public LegalTextsPlatformType PlatformType { get; set; }
        public bool HasTax { get; set; }


        public string GetCacheKey()
        {
            return string.Format(
                "CultureCode={0}&PlacementType={1}&OrderType={2}&DiscountType={3}&WalletType={4}&PlatformType={5}&HasTax={6}",
                CultureCode,
                PlacementType,
                OrderType,
                DiscountType,
                WalletType,
                PlatformType,
                HasTax);
        }
    }
}
