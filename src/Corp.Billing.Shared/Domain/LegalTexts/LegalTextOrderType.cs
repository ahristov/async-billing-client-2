﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public enum LegalTextOrderType
    {
        None,
        Subscription,
        AddOn,
        AddOnForInstallmentSubscriber,
        UnitProduct,
    }
}
