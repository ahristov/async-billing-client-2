﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public interface ILegalTextRequest
    {
        string CultureCode { get;  }
        LegalTextsPlatformType PlatformType { get; }
    }
}
