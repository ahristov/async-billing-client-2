﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public interface ILegalTextsResponse
    {
        string LegalDisclaimerTermsKey { get; }
        string LegalDisclaimerTerms { get; }
    }
}
