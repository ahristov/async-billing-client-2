﻿using Corp.Billing.Shared.Domain.SubscriptionBenefits.Data;
using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Extensions
{
    public static class BenefitStatusResponseExtensions
    {
        public static ICollection<BenefitFeature> FilterBenefitFeaturesByAcceptedFeatureTypes(
            this ICollection<BenefitFeature> benefitFeatures,
            string acceptedFeatureTypesJSONString)
        {
            if (benefitFeatures == null || string.IsNullOrWhiteSpace(acceptedFeatureTypesJSONString))
                return benefitFeatures;

            int[] acceptedFeatureTypes;
            try
            {
                acceptedFeatureTypes = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(acceptedFeatureTypesJSONString);
            }
            catch
            {
                acceptedFeatureTypes = new int[] { };
            }

            return FilterBenefitFeaturesByAcceptedFeatureTypes(benefitFeatures, acceptedFeatureTypes);
        }

        public static ICollection<BenefitFeature> FilterBenefitFeaturesByAcceptedFeatureTypes(
            this ICollection<BenefitFeature> benefitFeatures, 
            int[] acceptedFeatureTypes)
        {
            if (benefitFeatures == null || acceptedFeatureTypes == null)
                return benefitFeatures;

            return benefitFeatures.Where(f => acceptedFeatureTypes.Contains((int)f.FeatureType)).ToArray();
        }

        public static void SetNewBenefitFlagsAndCount(this BenefitStatusResponse res, DateTime lastBenefitsViewTime, DateTime now)
        {
            if (res == null || res.Features == null)
                return;

            foreach (var feature in res.Features)
            {
                DateTime featureLaunchDate = feature.FeatureLaunchDate;

                BenefitFeatureTypeEnum featureType = (BenefitFeatureTypeEnum)feature.FeatureType;
                if (featureType == BenefitFeatureTypeEnum.DiscountedEvents
                    || featureType == BenefitFeatureTypeEnum.HappyHour
                    || featureType == BenefitFeatureTypeEnum.MatchTalks)
                {
                    BenefitPurchaseTypeEnum purchaseType = (BenefitPurchaseTypeEnum)feature.PurchaseType;
                    bool isEligibleToUse = purchaseType == BenefitPurchaseTypeEnum.EligibleToPurchase
                        || purchaseType == BenefitPurchaseTypeEnum.Purchased;

                    //for comps
                    if (res.IsCurrentPackageBundle && res.CurrentPackageMonths == 0 && res.IsValueAddSubscription)
                        isEligibleToUse = true;

                    feature.IsNewGrant = isEligibleToUse && feature.BeginDate.GetValueOrDefault() >= lastBenefitsViewTime;
                    feature.IsNewFeature = isEligibleToUse && featureLaunchDate < now && featureLaunchDate >= lastBenefitsViewTime;
                }
                else
                {
                    bool isEligibleToUse = feature.ProvisionState == (byte)BenefitProvisionStateEnum.PendingActivation;

                    feature.IsNewGrant = isEligibleToUse && feature.EffectiveDate.GetValueOrDefault() >= lastBenefitsViewTime;
                    feature.IsNewFeature = isEligibleToUse && featureLaunchDate < now && featureLaunchDate >= lastBenefitsViewTime;
                }
            }

            res.CountOfNewBenefits = res.Features.Count(f => f.IsNewGrant || f.IsNewFeature);
        }
    }
}
