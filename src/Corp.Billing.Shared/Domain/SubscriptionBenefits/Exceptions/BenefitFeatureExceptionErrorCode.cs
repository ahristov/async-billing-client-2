﻿namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Exceptions
{
    public enum BenefitFeatureExceptionErrorCode : int
    {
        Success = 0,
        UserDoesNotExists = 100
    }
}
