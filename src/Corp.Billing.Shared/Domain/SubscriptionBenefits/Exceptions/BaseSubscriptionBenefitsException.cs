﻿using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Exceptions
{
    public abstract class BaseSubscriptionBenefitsException : Exception
    {
        public BenefitsStatusRequest Request { get; private set; }

        public BaseSubscriptionBenefitsException(BenefitsStatusRequest req)
        {
            Request = req ?? throw new ArgumentNullException(nameof(req));
        }
        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }

        public string DbgString() => $"userId={Request?.UserId},errorCode={ErrorCode},errorMessage={ErrorMessage}";

        public static string DbgString(BenefitsStatusRequest request) => $"userId={request?.UserId}";

    }
}
