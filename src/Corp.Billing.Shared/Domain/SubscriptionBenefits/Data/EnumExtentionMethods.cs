﻿using System;
using System.Linq;
using System.Reflection;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Data
{
    public static class EnumExtentionMethods
    {
        public static byte? GetProductFeatureId(this BenefitFeatureTypeEnum benefitFeature)
        {
            FieldInfo fi = benefitFeature.GetType().GetField(benefitFeature.ToString());

            IsValueAddedFeatureAttribute[] attributes =
                (IsValueAddedFeatureAttribute[])fi.GetCustomAttributes(typeof(IsValueAddedFeatureAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].ProdFeatureId;
            }

            return null;
        }

        public static bool IsValueAddedFeature(this BenefitFeatureTypeEnum benefitFeature)
        {
            FieldInfo fi = benefitFeature.GetType().GetField(benefitFeature.ToString());

            IsValueAddedFeatureAttribute[] attributes =
                (IsValueAddedFeatureAttribute[])fi.GetCustomAttributes(typeof(IsValueAddedFeatureAttribute), false);

            return attributes != null && attributes.Length > 0;
        }

        public static BenefitFeatureTypeEnum? GetValueFeatureByProductFeatureId(this byte prodFeatureId)
        {
            var benefitFeatureTypes = Enum.GetValues(typeof(BenefitFeatureTypeEnum)).Cast<BenefitFeatureTypeEnum>();

            foreach (var bft in benefitFeatureTypes)
            {
                FieldInfo fi = bft.GetType().GetField(bft.ToString());

                IsValueAddedFeatureAttribute[] attributes =
                    (IsValueAddedFeatureAttribute[])fi.GetCustomAttributes(typeof(IsValueAddedFeatureAttribute), false);

                if (attributes != null
                    && attributes.Length > 0
                    && attributes[0].ProdFeatureId == prodFeatureId)
                {
                    return bft;
                }
            }

            return null;
        }
    }
}
