﻿namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Data
{
    public enum BenefitPurchaseTypeEnum : byte
    {
        Undefined = 0,
        NotEligibleFor = 1,
        EligibleToPurchase = 2,
        Purchased = 3,
        Complimentary = 4,
    }
}
