﻿namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Data
{
    public enum BenefitFeatureTypeEnum : int
    {
        Undefined = 0,

        EmailCommunication = 1,
        HighlightedProfile = 3,
        FirstImpressions = 5,
        EmailReadNotification = 9,
        MatchPhone = 12,
        ReplyForFree = 28,
        PrivateMode = 29,

        [IsValueAddedFeature(0)]
        MatchTalks = 10000,

        [IsValueAddedFeature(4)]
        UnlimitedDateCoach = 10004,

        [IsValueAddedFeature(17)]
        ProfileProLite = 10017,

        [IsValueAddedFeature(26)]
        FreeBoost = 10026,

        [IsValueAddedFeature(39)]
        SuperLikes = 10039,

        [IsValueAddedFeature(21)]
        DiscountedEvents = 20021,

        [IsValueAddedFeature(21)]
        HappyHour = 30021,
    }
}
