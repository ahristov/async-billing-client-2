﻿using System;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Data
{
    [AttributeUsage(AttributeTargets.All)]
    public class IsValueAddedFeatureAttribute : Attribute
    {
        public byte ProdFeatureId { get; private set; }

        public IsValueAddedFeatureAttribute(byte prodFeatureId)
        {
            ProdFeatureId = prodFeatureId;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            IsValueAddedFeatureAttribute other = obj as IsValueAddedFeatureAttribute;

            return (other != null) && other.ProdFeatureId == this.ProdFeatureId;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool IsDefaultAttribute()
        {
            return this.ProdFeatureId == 0;
        }

    }
}
