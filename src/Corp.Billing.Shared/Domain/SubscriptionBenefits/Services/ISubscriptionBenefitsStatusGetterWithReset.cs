﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Services
{
    public interface ISubscriptionBenefitsStatusGetterWithReset : ISubscriptionBenefitsStatusGetter, IResetableCache<int>
    {
    }
}
