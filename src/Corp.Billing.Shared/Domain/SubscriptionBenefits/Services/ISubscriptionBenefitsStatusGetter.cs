﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Services
{
    public interface ISubscriptionBenefitsStatusGetter
    {
        BenefitsCountNewResponse GetCountOfNewSubscriptionBenefits(BenefitsStatusRequest req);
        BenefitStatusResponse GetSubscriptionBenefitsStatus(BenefitsStatusRequest req);
    }
}
