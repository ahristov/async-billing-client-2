﻿namespace Corp.Billing.Shared.Domain.UserFeatures
{
    public class UserFeaturesLightRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }
}
