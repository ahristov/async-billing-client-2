﻿namespace Corp.Billing.Shared.Domain.Profile
{
    public interface IUserProfileLiteGetter
    {
        GetUserProfileLiteResult GetUserProfileLite(GetUserProfileLiteRequest request);
    }
}
