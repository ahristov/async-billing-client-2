﻿namespace Corp.Billing.Shared.Domain.Orders
{
    public interface IOrderService
    {
        CardinalLogInsertResult CardinalLogInsert(CardinalLogInsertRequest request);
        OrderDiscountsResult GetOrderDiscounts(OrderDiscountsRequest request);
        OneMonthFeatureCompareResult GetOneMonthFeatureCompare(OneMonthFeatureCompareRequest request);
        OrderSummaryResult GetOrderSummary(OrderSummaryRequest request);
        SubscribeResult CreateOrder(SubscribeRequest request);
        SubscribeResultV3 CreateOrderV3(SubscribeRequestV3 request);


        // Transaction

        ProcessTransactionResult ProcessTransaction(ProcessTransactionRequest request);
        ProcessPayPalTransactionResult ProcessPayPalTransaction(ProcessPayPalTransactionRequest request);
        AppleProcessTransactionResult AppleProcessTransaction(AppleProcessTransactionRequest request);
        UpdateTrxStatusResult UpdateTransactionStatus(UpdateTrxStatusRequest request);
        UpdateOfflinePaymentStatusResponse UpdateOfflinePaymentStatus(UpdateOfflinePaymentStatusRequest request);
        GetOfflineTransferDataResult GetOfflineTransferData(GetOfflineTransferDataRequest request);
        
        // Apple
        
        SearchForAppleOrderResult SearchForAppleOrder(SearchForAppleOrderRequest request);
        AppleRetryQueueInsertResult AppleRetryQueueInsert(AppleRetryQueueInsertRequest request);
        

        // Unit products

        BillEventTicketsResult BillEventTickets(BillEventTicketsRequest request);
        ImpulseBuyLastPurchaseResult GetImpulseBuyLastPurchase(ImpulseBuyLastPurchaseRequest request);
    }
}
