﻿namespace Corp.Billing.Shared.Domain.Orders
{
    public class UpdateTrxStatusRequest
    {
        public int? UserId { get; set; }
        public int TrxID { get; set; }
        public int? AcctPymtID { get; set; } 
        public short Result { get; set; }
        public int VerisignCount { get; set; }
        public int? ProcessorLogID { get; set; }
        public int? TrxStatus { get; set; }
        public long? ReferenceNumber { get; set; }

    }
    public class UpdateTrxStatusResult
    {
        public int ReturnValue { get; set; }
        public int TrxStatusID { get; set; }
        public int ResultType { get; set; }
        public int EventAcctDtlID { get; set; }
        public byte EventStatus { get; set; }
        public bool AwardedComplimentary { get; set; }
        public int EcardSubmitStatus { get; set; }
        public string XmlMatchEventTicketsPurchased { get; set; }
        public decimal RefundAmount { get; set; }
    }
}
