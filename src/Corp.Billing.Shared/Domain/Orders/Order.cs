﻿using System;
using System.Collections.Generic;
using Corp.Billing.Shared.Facilities;
using Corp.Billing.Shared.Subscription.Payment;

namespace Corp.Billing.Shared.Domain.Orders
{
    public class Order
    {
        public User UserInfo { get; set; }
        public Payment PaymentInfo { get; set; }
        public Promotion PromotionInfo { get; set; }
        public Products ProductsToPurchase { get; set; }
    }

    public class Products
    {
        public IList<Product> ProductList { get; set; }
        public string OrderString { get; set; }
        public short AccountOrderModificationId { get; set; }   //what is this?

    }

    public class Product
    {

    }

    public class User
    {
        public int UserId { get; set; }
        public short UrlCode { get; set; }
        public int BrandId { get; set; }
        public short GeoStateTaxId { get; set; }
        public DateTime UserStatusDate { get; set; }  //funky usage...be careful

        public short StateId { get; set; }
        public short CountryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryCode { get; set; } //used when subscribing
        public string CountryName { get; set; } //used when updating or getting a method of payment
        public string Zip { get; set; }
        public string Phone { get; set; }
    }

    public class Payment
    {
        public decimal PaymentAmount { get; set; }
        public string AccountNumber { get; set; }
        public int AcctPaymentMethodId { get; set; }

        //CSA specific
        public bool IsComplementary { get; set; }
        public int EmployeeId { get; set; }
        public string Comment { get; set; }

        //TODO abstract these to enums
        public short CreditCode { get; set; }
        public string DetailCode { get; set; }

        //Credit Card specific
        public short ExpireMonth { get; set; }
        public short ExpireYear { get; set; }
        public string CreditCardCVV2 { get; set; }
        public string FullName { get; set; }

        //TODO still used?
        //Switch / Maestro specific
        public string SwitchStartDate { get; set; }
        public string SwitchIssueNumber { get; set; }

        //TODO are all fields used?
        //E-Check specific
        public string CheckNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string DriverLicenseCountry { get { return "US"; } }

        public virtual PaymentDefinitions.PaymentTypes PaymentType { get; set; }
        public virtual PaymentDefinitions.CreditCardTypes CardType { get; set; }
    }

    public class Promotion
    {
        public string PromoId { get; set; }
    }

    public class PaymentResult
    {
        public bool IsStackedSub { get; set; }
        public int TransactionId { get; set; }
        public short SubscribeTextId { get; set; }   //for state term usage
    }






    public class OrderDiscountsRequest : IApiRequestWithQueryString
    {
        public string OrderList { get; set; }
        public string PromoID { get; set; }
    }

    public class OrderDiscountsResult
    {
        public int ReturnValue { get; set; }
        public decimal PercentOff { get; set; }
        public short FreeTrialDays { get; set; }
        public DiscountOfferType PromoOfferType { get; set; }
        public short InitialDays { get; set; }
        public short DollarsOff { get; set; }
    }

    public class OrderSummaryRequest : IApiRequestWithQueryString
    {
        public int UserID { get; set; }
        public string OrderList { get; set; }
        public string PromoID { get; set; }
        public short CountryCode { get; set; }
        public byte StateCode { get; set; }
    }

    public class OrderSummaryResult
    {
        public int ReturnValue { get; set; }
        public List<SummaryItem> SummaryItems { get; set; }
    }

    public class SummaryItem
    {
        public byte LuProdFeatureID { get; set; }
        public long ProdAttributeMask { get; set; }
        public string ProductDescription { get; set; }
        public decimal RenewalPrice { get; set; }
        public decimal RenewalTax { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal SalesTax { get; set; }
    }

    public enum DiscountOfferType
    {
        None = 1,
        FreeTrial = 2,
        TimeAdded = 3,
        PercentOff = 4,
        DollarsOff = 5,
        Sweepstake = 6,
        SaveSub = 7,
        PaidTrial = 8,
        FreeAddOn = 9,
        FreeDaysNonSub = 10,
        RenewalDct = 11,
        AddonBndl = 12,
        EventDscnt = 13,
        FlatInit = 14,
    }


    public class OneMonthFeatureCompareRequest : IApiRequestWithQueryString
    {
        public int UserID { get; set; }
        public string PromoID { get; set; }
        public byte Months { get; set; }
        public string DefaultPromoID { get; set; }
        private byte? _monthstoCompare;

        public byte MonthstoCompare
        {
            get
            {
                if (_monthstoCompare.HasValue && _monthstoCompare.Value > 0)
                    return _monthstoCompare.GetValueOrDefault();
                return 1;

            }
            set { _monthstoCompare = value; }
        }
    }

    public class OneMonthFeatureCompareResult
    {
        public int ReturnValue { get; set; }
        public List<OneMonthFeatureCompareItem> Items = new List<OneMonthFeatureCompareItem>();
    }

    public class OneMonthFeatureCompareItem
    {
        public byte LUProdFeatureID { get; set; }
        public decimal InitialAmt { get; set; }
        public decimal DiscountAmtForNmonths { get; set; }
        public decimal AmtChargedWithoutTaxForNmonths { get; set; }
        public decimal PayableTaxForNmonths { get; set; }
    }


    public class CardinalLogInsertRequest
    {
        public string CardinalTransactionId { get; set; }
        public decimal TrxAmt { get; set; }
        public int UserId { get; set; }
        public byte RequestType { get; set; }
        public DateTime RequestDt { get; set; }
        public String RequestXml { get; set; }
        public DateTime? ResponseDt { get; set; }
        public string ResponseXml { get; set; }
        public string Enrolled { get; set; }
        public string EciFlag { get; set; }
        public string Cavv { get; set; }
        public string XId { get; set; }
        public string AuthenticationPath { get; set; }
    }


    public class CardinalLogInsertResult
    {
        public int ReturnValue { get; set; }
        public int CardinalLogId { get; set; }
    }


    public class BillEventTicketsRequest
    {
        public int TrxId { get; set; }
        public int UserId { get; set; }
    }

    public class BillEventTicketsResult
    {
        public int ReturnValue { get; set; }
        public IList<BillEventTicketItems> Items { get; set; }

        public BillEventTicketsResult()
        {
            Items = new List<BillEventTicketItems>();
        }
    }

    public class BillEventTicketItems
    {
        public int? EventId { get; set; }
        public int? UserId { get; set; }
        public int? AcctDtlId { get; set; }
        public string TicketType { get; set; }
    }



    public class SearchForAppleOrderRequest : IApiRequestWithQueryString
    {
        public int? UserId { get; set; }
        public long? WebOrderLineItemId { get; set; }
    }

    public class SearchForAppleOrderResult
    {
        public int ReturnValue { get; set; }
        public IList<SearchForAppleOrderItem> Items;

        public SearchForAppleOrderResult()
        {
            Items = new List<SearchForAppleOrderItem>();
        }
    }



    public class SearchForAppleOrderItem
    {
        public int? TrxID { get; set; }
        public int? UserID { get; set; }
        public int? AppleResult { get; set; }
        public string ProductID { get; set; }
        public int? Quantity { get; set; }
        public long? TransactionID { get; set; }
        public long? OrigTransactionID { get; set; }
        public DateTime? PurchaseDt { get; set; }
        public DateTime? OrigPurchaseDt { get; set; }
        public DateTime? SubExpirationDt { get; set; }
        public DateTime? CancellationDt { get; set; }
        public string AppItemID { get; set; }
        public long? ExtVersionID { get; set; }
        public long? WebOrderLineItemID { get; set; }
        public DateTime? LastSubmitStatusDt { get; set; }
        public string Receipt { get; set; }
    }


    // Impulse buy

    public interface IImpulseBuyLastPurchaseGetter : IResetableCache<int>
    {
        ImpulseBuyLastPurchaseResult GetImpulseBuyLastPurchase(ImpulseBuyLastPurchaseRequest request);
    }

    [Serializable]
    public class ImpulseBuyLastPurchaseRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public byte ProdFeatureId { get; set; }
    }

    [Serializable]
    public class ImpulseBuyLastPurchaseResult
    {
        public int ReturnValue { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDt { get; set; }
        public int UnclaimedFeatureCount { get; set; }
        public byte UnclaimedComplimentaryCount { get; set; }
        public DateTime? NextExpirationDateUnclaimed { get; set; }
        public byte UnclaimedComplimentaryNextSponserID { get; set; }
        public byte ActiveTopSpotSponsorID { get; set; }
        public bool? ActiveTopSpotComplimentaryBit { get; set; }

        public byte? LasttopspotSponsorID { get; set; }
        public byte? CurrenttopspotSponsorID { get; set; }
        public byte? NexttopspotSponsorID { get; set; }
    }



    // Boleto and offline payments


    public class BoletoInfoResult
    {
        public string FullName { get; set; }
        public decimal OrderAmount { get; set; }
        public DateTime ExpireDate { get; set; }
        public int ReturnValue { get; set; }
        public DateTime TransactionDate { get; set; }
    }

    public class BoletoInfoRequest
    {
        public int TrxId { get; set; }


    }

    public class UpdateOfflinePaymentStatusRequest
    {
        public int? ProcessorLogID { get; set; }
        public int TrxId { get; set; }
        public int TrxStatusId { get; set; }
        public decimal? Amount { get; set; }
        public string CurrencyCode{get; set; }

    }

    public class UpdateOfflinePaymentStatusResponse
    {
        public int ReturnValue { get; set; }
        public int? UserId { get; set; }
    }


    public class GetOfflineTransferDataRequest : IApiRequestWithQueryString
    {
        public int TransactionId { get; set; }
    }

    public class GetOfflineTransferDataResult
    {
        public int? PaymentMethodToken { get; set; }
        public string PaymentMethodType { get; set; }
        public string FullName { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal? TransactionAmount { get; set; }
        public string ISOCurrencyCode { get; set; }
        public byte? luTransactionStatusId { get; set; }
        public int? ReturnValue { get; set; }

        public string BankBranch { get; set; }
        public string AccountNumber { get; set; }

        private const string DirectDebitMethod = "DB";
        public bool IsDirectDebitMethod()
        {
            if (string.IsNullOrEmpty(PaymentMethodType))
                return false;

            return PaymentMethodType.ToUpper().Trim() == DirectDebitMethod;
        }
    }


    public class SearchForDebitPendingTrxRequest : IApiRequestWithQueryString
    {
        public const byte PaymentProcessorId = 15;
        public DateTime? StartPurchaseDateRange { get; set; }
        public DateTime? EndPurchaseDateRange { get; set; }
        public int Bank { get; set; }
    }

    public class SearchForDebitPendingTrxResult
    {
        public int ReturnValue { get; set; }
        public IList<SearchForDebitPendingTrxItem> Items;

        public SearchForDebitPendingTrxResult()
        {
            Items = new List<SearchForDebitPendingTrxItem>();
        }
    }

    public class SearchForDebitPendingTrxItem
    {
        public int TrxID { get; set; }
        public string BankID { get; set; }
        public int PaymentMethodToken { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string FullName { get; set; }
        public decimal Amount  { get; set; }
    }

}
