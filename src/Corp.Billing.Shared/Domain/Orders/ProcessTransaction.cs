﻿using System;

namespace Corp.Billing.Shared.Domain.Orders
{
    public class ProcessTransactionRequest
    {
        public int? UserId { get; set; }
        public int TrxID { get; set; }
        public string CreditCardCVV { get; set; }
        public int? AcctPymtID { get; set; } 
    }

    public class ProcessTransactionResult
    {
        public int ReturnValue { get; set; }
        public short Result { get; set; }
        public int VerisignCount { get; set; }
        public int ProcessorLogID {get;set;}
        public bool IsStackedSub { get; set; }
        public byte MessageResponseID { get; set; }
    }

    public class ProcessPayPalTransactionRequest
    {
        private string _payer;
        private string _payerId;
        private string _payerStatus;
        private string _payerCountry;
        private string _payerFirstName;
        private string _payerLastName;
        private string _billingAddressStatus;
        private string _billingAddressStreet1;
        private string _billingAddressStreet2;
        private string _billingAddressCity;
        private string _billingAddressState;
        private string _billingAddressZip;
        private string _billingAddressCountry;
        private decimal? _grossAmount;
        private decimal? _feeAmount;
        private decimal? _settleAmount;
        private decimal? _exchangeRate;

        public int TrxId { get; set; }
        public string BillingAgreementId { get; set; }
        public string TransactionId { get; set; }

        public decimal? GrossAmount
        {
            get { return _grossAmount ?? 0; }
            set { _grossAmount = value; }
        }

        public decimal? FeeAmount
        {
            get { return _feeAmount ?? 0; }
            set { _feeAmount = value; }
        }

        public decimal? SettleAmount
        {
            get { return _settleAmount ?? 0; }
            set { _settleAmount = value; }
        }

        public decimal? ExchangeRate
        {
            get { return _exchangeRate ?? 0; }
            set { _exchangeRate = value; }
        }

        public string PaymentStatus { get; set; }
        public string PendingStatusCodeType { get; set; }
        public int? CorrelationId { get; set; }

        // not set from Match8
        public string ReasonCode { get; set; }
        public bool RenewalService { get; set; }

        public string Payer
        {
            get { return _payer ?? string.Empty; }
            set { _payer = value; }
        }

        public string PayerId
        {
            get { return _payerId ?? string.Empty; }
            set { _payerId = value; }
        }

        public string PayerStatus
        {
            get { return _payerStatus ?? string.Empty; }
            set { _payerStatus = value; }
        }

        public string PayerCountry
        {
            get { return _payerCountry ?? string.Empty; }
            set { _payerCountry = value; }
        }

        public string PayerFirstName
        {
            get { return _payerFirstName ?? string.Empty; }
            set { _payerFirstName = value; }
        }

        public string PayerLastName
        {
            get { return _payerLastName ?? string.Empty; }
            set { _payerLastName = value; }
        }

        public string BillingAddressStatus
        {
            get { return _billingAddressStatus ?? string.Empty; }
            set { _billingAddressStatus = value; }
        }

        public string BillingAddressStreet1
        {
            get { return _billingAddressStreet1 ?? string.Empty; }
            set { _billingAddressStreet1 = value; }
        }

        public string BillingAddressStreet2
        {
            get { return _billingAddressStreet2 ?? string.Empty; }
            set { _billingAddressStreet2 = value; }
        }

        public string BillingAddressCity
        {
            get { return _billingAddressCity ?? string.Empty; }
            set { _billingAddressCity = value; }
        }

        public string BillingAddressState
        {
            get { return _billingAddressState ?? string.Empty; }
            set { _billingAddressState = value; }
        }

        public string BillingAddressZip
        {
            get { return _billingAddressZip ?? string.Empty; }
            set { _billingAddressZip = value; }
        }

        public string BillingAddressCountry
        {
            get { return _billingAddressCountry ?? string.Empty; }
            set { _billingAddressCountry = value; }
        }
    }

    public class ProcessPayPalTransactionResult
    {
        public int ReturnValue { get; set; }
        public bool IsStackedSub { get; set; }
        public int TrxStatusID { get; set; }

        public bool IsCommFailure
        {
            get
            {
                return TrxStatusID == 7;
            }
        }
        public bool IsGeneralFailure
        {
            get
            {
                return TrxStatusID == 2;
            }
        }

        public string XmlMatchEventTicketsPurchased { get; set; }
        public decimal RefundAmount { get; set; }
    }

    public class AppleProcessTransactionRequest
    {
        public string AppleItemId { get; set; }
        public int AppleStatus { get; set; }
        public long AppleTransactionId { get; set; }
        public int MatchResult { get; set; }
        public long OriginalAppleTransactionId { get; set; }
        public DateTime OriginalPurchaseDate { get; set; }
        public string ProductId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int Quantity { get; set; }
        public string Receipt { get; set; }
        public DateTime? SubscriptionExpirationDate { get; set; }
        public int TransactionId { get; set; }
        public int UserId { get; set; }
        public long? WebOrderLineItemId { get; set; }
        public long? ExtVersionID { get; set; }
        public bool IsDoubleBilled { get; set; }

        public short? ApplicationVersion { get; set; }
        public bool IsAppleIAPSandbox { get; set; }
        public bool AutoRenewStatus { get; set; }
    }

    public class AppleProcessTransactionResult
    {
        public bool? IsDoubleBilled { get; set; }
        public int ReturnValue { get; set; }
    }

    public class AppleRetryQueueInsertRequest
    {
        public int UserId { get; set; }
        public Guid SessionId { get; set; }
        public string ProductId { get; set; }
        public string ProductDetailId { get; set; }
        public string Receipt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AppleStatusId { get; set; }
        public int URLCode { get; set; }
        public string ClientIP { get; set; }
        public string DefaultPromoId { get; set; }
    }

    public class AppleRetryQueueInsertResult
    {
        public int ReturnValue { get; set; }
    }

}
