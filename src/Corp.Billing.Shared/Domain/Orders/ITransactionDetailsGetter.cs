﻿namespace Corp.Billing.Shared.Domain.Orders
{
    public interface ITransactionDetailsGetter
    {
        TransactionDetailsResult GetTrxDetails(TransactionDetailsRequest request);
    }
}
