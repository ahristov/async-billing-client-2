﻿using Corp.Billing.Shared.Domain.PaymentV3.Data.Response;
using Corp.Billing.Shared.Subscription.PaymentMethodType;
using System;

namespace Corp.Billing.Shared.Domain.Orders
{
    [Serializable]
    public class TransactionDetailsResult: IApiResponse
    {
        public string ConfirmationNumber { get; set; } // from request
        public string Last4OfCreditCard { get; set; } // @PaymentMethodNumber
        public string PaymentMethodDtlCode { get; set; }
        public string PaymentMethodBaseCode { get; set; }
        public decimal TotalAmountIncludingTax { get; set; } // @SalesAmountInclTax
        public decimal TaxAmount { get; set; } // @SalesTaxAmount
        public DateTime? TransactionDate { get; set; } // @OrderCreateDt
        public DateTime? PaidThroughDate { get; set; } // @PaidThroughDate
        public decimal? RenewalAmountIncludingTax { get; set; } // @RenewalAmountInclTax
        public decimal? RenewalTaxAmount { get; set; } // @RenewalTaxAmount
        public string ISOCurrencyCode { get; set; } // @ISOCurrencyCode
        public string PromoId { get; set; } // @OrderPromoID
        public int FreeTrailDays { get; set; } // @FreeTrialDays
        public bool DiscountApplied { get; set; } // lookup promo details
        public bool DiscountAppliesToRenewal { get; set; }  // lookup promo details
        public int? OriginAreaId { get; set; } // Origins
        public byte? OriginPlatformId { get; set; } // Origins
        public int SubUserState { get; set; } // Sub Details
        public int TransactionStatus { get; set; }
        public string TransactionStatusName => ((PaymentResultTypeEnum)this.TransactionStatus).ToString();
        public bool IsMembershipUpgrade { get; set; }
        public decimal? RefundAmount { get; set; }
        public int PaymentMethodBaseType { get; set; }
        public string PaymentMethodBaseTypeName => ((PaymentMethodBaseTypeEnum)this.PaymentMethodBaseType).ToString();
        public int PaymentMethodType { get; set; }
        public string PaymentMethodTypeName => ((PaymentMethodTypeEnum)this.PaymentMethodType).ToString();
        public Guid? RateCardRefId { get; set; }
        public decimal? InstallmentSalesTaxAmount { get; set; }
        public decimal? InstallmentSalesAmountInclTax { get; set; }
        public decimal? InstallmentRenewalTaxAmount { get; set; }
        public decimal? InstallmentRenewalAmountInclTax { get; set; }
        public int NumberOfInstallments { get; set; }
        public int NumberOfRenewalInstallments { get; set; }

    }
}
