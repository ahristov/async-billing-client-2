﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Data;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Services
{
    public class ProvisionIAPResponse
    {
        public PerformedActionType PerformedAction { get; set; }
        public IList<ProvisionedFeature> AddedFeatures { get; set; }
        public Guid? RateCardRefId { get; set; }

        public ProvisionIAPResponse()
        {
            AddedFeatures = new List<ProvisionedFeature>();
        }
    }
}
