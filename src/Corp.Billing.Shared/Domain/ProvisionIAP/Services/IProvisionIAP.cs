﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Data;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Services
{
    public interface IProvisionIAP
    {
        ProvisionIAPResponse ProvisionReceipt(ProvisionIAPRequest req);
    }
}
