﻿using System;
using ReceiptData = Corp.Billing.Shared.Domain.ProvisionIAP.Data;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Services
{
    public class ProvisionIAPRequest : IApiRequestWithQueryString
    {
        public ReceiptData.ReceiptSourceType ReceiptSource { get; set; }
        public ReceiptData.ReceiptFlowType ReceiptFlow { get; set; }

        public string Receipt { get; set; }
        public int? OtherUserId { get; set; }

        public int? UserId { get; set; }
        public byte? PlatformId { get; set; }
        public Guid? SessionId { get; set; }

        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }
		public string UserAgentHash { get; set; }
		public string MachineId { get; set; }
		public string EventCorrelationId { get; set; }

        public int? AppRateCardKey { get; set; }
        public Guid? RateCardRefId { get; set; }
    }
}
