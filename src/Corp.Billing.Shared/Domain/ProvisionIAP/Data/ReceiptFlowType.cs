﻿namespace Corp.Billing.Shared.Domain.ProvisionIAP.Data
{
    public enum ReceiptFlowType
    {
        Undefined,
        PurchaseFlow,
        RestorePurchaseFlow,
        PurchaseQueueFlow,
    }
}
