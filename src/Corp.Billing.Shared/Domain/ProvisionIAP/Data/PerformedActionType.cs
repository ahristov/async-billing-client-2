﻿namespace Corp.Billing.Shared.Domain.ProvisionIAP.Data
{
    public enum PerformedActionType
    {
        None = 0,
        NewPurchase = 1,
        Renewal = 2,
        ChargeBack = 3,
        Downgrade = 4,
        Upgrade = 5,
        CancelRenewal = 6,
    }
}
