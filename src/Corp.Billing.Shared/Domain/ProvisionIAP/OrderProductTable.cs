﻿using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.Subscription;

namespace Corp.Billing.Shared.Domain.ProvisionIAP
{
    public class OrderProductTable : OrderDetailTable
    {
        public Duration Duration { get; set; }
        public Quantity Quantity { get; set; }

        public OrderProductTable()
        {
            Duration = new Duration();
            Quantity = new Quantity();
        }
    }
}
