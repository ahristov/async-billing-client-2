﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class OrderCreateException : BaseProvisionIAPException
    {
        int? _orderCreateRetutnCode;

        public OrderCreateException() : base()  { }

        public OrderCreateException(ProvisionIAPRequest req)
            : this(req, null)
        { }

        public OrderCreateException(ProvisionIAPRequest req, int? orderCreateRetutnCode)
            : base(req)
        {
            _orderCreateRetutnCode = orderCreateRetutnCode;
        }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.OrderCreateError; } }
        public override string ErrorMessage { get { return string.Format("Order create error"); } }
        public override int? OrderCreateReturnCode { get { return _orderCreateRetutnCode; } }
    }
}
