﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Data;
using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class IncorrectAssociationException : BaseProvisionIAPException
    {
        private readonly UserToReceiptAssociationType _association;
        private readonly ExceptionReceiptData _receiptData;

        public IncorrectAssociationException() : base()  { }

        public IncorrectAssociationException(ProvisionIAPRequest req, UserToReceiptAssociationType association, ExceptionReceiptData receiptData)
            : base(req)
        {
            _association = association;
            _receiptData = receiptData;
        }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.IncorrectAssociation; } }
        public override string ErrorMessage { get { return "Incorrect association"; } }
        public override UserToReceiptAssociationType? UserToReceiptAssociation { get { return _association; } }
        public override ExceptionReceiptData ReceiptData { get { return _receiptData; }  }
    }
}
