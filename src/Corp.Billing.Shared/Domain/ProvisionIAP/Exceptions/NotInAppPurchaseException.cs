﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class NotInAppPurchaseException : BaseProvisionIAPException
    {

        public NotInAppPurchaseException() : base() { }

        public NotInAppPurchaseException(ProvisionIAPRequest req)
            : base(req)
        { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.NotInAppPurchase; } }
        public override string ErrorMessage { get { return "Not in app purchase"; } }
    }
}
