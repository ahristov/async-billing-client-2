﻿namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class ExceptionReceiptData
    {
        public bool IsConsumable { get; set; }
        public string ExternalProductId { get; set; }
        public long TransactionId { get; set; }
        public long OriginalTransactionId { get; set; }
        public long? WebOrderLineItemId { get; set; }
    }
}
