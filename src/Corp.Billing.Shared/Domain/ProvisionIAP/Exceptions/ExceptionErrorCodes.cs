﻿namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public enum ExceptionErrorCodes
    {
        Undefined = 0,

        NotInAppPurchase = 1010,
        AlreadyProvisioned = 1020,
        IncorrectAssociation = 1030,
        ReceiptVerificationError = 1040,
        UserNotFoundError = 1050,
        ExternalProductIdNotFound = 1060,

        OrderCreateError = 2010,
    }
}
