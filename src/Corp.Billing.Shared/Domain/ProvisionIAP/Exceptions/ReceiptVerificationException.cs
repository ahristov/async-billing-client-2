﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class ReceiptVerificationException : BaseProvisionIAPException
    {
        private readonly int _receiptVerificationStatusCode;

        public ReceiptVerificationException() : base() { }


        public ReceiptVerificationException(ProvisionIAPRequest req, int receiptVerificationStatusCode)
            : base(req)
        {
            _receiptVerificationStatusCode = receiptVerificationStatusCode;
        }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.ReceiptVerificationError; } }
        public override string ErrorMessage { get { return "Error verifying receipt"; } }
        public override int? ReceiptVerificationStatusCode { get { return _receiptVerificationStatusCode; } }
    }
}
