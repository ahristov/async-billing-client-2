﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class UserNotFoundException : BaseProvisionIAPException
    {
        public UserNotFoundException() : base() { }

        public UserNotFoundException(ProvisionIAPRequest req)
            : base(req)
        { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.UserNotFoundError; } }
        public override string ErrorMessage { get { return string.Format("User not found"); } }
    }
}
