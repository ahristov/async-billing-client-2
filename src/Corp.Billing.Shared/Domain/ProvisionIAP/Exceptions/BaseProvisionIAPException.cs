﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Data;
using Corp.Billing.Shared.Domain.ProvisionIAP.Services;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public abstract class BaseProvisionIAPException : Exception
    {
        public BaseProvisionIAPException()
        { }

        public BaseProvisionIAPException(ProvisionIAPRequest req)
        {
            Init(req);
        }

        public void Init(ProvisionIAPRequest req)
        {
            Request = req;
        }

        public ProvisionIAPRequest Request { get; private set; }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }

        public virtual int? ReceiptVerificationStatusCode { get { return null; } }
        public virtual UserToReceiptAssociationType? UserToReceiptAssociation { get { return null; } }
        public virtual string ExternalProductId { get { return null; } }
        public virtual int? OrderCreateReturnCode { get { return null; } }
        public virtual ExceptionReceiptData ReceiptData { get { return null; } }

    }
}
