﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class AlreadyProvisionedException : BaseProvisionIAPException
    {
        public AlreadyProvisionedException() : base() { }

        public AlreadyProvisionedException(ProvisionIAPRequest req)
            : base(req)
        { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.AlreadyProvisioned; } }
        public override string ErrorMessage { get { return "User already provisioned"; } }
    }
}
