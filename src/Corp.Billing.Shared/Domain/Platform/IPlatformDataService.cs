﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Platform
{
    public interface IPlatformDataService
    {
        IDictionary<byte, bool> GetPlatformIdIsMobileMappings();
    }
}
