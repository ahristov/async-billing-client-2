﻿using System;

namespace Corp.Billing.Shared.Domain.Platform
{
    [Serializable]
    public class RegistrationPlatformResult
    {
        public byte? PlatformId { get; set; }
        public bool? IsMobile { get; set; }
    }
}
