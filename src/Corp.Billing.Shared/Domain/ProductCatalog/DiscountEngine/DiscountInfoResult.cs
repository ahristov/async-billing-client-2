﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Corp.Billing.Shared.Domain.Promos;

namespace Corp.Billing.Shared.Domain.ProductCatalog.DiscountEngine
{
    public class DiscountInfoResult : PCRulesResult
    {
        private string _promoId;

        public string PromoId
        {
            get
            {
                return _promoId.NormalizePromoId();
            }
            set
            {
                _promoId = value.NormalizePromoId();
                IsDefaultPromoId = _promoId == DefaultPromoId;
            }
        }

        public bool IsDefaultPromoId { get; set; }

        public GetPromoDataResult PromoData { get; set; }


        public static string DefaultPromoId
        {
            get
            {
                return string.Empty.NormalizePromoId();
            }
        }

        public DiscountInfoResult()
        {
            PromoId = DefaultPromoId;
            PromoData = new GetPromoDataResult();
        }

        public bool DiscountOnApplyOnly
        {
            get
            {
                try
                {
                    const string KEY_ON_APPLY_ONLY = "DiscountOnApplyOnly";

                    if (this.RuleResults == null
                        || !this.RuleResults.ContainsKey(KEY_ON_APPLY_ONLY))
                        return false;

                    var val = (this.RuleResults[KEY_ON_APPLY_ONLY] ?? string.Empty).Trim();

                    bool res;
                    bool.TryParse(val ?? "false", out res);
                    return res;
                }
                catch
                {
                    return false;
                }
            }
        }

    }

}