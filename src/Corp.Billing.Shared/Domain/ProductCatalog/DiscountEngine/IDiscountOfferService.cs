﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.DiscountEngine
{
    public interface IDiscountOfferService
    {
        DiscountInfoResult GetDiscountInfo(PCRulesRequest request);
    }
}
