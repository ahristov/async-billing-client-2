﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PricingEngine
{
    public class PriceTargetingInfoV2Result : PCRulesResult
    {
        public string RateCardKey { get; set; }

        public string RootRateCardKey { get; set; }
        public IList<int> RateCardOverrideKeys { get; set; }

        public PriceTargetingInfoV2Result()
        {
            RateCardOverrideKeys = new List<int>();
        }
    }
}
