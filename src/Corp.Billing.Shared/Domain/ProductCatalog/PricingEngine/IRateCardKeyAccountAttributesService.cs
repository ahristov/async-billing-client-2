﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.PricingEngine
{
    public interface IRateCardKeyAccountAttributesService
    {
        void SetRateCardKeyAccountAttributes(int userId, string rateCardKey, RateCardKeyAccountAttributesItem accountAttributes);
        RateCardKeyAccountAttributesItem GetRateCardKeyAccountAttributes(int userId, string rateCardKey);
    }
}
