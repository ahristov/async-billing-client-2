﻿using System;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PricingEngine
{
    [Serializable]
    public class RateCardKeyAccountAttributesItem
    {
        public int PriceGrpId { get; set; }
        public int CBSACode { get; set; }
        public string PostalCode { get; set; }
    }
}
