﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride;
using Corp.Billing.Shared.Domain.ProductCatalog.RateCards;
using Corp.Billing.Shared.Domain.RateCardV3.Data;

namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public interface IPCRulesContext : IApiRequestWithQueryString, IHasTraceParams
    {
        /// <summary>
        /// User ID. The ID of the user.
        /// </summary>
        int UserId { get; set; }

        /// <summary>
        /// Browser user agent. User agent string from the user's browser. Used from some tests (Android discounts etc.)
        /// </summary>
        string UserAgent { get; set; }

        /// <summary>
        /// Gets or sets the application version of the client facing app.
        /// </summary>
        /// <value>
        /// The application version of the client facing app.
        /// </value>
        string AppVersion { get; set; }


        /// <summary>
        /// Platform ID. The Platform ID of the application calling the BillAPI.
        /// </summary>
        byte? PlatformId { get; set; }

        /// <summary>
        /// The site/app passes in what is the current session promo id.
        /// </summary>
        /// <remarks>
        /// It is needed to get the base ratecard.
        /// It is optional for the discounting engine, only is used for the "Free trial, CRM link" path so far.
        /// </remarks>
        string PromoId { get; set; }


        /// <summary>
        /// Optional: It was passed in to the site query string parameter to the landing page.
        /// Note: On Match8 this is stored on the session object as `bool HasPriceUpParam`.
        /// </summary>
        bool PriceUpParam { get; set; }

        /// <summary>
        /// Specific to discount engine; If true, skip discounts that only apply on banner click and not automatically, such as FYO.
        /// </summary>
        bool SkipOnApplyOnlyDiscountsParam { get; set; }

        byte? ProdFeatureId { get; set; }

        /// <summary>
        /// Upfront specify what kind of price override to evaluate.
        /// That gives a way to prevent rules to be executed in standard flows and only be executed in specific flows.
        /// </summary>
        /// <value>
        /// The type of the price override upsell.
        /// </value>
        PriceOverrideUpsellTypeEnum PriceOverrideUpsellTypeParam { get; set; }

        RequestedRateCardType RequestedRateCardType { get; set; }

        CatalogType CatalogTypeParam { get; set; }

        CatalogSource CatalogSourceParam { get; set; }

        /******************************************************************************************
         * Optional params to override user data - typically only used for testing
         ******************************************************************************************/
        /// <summary>
        /// Optional: If specified, use this instead of user's registration platform id.
        /// </summary>
        byte? RegistrationPlatformId { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's registration IsMobile flag.
        /// </summary>
        bool? RegistrationIsMobile { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's country code.
        /// </summary>
        short? CountryCode { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's state code.
        /// </summary>
        short? StateCode { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's site code.
        /// </summary>
        byte? SiteCode { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's CBSA code.
        /// </summary>
        int? CBSACode { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's CBSA group.
        /// </summary>
        int? CBSAGroup { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's age.
        /// </summary>
        int? Age { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's channel.
        /// </summary>
        string ChannelName { get; set; }

        /// <summary>
        /// Optional: If specified, use this instead of user's brand/non-brand rollup.
        /// </summary>
        string BrandNonBrandRollUp { get; set; }
    }
}
