﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public static class PCRulesContextExtensions
    {
        /// <summary>
        /// Copies data from one context object to another.
        /// It is used to set HttpContext data from the request parameters, that is later from the rule condition evaluators.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        public static void CopyTo(this IPCRulesContext source, IPCRulesContext target)
        {
            target.TraceLevel = source.TraceLevel;
            target.TraceVerbosity = source.TraceVerbosity;

            target.UserId = source.UserId;
            if (source.PlatformId.HasValue) { target.PlatformId = source.PlatformId; }
            target.UserAgent = source.UserAgent;
            target.AppVersion = source.AppVersion;

            target.PriceUpParam = source.PriceUpParam;
            target.SkipOnApplyOnlyDiscountsParam = source.SkipOnApplyOnlyDiscountsParam;
            target.ProdFeatureId = source.ProdFeatureId;

            target.PromoId = source.PromoId;
            target.PriceOverrideUpsellTypeParam = source.PriceOverrideUpsellTypeParam;
            target.RequestedRateCardType = source.RequestedRateCardType;

            target.CatalogTypeParam = source.CatalogTypeParam;
            target.CatalogSourceParam = source.CatalogSourceParam;

            target.TraceLevel = source.TraceLevel;
            target.TraceVerbosity = source.TraceVerbosity;

            // Optional properties
            if (source.RegistrationPlatformId.HasValue) { target.RegistrationPlatformId = source.RegistrationPlatformId; }
            if (source.RegistrationIsMobile.HasValue) { target.RegistrationIsMobile = source.RegistrationIsMobile; }
            if (source.CountryCode.HasValue) { target.CountryCode = source.CountryCode; }
            if (source.StateCode.HasValue) { target.StateCode = source.StateCode; }
            if (source.SiteCode.HasValue) { target.SiteCode = source.SiteCode; }
            if (source.CBSACode.HasValue) { target.CBSACode = source.CBSACode; }
            if (source.CBSAGroup.HasValue) { target.CBSAGroup = source.CBSAGroup; }
            if (source.Age.HasValue) { target.Age = source.Age; }

            if (!string.IsNullOrEmpty(source.ChannelName)) { target.ChannelName = source.ChannelName; }
            if (!string.IsNullOrEmpty(source.BrandNonBrandRollUp)) { target.BrandNonBrandRollUp = source.BrandNonBrandRollUp; }
        }
    }
}
