﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    [Serializable]
    public class EvaluateTestVariablesResult
    {
        public bool Result { get; set; }
        public Dictionary<string, string> Variables { get;  set; }

        public EvaluateTestVariablesResult()
        {
            Variables = new Dictionary<string, string>();
        }
    }
}
