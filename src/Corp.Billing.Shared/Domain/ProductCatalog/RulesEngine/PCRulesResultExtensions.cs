﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public static class PCRulesResultExtensions
    {
        public static void CopyTo(this PCRulesResult source, PCRulesResult target)
        {
            target.RuleId = source.RuleId;
            target.RuleResults = source.RuleResults;
            target.RuleConditionTraces.AddRange(source.RuleConditionTraces.ToArray());
        }

        public static T InitFrom<T>(this T target, PCRulesResult source)
            where T : PCRulesResult
        {
            source.CopyTo(target);
            return target;
        }

        public static PCRulesResult CreateFrom(this PCRulesResult source)
        {
            var target = new PCRulesResult();
            source.CopyTo(target);
            return target;
        }

    }
}
