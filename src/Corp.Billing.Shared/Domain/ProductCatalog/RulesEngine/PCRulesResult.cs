﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public class PCRulesResult
    {
        public PCRulesResult()
        {
            RuleResults = new Dictionary<string, string>();
            RuleConditionTraces = new List<PCRuleConditionTrace>();
        }

        public string RuleId { get; set; }
        public Dictionary<string, string> RuleResults { get; set; }

        public List<PCRuleConditionTrace> RuleConditionTraces { get; private set; }
    }
}
