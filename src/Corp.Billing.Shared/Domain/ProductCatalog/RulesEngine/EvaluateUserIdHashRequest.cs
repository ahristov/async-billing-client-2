﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public class EvaluateUserIdHashRequest : IApiRequestWithQueryString
    {
        public string TestName { get; set; }
        public string MatchChars { get; set; }
        public byte MatchPosition { get; set; }
        public int UserId { get; set; }
    }
}
