﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public interface IEvaluateTestService
    {
        bool Evaluate(PCRulesRequest request, string testName);
        EvaluateTestVariablesResult EvaluateTestVariables(PCRulesRequest request, string testName);
    }
}
