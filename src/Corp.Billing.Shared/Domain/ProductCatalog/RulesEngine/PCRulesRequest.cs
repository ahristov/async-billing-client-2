﻿using System;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.RateCardV3.Data;
using Corp.Billing.Shared.Domain.ProductCatalog.RateCards;

namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    /// <summary>
    /// Contains request data for discount engine, pricing engine, base rate card, etc.
    /// </summary>
    /// <seealso cref="Corp.Billing.Shared.ApiClient.Base.BaseRequestWithTraceParams" />
    /// <seealso cref="Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine.IPCRulesContext" />
    public class PCRulesRequest : BaseRequestWithTraceParams, IPCRulesContext
    {
        //------------------------------------------------------
        // When adding properties, also change the copy method
        // from PCRulesRequestExtensions
        //------------------------------------------------------

        public int UserId { get; set; }
        public byte? PlatformId { get; set; }
        public Guid? SessionId { get; set; }

        public string UserAgent { get; set; }
        public string AppVersion { get; set; }

        public bool PriceUpParam { get; set; }


        private string _promoId;
        public string PromoId
        {
            get { return _promoId.NormalizePromoId(); }
            set { _promoId = value; }
        }

        private PriceOverrideUpsellTypeEnum _priceOverrideUpsellType;
        public PriceOverrideUpsellTypeEnum PriceOverrideUpsellTypeParam
        {
            get { return _priceOverrideUpsellType; }
            set { _priceOverrideUpsellType = value; }
        }

        private CatalogType _catalogTypeParam;
        public CatalogType CatalogTypeParam
        {
            get { return _catalogTypeParam; }
            set { _catalogTypeParam = value; }
        }

        private CatalogSource _catalogSourceParam;
        public CatalogSource CatalogSourceParam
        {
            get { return _catalogSourceParam; }
            set { _catalogSourceParam = value; }
        }

        public byte? ProdFeatureId { get; set; }
        public RequestedRateCardType RequestedRateCardType { get; set; }

        public int? AreaId { get; set; }

        // User data overrides - typically for testing only ////////////////////////////////////

        public byte? RegistrationPlatformId { get; set; }
        public bool? RegistrationIsMobile { get; set; }
        public short? CountryCode { get; set; }
        public short? StateCode { get; set; }
        public byte? SiteCode { get; set; }
        public int? CBSACode { get; set; }
        public int? CBSAGroup { get; set; }
        public int? Age { get; set; }
        public string ChannelName { get; set; }
        public string BrandNonBrandRollUp { get; set; }
        public bool SkipOnApplyOnlyDiscountsParam { get; set; }
    }
}
