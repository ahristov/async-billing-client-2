﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCards
{
    public interface IRateCardXmlGetter
    {
        RateCardXmlDataResult GetRateCardXml(RateCardXmlDataRequest request);
    }
}
