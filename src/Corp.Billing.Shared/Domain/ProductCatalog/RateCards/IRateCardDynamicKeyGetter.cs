﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCards
{
    public interface IRateCardDynamicKeyGetter
    {
        RateCardDynamicKeyResult GetRateCardDynamicKey(RateCardDynamicKeyRequest request);
    }
}
