﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCardOverride
{
    public interface IRateCardOverrideXmlGetter
    {
        RateCardOverrideXmlDataResult GetRateCardOverrideXml(RateCardOverrideXmlDataRequest request);
    }
}
