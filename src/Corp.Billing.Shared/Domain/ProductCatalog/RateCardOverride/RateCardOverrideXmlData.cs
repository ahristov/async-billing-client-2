﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCardOverride
{
    public class RateCardOverrideXmlDataRequest : IApiRequestWithQueryString
    {
        public int RateCardOverrideKey { get; set; }
    }

    public class RateCardOverrideXmlDataResult
    {
        public string RateCardOverride { get; set; }
    }
}
