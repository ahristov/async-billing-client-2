﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public interface IPriceOverrideResult
    {
        int? RateCardOverrideKey { get; }
        PriceOverrideRuleCollection PriceOverrides { get; }
        PriceOverrideUpsellTypeEnum UpsellType { get;  }
    }
}
