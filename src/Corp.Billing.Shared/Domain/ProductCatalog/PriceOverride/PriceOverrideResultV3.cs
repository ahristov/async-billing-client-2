﻿using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideResultV3 : PriceOverrideRulesResultV3, IPriceOverrideResultV3
    {
        public PriceOverrideRuleCollection PriceOverrides { get; set; }

        public PriceOverrideResultV3()
        {
            PriceOverrides = new PriceOverrideRuleCollection();
        }

        public static PriceOverrideResultV3 InitFrom(PriceOverrideRulesResultV3 copyFrom)
        {
            var res = new PriceOverrideResultV3();

            if (copyFrom != null)
            {
                copyFrom.CopyTo(res);

                res.RateCardOverrideKeys = copyFrom.RateCardOverrideKeys;
                res.UpsellType = copyFrom.UpsellType;
            }

            return res;
        }
    }
}
