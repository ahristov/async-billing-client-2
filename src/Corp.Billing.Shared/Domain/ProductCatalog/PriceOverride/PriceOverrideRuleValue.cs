﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideRuleValue
    {
        public byte Months { get; set; }
        public bool IsBundled { get; set; }
        public bool IsUpsell { get; set; }
        public decimal? OverrideSalesPrice { get; set; }
        public decimal? OverrideRenewalPrice { get; set; }
        public byte? luProdFeatureId { get; set; }
    }

}
