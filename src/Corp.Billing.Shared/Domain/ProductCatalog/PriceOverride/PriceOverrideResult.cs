﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideResult : PCRulesResult, IPriceOverrideResult
    {
        public int? RateCardOverrideKey { get { return null; } }
        //
        public PriceOverrideRuleCollection PriceOverrides{get; set;}
        public PriceOverrideUpsellTypeEnum UpsellType { get; set; }

        public PriceOverrideResult()
        {
            PriceOverrides = new PriceOverrideRuleCollection();
        }
    }
}
