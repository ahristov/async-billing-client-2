﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public enum PriceOverrideUpsellTypeEnum : byte
    {
        None,

        /// <summary>
        /// For this upsell type, if the user selected 1 month on the rate card, we provive upsell to 6m bundle page instead of add ons page.
        /// </summary>
        OrderUpsell,

        /// <summary>
        /// Stacked sub given to users to upsell to higher packages.
        /// </summary>
        MemberUpgrade,
    }
}
