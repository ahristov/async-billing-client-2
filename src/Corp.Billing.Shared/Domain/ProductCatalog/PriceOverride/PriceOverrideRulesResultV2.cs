﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideRulesResultV2 : PCRulesResult
    {
        public int? RateCardOverrideKey { get; set; }
        public PriceOverrideUpsellTypeEnum UpsellType { get; set; }
    }
}
