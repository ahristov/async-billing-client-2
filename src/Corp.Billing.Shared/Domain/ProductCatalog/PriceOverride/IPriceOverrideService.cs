﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public interface IPriceOverrideService 
    {
        PriceOverrideResult GetPriceOverrideInfo(PCRulesRequest request);
        PriceOverrideResultV2 GetPriceOverrideInfoV2(PCRulesRequest request);
        PriceOverrideResultV3 GetPriceOverrideInfoV3(PCRulesRequest request);
        bool GetMemberUpgradeEligible(PCRulesRequest request);
    }
}
