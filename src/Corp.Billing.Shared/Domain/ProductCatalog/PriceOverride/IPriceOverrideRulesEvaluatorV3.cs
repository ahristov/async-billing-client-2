﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public interface IPriceOverrideRulesEvaluatorV3
    {
        PriceOverrideRulesResultV3 GetPriceOverrideRulesResultV3(PCRulesRequest request);
    }
}
