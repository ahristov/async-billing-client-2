﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public static class PriceOverrideRulesResultV2Extensions
    {
        public static void CopyTo(this PriceOverrideRulesResultV2 source, PriceOverrideRulesResultV2 target)
        {
            PCRulesResultExtensions.CopyTo(source, target);

            target.RuleId = source.RuleId;
            target.RuleResults = source.RuleResults;
            target.RuleConditionTraces.AddRange(source.RuleConditionTraces.ToArray());
        }

        public static T InitFrom<T>(this T target, PriceOverrideRulesResultV2 source)
            where T : PriceOverrideRulesResultV2
        {
            source.CopyTo(target);
            return target;
        }

        public static PriceOverrideRulesResultV2 CreateFrom(this PriceOverrideRulesResultV2 source)
        {
            var target = new PriceOverrideRulesResultV2();
            source.CopyTo(target);
            return target;
        }
    }
}
