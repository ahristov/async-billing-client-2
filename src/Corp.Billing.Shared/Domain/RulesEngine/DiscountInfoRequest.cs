﻿namespace Corp.Billing.Shared.Domain.RulesEngine
{
    public class DiscountInfoRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }
}
