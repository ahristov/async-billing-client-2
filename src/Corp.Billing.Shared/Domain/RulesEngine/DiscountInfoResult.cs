﻿namespace Corp.Billing.Shared.Domain.RulesEngine
{
    public class DiscountInfoResult
    {
        public string RuleId { get; set; }
        public string PromoId { get; set; }
    }
}
