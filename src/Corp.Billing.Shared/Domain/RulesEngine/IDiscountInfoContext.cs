﻿namespace Corp.Billing.Shared.Domain.RulesEngine
{
    public interface IDiscountInfoContext
    {
        int UserId { get; set; }
    }
}
