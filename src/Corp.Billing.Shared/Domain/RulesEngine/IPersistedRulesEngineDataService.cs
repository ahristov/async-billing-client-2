﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.RulesEngine
{
    public interface IPersistedRulesEngineDataService : IResetableCache<int>
    {
        PersistedPlatformIdDataResult GetPersistedPlatformIdData(int userId);
        PersistedPlatformIdDataResult SetPersistedPlatformIdData(int userId, int platformId, bool isMobile);
    }
}
