﻿using System;

namespace Corp.Billing.Shared.Domain.RulesEngine
{
    [Serializable]
    public class PersistedPlatformIdDataResult
    {
        public int PlatformId { get; set; }
        public bool IsMobile { get; set; }
    }
}
