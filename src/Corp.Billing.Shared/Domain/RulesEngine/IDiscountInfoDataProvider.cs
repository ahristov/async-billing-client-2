﻿using System;

namespace Corp.Billing.Shared.Domain.RulesEngine
{
    public interface IDiscountInfoDataProvider
    {
        // IUserProfileLiteGetter data

        DateTime RegistrationDate { get; }
        int Age { get; }
        short SiteCode { get; }
        short CountryCode { get; }
        short StateCode { get; }
        short UrlCode { get; }

        byte GenderCode { get; }
        byte GenderGenderSeekCode { get; }
        bool IsProfileOn { get; }

        // IUserSubscriptionStatusGetter data

        bool IsSubscriber { get; }
        bool NeverSubscribed { get; }
        bool IsFreeTrialUser { get; }
    }
}
