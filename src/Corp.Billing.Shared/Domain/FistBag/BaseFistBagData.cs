﻿using System;

namespace Corp.Billing.Shared.Domain.FistBag
{

    public class GetFistBagRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class GetFistBagResult
    {
        public int ReturnValue { get; set; }
        public BaseFistBagData FistBagData { get; set; }
    }


    [Serializable]
    public abstract class BaseFistBagData
    {
        // PriceTargeting ///////////////////////////////////////////

        public PriceTargetingData PriceTargeting { get; set; }


        // HadSubmittedPhotoFirstRCView ///////////////////////////////////////////

        public FirstRateCardViewPhotoStatus HadSubmittedPhotoFirstRCView { get; set; }



        // PromoIdLastApplied /////////////////////////////////////////////////////
        public string PromoIdLastApplied { get; set; }

    }
}
