﻿namespace Corp.Billing.Shared.Domain.FistBag
{
	public interface ISookieKeyGetter
	{
		int? GetSookieKeyId(string keyName);
		string GetSookieKeyName(int id);
	}
}
