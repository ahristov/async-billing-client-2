﻿namespace Corp.Billing.Shared.Domain.FistBag
{
    public enum FirstRateCardViewPhotoStatus
    {
        NotEvaluated,
        Submitted,
        NotSubmitted
    };
}
