﻿namespace Corp.Billing.Shared.Domain.FistBag
{
    public interface IFistBagCalculator
    {
        /// <summary>
        /// Calculates whatever data is missing in the passed in fist bag object.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="fbData">The FistBag data.</param>
        /// <returns>true if it did some changes</returns>
        bool Calculate(int userId, BaseFistBagData fbData);
    }
}
