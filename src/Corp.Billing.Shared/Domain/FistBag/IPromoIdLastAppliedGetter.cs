﻿namespace Corp.Billing.Shared.Domain.FistBag
{
    public interface IPromoIdLastAppliedGetter
    {
        string GetPromoIdLastApplied(int userId);
    }
}
