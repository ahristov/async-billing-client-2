﻿namespace Corp.Billing.Shared.Domain.FistBag
{
    public interface IFistBagGetter
    {
        GetFistBagResult GetFistBagData(GetFistBagRequest request);
    }
}
