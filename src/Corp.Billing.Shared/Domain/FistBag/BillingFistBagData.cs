﻿using System;

namespace Corp.Billing.Shared.Domain.FistBag
{
    public class SetBillingFistBagRequest
    {
        public int UserId { get; set; }
        public BillingFistBagData FistBagData { get; set; }
    }

    public class SetBillingFistBagResult
    {
        public int ReturnValue { get; set; }
    }


    [Serializable]
    public class BillingFistBagData : BaseFistBagData
    {
        public BillingFistBagData()
        { }

        public BillingFistBagData(BaseFistBagData that)
        {
            this.PriceTargeting = that.PriceTargeting;
            this.HadSubmittedPhotoFirstRCView = that.HadSubmittedPhotoFirstRCView;
        }

    }
}
