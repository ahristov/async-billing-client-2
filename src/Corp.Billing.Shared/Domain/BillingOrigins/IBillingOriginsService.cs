﻿using Corp.Billing.Shared.Domain.BillingOrigins.Data;
using System;

namespace Corp.Billing.Shared.Domain.BillingOrigins
{
    public interface IBillingOriginsService
    {
        void SetBillingOrigins(int userId, Guid sessionId, BillingOriginsData billingOrigins);
        BillingOriginsStoredData GetBillingOrigins(int userId);
        BillingOriginsData GetBillingOrigins(int userId, Guid sessionId);
        void DeleteBillingOrigins(int userId);
        void DeleteBillingOrigins(int userId, Guid sessionId);
    }
}
