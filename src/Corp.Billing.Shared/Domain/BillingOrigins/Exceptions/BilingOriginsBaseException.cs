﻿using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.BillingOrigins.Exceptions
{
    public abstract class BilingOriginsBaseException : Exception
    {
        public int UserId { get; private set; }

        public BilingOriginsBaseException(int userId)
        {
            UserId = userId;
        }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }
        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
