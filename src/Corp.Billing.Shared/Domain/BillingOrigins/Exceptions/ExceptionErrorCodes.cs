﻿namespace Corp.Billing.Shared.Domain.BillingOrigins.Exceptions
{
    public enum ExceptionErrorCodes: byte
    {
        Undefined = 0,

        BillingOriginsExistsWithDifferentData = 1,
    }
}
