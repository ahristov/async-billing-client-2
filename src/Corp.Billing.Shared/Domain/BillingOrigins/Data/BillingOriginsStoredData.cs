﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.BillingOrigins.Data
{
    [Serializable]
    public class BillingOriginsStoredData
    {
        public IDictionary<Guid, BillingOriginsData> Sessions { get; set; }

        public BillingOriginsStoredData()
        {
            Sessions = new Dictionary<Guid, BillingOriginsData>();
        }
    }
}
