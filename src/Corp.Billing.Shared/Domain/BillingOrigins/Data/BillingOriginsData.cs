﻿using System;

namespace Corp.Billing.Shared.Domain.BillingOrigins.Data
{
    [Serializable]
    public class BillingOriginsData
    {
        public byte OriginPlatformId { get; set; }
        public string OriginUserAgentString { get; set; }
        public int OriginAreaId { get; set; }
        public DateTime GeneratedAt { get; } = DateTime.Now;

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// Two 
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            var that = obj as BillingOriginsData;
            if (that == null)
                return false;

            return this.OriginPlatformId == that.OriginPlatformId
                && this.OriginUserAgentString == that.OriginUserAgentString
                && this.OriginAreaId == that.OriginAreaId;
        }

        public override int GetHashCode()
        {
            return $"{OriginPlatformId}-${OriginUserAgentString}-${OriginAreaId}".GetHashCode();
        }

    }
}
