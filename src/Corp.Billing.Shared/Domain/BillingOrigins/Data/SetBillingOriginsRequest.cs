﻿using System;

namespace Corp.Billing.Shared.Domain.BillingOrigins.Data
{
    [Serializable]
    public class SetBillingOriginsRequest
    {
        public int UserId { get; set; }
        public Guid SessionId { get; set; }
        public byte OriginPlatformId { get; set; }
        public string OriginUserAgentString { get; set; }
        public int OriginAreaId { get; set; }
    }
}
