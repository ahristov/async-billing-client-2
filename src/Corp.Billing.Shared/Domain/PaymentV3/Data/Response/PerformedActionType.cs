﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Response
{
    public enum PerformedActionType
    {
        None = 0,
        NewPurchase = 1,
        Upgrade = 2,
    }
}
