﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Response
{
    public enum PaymentResultTypeEnum : int
    {
        Undefined = 0,
        PaymentSuccess = 1,
        PaymentRetryState = 2,
        PaymentDecline = 3,

        //InsufficientFundsError = 111,
        //SecurityCodeError = 112,
        //ExpirationDateError = 113,
    }
}
