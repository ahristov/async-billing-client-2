﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface IOrderPayment : IPaymentRequestBase, ISessionInformation, IBillingAddress
    {
        ICollection<string> OrderItems { get; set; }
    }
}
