﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface IPaymentRequestBase
    {
        int UserId { get; set; }
        string PromoId { get; set; }
        int? RateCardKey { get; set; }
        string PaymentToken { get; set; }
        string MerchantId { get; set; }
        PaymentType PaymentType { get; set; }
    }
}
