﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events
{
    [Serializable]
    public class PaymentRequest : Request.PaymentRequestBase, IEventsPaymentRequest
    {
        public int EventId { get; set; }
        public decimal EventTotalPriceIncludingGuestsAndTax { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }

        public byte SiteCode { get; set; }
        public short UrlCode { get; set; }

        // IBillingAddress
        public short CountryCode { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }

        public PaymentRequest()
            : base()
        {
            OrderItems = new List<OrderItem>();
            CountryCode = 1;
        }

        public virtual PaymentRequest InitFrom(PaymentRequest x)
        {
            base.InitFrom(x);

            this.EventId = x.EventId;
            this.EventTotalPriceIncludingGuestsAndTax = x.EventTotalPriceIncludingGuestsAndTax;
            if (x.OrderItems != null)
            {
                foreach(var item in x.OrderItems)
                {
                    this.OrderItems.Add(item.Clone());
                }
            }
            this.SiteCode = x.SiteCode;
            this.UrlCode = x.UrlCode;
            this.CountryCode = x.CountryCode;
            this.PostalCode = x.PostalCode;
            this.FirstName = x.FirstName;
            this.LastName = x.LastName;

            return this;
        }
    }
}
