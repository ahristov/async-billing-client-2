﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events
{
    public interface IEventsPaymentRequest : IPaymentRequestBase, IBillingAddress, ISessionInformation
    {
        byte SiteCode { get; set; }
        short UrlCode { get; set; }
        ICollection<OrderItem> OrderItems { get; set; }
        int EventId { get; set; }
        decimal EventTotalPriceIncludingGuestsAndTax { get; set; }
    }
}
