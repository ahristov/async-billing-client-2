﻿using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events
{
    [Serializable]
    public class OrderItem
    {
        public int? ProdId { get; set; }
        public byte ProdFeatureId { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal FullPrice { get; set; }
        public string ISOCurrencyCode { get; set; }

        public OrderItem Clone()
        {
            var res = MemberwiseClone() as OrderItem;
            return res;
        }
    }
}
