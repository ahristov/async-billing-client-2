﻿using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events
{
    [Serializable]
    public class RequestWithPaymentMethodToken : PaymentRequest, IHasPaymentMethodToken
    {
        public int PaymentMethodToken { get; set; }
    }
}
