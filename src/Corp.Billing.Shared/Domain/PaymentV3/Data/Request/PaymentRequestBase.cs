﻿using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    [Serializable]
    public abstract class PaymentRequestBase : IPaymentRequestBase, ISessionInformation
    {
        public int UserId { get; set; }
        public string PromoId { get; set; }
        public int? RateCardKey { get; set; }
        public string PaymentToken { get; set; }
        public string MerchantId { get; set; }
        public PaymentType PaymentType { get; set; }

        // ISessionInformation
        public byte PlatformId { get; set; }
        public Guid? SessionId { get; set; }
        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }
        
        public virtual PaymentRequestBase InitFrom(PaymentRequestBase x)
        {
            this.UserId = x.UserId;
            this.PromoId = x.PromoId;
            this.RateCardKey = x.RateCardKey;
            this.PaymentToken = x.PaymentToken;
            this.MerchantId = x.MerchantId;
            this.PaymentType = x.PaymentType;
            this.PlatformId = x.PlatformId;
            this.SessionId = x.SessionId;
            this.ClientIp1 = x.ClientIp1;
            this.ClientIp2 = x.ClientIp2;
            this.ClientIp3 = x.ClientIp3;
            this.ClientIp4 = x.ClientIp4;

            return this;
        }
    }
}
