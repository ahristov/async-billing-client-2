﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
	public enum PaymentType
	{
		TokenizedCard = 1,
		PayPal = 2,
		OneClick = 3,
		ApplePay = 4,
	}
}
