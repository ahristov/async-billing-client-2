﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface IBillingAddress
    {
        short CountryCode { get; set; }
        string PostalCode { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string FullName { get; set; }
    }
}
