﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public class OrderPaymentRequest : PaymentRequestBase, IOrderPayment
    {
        public ICollection<string> OrderItems { get; set; }

        public short CountryCode { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }

        public OrderPaymentRequest()
        {
            OrderItems = new List<string>();
        }
    }
}
