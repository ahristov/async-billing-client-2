﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface ISessionInformation
    {
        byte PlatformId { get; set; }
        Guid? SessionId { get; set; }

        byte? ClientIp1 { get; set; }
        byte? ClientIp2 { get; set; }
        byte? ClientIp3 { get; set; }
        byte? ClientIp4 { get; set; }

    }
}
