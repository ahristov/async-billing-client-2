﻿using Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OrderItems;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OneClickPayment
{
    [Serializable]
    public class OneClickPaymentRequest : PaymentRequestBase, IOneClickPaymentRequest
    {
        public ICollection<OrderItem> OrderItems { get; set; }

        public OneClickPaymentRequest()
        {
            OrderItems = new List<OrderItem>();
        }
    }
}
