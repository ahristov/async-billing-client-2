﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface IHasPaymentMethodToken
    {
        int PaymentMethodToken { get; set; }
    }
}
