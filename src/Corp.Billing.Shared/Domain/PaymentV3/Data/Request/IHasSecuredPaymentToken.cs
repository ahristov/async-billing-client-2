﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface IHasSecuredPaymentToken
    {
        string SecuredPaymentToken { get; set; }
    }
}
