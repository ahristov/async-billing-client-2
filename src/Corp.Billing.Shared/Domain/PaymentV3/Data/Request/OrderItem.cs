﻿using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OrderItems
{
    [Serializable]
    public class OrderItem
    {
        public int ProductId { get; set; }
        public int? BundleId { get; set; }
    }
}
