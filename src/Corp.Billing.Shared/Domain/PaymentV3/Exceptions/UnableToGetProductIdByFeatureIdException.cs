﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class UnableToGetProductIdByFeatureIdException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.UnableToGetProductIdByFeatureId;
        public override string ErrorMessage => ErrorMessages.UNABLE_TO_FIND_PRODUCTS_BY_FEATURE_ID;
    }
}
