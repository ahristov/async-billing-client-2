﻿using Corp.Billing.Shared.Domain.PaymentV3.Data.Request;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public abstract class ExceptionBase : Exception
    {
        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }
        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
