﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class SecuredPaymentTokenDoesNotExistException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.SecuredPaymentTokenDoesNotExist;
        public override string ErrorMessage => ErrorMessages.SECU_TOKEN_DOES_NOT_EXISTS;
    }
}
