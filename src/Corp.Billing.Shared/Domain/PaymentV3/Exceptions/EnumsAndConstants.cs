﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public static class ErrorMessages
    {
        public const string ORDER_TABLE_CONTAINS_INVALID_FEATURE_ID = "The order table contains invalid feature ID";
        public const string ORDER_TABLE_CONTAINS_INVALID_PRODUCT_ID = "The order table contains invalid product ID";
        public const string UNABLE_TO_FIND_USER = "Unable to find user";
        public const string RATECARDKEY_NOT_SPECIFIED = "Rate card key not specified";
        public const string UNABLE_TO_FIND_RATECARD_BY_KEY = "Unable to find rate card by the specified key";
        public const string UNABLE_TO_FIND_PRODUCTS_IN_RATECARD = "Unable to find product in rate card";
        public const string UNABLE_TO_FIND_PRODUCTS_BY_FEATURE_ID = "Unable to get product by feature ID";
        public const string INVALID_PRODUCT_TOKEN = "Invalid product token";
        public const string UNABLE_TO_FIND_DEFAULT_MOP = "Unable to find default method of payment";
        public const string CANNOT_PROCESS_MOP_AS_ONECLICK = "Cannot process the specified MOP type as one-click";
        public const string SECU_TOKEN_DOES_NOT_EXISTS = "The secured payment token does not exist or has expired";
        public const string PAYPAL_ERROR = "Paypal error";
    }

    public enum ExceptionErrorCodesEnum
    {
        Undefined = 0,
        OrderTableContainsInvalidFeatureId = 10101,
        OrderTableContainsInvalidProductId = 10111,
        UnableToFindUser = 10201,
        RateCardKeyNotSpecified = 10211,
        UnableToFindRateCardByKey = 10212,
        UnableToFindProductInRateCard = 10213,
        UnableToGetProductIdByFeatureId = 10214,
        InvalidProductToken = 10215,
        UnableToFindDefaultMethodOfPayment = 10221,
        UnableToProcessMopTypeAsOneClick = 10222,
        SecuredPaymentTokenDoesNotExist = 84001,
        PayPalError = 85001,
    }
}
