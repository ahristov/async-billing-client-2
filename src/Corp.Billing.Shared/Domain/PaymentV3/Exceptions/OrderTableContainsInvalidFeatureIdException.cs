﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class OrderTableContainsInvalidFeatureIdException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.OrderTableContainsInvalidFeatureId;
        public override string ErrorMessage => ErrorMessages.ORDER_TABLE_CONTAINS_INVALID_FEATURE_ID;
    }
}
