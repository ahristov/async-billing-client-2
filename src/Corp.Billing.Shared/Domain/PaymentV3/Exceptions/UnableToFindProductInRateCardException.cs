﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class UnableToFindProductInRateCardException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.UnableToFindProductInRateCard;
        public override string ErrorMessage => ErrorMessages.UNABLE_TO_FIND_PRODUCTS_IN_RATECARD;
    }
}
