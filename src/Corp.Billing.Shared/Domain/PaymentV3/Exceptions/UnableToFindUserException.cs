﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class UnableToFindUserException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.UnableToFindUser;
        public override string ErrorMessage => ErrorMessages.UNABLE_TO_FIND_USER;
    }
}
