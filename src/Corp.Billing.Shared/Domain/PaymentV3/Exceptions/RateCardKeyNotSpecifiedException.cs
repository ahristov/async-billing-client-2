﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class RateCardKeyNotSpecifiedException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.RateCardKeyNotSpecified;
        public override string ErrorMessage => ErrorMessages.RATECARDKEY_NOT_SPECIFIED;
    }
}
