﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class InvalidProductTokenException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.InvalidProductToken;
        public override string ErrorMessage => ErrorMessages.INVALID_PRODUCT_TOKEN;
    }
}
