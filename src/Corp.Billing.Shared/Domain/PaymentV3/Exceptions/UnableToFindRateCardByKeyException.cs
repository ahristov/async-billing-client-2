﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class UnableToFindRateCardByKeyException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.UnableToFindRateCardByKey;
        public override string ErrorMessage => ErrorMessages.UNABLE_TO_FIND_RATECARD_BY_KEY;
    }
}
