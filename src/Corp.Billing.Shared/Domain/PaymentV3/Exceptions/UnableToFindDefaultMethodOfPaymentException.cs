﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class UnableToFindDefaultMethodOfPaymentException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.UnableToFindDefaultMethodOfPayment;
        public override string ErrorMessage => ErrorMessages.UNABLE_TO_FIND_DEFAULT_MOP;
    }
}
