﻿namespace Corp.Billing.Shared.ApiPaths
{
    public static class ApiPathsV1
    {
        public const string BasePath = "api/v1";

        // Payments

        public static class Proxy
        {
            public const string PaymentProxy = "paymentProxy";
            public const string AuthorizeProxy = "authorizeProxy";
            public const string RefundProxy = "refundProxy";
            public const string GetRefundBatch = "refundBatch";
        }


        // Account

        public static class Account
        {
            public static class Payment
            {
                public const string GetAccountPaymentMethodPath = "account/paymentMethod";
                public const string GetAccountPaymentMethodV2Path = "account/paymentMethodV2";
                public const string UpdateAccountPaymentMethodPath = "account/paymentMethod";
                public const string UpdateAccountPaymentMethodV2Path = "account/paymentMethodV2";
                public const string UpdatePaymentMethodFirstAndLastName = "account/paymentMethod/name";
                public const string GetPaymentInfoPath = "account/payment";

                public static class Boleto
                {
                    public const string BoletoPayment = "account/payment/boleto";
                    public const string BoletoPaymentInfo = "info";
                    public const string BoletoPaymentAuth = "auth";
                    public const string BoletoPaymentProcess = "process";
                    public const string BoletoNofityRequest = "notify";

                }
                public static class Debito
                {
                    public const string DebitoPayment = "account/payment/debito";
                    public const string DebitPaymentInfo = "info";
                    public const string DebitoPaymentAuth = "auth";
                    public const string DebitoPaymentProcess = "process";
                    public const string DebitoPendingTransactions = "pendingTrx";
                }
                public static class DineroMail
                {
                    public const string DineroMailPayment = "account/payment/dineroMail";
                    public const string DineroMailPaymentAuth = "auth";
                    public const string DineroMailPaymentProcess = "process";
                    public const string DineroMailPaymentProcessNotification = "processNotification";
                }

            }

            public static class Debito
            {
                public const string DebitoPayment = "account/payment/debito";
                public const string DebitPaymentInfo = "info";
                public const string DebitoPaymentAuth = "auth";
                public const string DebitoPaymentProcess = "process";
                public const string DebitoPendingPaymentRequests = "pendingRequests";

            }
            public static class DineroMail
            {
                public const string DineroMailPayment = "account/payment/dineroMail";
                public const string DineroMailPaymentAuth = "auth";
                public const string DineroMailPaymentProcess = "process";
            }

            public static class Lock
            {
                public const string LockAccountPath = "account/lock/obtain";
                public const string ReleaseAccountLockPath = "account/lock/release";
            }

            public static class Purge
            {
                public const string PurgeAccount = "account/";
                public const string GetPendingAccounts = "account/purgequeue/";
            }

            public const string AccountBilCredit = "account/bilCredit";
            public const string AccountBaseUrl = "account/";
            public const string GetPendingActions = AccountBaseUrl + "updates";
            public const string Register = "/encryption";
            public const string ReRegister = AccountBaseUrl + "{id}/reregister";
            public const string GetExternalTokensPath = AccountBaseUrl + "{id}/external";
        }

        // Discount

        public static class Discount
        {
            public static class Renewal
            {
                public const string GetRenewalDiscountMessagingPath = "discount/renewal";
                public const string GetRenewalDiscountMessagingRntPath = "discount/renewal/rnt";
                public const string RedeemRenewalDiscountPath = "discount/renewal";
                public const string RedeemRenewalDiscountRntPath = "discount/renewal/rnt";
                public const string RenewalUpsell = "discount/renewal/upsell";
                public const string RenewalUpsellV2 = "discount/renewal/upsell/v2";
                public const string RenewalUpsell2 = "discount/renewal/upsell2";
            }

            public static class Guarante
            {
                public const string RedeemGuarantee = "discount/guarantee/redeem";
            }

            public static class Offer
            {
                public const string DiscountEngineOffer = "discount/offer";
                public const string SaveOffer = "saveoffer/{saveOfferFlow}";
            }
        }


        // Rate cards

        public static class RateCard
        {
            public const string GetBaseRateCardPath = "ratecard/base";
            public const string GetAddOnsRateCardPath = "ratecard/addOns";
            public const string GetUnitRateCardPath = "ratecard/unit";

            public const string GetRateCardV2BasePath = "ratecard/base/v2";
            public const string GetRateCardV2AddOnsBundlePath = "ratecard/addOns/bundle/v2";
            public const string GetRateCardV2AddOnsPath = "ratecard/addOns/v2";
            public const string GetRateCardV2PowerUpsPath = "ratecard/powerUps/v2";
            public const string GetRateCardV2UnitPath = "ratecard/unit/v2";

            public const string GetRateCardV2FeatureComparePath = "ratecard/feature/compare/v2";

            public const string GetRateCardV3 = "rateCardV3/{catalogType}/{catalogSource}";
            public const string GetExtendedRateCardV3 = "extendedRateCardV3/{catalogType}/{catalogSource}";
            public const string GetExtendedRateCardV3WithDefaults = "extendedRateCardV3";
            public const string GetExtendedRateCardV3Product = "extendedRateCardV3/product/{token}";
            public const string GetProdTokensToSellable = "extendedRateCardV3/prodTokensToSellable";
            public const string GetProdTokensToSellableUnits = "extendedRateCardV3/prodTokensToSellableUnits";
        }

        public static class MembershipUpgrade
        {
            public const string MembershipUpgradeConfigurationView = "membership-upgrade/configuration/view";
            public const string MembershipUpgradeConfigurationEvaluate = "membership-upgrade/configuration/evaluate";
            public const string MembershipUpgradeEvaluate = "membership-upgrade/evaluate";
        }

        // IAP

        public static class Provision
        {
            public const string ProvisionIAP = "provision/inapp/{receiptSource}/{receiptFlow}";
        }

        public static class IAP
        {
            public const string GetOrderProductsIAP = "inapp/orderProductsFromExternal/{receiptSource}/{receiptFlow}"; // for testing only
            public const string VerifyReceipt = "inapp/verifyReceipt/{receiptSource}/{receiptFlow}"; // for testing only
        }



        // Pricing 

        public static class Pricing
        {
            public const string GetPriceGroupCbsaPath = "pricing/prices/cbsa";
            public const string GetUserScoreData = "pricing/prices/userScore";
            public const string GetBillCurrencyConversionRates = "pricing/conversionRates";
            public const string GetDiscount = "pricing/conversionRates";
        }


        // Products

        public static class Products
        {
            public static class DateCoach
            {
                public const string GetDateCoachStatus = "products/dateCoach/status";
                public const string ToggleDateCoachRenewal = "products/dateCoach/renewal/{toggleType}";
            }

            public static class MatchMe
            {
                public const string GetMatchMe30DayEligibilityCheck = "products/matchMe/eligible";
                public const string PutMeInUserList = "products/matchMe/userList/addMe";
            }

            public static class TopSpot
            {
                public const string GetTopSpotStatusPath = "products/topSpot/status";
                public const string ProvisionAndActivateTopSpot = "products/topSpot/provision";
                public const string ActivateUnclaimedImpulseFeature = "products/topSpot/activate";
            }

            public static class ProfilePro
            {
                public const string GetUnitUserStatusPcsPath = "products/profilePro/eligible";
            }

            public static class UnitProducts
            {
                public const string GetUnitUserStatusPath = "products/unit/eligible";
                public const string GetUnitDisplayStatusPath = "products/unit/status";
                public const string GetUnitPurchasedUnitsCountPath = "products/unit/countPurchasedUnits";
            }

            public static class ImpulseFeatures
            {
                public const string StatusAndActivate = "impulseFeatures/{productFeature}";
            }

            public static class PowerUps
            {
                public const string PowerUpsPurchaseEligibility = "powerUps/purchaseEligibility";
            }

        }


        // Product Catalog

        public class ProductCatalog
        {
            public class DiscountEngine
            {
                public const string DiscountOffer = "product_catalog/discount/offer";
            }

            public class PricingEngine
            {
                public const string PriceTargeting = "product_catalog/price/targeting";
                public const string PriceTargetingV2 = "product_catalog/price/targeting/v2";
            }

            public class PriceOverride
            {
                public const string PriceOverrideUrl = "product_catalog/price/override";
                public const string PriceOverrideUrlV2 = "product_catalog/price/override/v2";
                public const string PriceOverrideUrlV3 = "product_catalog/price/override/v3";
                public const string MemberUpgradeUrl = "product_catalog/price/override/memberUpgrade";
            }

            public class RateCardDynamic
            {
                public const string GetRateCardDynamicKey = "product_catalog/rateCardDynamicKey";
            }
        }

        // Rules Engine
        public class RulesEngine
        {
            public const string EvaluateTest = "rules_engine/evaluate/{testName}";
            public const string EvaluateTestVariables = "rules_engine/evaluate/variables/{testName}";
            public const string EvaluateUserIdHash = "rules_engine/evaluateUserIdHash";
        }



        // Promos

        public static class Promos
        {
            public const string GetPromoEligibilityPath = "promos/eligibility";
            public const string ValidateRedemptionKeyPath = "promos/promo/redeem";
            public const string GetPromoValidationPath = "promos/promo/validation";
            public const string GetPromoInfoPath = "promos/promo";
            public const string GetPromoDataPath = "promos/promo/data";

            // Gifts

            public static class Gift
            {
                public const string RedeemGift = "promos/gift/redeem";
                public const string ValidateGiftKeys = "promos/gift/validateKeys";
            }
        }

        public static class DiscountSchedule
        {
            public const string DiscountScheduleByUser = "discount_schedule/by_user";
            public const string DiscountScheduleByPromo = "discount_schedule/by_promo";
        }

        // Gift Cards
        public static class GiftCard
        {
            public const string GetGiftCardEligibilityPath = "giftCard/eligibility";
            public const string GiftCardProcessPath = "giftCard/process";
        }


        // Resignation

        public static class Resignation
        {
            public const string GetResignStatus = "resignation/status";
            public const string Resign = "resignation/resign";

            // Free trial

            public static class FreeTrial
            {
                public const string ReactivateFreeTrial = "resignation/freetrial/reactivate";
                public const string CancelFreeTrial = "resignation/freetrial/resign";
            }
        }


        // Fraud

        public static class Fraud
        {
            public const string LogFraudEvent = "fraud/event";
            public const string GetFraudProperties = "fraud/properties";
        }



        // Orders

        public static class Orders
        {
            public const string CardinalLogInsertPath = "orders/cardinal/log";
            public const string GetOrderDiscountsPath = "orders/discounts";
            public const string GetOneMonthFeatureComparePath = "orders/oneMonthCompare";
            public const string GetOrderSummaryPath = "orders/summary";
            public const string CreateOrderPath = "orders/order";
            public const string CreateOrderPathV3 = "orders/order/v3";

            public static class Transaction
            {
                public const string ProcessTransactionPath = "orders/transaction/process";
                public const string ProcessPayPalTransactionPath = "orders/transaction/process/paypal";
                public const string AppleProcessTransactionPath = "orders/transaction/process/apple";
                public const string UpdateTransactionStatusPath = "orders/transaction/status";
                public const string UpdateOfflineTransactionStatusPath = "orders/transaction/status/offline";
                public const string TransactionDetails = "transaction";
            }

            public static class Apple
            {
                public const string AppleRetryQueueInsertPath = "orders/apple/retryQueueInsert";
                public const string SearchForAppleOrderPath = "orders/apple/search";
            }

            public static class UnitProducts
            {
                public const string BillEventTicketsPath = "orders/event/tickets";
                public const string GetImpulseBuyLastPurchase = "orders/unit/last";
            }
        }


        // Subscription

        public static class Subscription
        {
            public const string GetSubscriptionSummary = "subscription/summary";
            public const string GetDisplayStatusPath = "subscription/status/display";
            public const string GetDisplayStatusPathV2 = "subscription/status/display/v2";
            public const string GetUserPromoInfoPath = "subscription/status/promoInfo";
            public const string GetUserSubStatusPath = "subscription/status/details";
            public const string GetUserSubStatusMessagePath = "subscription/status/message";
            public const string GetUserStatusDatePath = "subscription/status/date";
            public const string GetUserStatusDateV1 = "subscription/status/date/v1";
            public const string GetUserFeaturesPath = "subscription/status/features";
            public const string GetUserFeaturesLightPath = "subscription/status/userFeaturesLight";
            public const string CancelProduct = "subscription/features/activation";
            public const string GetUserBillingData= "subscription/billing/data";
            public const string GetCurrentPackage = "subscription/currentPackage";
            public const string UserNonRenewableFeatures = "subscription/userNonRenewableFeatures";

            public const string ReactivateSubscription = "subscription/reactivate";

            public const string SubscriptionBenefits = "subscription/benefits";
            public const string SubscriptionBenefitsCountNew = "subscription/benefits/countNew";

            public const string ValueAddedBenefitsConfigurationView = "value-added-benefits/configuration/view";
            public const string ValueAddedBenefitsConfigurationEvaluate = "value-added-benefits/configuration/evaluate";
            public const string ValueAddedBenefitsConfigurationEvaluateToString = "value-added-benefits/configuration/evaluate/to-string";

            public const string InstallmentsConfigurationView = "installments/configuration/view";
            public const string InstallmentsConfigurationEvaluate = "installments/configuration/evaluate";
            public const string InstallmentsConfigurationEvaluateToString = "installments/configuration/evaluate/to-string";
            public const string InstallmentsConfigurationEvaluateByUserId = "installments/configuration/evaluate-byuserid";
            public const string InstallmentsConfigurationEvaluateByUserIdToString = "installments/configuration/evaluate-byuserid/to-string";
        }

        public static class Payment
        {
            public static class V3
            {
                public const string EventsCreditCardPaymentWithSecuredPaymentToken = "payment/v3/events/creditCard/withSecuredToken";
                public const string EventsCreditCardPaymentWithPaymentMethodToken = "payment/v3/events/creditCard/withPaymentToken";
                public const string EventsOneClickPaymentWithDefaultPaymentMethod = "payment/v3/events/oneClick/withDefaultPaymentMethod";
                public const string EventsPayment = "payment/v3/events";

                public const string OneClickPaymentWithDefaultPaymentMethod = "payment/v3/oneClick";
                public const string OrderPayment = "payment/v3/orderPayment";
            }

            public static class PayPal
            {
                public const string CreateBillingAgreementToken = "payment/paypal/billingAgreement/token";
                public const string CreateBillingAgreement = "payment/paypal/billingAgreement";
                public const string PlaceOrder = "payment/paypal/order";
            }

            public static class CreditCard
            {
                public const string CardinalLookUp = "payment/creditCard/cardinalLookUp";
            }

            public static class Amazon
            {
                public const string PlaceOrder = "payment/amazon/order";
                public const string Consent = "payment/amazon/consent";
            }

            public static class Settlement
            {
                public const string CreateBatch = "payment/settlement/begin";
                public const string AddTransaction = "payment/settlement/{batchID}/{transactionID}";
                public const string CompleteBatch = "payment/settlement/{batchID}/complete";
            }

            public static class Transactions
            {
                public const string Path = "transactions";
                public const string Search = Path + "/search";
            }
        }

        public static class OrderCreateV2
        {
            public const string CreditCardOrderCreatePath = "order/create/creditCard";
            public const string PayPalOrderCreatePath = "order/create/payPal";
            public const string VisaCheckoutOrderCreatePath = "order/create/visaCheckout";
            public const string MasterPassOrderCreatePath = "order/create/masterPass";
            public const string PayWithAmazonOrderCreatePath = "order/create/amazon";
            public const string DineroMailOrderCreatePath = "order/create/dineroMail";
            public const string BoletoOrderCreatePath = "order/create/boleto";
            public const string DebitOrderCreatePath = "order/create/debit";
            public const string ApplePayOrderCreatePath = "order/create/applePay";
            public const string CashOrderCreatePath = "order/create/cash";
        }


        public static class Renewals
        {
            public const string RenewUser = "renewals/";
        }

        public static class Site
        {
            public const string GetSitePaymentMatrix = "site/payment/matrix";
            public const string GetSitePaymentMatrixAll = "site/payment/matrix/all";
            public const string GetSiteFeatures = "site/features";
            public const string GetSiteFeaturesAll = "site/features/all";
            public const string GetSalesTaxRates = "site/tax/rates";
        }

        public static class TaxRates
        {
             public const string GetSalesTaxRatesByUser = "tax/rates";
        }

        //Visa Checkout
        public static class VisaCheckout
        {
            public static class Wallet
            {
                public const string UpdatePaymentInfo = "visaCheckout/wallet/paymentInfo";
                public const string DecryptPaymentData = "visaCheckout/wallet/decrypt";
            }
            
        }

        public static class MasterPass
        {
            public static class Wallet
            {
                public const string OAuthToken = "masterpass/wallet/oauthToken";
                public const string Checkout = "masterpass/wallet/checkout";
                public const string Log = "masterpass/wallet/log";
            }

        }

        public static class ApplePay
        {
            public const string ValidateMerchant = "applepay/merchant/validate";
        }

        public static class Chargebacks
        {
            public const string PerformChargeback = "chargebacks";
        }

        //Credit Card Updater
        public static class CreditCardUpdater
        {
            public const string CreditCardUpdaterBaseUrl = "creditCard/";
            public const string UpdateCreditCard = CreditCardUpdaterBaseUrl + "update";
            public const string RequestUpdateCreditCard = CreditCardUpdaterBaseUrl + "requestUpdate";
        }

        public static class Manage
        {
            public const string RedisCache = "manage/cache/redis/data/{cacheKey}";
            public const string RedisCacheAllUserData = "manage/cache/redis/alluserdata";
        }

        public static class Legal
        {
            public const string LegalTexts = "legal/texts";
            public const string ShoppingCartLegalTexts = "legal/shoppingCart/texts";
        }

        public static class ProfileProLite
        {
            public const string Redemption = "profileProLite/redemption";
        }

        public static class UserVerifications
        {
            public const string VerifiedMachines = "userVerifications/verifiedMachines";
        }

        public static class BillingOrigins
        {
            public const string BillingOriginsPath = "billingorigins";
            public const string BillingOriginsPathWithSession = "billingorigins/{sessionId}";
        }

    }

}