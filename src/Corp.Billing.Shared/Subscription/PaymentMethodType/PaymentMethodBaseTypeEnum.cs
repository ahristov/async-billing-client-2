﻿using System;

namespace Corp.Billing.Shared.Subscription.PaymentMethodType
{
    [Serializable]
    public enum PaymentMethodBaseTypeEnum
    {
        /// <summary>
        /// N/A, base code "N"
        /// </summary>
        NotApplicable = 0,
        /// <summary>
        /// Card, base code "C"
        /// </summary>
        Card = 1,
        /// <summary>
        /// PayPal, base code "L"
        /// </summary>
        PayPal = 2,
        /// <summary>
        /// Online check, base code "O"
        /// </summary>
        OnlineCheck = 3,
        /// <summary>
        /// Mailed check, base code "P"
        /// </summary>
        Check = 4,
        /// <summary>
        /// Gift redemption, base code "G"
        /// </summary>
        Gift = 5,
        /// <summary>
        /// Cash payment, base code "S"
        /// </summary>
        Cash = 6,
        /// <summary>
        /// Money order, base code "M"
        /// </summary>
        MoneyOrder = 7,
        /// <summary>
        /// Electronic check, base code "A"
        /// </summary>
        ECheck = 8,
        /// <summary>
        /// Direct debit (bank, offline), base code "T", DtlCode "DB"
        /// </summary>
        DirectDebit = 9,
        /// <summary>
        /// Boleto (bank, offline), base code "T", DtlCode "BO"
        /// </summary>
        Boleto = 10,
        /// <summary>
        /// Dinero Mail (bank, offline), base code "T", DtlCode "DM"
        /// </summary>
        DineroMail = 11,
        /// <summary>
        /// Apple in App, base code "E"
        /// </summary>
        AppleIAP = 12,
        /// <summary>
        /// Pay with Amazon, base code "Z"
        /// </summary>
        PayWithAmazon = 13,
        /// <summary>
        /// Apple pay, base code "X"
        /// </summary>
        ApplePay = 14,
        /// <summary>
        /// Offline payment, base code "T".
        /// </summary>
        Offline = 15,
    }
}
