﻿using System;

namespace Corp.Billing.Shared.Subscription.PaymentMethodType
{
    public class PaymentMethodTypeDataAttribute : Attribute
    {
        public int ServiceCategoryId { get; }
        public bool CanBeDefault { get; }
        public string DtlCode { get; }
        public string BaseCode { get; }
        public bool Active { get; }
        public PaymentMethodBaseTypeEnum BasePaymentType { get; }


        public PaymentMethodTypeDataAttribute(
            int serviceCategoryID,
            bool canBeDefault,
            string dtlCode,
            string baseCode,
            bool active,
            PaymentMethodBaseTypeEnum basePaymentType)
        {
            ServiceCategoryId = serviceCategoryID;
            CanBeDefault = canBeDefault;
            DtlCode = dtlCode;
            BaseCode = baseCode;
            Active = active;
            BasePaymentType = basePaymentType;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            PaymentMethodTypeDataAttribute other = obj as PaymentMethodTypeDataAttribute;
            return (other != null)
                && other.ServiceCategoryId == this.ServiceCategoryId
                && other.CanBeDefault == this.CanBeDefault
                && (""+other.DtlCode).Equals(this.DtlCode)
                && (""+other.BaseCode).Equals(this.BaseCode)
                && other.Active == this.Active
                && other.BasePaymentType == this.BasePaymentType;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => string.IsNullOrEmpty(this.DtlCode);
    }
}
