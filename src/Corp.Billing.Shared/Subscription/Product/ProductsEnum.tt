﻿<#@ template debug=" " hostspecific="false" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ assembly name="System.Data" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Text.RegularExpressions" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Data" #>
<#@ import namespace="System.Data.SqlClient" #>
<#@ import namespace="System.Text" #>
<#@ output extension=".cs" #>

// Do not modify, this is generated from T4 template

using System;
using Corp.Billing.Shared.Domain.Products;

namespace Corp.Billing.Shared.Subscription.Product
{
    [Serializable]
    public enum ProductsEnum
    {
<#
    foreach(var row in GetProductRows())
    {
        if (String.IsNullOrWhiteSpace(row.ProductName))
        {
            continue;
        }

        int prodId = row.ProductID;
        string prodEnumName = ToTitle(row.ProductName) + "_" + row.ProductID;
        string attributesList = String.Join(",", 
            GetProductAttributes(prodId).ToArray().Select(x => $"(ProductAttributeTypeEnum){x}"));
#>
        <#=$"[ProductData({prodId}, \"{row.ProductName}\", (ProductFeatureType){row.ProductFeatureID}, {row.IsRenewable.ToString().ToLower()}, {row.NumberOfUnits}, {row.ImpulseMinutes}, new ProductAttributeTypeEnum[] {{ {attributesList} }})]"#>
        <#=$"{prodEnumName} = {prodId},"#>
<#
    }
#>
    }
}

<#+
    private const string ConnectionString = @"Integrated Security=True;Initial Catalog=BillingService;Data Source=tcp:MASWD1,1433;APP=BillingService;MultiSubnetFailover=True";

    private const string Sql = @"
select
  p.ProdID as ProductID,
  p.Descr as ProductName,
  p.luProdFeatureID as ProductFeatureID,
  p.IsRenewable as IsRenewable,
  p.Units as NumberOfUnits,
  p.ImpulseMinutes as ImpulseMinutes
from BillingData..Prod p
order by p.ProdID
";

    private const string SqlAttributes= @"
select
  distinct pam.luProdAttributeID as ProductAttributeID
from BillingData..ProdAttributeMatrix pam
where
  pam.ProdID = {0}
order by pam.luProdAttributeID
";

    public class ProductsRowData
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public byte ProductFeatureID { get; set; }
        public bool IsRenewable { get; set; }
        public short NumberOfUnits { get; set; }
        public short ImpulseMinutes { get; set; }
    }

    private IEnumerable<int> GetProductAttributes(int productId)
    {
        using (SqlConnection connection = new SqlConnection(ConnectionString))
        {
            connection.Open();

            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.CommandText = String.Format(SqlAttributes, productId);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return reader.GetInt16(reader.GetOrdinal("ProductAttributeID"));
                    }

                    reader.Close();
                }
            }

            connection.Close();
        }
    }

    private IEnumerable<ProductsRowData> GetProductRows()
    {
        using (SqlConnection connection = new SqlConnection(ConnectionString))
        {
            connection.Open();

            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.CommandText = Sql;

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new ProductsRowData
                            {
                                ProductID = reader.GetInt16(reader.GetOrdinal("ProductID")),
                                ProductName =  reader.GetString(reader.GetOrdinal("ProductName")),
                                ProductFeatureID = reader.GetByte(reader.GetOrdinal("ProductFeatureID")),
                                IsRenewable = reader.GetBoolean(reader.GetOrdinal("IsRenewable")),
                                NumberOfUnits = reader.GetInt16(reader.GetOrdinal("NumberOfUnits")),
                                ImpulseMinutes = reader.GetInt16(reader.GetOrdinal("ImpulseMinutes")),
                            };
                    }

                    reader.Close();
                }
            }

            connection.Close();
        }
    }

    static string ToTitle(string s)
    {
        return string.Join("", Regex.Split((s ?? string.Empty).Trim().ToLowerInvariant(), "[^a-z]", RegexOptions.IgnoreCase)
            .Where(x => x.Length > 0)
            .Select(x => ToName(x)));
    }

    static string ToName(string s)
    {
        s = (""+s).Trim().ToLowerInvariant();
        return string.IsNullOrWhiteSpace(s)
            ? s
            : char.ToUpperInvariant(s[0]) + s.Substring(1);
    }

#>