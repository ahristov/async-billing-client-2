﻿using System;

namespace Corp.Billing.Shared.Subscription.Product
{
    public class ProductBundleDataAttribute: Attribute
    {
        public bool IsUpsellBundle { get; set; }
        public ProductBundleTypeEnum ProductBundleType { get; set; }

        public ProductBundleDataAttribute(bool isUpsellBundle, ProductBundleTypeEnum productBundleType)
        {
            IsUpsellBundle = isUpsellBundle;
            ProductBundleType = productBundleType;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ProductBundleDataAttribute other = obj as ProductBundleDataAttribute;
            return (other != null)
                && other.IsUpsellBundle == this.IsUpsellBundle
                && other.ProductBundleType == this.ProductBundleType;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => this.IsUpsellBundle == false
            && ProductBundleType == ProductBundleTypeEnum.None;
    }
}
