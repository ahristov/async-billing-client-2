﻿using Corp.Billing.Shared.Domain.Subscription;
using System.Reflection;

namespace Corp.Billing.Shared.Subscription.Product
{
    public static class ProductBundleExtensions
    {
        public static ProductBundleDataAttribute GetProductBundleData(
            this ProductBundleEnum productBundle)
        {
            FieldInfo fi = productBundle.GetType().GetField(productBundle.ToString());
            if (fi == null)
                return null;

            ProductBundleDataAttribute[] attributes =
                (ProductBundleDataAttribute[])fi.GetCustomAttributes(typeof(ProductBundleDataAttribute), false);
            return (attributes != null && attributes.Length > 0)
                ? attributes[0]
                : null;
        }

        public static ProductBundleTypeEnum GetProductBundleType(this ProductBundleEnum productBundle)
            => GetProductBundleData(productBundle)?.ProductBundleType ?? ProductBundleTypeEnum.None;

        public static bool GetIsUpsellBundle(this ProductBundleEnum productBundle)
            => GetProductBundleData(productBundle)?.IsUpsellBundle ?? false;

        public static SubscriptionPackageTypeEnum GetSubscriptionPackageType(this ProductBundleEnum productBundle)
            => GetProductBundleType(productBundle).GetSubscriptionPackageType();
    }
}
