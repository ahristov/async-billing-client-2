﻿using Corp.Billing.Shared.Domain.Products;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Subscription.Product
{
    public static class ProductEnumExtensions
    {
        public static IEnumerable<ProductsEnum> GetAllProductsEnumValues() => System.Enum.GetValues(typeof(ProductsEnum)).Cast<ProductsEnum>();

        public static IEnumerable<TotalsByProductType> TransformToProductsByType(this IEnumerable<TotalsByProductID> listOfTotalsByProductID)
        {
            if (listOfTotalsByProductID == null || listOfTotalsByProductID.Count() == 0)
                return new List<TotalsByProductType>();

            Dictionary<ProductAttributeTypeEnum, TotalsByProductType> collected = new Dictionary<ProductAttributeTypeEnum, TotalsByProductType>();

            foreach (var totalsByProductID in listOfTotalsByProductID)
            {
                ProductAttributeTypeEnum productType = ProductAttributeTypeEnum.None;

                if (totalsByProductID.ProdID.HasValue)
                {
                    var productData = totalsByProductID.ProdID.Value.GetProductData();
                    var productAttributes = productData?.ProductAttributes;
                    if (productAttributes != null && productAttributes.Count() > 0)
                    {
                        productType = productAttributes[0];
                    }
                }

                if (!collected.ContainsKey(productType))
                {
                    collected.Add(productType, new TotalsByProductType() { ProductType = productType });
                }

                collected[productType].PaidCount += totalsByProductID.PaidCount;
                collected[productType].FreeCount += totalsByProductID.FreeCount;
                collected[productType].UnusedPaidCount += totalsByProductID.UnusedPaidCount;
                collected[productType].UnusedFreeCount += totalsByProductID.UnusedFreeCount;
            }

            return collected.ToArray().OrderBy(x => x.Key).Select(x => x.Value);
        }
    }
}
