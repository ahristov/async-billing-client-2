﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Corp.Billing.Shared.Subscription.Product
{
    public static class ProductDataExtensions
    {
        public static ProductDataAttribute GetProductData(this int productId)
        {
            if (!Enum.IsDefined(typeof(ProductsEnum), productId))
                return null;

            ProductsEnum product = (ProductsEnum)productId;
            ProductDataAttribute productData = product.GetProductData();
            return productData;
        }

        public static ProductDataAttribute GetProductData(this ProductsEnum product)
        {
            FieldInfo fi = product.GetType().GetField(product.ToString());
            if (fi == null)
                return null;
            ProductDataAttribute[] attributes =
                (ProductDataAttribute[])fi.GetCustomAttributes(typeof(ProductDataAttribute), false);
            return (attributes != null && attributes.Length > 0)
                ? attributes[0]
                : null;
        }

        public static ProductAttributeTypeEnum[] GetProductAttributes(this int productId)
            => productId.GetProductData()?.ProductAttributes ?? new ProductAttributeTypeEnum[] { };

        public static ProductAttributeTypeEnum[] GetProductAttributes(this ProductsEnum product)
            => product.GetProductData()?.ProductAttributes ?? new ProductAttributeTypeEnum[] { };

        static readonly ProductAttributeTypeEnum[] RushHourAttributeIDs = new ProductAttributeTypeEnum[] { ProductAttributeTypeEnum.RushhourBoost, };

        public static bool GetIsRushHour(this int productId)
            => productId.GetProductAttributes().Any(x => RushHourAttributeIDs.Contains(x));

        public static bool GetIsRushHour(this ProductsEnum product)
            => product.GetProductAttributes().Any(x => RushHourAttributeIDs.Contains(x));

        static readonly ProductAttributeTypeEnum[] BoostWithRffAttributeIDs = new ProductAttributeTypeEnum[] { ProductAttributeTypeEnum.BoostWithRff, };

        public static bool GetIsBoostWithRff(this int productId)
            => productId.GetProductAttributes().Any(x => BoostWithRffAttributeIDs.Contains(x));

        public static bool GetIsBoostWithRff(this ProductsEnum product)
            => product.GetProductAttributes().Any(x => BoostWithRffAttributeIDs.Contains(x));

        public static int? GetCorrespondingBoostWithRffProductID(this int productId)
        {
            return (int?)productId.GetCorrespondingBoostWithRff();
        }

        public static ProductsEnum? GetCorrespondingBoostWithRff(this int productId)
            => GetCorrespondingBoostWithRff((ProductsEnum)productId);

        public static ProductsEnum? GetCorrespondingBoostWithRff(this ProductsEnum regularBoost)
        {
            var regularBoostData = regularBoost.GetProductData();
            if (regularBoostData == null
                || regularBoostData.ProductFeature != Domain.Products.ProductFeatureType.TopSpot
                || regularBoostData.NumberOfUnits == 0
                || regularBoost.GetIsBoostWithRff()
                || regularBoost.GetIsRushHour())
            {
                return null;
            }

            foreach (var product in ProductEnumExtensions.GetAllProductsEnumValues().OrderByDescending(x => x))
            {
                var productData = product.GetProductData();
                if (productData != null
                    && productData.ProductFeature == regularBoostData.ProductFeature
                    && productData.NumberOfUnits == regularBoostData.NumberOfUnits
                    && product.GetIsBoostWithRff())
                {
                    return product;
                }
            }

            return null;
        }

        public static ProductsEnum? GetBoostProducts(int minutes, int units, bool boostWithRff)
        {
            foreach (var product in ProductEnumExtensions.GetAllProductsEnumValues().OrderByDescending(x => x))
            {
                var productData = product.GetProductData();
                if (productData.ImpulseMinutes == minutes
                    && productData.NumberOfUnits == units
                    && productData.ProductFeature == Domain.Products.ProductFeatureType.TopSpot
                    && product.GetIsRushHour() == false
                    && product.GetIsBoostWithRff() == boostWithRff)
                {
                    return product;
                }
            }

            return null;
        }
    }
}
