﻿using System;
using System.ComponentModel;

namespace Corp.Billing.Shared.Subscription.Product
{
    public class ProductDefinitions
    {
        public enum ProductAttributes
        {
            LoyaltyPricing = 1,
            GuaranteeProduct = 2,  // six month guarantee
            CsaFreeAddOn = 4,
            EventsServiceFee = 8,
            UpsellProduct = 16,
        }

        public enum ProductFeatureIds
        {
            EmailCommunication = 1,
            MatchPhone = 12,
            ServiceFeeFeatureID = 24,
        }

        [Flags]
        public enum ProductFeatures
        {
            EmailCommunication = 1,	//2^^0 (luprodfeatureID - 1)
            MatchManager = 2,	//2^^1
            NoAds = 8,	//2^^3

            Voice = 32,	//2^^5
            ReplyForFree = 134217728,	//2^^27
            MatchTracker = 256,	//2^^8

            Mobile = 4096, //2^^12
            ChemistrySubscription = 524288, //2^^19

            //Features that have HTML text
            ProfileHighlighting = 4,	//2^^2
            VenusPlacement = 16,	//2^^4  - also called First Impressions
            EmailReadNotification = 256, //2^^8 
            MatchTalk = 2048,	//2^^11 -- Also called Match Phone

            PrivateMode = 268435456  //2^^28
        }
    }

}
