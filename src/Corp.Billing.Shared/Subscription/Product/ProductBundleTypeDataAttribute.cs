﻿using Corp.Billing.Shared.Domain.Subscription;
using System;

namespace Corp.Billing.Shared.Subscription.Product
{
    public class ProductBundleTypeDataAttribute: Attribute
    {
        public SubscriptionPackageTypeEnum SubscriptionPackageType { get; set; }

        public ProductBundleTypeDataAttribute(SubscriptionPackageTypeEnum subscriptionPackageType)
        {
            SubscriptionPackageType = subscriptionPackageType;
        }
        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ProductBundleTypeDataAttribute other = obj as ProductBundleTypeDataAttribute;
            return (other != null)
                && other.SubscriptionPackageType == this.SubscriptionPackageType;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => SubscriptionPackageType == SubscriptionPackageTypeEnum.NonSubscriber;

    }
}
