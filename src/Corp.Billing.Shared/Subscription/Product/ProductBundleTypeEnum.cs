﻿
// Do not modify, this is generated from T4 template

using System;
using Corp.Billing.Shared.Domain.Subscription;

namespace Corp.Billing.Shared.Subscription.Product
{
    [Serializable]
    public enum ProductBundleTypeEnum
    {
        [ProductBundleTypeData((SubscriptionPackageTypeEnum)1)]
        None = 0,
        /// <summary>
        /// Premium
        /// </summary>
        [ProductBundleTypeData((SubscriptionPackageTypeEnum)2)]
        Premium = 1,
        /// <summary>
        /// Elite
        /// </summary>
        [ProductBundleTypeData((SubscriptionPackageTypeEnum)3)]
        Elite = 2,
        /// <summary>
        /// Latam Gold
        /// </summary>
        [ProductBundleTypeData((SubscriptionPackageTypeEnum)4)]
        LatamGold = 3,
        /// <summary>
        /// Latam Platinum
        /// </summary>
        [ProductBundleTypeData((SubscriptionPackageTypeEnum)5)]
        LatamPlatinum = 4,
    }
}

