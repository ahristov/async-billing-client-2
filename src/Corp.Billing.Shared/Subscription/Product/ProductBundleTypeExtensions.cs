﻿using Corp.Billing.Shared.Domain.Subscription;
using System.Reflection;

namespace Corp.Billing.Shared.Subscription.Product
{
    public static class ProductBundleTypeExtensions
    {
        public static ProductBundleTypeDataAttribute GetProductBundleTypeData(
            this ProductBundleTypeEnum productBundleType)
        {
            FieldInfo fi = productBundleType.GetType().GetField(productBundleType.ToString());
            if (fi == null)
                return null;

            ProductBundleTypeDataAttribute[] attributes =
                (ProductBundleTypeDataAttribute[])fi.GetCustomAttributes(typeof(ProductBundleTypeDataAttribute), false);
            return attributes != null && attributes.Length > 0
                ? attributes[0]
                : null;
        }

        public static SubscriptionPackageTypeEnum GetSubscriptionPackageType(this ProductBundleTypeEnum productBundleType)
            => GetProductBundleTypeData(productBundleType)?.SubscriptionPackageType ?? SubscriptionPackageTypeEnum.StandardPackage;
    }
}
