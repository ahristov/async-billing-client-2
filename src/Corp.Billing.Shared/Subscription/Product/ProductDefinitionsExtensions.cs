﻿namespace Corp.Billing.Shared.Subscription.Product
{
    public static class ProductDefinitionsExtensions
    {
        public static bool HasFeature(this long features, ProductDefinitions.ProductFeatures feature)
        {
            var res = (((long)feature) & features) > 0;
            return res;
        }
    }
}
