﻿
// Do not modify, this is generated from T4 template

using System;

namespace Corp.Billing.Shared.Subscription.Product
{
    [Serializable]
    public enum ProductBundleEnum
    {
        [ProductBundleData(false, (ProductBundleTypeEnum)0)]
        None = 0,

        /// <summary>
        /// Premium Bundle (Deprecated)
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)1)]
        PremiumBundleDeprecated = 10001,

        /// <summary>
        /// Premium Bundle Upsell (Deprecated)
        /// </summary>
        [ProductBundleData(true, (ProductBundleTypeEnum)1)]
        PremiumBundleUpsellDeprecated = 10002,

        /// <summary>
        /// LatAm Gold Bundle
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)3)]
        LatamGoldBundle = 10003,

        /// <summary>
        /// LatAm Platinum Bundle
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)4)]
        LatamPlatinumBundle = 10004,

        /// <summary>
        /// Intuition Bundle (Deprecated)
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)0)]
        IntuitionBundleDeprecated = 10005,

        /// <summary>
        /// Invisibility Bundle (Deprecated)
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)0)]
        InvisibilityBundleDeprecated = 10006,

        /// <summary>
        /// Magnetism Bundle (Deprecated)
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)0)]
        MagnetismBundleDeprecated = 10007,

        /// <summary>
        /// Super Strength Bundle (Deprecated)
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)0)]
        SuperStrengthBundleDeprecated = 10008,

        /// <summary>
        /// LatAm Gold Bundle : Full Package Upsell
        /// </summary>
        [ProductBundleData(true, (ProductBundleTypeEnum)3)]
        LatamGoldBundleFullPackageUpsell = 10009,

        /// <summary>
        /// LatAm Platinum Bundle : Full Package Upsell
        /// </summary>
        [ProductBundleData(true, (ProductBundleTypeEnum)4)]
        LatamPlatinumBundleFullPackageUpsell = 10010,

        /// <summary>
        /// Base And MRA Bundle
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)1)]
        BaseAndMraBundle = 10011,

        /// <summary>
        /// Premium Bundle
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)1)]
        PremiumBundle = 10012,

        /// <summary>
        /// Premium Bundle Upsell
        /// </summary>
        [ProductBundleData(true, (ProductBundleTypeEnum)1)]
        PremiumBundleUpsell = 10013,

        /// <summary>
        /// Elite Bundle
        /// </summary>
        [ProductBundleData(false, (ProductBundleTypeEnum)2)]
        EliteBundle = 10014,

    }
}

