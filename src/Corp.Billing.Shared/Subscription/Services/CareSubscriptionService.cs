﻿using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Care;
using Newtonsoft.Json;
using System;
using Corp.Billing.Shared.Domain.Account;
using Corp.Billing.Shared.Domain.Orders;
using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.Subscription;
using Corp.Billing.Shared.ApiClient;

namespace Corp.Billing.Shared.Subscription.Services
{
    public class CareSubscriptionService
    {
        private ApiCommand ApiService { get; set; }

        public CareSubscriptionService()
        {
            ApiService = new ApiCommand();
        }

        public CareSubscriptionService(ApiCommand apiService)
        {
            ApiService = apiService;
        }

        #region Subscription

        public ComplimentarySubResponse CompRequest(ComplimentarySubRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/comprequest/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            ComplimentarySubResponse result = JsonConvert.DeserializeObject<ComplimentarySubResponse>(apiResult);

            return result;
        }

        public CompDaysDetailIdResponse CompRequestProdDetailId(CompDaysDetailIdRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/compprodid/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CompDaysDetailIdResponse result = JsonConvert.DeserializeObject<CompDaysDetailIdResponse>(apiResult);

            return result;
        }

        public EligibleFreeAddOnsResponse FreeAddOnsList(EligibleFreeAddOnsRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/freeaddonlist/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            EligibleFreeAddOnsResponse result = JsonConvert.DeserializeObject<EligibleFreeAddOnsResponse>(apiResult);

            return result;
        }

        public ProcessFreeAddOnResponse ProcessFreeAddOns(ProcessFreeAddOnRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/processfreeaddons/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            ProcessFreeAddOnResponse result = JsonConvert.DeserializeObject<ProcessFreeAddOnResponse>(apiResult);

            return result;
        }

        public EligibleCompProducts CompProductList()
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/compproducts/";
            string apiResult = ApiService.WebRequestGet(url);
            EligibleCompProducts result = JsonConvert.DeserializeObject<EligibleCompProducts>(apiResult);

            return result;
        }

        public CompOrderResponse PlaceCompOrder(CompOrderRequest order)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/comprequestv2/";
            string values = JsonConvert.SerializeObject(order);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CompOrderResponse result = JsonConvert.DeserializeObject<CompOrderResponse>(apiResult);

            return result;
        }

        public CSACreateOrderResult PlaceOrder(CSACreateOrderRequest order)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/placeorder/";
            string values = JsonConvert.SerializeObject(order);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSACreateOrderResult result = JsonConvert.DeserializeObject<CSACreateOrderResult>(apiResult);

            return result;
        }

        public CSARateCardAddOnResult RateCardAddOns(CSARateCardAddOnRequest addOns)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/addons/";
            string values = JsonConvert.SerializeObject(addOns);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSARateCardAddOnResult result = JsonConvert.DeserializeObject<CSARateCardAddOnResult>(apiResult);

            return result;
        }

        public CSAUpdateStatusResult UpdateBillingStatus(CSAUpdateStatusRequest updateStatus)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/updateStatus/";
            string values = JsonConvert.SerializeObject(updateStatus);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSAUpdateStatusResult result = JsonConvert.DeserializeObject<CSAUpdateStatusResult>(apiResult);

            return result;
        }

        public CSARateCardResult GetUnitRateCard(CSARateCardRequest unitRateCardRequest)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/unitRateCard/";
            string values = JsonConvert.SerializeObject(unitRateCardRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSARateCardResult result = JsonConvert.DeserializeObject<CSARateCardResult>(apiResult);

            return result;
        }

        public CSACreditCardListResult GetCreditCardList(CSACreditCardListRequest cardList)
        {
            string url = ApiConfiguration.BillingServiceUrl + string.Format("api/csa/cardList/?userid={0}&creditCardType={1}",cardList.UserID, cardList.CreditCardType);
            string apiResult = ApiService.WebRequestGet(url);
            CSACreditCardListResult result = JsonConvert.DeserializeObject<CSACreditCardListResult>(apiResult);

            return result;
        }

        public CSAUserPCSStatusResult GetPCSUserStatusBy(int userId)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/PCSUserStatus/?userid=" + userId.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            CSAUserPCSStatusResult result = JsonConvert.DeserializeObject<CSAUserPCSStatusResult>(apiResult);

            return result;

        }

        public PromoSavesResult GetPromoSaves(bool save)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/promosaves/?save=" + save.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            PromoSavesResult result = JsonConvert.DeserializeObject<PromoSavesResult>(apiResult);

            return result;
        }

        public PromoDisplayResult GetPromoList(int userId, bool skipRuleChecks)
        {
            string url = ApiConfiguration.BillingServiceUrl + string.Format("api/csa/PromoDisplay/?userid={0}&skipRuleChecks={1}", userId, skipRuleChecks);
            string apiResult = ApiService.WebRequestGet(url);
            PromoDisplayResult result = JsonConvert.DeserializeObject<PromoDisplayResult>(apiResult);

            return result;
        }

        public PromoEligibilityResult GetPromoEligibility(PromoEligibilityRequest pmoRequest)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/promoeligibility/";
            string values = JsonConvert.SerializeObject(pmoRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            PromoEligibilityResult result = JsonConvert.DeserializeObject<PromoEligibilityResult>(apiResult);

            return result;
        }

        public LockAccountResult GetMatchLock(int userId)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/matchlockget/?userid=" + userId.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            LockAccountResult result = JsonConvert.DeserializeObject<LockAccountResult>(apiResult);

            return result;
        }

        public int MatchLockRelease(Guid lockGuid)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/matchlockrelease/?lockId=" + lockGuid.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            int result = JsonConvert.DeserializeObject<int>(apiResult);

            return result;
        }

        public ProcessTransactionResult ProcessTransaction(ProcessTransactionRequest trxRequest)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/processtransaction/";
            string values = JsonConvert.SerializeObject(trxRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            ProcessTransactionResult result = JsonConvert.DeserializeObject<ProcessTransactionResult>(apiResult);

            return result;
        }

        public CSAOrderDetailResponse GetOrderDetail(CSAOrderDetailRequest detailRequest)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/orderdetail/";
            string values = JsonConvert.SerializeObject(detailRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSAOrderDetailResponse result = JsonConvert.DeserializeObject<CSAOrderDetailResponse>(apiResult);

            return result;
        }

        public OrderDiscountsResult GetOrderDiscounts(OrderDiscountsRequest discountsRequest)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/orderdiscounts/";
            string values = JsonConvert.SerializeObject(discountsRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            OrderDiscountsResult result = JsonConvert.DeserializeObject<OrderDiscountsResult>(apiResult);

            return result;
        }

        public OrderSummaryResult GetOrderSummary(OrderSummaryRequest discountsRequest)
        {
            if (discountsRequest == null)
                throw new ArgumentNullException("discountsRequest");

            if (string.IsNullOrEmpty(discountsRequest.OrderList))
                throw new ArgumentException("OrderList parameter must be provided");

            string url = ApiConfiguration.BillingServiceUrl + "api/csa/ordersummary/";
            string values = JsonConvert.SerializeObject(discountsRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            OrderSummaryResult result = JsonConvert.DeserializeObject<OrderSummaryResult>(apiResult);

            return result;
        }

        public UserFeatureResult GetUserFeatures(int userId)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/userfeatures/?userid=" + userId.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            UserFeatureResult result = JsonConvert.DeserializeObject<UserFeatureResult>(apiResult);

            return result;
        }

        public CSACashCalcResult CashCalculation(CSACashCalcRequest cashCalcRequest)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/cashcalculate/";
            string values = JsonConvert.SerializeObject(cashCalcRequest);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSACashCalcResult result = JsonConvert.DeserializeObject<CSACashCalcResult>(apiResult);

            return result;
        }

        public UserPromoIDResult GetUserPromo(int userId)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/userpromoid/?userid=" + userId.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            UserPromoIDResult result = JsonConvert.DeserializeObject<UserPromoIDResult>(apiResult);

            return result;
        }

        public CSAEligibleAddonCountResult GetEligibleAddOnCount(CSARateCardAddOnRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/eligibleaddoncount/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSAEligibleAddonCountResult result = JsonConvert.DeserializeObject<CSAEligibleAddonCountResult>(apiResult);

            return result;
        }

        public PromoDisplayAddOnsResult PromoAddOnDisplay(int userId)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/promoaddondisplay/?userid=" + userId.ToString();
            string apiResult = ApiService.WebRequestGet(url);
            PromoDisplayAddOnsResult result = JsonConvert.DeserializeObject<PromoDisplayAddOnsResult>(apiResult);

            return result;
        }

        #endregion

        #region Refunds

        public CSACalcRefundResult CalculateRefund(CSACalcRefundRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/calculaterefund/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSACalcRefundResult result = JsonConvert.DeserializeObject<CSACalcRefundResult>(apiResult);

            return result;
        }

        public CSARefundSubmitResult SubmitRefund(CSARefundSubmitRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/refundsubmit/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSARefundSubmitResult result = JsonConvert.DeserializeObject<CSARefundSubmitResult>(apiResult);

            return result;
        }

        public CSAChbRefundResult SubmitChargebackOrHistoricalRefund(CSAChbRefundRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/chbrefundsubmit/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSAChbRefundResult result = JsonConvert.DeserializeObject<CSAChbRefundResult>(apiResult);

            return result;
        }

        public GenericResult SubmitMassBlocker(CSAInsAbuseRefundRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/abuseresignrefund/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            GenericResult result = JsonConvert.DeserializeObject<GenericResult>(apiResult);

            return result;
        }

        public GenericResult SubmitMassBlockerV2(MassBlockerRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/abuseresignrefundV2/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            GenericResult result = JsonConvert.DeserializeObject<GenericResult>(apiResult);

            return result;
        }

        public CSAUnitStageRefundResult UnitStageRefund(CSAUnitStageRefundRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/unitstagerefund/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            CSAUnitStageRefundResult result = JsonConvert.DeserializeObject<CSAUnitStageRefundResult>(apiResult);

            return result;
        }

        public IVRRefundResult IVRRefund(IVRRefundRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/ivrrefund/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            IVRRefundResult result = JsonConvert.DeserializeObject<IVRRefundResult>(apiResult);

            return result;
        }

        //Refund Uber
        public StageRefundResult StageRefund(StageRefundRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/stagerefund/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            StageRefundResult result = JsonConvert.DeserializeObject<StageRefundResult>(apiResult);

            return result;
        }

        public RefundBatchResult GetRefundBatch(RefundBatchRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/getrefundbatch/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            RefundBatchResult result = JsonConvert.DeserializeObject<RefundBatchResult>(apiResult);

            return result;
        }

        public PerformRefundResult PerformRefund(PendingRefundItem request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/performrefund/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            PerformRefundResult result = JsonConvert.DeserializeObject<PerformRefundResult>(apiResult);

            return result;
        }


        #endregion

        #region Search

        public TransactionSearchResults GetSearch(TransactionSearchRequest request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/search/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            TransactionSearchResults result = JsonConvert.DeserializeObject<TransactionSearchResults>(apiResult);

            return result;
        }

        public UserSearchResults GetUserSearch(BaseSearchCriteria request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/csa/usersearch/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            UserSearchResults result = JsonConvert.DeserializeObject<UserSearchResults>(apiResult);

            return result;
        }

        #endregion

    }
}
