﻿using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Orders;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.Subscription.Services
{
    public class NextService
    {
        private ApiCommand ApiService { get; set; }

        public NextService()
        {
            ApiService = new ApiCommand();
        }

        public NextService(ApiCommand apiService)
        {
            ApiService = apiService;
        }

        public NextProcessOrderResult PostProcessOrder(SubscribeRequestV3 request)
        {
            string url = ApiConfiguration.BillingServiceUrl + "api/next/processorder/";
            string values = JsonConvert.SerializeObject(request);
            string apiResult = ApiService.WebRequestPostContent(url, values, "application/json");
            NextProcessOrderResult result = JsonConvert.DeserializeObject<NextProcessOrderResult>(apiResult);

            return result;
        }

    }
}
