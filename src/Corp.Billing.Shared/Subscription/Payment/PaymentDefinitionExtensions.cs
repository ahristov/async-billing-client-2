﻿namespace Corp.Billing.Shared.Subscription.Payment
{
    public static class PaymentDefinitionExtensions
    {
        public static bool IsDtlCodeCreditCard(this string dtlCode)
        {
            if (string.IsNullOrEmpty(dtlCode))
                return false;

            return (dtlCode == PaymentDefinitions.CardTypes.AmericanExpress
                    || dtlCode == PaymentDefinitions.CardTypes.DinersClub
                    || dtlCode == PaymentDefinitions.CardTypes.Discover
                    || dtlCode == PaymentDefinitions.CardTypes.JCB
                    || dtlCode == PaymentDefinitions.CardTypes.MasterCard
                    || dtlCode == PaymentDefinitions.CardTypes.Switch
                    || dtlCode == PaymentDefinitions.CardTypes.Visa);
        }
    }
}
