﻿namespace Corp.Billing.Shared.Subscription.Payment
{
    public class PaymentDefinitions
    {
        public enum WalletType
        {
            Undefined,
            CreditCard,
            PayPal,
            MasterPass,
            VisaCheckout,
            PayWithAmazon,
            ApplePay,
            AppleIAP
        }

        public enum MethodOfPayment
        {
            None,
            AmericanExpress,
            DinersClub,
            Discover,
            JCB, // (Japanese Credit)
            MasterCard,
            Visa,
            OnlineCheck,
            Check,
            Cash,
            MoneyOrder,
            ElectronicCheck, // Electronic Check Processing/Automatic Clearing House 
            GiftSubscriptionRedemption,
            SwitchMaestro,
            PayWithAmazon,
            ApplePay,
        }

        /// <summary>
        /// DEPRECATED: See UserSubStatusResult.IneligibilityReasonType
        /// </summary>
        public enum enumIneligibilityReasons : byte
        {
            ELIGIBILE = 0,
            INVALIDUSERID = 1,
            EXCEEDSSTACKSUBRULE = 2,
            FAILED15MINUTERULE = 3,
            USERINFREETRIAL = 4
        }

        public enum PayPalErrorCode
        {
            SuccessfulPaypalTransaction = 1,
            FailedGetBillingAgreementDetailsCall = 2,
            FailedFinalizeBillingAgreementCall = 3,
            FailedOpenPaymentRequest = 4,
            FailedToFindResubProduct = 5,
            FailedCollectPaymentApiCall = 10417 //FYI - This particular number is defined by PayPal.
        }

        public enum PurchasePaymentTypes
        {
            CreditCard,
            PayPal,
            OnlineCheck,
            MailedCheck,
            GiftSubscription,
            Cash,
            MoneyOrder,
            ElectronicCheckACH,
            DirectDebit,
            Boleto,
            DineroMail,
            NotApplicable,
            PayWithAmazon,
            ApplePay,
            AppleIAP,
        }
        public enum CreditCardTypes
        {
            Visa,
            MasterCard,
            Discover,
            AmericanExpress,
            DinersClub,
            JCB,
            PayPal,
            Switch,  //not used on domestic
            NotApplicable,
            Elo,
            PayWithAmazon,
            ApplePay,
        }

        public struct CardTypes
        {
            public const string AmericanExpress = "AX";
            public const string DinersClub = "DC";
            public const string Discover = "DI";
            public const string JCB = "JC";
            public const string MasterCard = "MC";
            public const string Switch = "SM";
            public const string Visa = "VI";
            public const string PayPal = "PP";
            public const string NotApplicable = "NA";
            public const string Boleto = "BO";
            public const string Debito = "DB";
            public const string DineroMail = "DM";
            public const string PayWithAmazon = "AZ";
            public const string ApplePay = "PL";
            public const string AppleIAP = "AP";
            public const string Cash = "CA";
        }

        public struct CardTypeDescriptions
        {
            public const string AmericanExpress = "American Express";
            public const string DinersClub = "Diners Club";
            public const string Discover = "Discover";
            public const string JCB = "JCB";
            public const string MasterCard = "MasterCard";
            public const string Switch = "Switch/Maestro";
            public const string Visa = "Visa";
            public const string PayPal = "PayPal";
            public const string PayWithAmazon = "PayWithAmazon";
            public const string ApplePay = "ApplePay";
        }

        public struct PaymentTypes
        {
            public const string CreditCard = "C";
            public const string OnlineCheck = "A";
            public const string Check = "P";   // mailed in check
            public const string GiftSubscription = "G";
            public const string PayPal = "L";
            public const string Cash = "S";
            public const string MoneyOrder = "M";
            public const string NotApplicable = "N";
            public const string Offline = "T";
            public const string Apple = "E";
            public const string PayWithAmazon = "Z";
            public const string ApplePay = "X";
        }

        public static string CardNameFromCardType(CreditCardTypes cardType)
        {
            string cardName = string.Empty;

            switch (cardType)
            {
                case CreditCardTypes.AmericanExpress:
                    cardName = CardTypeDescriptions.AmericanExpress;
                    break;
                case CreditCardTypes.DinersClub:
                    cardName = CardTypeDescriptions.DinersClub;
                    break;
                case CreditCardTypes.Discover:
                    cardName = CardTypeDescriptions.Discover;
                    break;
                case CreditCardTypes.JCB:
                    cardName = CardTypeDescriptions.JCB;
                    break;
                case CreditCardTypes.MasterCard:
                    cardName = CardTypeDescriptions.MasterCard;
                    break;
                case CreditCardTypes.PayPal:
                    cardName = CardTypeDescriptions.PayPal;
                    break;
                case CreditCardTypes.Switch:
                    cardName = CardTypeDescriptions.Switch;
                    break;
                case CreditCardTypes.Visa:
                    cardName = CardTypeDescriptions.Visa;
                    break;
                case CreditCardTypes.PayWithAmazon:
                    cardName = CardTypeDescriptions.PayWithAmazon;
                    break;
                case CreditCardTypes.ApplePay:
                    cardName = CardTypeDescriptions.ApplePay;
                    break;
            }

            return cardName;
        }

        public static string CardNameFromDtlCode(string dtlCode)
        {
            CreditCardTypes cardType = CardTypeFromDtlCode(dtlCode);
            return CardNameFromCardType(cardType);
        }

        public static string DtlCodeFromCardType(CreditCardTypes cardType)
        {
            string dtlCode = string.Empty;

            switch (cardType)
            {
                case CreditCardTypes.AmericanExpress:
                    dtlCode = CardTypes.AmericanExpress;
                    break;
                case CreditCardTypes.DinersClub:
                    dtlCode = CardTypes.DinersClub;
                    break;
                case CreditCardTypes.Discover:
                    dtlCode = CardTypes.Discover;
                    break;
                case CreditCardTypes.JCB:
                    dtlCode = CardTypes.JCB;
                    break;
                case CreditCardTypes.MasterCard:
                    dtlCode = CardTypes.MasterCard;
                    break;
                case CreditCardTypes.PayPal:
                    dtlCode = CardTypes.PayPal;
                    break;
                case CreditCardTypes.Switch:
                    dtlCode = CardTypes.Switch;
                    break;
                case CreditCardTypes.Visa:
                    dtlCode = CardTypes.Visa;
                    break;
                case CreditCardTypes.PayWithAmazon:
                    dtlCode = CardTypes.PayWithAmazon;
                    break;
                case CreditCardTypes.ApplePay:
                    dtlCode = CardTypes.ApplePay;
                    break;
            }

            return dtlCode;
        }

        public static CreditCardTypes CardTypeFromDtlCode(string dtlCode)
        {
            CreditCardTypes cardType = CreditCardTypes.Visa;

            switch (dtlCode)
            {
                case CardTypes.AmericanExpress:
                    cardType = CreditCardTypes.AmericanExpress;
                    break;
                case CardTypes.DinersClub:
                    cardType = CreditCardTypes.DinersClub;
                    break;
                case CardTypes.Discover:
                    cardType = CreditCardTypes.Discover;
                    break;
                case CardTypes.JCB:
                    cardType = CreditCardTypes.JCB;
                    break;
                case CardTypes.MasterCard:
                    cardType = CreditCardTypes.MasterCard;
                    break;
                case CardTypes.PayPal:
                    cardType = CreditCardTypes.PayPal;
                    break;
                case CardTypes.Switch:
                    cardType = CreditCardTypes.Switch;
                    break;
                case CardTypes.Visa:
                    cardType = CreditCardTypes.Visa;
                    break;
                case CardTypes.PayWithAmazon:
                    cardType = CreditCardTypes.PayWithAmazon;
                    break;
                case CardTypes.ApplePay:
                    cardType = CreditCardTypes.ApplePay;
                    break;

            }

            return cardType;
        }

        public static string BaseCodeFromPaymentType(PurchasePaymentTypes paymentType)
        {

            string baseType = string.Empty;

            switch (paymentType)
            {
                case PurchasePaymentTypes.CreditCard:
                    baseType = PaymentTypes.CreditCard;
                    break;
                case PurchasePaymentTypes.Cash:
                    baseType = PaymentTypes.Cash;
                    break;
                case PurchasePaymentTypes.MailedCheck:
                    baseType = PaymentTypes.Check;
                    break;
                //case PaymentTypes.ElectronicCheckACH:
                //    baseType = PaymentTypes.ElectronicCheck;
                //    break;
                case PurchasePaymentTypes.GiftSubscription:
                    baseType = PaymentTypes.GiftSubscription;
                    break;
                case PurchasePaymentTypes.MoneyOrder:
                    baseType = PaymentTypes.MoneyOrder;
                    break;
                case PurchasePaymentTypes.OnlineCheck:
                    baseType = PaymentTypes.OnlineCheck;
                    break;
                case PurchasePaymentTypes.PayPal:
                    baseType = PaymentTypes.PayPal;
                    break;
                case PurchasePaymentTypes.PayWithAmazon:
                    baseType = PaymentTypes.PayWithAmazon;
                    break;
                case PurchasePaymentTypes.ApplePay:
                    baseType = PaymentTypes.ApplePay;
                    break;
            }

            return baseType;
        }

        public static PurchasePaymentTypes PaymentTypeFromBaseCode(string baseCode)
        {
            PurchasePaymentTypes paymentType = PurchasePaymentTypes.CreditCard;

            switch (baseCode)
            {
                case PaymentTypes.Cash:
                    paymentType = PurchasePaymentTypes.Cash;
                    break;
                case PaymentTypes.Check:
                    paymentType = PurchasePaymentTypes.MailedCheck;
                    break;
                case PaymentTypes.CreditCard:
                    paymentType = PurchasePaymentTypes.CreditCard;
                    break;
                //case PaymentTypes.ElectronicCheck:
                //    paymentType = PaymentTypes.ElectronicCheckACH;
                //    break;
                case PaymentTypes.GiftSubscription:
                    paymentType = PurchasePaymentTypes.GiftSubscription;
                    break;
                case PaymentTypes.MoneyOrder:
                    paymentType = PurchasePaymentTypes.MoneyOrder;
                    break;
                case PaymentTypes.OnlineCheck:
                    paymentType = PurchasePaymentTypes.OnlineCheck;
                    break;
                case PaymentTypes.PayPal:
                    paymentType = PurchasePaymentTypes.PayPal;
                    break;
                case PaymentTypes.PayWithAmazon:
                    paymentType = PurchasePaymentTypes.PayWithAmazon;
                    break;
                case PaymentTypes.ApplePay:
                    paymentType = PurchasePaymentTypes.ApplePay;
                    break;
            }

            return paymentType;
        }

        public enum enumUserPath
        {
            DIRECT = 1,
            CONTACT = 2,
            MAS = 3, //My Account Settings
            CONTACT_FT = 4,
            PAYTORECEIVEFATE = 5
        }

        public enum enumUserStatus
        {
            ERROR = -1,
            INVALIDUSERID = 1,              // Invalid User
            NEVERSUBSCRIBED = 2,            // Never Had Site Access (Not Used By Site)
            CHARTERMEMBER = 3,              // Charter Member
            FREETRIAL = 4,                  // in the Free Trial period (UserAccount PTD is null or in the past)
            COMPACCOUNT = 5,                // CURRENT Comp Account (Not Used By Site)
            RESIGNEDPTDPAST = 6,            // EXPIRED Resigned Subscription (Not Used By Site)
            CURRENTSUBSCRIBER = 7,          // CURRENT Subscription
            PASTSUBCRIBER1 = 8,             // Paid w/ credit card and failed renewal w/in last 60 days
            PASTSUBCRIBER2 = 9,             // failed renewal
            RESIGNEDPTDFUTURE = 10,         // CURRENT Resigned Subscription
            RESIGNEDFREETRIAL = 11,         // User resigned while in Free Trial period
            EXPIREDCOMPACCOUNT = 12,        // EXPIRED Comp Account
            EXPIREDFREETRIAL = 13,          // EXPIRED Free-Trial RESIGNED Prior To Conversion
            ECHECK_REVIEW_PERIOD = 14,      // E-Check Review Period
            CURRENTITUNESACTIVESUB = 15,    // CURRENT Apple In-App active Sub
            PENDINGSUBSCRIPTION = 16,       // PENDING Subscription - Offline Payment Method
            EXPIREDFREETRIALOTHER = 101,    // All Other EXPIRED Free-Trial Scenarios
            EXPIREDSUBSCRIPTION = 102       // All Other EXPIRED Subscription Scenarios
        }

        public enum enumSubscribeStatusMessage
        {
            ERROR = -1,
            CURRENTSUBSCRIPTIONRNT = 1,      // Current Subscription: Resigned Not Termed
            CURRENTSUBSCRIPTIONLP = 2,      // Current Subscription: Loyalty Pricing
            CURRENTSUBSCRIPTION1MONTH = 4,  // Current Subscription: 1-Month
            CURRENTSUBSCRIPTION3MONTH = 8,  // Current Subscription: 3-Month
            CURRENTSUBSCRIPTION6MONTH = 16, // Current Subscription: 6-Month
            CURRENTSUBSCRIPTION12MONTH = 32, // Current Subscription: 12-Month
            CURRENTADDONHIGHLIGHTEDPROFILE = 64,     // Current Add-On: Highlighted Profile
            CURRENTADDONHIGHLIGHTEDPROFILERNT = 128, // Current Add-On: Highlighted Portrait Resigned Not Termed
            CURRENTADDONFIRSTIMPRESSIONS = 256,      // Current Add-On: First Impressions
            CURRENTADDONFIRSTIMPRESSIONSRNT = 512,	// Current Add-On: First Impressions Resigned Not Termed
            CURRENTADDONMINDFINDBIND = 1024,	// Current Add-On: Mind-Find-Bind
            CURRENTADDONMINDFINDBINDRNT = 2048,   // Current Add-On: Mind-Find-Bind Resigned Not Termed
            CURRENTADDONEMAILREADNOTIFY = 4096,   // Current Add-On: Email Read Notification
            CURRENTADDONEMAILREADRNT = 8192,   // Current Add-On: Email Read Notification Resigned Not Termed
            CURRENTADDONMATCHTALK = 16384,  // Current Add-On: matchTalk
            CURRENTADDONMATCHTALKRNT = 32768,   // Current Add-On: matchTalk Resigned Not Termed
            CURRENTADDONMATCHMOBILE = 65536,  // Current Add-On: matchMobile
            CURRENTADDONMATCHMOBILERNT = 131072   // Current Add-On: matchMobile Resigned Not Termed
        }

        //Return Code offsets are calculated so the logic for returning from Submit Order is not confusing
        public struct ReturnCodeOffsets
        {
            public const int PROCESS_TRX = 44;
            public const int UPDATE_STATUS = 66;
            public const int PTCCERROR = 110; // SP bilProcessTxr return codes for failed CC payment with offset added +110
        }
        // Submit Order Return Codes

        public enum enumSubmitOrderReturnCodeRollup
        {
            SOFAILEDTOGETALOCK = -3,
            SORETURNCODEINITIALIZEDBUTNEVERUSED = -2,
            SODLERROR = -1,
            SOSUCCESS = 0,
            SOCANNOTACCESSACCTPMTMETHTYPE = 1,
            SOMISSINGROUTINGORCREDITCARDNUMBER = 3,
            SONOCHECKNUMBERSUPPLIED = 4,
            SOCANNOTHASHACCOUNTNUMBER = 5,
            SOCANNOTENCRYPTACCOUNTNUMBER = 6,
            SOCREDITCARDBLOCKED = 7,
            SOECHECKACCOUNTNUMBERINVALID = 8,
            SOCREDITCARDEXPIRED = 9,
            SOFAILEDEXECLOGFRAUDEVENT = 10,
            SOINVALIDDATAINORDERLISTARRAY = 11,
            SOEMPTYORDER = 12,
            SOFAILEDEXECBILGETUSERSTATUS = 13,
            SOBILLINGSTATUSCHANGEDFORUSER = 14,
            SOFAILEDEXECBILCANCELUSER = 15,
            SOCANNOTACCESSACCOUNTPAYMENTMETHOD = 16,
            SOUNABLETOCREATEPAYMENTMETHOD = 17,
            SOUNABLETOUPDATEACCOUNTPAYMENTMETHOD = 18,
            SOUNABLETOCREATEACCOUNT = 19,
            SOCANNOTUPDATEACCOUNTRECORD = 20,
            SOUNABLETOCREATEACCOUNTORDER = 21,
            SOUNABLETOCREATEDUALREVENUELOG = 22,
            SOCANNOTCREATEACCOUNTDETAILRECORD = 23,
            SOFAILEDTOBUILDACCTDETAILLISTTRIAL = 24,
            SOFAILEDTOSUMPRETAXAMOUNTTRIAL = 25,
            SOFAILEDTOBUILDACCTDETAILLIST = 26,
            SOFAILEDTOSUMPRETAXAMOUNT = 27,
            SOFAILEDEXECBILCREATETRX = 28,
            SOFAILEDEXECSUBORDERRULES = 29,
            SOORDERVIOLATESSUBSCRIPTIONRULES = 30,
            SOFAILEDSELECTMATCH4GEODATA = 31,
            SOUSERALREADYHASSTACKEDSUB = 32,
            SOORDERPLACEDTHISUSERINLAST15MINUTES = 33,
            SOPROBLEMVERIFYINGENCRYPTION = 34,
            SOFAILEDFRAUDCHECK = 35,
            SODOUBLECLICKSUCCESS = 36,
            SODOUBLECLICKCOMMERROR = 37,
            SODOUBLECLICKFAILEDBILLING = 38,
            SOMISMATCHEDCURRENCY = 40,
            SOINVALIDSWITCHPARAMETERS = 41,
            //ProcessTrx return codes with Offset added + 44
            PTSUCCESS = 44,
            PTERRORGETTINGDATAFROMTRX = 45,
            PTCOULDNOTGETPNREFFROMORIGINALTRX = 46,
            PTINVALIDPAYMENTTYPE = 47,
            PTFAILEDTOGETCCINFO = 48,
            PTFAILEDTOGETOLCHECKOINFO = 49,
            PTERRORINSERTINGPROCESSORLOGVERISIGN = 50,
            PTERRORCALLINGBILSUBMITVERISIGN = 51,
            PTERRORINFNVERISIGNRETRY = 52,
            PTCOMMERROR = 53,
            PTVERISIGNOFFINAPPLOBJ = 54,
            PTBPPSUCCEEDEDVERISIGNFAILED = 55,
            //Match Me unit product specific return codes on ordCreateOrder
            //SOMATCHMEERRORUSERPAIRALREADYEXISTSANDNOTRATED = 56,
            //SOMATCHMEERRORUSERPAIRALREADYEXISTSANDRATED = 57,
            //SOMATCHMEERROROTHERUSERIDNOTPROVIDED = 58,
            //bilUpdateStatus return codes with offset added + 66
            USSUCCESS = 66,
            USREQUIREDPARAMSNOTPOPULATED = 67,
            USERRGETTINGCURRENTTRX = 68,
            USERRCALCSUBMITDAYS = 69,
            USERRCALCLUTRXSTATUSID = 70,
            USERRUPDATINGTRX = 71,
            USERRUPDATINGPROFILEDT = 72,
            USERRUPDATINGACCTDTL = 73,
            USERRINSERTINGTBLUSERPROFILEFEATREEDTL = 74,
            USERRUPDATINGTBLUSERPROFILEFEATUREDTLTRIAL = 75,
            USERRINSERTINGTBLUSERPROFILEFEATURE = 76,
            USERRUPDATINGUSERPROFILEFEATURE = 77,
            USERRINSERTINGUSERPROFILEFEATURE = 78,
            USERRINSERTINGACCTDTLLAPSE = 79,
            USERRDEACTIVATINGACCTPMTMETHONFAILURE = 80,
            USERRGETTINGACCTPMTMETHIDFROMTRX = 81,
            USERRUPDATINGDEFAULTACCTPYMTMETHID = 82,
            USERRUPDATINGACCTPYMTMATCHINACTIVEDT = 83,
            USERRMISSINGVERISIGNRESULTORLUTRXSTATUSID = 84,
            USDLERROR = 85,
            USDLERRORCALLINGBILCANCELUSER = 86,
            USERRORCALLINGBILPROVISION = 87,
            USERRORREMOVINGACCTDEFAULTACCTPYMTMETHID = 88,
            USERRORCOULDNOTUNCANCELSTACKEDSUB = 89,
            USERRORUPDATINGACCTDDTLRENEWALINFO = 90,
            USERRORUPDATINGACCTORDERISRESUB = 91,
            //Verisign Result Type Codes + offset of 100 
            USVERISIGNSUCCESS = 100,
            USVERISIGNHARDFAILURE = 101,
            USVERISIGNHARDFAILUREBADACCT = 102,
            USVERISIGNCOMMERROR = 103,
            //td 3247
            SOGUARANTEENOTCOMPELIGIBLE = 110,

            // SP bilProcessTxr return codes for failed CC Card payment with offset added +110
            PTCCERROR_INSUFFICIENTFUNDS = 111,
            PTCCERROR_SECURITYCODE = 112,
            PTCCERROR_EXPIRATIONDATE = 113,
            PTCCERROR_OTHER = 114,
        }

        public enum enumVerisignResultTypeCodes
        {
            VERISIGNSUCCESS = 0,
            VERISIGNHARDFAILURE = 1,
            VERISIGNHARDFAILUREBADACCT = 2,
            VERISIGNCOMMERROR = 3
        }
    }
}
