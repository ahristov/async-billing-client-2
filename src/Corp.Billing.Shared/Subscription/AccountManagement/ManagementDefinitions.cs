﻿namespace Corp.Billing.Shared.Subscription.AccountManagement
{
    public class ManagementDefinitions
    {
        public enum FeatureAction
        {
            CancelOrResign,
            ActivateOrUnResign,
            None
        }
    }
}
