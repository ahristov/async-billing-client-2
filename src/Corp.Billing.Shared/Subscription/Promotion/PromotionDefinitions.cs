﻿namespace Corp.Billing.Shared.Subscription.Promotion
{
    public class PromotionDefinitions
    {
        public enum PromoValidationCode
        {
            Valid = 0,
            Invalid = 1,
            Future = 2,
            Expired = 3,
            InvalidPromoPage = 4
        }

        public enum PromoEligibilityMessage
        {
            IneligibleSubscribeCTA1,
            IneligibleSubscribeCTA2,
            IneligibleSearchCTA1,
            IneligibleProfileCTA1,
            IneligiblePhotoCTA1
        }

        public enum PromoValidateRedemptionKeyCode
        {
            Success = 0,
            IncorrectRedemptionKey = 1,
            RedemptionKeyDoesNotExist = 2,
            RedemptionKeyExpired = 3,
            RedemptionKeyAlreadyUsed = 4,
            InValidUserId = 5
        }

        public enum DiscoverTenOff : int
        {
            Off = 1,
            On = 2
        }

        public enum DiscoverSevenDays : int
        {
            Off = 1,
            On = 2
        }
    }
}
