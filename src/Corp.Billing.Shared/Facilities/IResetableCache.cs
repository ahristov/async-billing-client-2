﻿namespace Corp.Billing.Shared.Facilities
{
    public interface IResetableCache<TEntityId>
    {
        void ResetCache(TEntityId key);
    }
}
