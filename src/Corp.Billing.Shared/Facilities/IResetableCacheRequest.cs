﻿namespace Corp.Billing.Shared.Facilities
{
    public interface IResetableCacheRequest
    {
        bool ResetCache { get; set; }
    }
}
