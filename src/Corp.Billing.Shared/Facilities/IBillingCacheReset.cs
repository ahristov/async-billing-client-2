﻿namespace Corp.Billing.Shared.Facilities
{
    public interface IBillingCacheReset
    {
        void ResetBillingCacheForUser(int userId);
    }
}
