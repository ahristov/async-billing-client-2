﻿namespace Corp.Billing.Shared
{
    public interface IApiRequest
    {
    }

    public interface IApiRequestWithQueryString : IApiRequest
    {
    }

    public interface IApiResponse
    {
    }
}
