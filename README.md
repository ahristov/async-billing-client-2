# Billing API async client

## Layout

```sh
/src
  Corp.Billing.AsyncShared # Implementation of the async client
  Corp.Billing.Shared      # A copy of the current synchronous shared client
  Corp.Billing.Shared.CLI  # Command line project with async client examples usage examples
/doc
  ApiCalls.xlsx            # End points usage April-QA1 and the corresponding async client for each end point
```

## Usage

```sh
cd src
dotnet run -p Corp.Billing.Shared.CLI
```

## Implementation notes

The code for the async client is in directory `Corp.Billing.AsyncShared`. Will be moved inside `Corp.Billing.Shared` upon  integration back to Billing, unless we decide to have a separate Nuget package.

The async client project contains directory per domain. Inside each of those we have interface and class pairs:

```sh
/Orders
  IOrderApiAsyncClient
  OrderApiAsyncClient
```

Directory `ApiCommand` contains all the infrastructure code. The class responsible the API calls is `AsyncApiCommand`. It utilizes a static instance of `System.Net.Http.HttpClient`.

## Exceptions

Custom exception of type `ApiCommandException` is thrown when Client (400-499) or Server (500-599) HTTP response code is received.

This is the default behavior.

```csharp
public static ExceptionLevelEnum ExceptionLevel = ExceptionLevelEnum.ClientErrors
    | ExceptionLevelEnum.ServerErrors;
```

It can be changed by setting:

```csharp
AsyncApiCommand.ExceptionLevel = AsyncApiCommand.ExceptionLevelEnum.None;
```

Here is an example how to handle the `ApiCommandException`:

```csharp
    try
    {
        await ApiClient.ToggleDateCoachAutoRenewalAsync(userId);
    }
    catch (ApiCommandException apiEx)
    {
        Console.WriteLine($"{nameof(ApiCommandException)}: {(int)apiEx.HttpStatusCode}({apiEx.HttpStatusCode}) - Code={apiEx.ErrorCode} Message={apiEx.Message}");
    }
```

![ApiCommandException screenshot](./doc/img/ApiCommandException.png "ApiCommandException")

## Debug callback

You can utilize a callback after each API call to see the URL, request, response, etc.

By default the debug callback is turned off:

```csharp
AsyncApiCommand.DebugLevel = AsyncApiCommand.DebugLevelEnum..None;
```

It can be turned on for one or another category of http response codes:

```csharp

        public enum DebugLevelEnum : short
        {
            None = 0,
            /// <summary>
            /// Log Informational responses (100–199)
            /// </summary>
            InformationalResponses = 1,
            /// <summary>
            /// Successful responses (200–299)
            /// </summary>
            SuccessfulResponses = 2,
            /// <summary>
            /// Log Redirects (300–399)
            /// </summary>
            Redirects = 4,
            /// <summary>
            /// Client errors (400–499)
            /// </summary>
            ClientErrors = 8,
            /// <summary>
            /// Server errors (500–599)
            /// </summary>
            ServerErrors = 16,
        }
```

The default callback is internal function that prints information to the console:

```javascript
HTTP/1.1 GET http://mabillapi.match.com/api/v1/subscription/benefits/countNew/?userId=332448494
Host: mabillapi.match.com


200 OK
Cache-Control: no-cache
Pragma: no-cache
Server: Microsoft-IIS/10.0
X-AspNet-Version: 4.0.30319
X-Powered-By: ASP.NET
Date: Fri, 14 May 2021 15:17:49 GMT

BenefitsCountNewResponse = {
  "countOfNewBenefits": 0
}
```

Custom callback can be set instead of the default one:

```csharp
AsyncApiCommand.DebugCallback = new Func<System.Net.Http.HttpRequestMessage, object, System.Net.Http.HttpResponseMessage, object, ServerErrorResponseDTO, Task> ...
```
